// Macro : I4P / Igor Pro Paris Photoemission Package
// Author :  R�mi LAZZARI (lazzari@insp.jussieu.fr)
// Date : 2019
// Usage : Analysis anf fit of XPS (see manual)


#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma version = 1.0		// Current version  of I4P
#pragma IgorVersion = 6.3	// Required version of Igor Pro


#include "common_tools"
#include "i4p_tools"
#include "i4p_load_omicron"
#include "i4p_load_specs"
#include "i4p_load_vamas"
#include "i4p_load_vamas_SG"
#include "i4p_convert_spectrum"
#include "i4p_plot"
#include "i4p_overlap_peaks"
#include "i4p_peak_characteristics"
#include "i4p_shift_be"
#include "i4p_transmission_function"
#include "i4p_background"
#include "i4p_satellites"
#include "i4p_ghostlines"
#include "i4p_quantification_sa"
#include "i4p_quantification_ss"
#include "i4p_mfp"
#include "i4p_database_mfp"
#include "i4p_eal"
#include "i4p_iss"
#include "i4p_fit_14"
#include "i4p_fit_peaks"
#include "i4p_fit_lineshapes"
#include "i4p_fit_around"
#include "i4p_fit_report"
#include "i4p_periodic_table"
#include "i4p_database"
#include "i4p_database_YL"
#include "i4p_database_Phi"
#include "i4p_help"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Menu ""
End
Menu "==> I4P"
End
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Menu "I4P Load/Plot"
	"Load Omicron EIS", 		load_omicron#Load_EIS()
	"Load SpecsLabProdigy",	load_specs#Load_SpecsLabProdigy()
	"Load Vamas",			load_vamas#Load_Vamas()
	"Load VAMAS SG",		load_vamas_sg#Load_VAMAS_SG()
	"Convert to spectrum",		convert_spectrum#Convert2Spectrum()
	"Plot spectra",			ploti4p#Plot_CoreLevel()
	"Overlap peaks",			overlap_peaks#Overlap_Photoemission()
	"Binding to kinetic energy",	 ploti4p#BE2KE_Plot()
	"Kinetic to binding energy", 	ploti4p#KE2BE_Plot()
	"XPS graph style", 		I4P_GraphStyle()
	"ISS graph style", 			ISS_GraphStyle()
End
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Menu "I4P Analysis"
	"Peak characteristics",				peak_characteristics#Core_Level_Characteristics()
	"Shift binding energy",				shift_be#Shift_BindingEnergy()
	"Correct from transmission function",	transmission_function#CorrectFromTF()
	"Get transmission function",			transmission_function#GetTFfromScan()	
	"Substract background",			background#Substract_Background()
	"Substract satellites",				satellites#Substract_Satellites()
	"Deconvolute satellites",			satellites#Deconvolute_Satellites()
	"Substract ghost lines",				ghostlines#Substract_GhostLines() 
	"Ion Scattering Spectroscopy",  		iss#ISS_KineticEnergy2Mass()
End
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Menu "I4P Database"
	"Periodic Table", 			periodic_table#PeriodicTableStart()
	"Find peaks",				i4pdatabase#BE2Peaks()
	"Mean free paths and EALs",mfp_calc#MFPStart()
	"Quantification F|S",		quantification_sa#QuantificationStart_SA() 
	"Quantification S|S",		quantification_ss#QuantificationStart_SS()   
	"Clean I4P Database",		i4pdatabase#Clean_I4PDatabase()
End
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
menu "I4P Fit"
	"Start I4P Fit",				start_i4pfit()
	"Load spectrum",			i4pfit_around#load_in_i4pfit()
	"Load peak database",		load_database()
	"Load data+fit",			load_fitwaves()
	"Save peak database",		save_database()
	"Save data+fit",			save_fitwaves()
	"Batch fit",				i4pfit_around#fit_batch()
	"Clean I4P Fit",			i4pfit_around#clean_i4pfit()
end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
menu "I4P Help"
	"Clean all",			i4phelp#clean_i4p()
	"Help Command",		i4phelp#help_command_i4p()
	"Help Plot Command ",	i4phelp#help_plot_command_i4p()
	"I4P manual",			i4phelp#help_i4p()
	"About I4P",			i4phelp#about_i4p()
end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////