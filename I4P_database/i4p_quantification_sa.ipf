// Module : quantification_sa
// Author : Remi Lazzari; lazzari@insp.jussieu.fr
// Date : December 25th, 2018
// Usage : quantification from absorbate and substrate intensities


#pragma rtGlobals = 3		
#pragma ModuleName = quantification_sa


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start the Quantification panel
Static Function QuantificationStart_SA() : Panel

if (datafolderexists("root:I4P:I4Pquantification_sa")==0)
	QuantificationInit_SA()
	DisplayPanelQuantification_SA()
else
	// bring an existing panel forward
	if (strlen(WinList("QuantificationPanel_SA", ";", "WIN:64"))!=0)
		DoWindow/F QuantificationPanel_SA
	else
		DisplayPanelQuantification_SA()
	endif

endif
	
End

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialize the Quantification panel
Static Function QuantificationInit_SA()

string cdf = GetDataFolder(1)

newdatafolder /o root:I4P
newdatafolder /o/s  root:I4P:I4Pquantification_sa
		
/// SUBSTRATE
//// Integrated intensities (Arb. units) = substrate
variable /G Is = 4000
//// Transmission function  (Arb. units) = substrate
variable /G Ts = 1	
//// Photoionization cross section  (Arb. units) = substrate
variable /G Ss = 1	
//// SEP = substrate@substrate 
variable /G SEPss = 0		
//// SEP = substrate@layer
variable /G SEPsl = 0		
//// SEP = substrate@film
variable /G SEPsa = 0		
//// Effective attenuation length (A) = substrate in substrate(bulk)
variable /G EALbss = 20	
//// Effective attenuation length (A) = substrate in film(film)
variable /G EALfsa = 30
//// Atomic concentration (Arb. units) = substrate
// Example : density(g/cm^3) / molar mass (g/mol) * stoeichiometry
variable /G Ns = 0.1

/// FILM	(The index a in the variables means adsorbate=film) 
//// Integrated intensities (Arb. units) = film
variable /G Ia = 1000
//// Transmission function  (Arb. units) = film
variable /G Ta = 1
//// Photoionization cross section  (Arb. units) = film
variable /G Sa = 1
//// SEP = film@film 
variable /G SEPaa = 0		
//// Effective attenuation length (A) = film in film(bulk)
variable /G EALbaa = 30
//// Effective attenuation length (A) = film in film(film)
variable /G EALfaa = 30
//// Atomic concentration (Arb. units) = film
// Example : density(g/cm^3) / molar mass (g/mol) * stoeichiometry
variable /G Na = 0.2

/// CALCULATION PARAMETERS	
/// Maximum thickness (A)
variable /G tmax = 100
/// Precision in dichotomy (A)
variable /G dtmin = 0.05

/// GEOMETRY	
/// Sample normal/analyzer angle (deg)
variable /G Alpha = 0
/// Coverage (be carefull if Alpha/=0 then Cov=1)
variable /G Cov = 0.5
// Thickness of the overlayer (A)
variable /G tlayer = 0
//// Effective attenuation length (A) = substrate in overlayer
variable /G EALfsl = 10
	
/// OUTPUTS	
///  Film thickness in a continuous film model (A)
variable /G tfilm = nan
/// Film thickness in a pancake model (A) 
variable /G tpancake = nan
/// Heigth of a pancake (A)
variable /G hpancake = nan
/// Film thickness in a hemisphere model (A)
variable /G themisphere = nan
/// Radius or heigth in a hemisphere model (A)
variable /G rhemisphere = nan
///  Stoichiometry (substrate/film)
variable /G stoichiometry = nan

/// CALCULATION PARAMETERS	
/// Projected EAL (�)
variable /G pEALfaa, pEALfsa

setdatafolder cdf
	
return 0

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Create Quantification panel 
Static Function  DisplayPanelQuantification_SA()

//--size and position of the panel
variable top = 10
variable left =	 10
variable bw = 100
variable bh =	20
variable pw = 90
variable dfs  = 16
variable pleft, ptop
variable x1, y1, x2, y2
//--background color
variable CbackR = 49152
variable CbackG = 49152
variable CbackB = 49152
//--value background color
variable CvalbackR = 65535
variable CvalbackG = 65535
variable CvalbackB = 65535
//--font color (input)
variable CfontR_in = 0
variable CfontG_in = 0
variable CfontB_in = 0
//--font color (output)
variable CfontR_out = 39168
variable CfontG_out = 0
variable CfontB_out = 15616
//--format
string fmtstr = "%7.3f"

//--create the panel
NewPanel/K=1/W=(top,left,top+12.75*bw,left+19.5*bh)
ModifyPanel cbRGB=(CbackR,CbackG,CbackB), fixedSize=1
DoWindow/C/T QuantificationPanel_SA,"I4P : QUANTIFICATION FROM FILM/SUBSTRATE INTENSITIES"
DefaultGUIControls/W=QuantificationPanel_SA native


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SUBSTRATE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft = left+bw
ptop = top

NVAR gIs = root:I4P:I4Pquantification_sa:Is
Setvariable Quant_Is,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Substrate peak area",limits={0,inf,1}
Setvariable Quant_Is,help={"Enter the substrate core-level area (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Is,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gIs

ptop += 1.5*bh
NVAR gTs = root:I4P:I4Pquantification_sa:Ts
Setvariable Quant_Ts,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Substrate transmission function",limits={0,inf,0.001}
Setvariable Quant_Ts,help={"Enter the substrate core-level transmission function  (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Ts,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gTs

ptop += 1.5*bh
NVAR gSs = root:I4P:I4Pquantification_sa:Ss
Setvariable Quant_Ss,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Substrate photo-ionization cross-section",limits={0,inf,0.001}
Setvariable Quant_Ss,help={"Enter the substrate core-level photo-ionization cross-section (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Ss,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSs

ptop += 1.5*bh
NVAR gSEPss = root:I4P:I4Pquantification_sa:SEPss
Setvariable Quant_SEPss,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title=" SEP substrate@substrate",limits={0,inf,0.001}
Setvariable Quant_SEPss,help={"Enter the surface excitation parameter of the substrate photoelectron at substrate surface."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEPss,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEPss

ptop += 1.5*bh
NVAR gSEPsl = root:I4P:I4Pquantification_sa:SEPsl
Setvariable Quant_SEPsl,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title=" SEP substrate@layer",limits={0,inf,0.001}
Setvariable Quant_SEPsl,help={"Enter the surface excitation parameter of the substrate photoelectron at layer surface."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEPsl,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEPsl

ptop += 1.5*bh
NVAR gSEPsa = root:I4P:I4Pquantification_sa:SEPsa
Setvariable Quant_SEPsa,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title=" SEP substrate@film",limits={0,inf,0.001}
Setvariable Quant_SEPsa,help={"Enter the surface excitation parameter of the substrate photoelectron at film surface."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEPsa,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEPsa

ptop += 1.5*bh
NVAR gEALbss = root:I4P:I4Pquantification_sa:EALbss
Setvariable Quant_EALbss,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="EAL(bulk) (�) substrate@substrate",limits={0,inf,0.01}
Setvariable Quant_EALbss,help={"Enter the bulk effective attenuation length of the substrate photoelectron in the substrate material  (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALbss,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALbss

ptop += 1.5*bh
NVAR gEALfsa = root:I4P:I4Pquantification_sa:EALfsa
Setvariable Quant_EALfsa,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="EAL(film) (�) substrate@film",limits={0,inf,0.01}
Setvariable Quant_EALfsa,help={"Enter the practical (film) effective attenuation length of the substrate photoelectron in the film material  (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALfsa,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALfsa

ptop += 1.5*bh
NVAR gNs = root:I4P:I4Pquantification_sa:Ns	
Setvariable Quant_Ns,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Substrate atomic concentration",limits={0,inf,0.001}
Setvariable Quant_Ns,help={"Enter the substrate atomic concentration  (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Ns,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gNs

SetDrawEnv linethick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 = top+5
y1= left-5
x2 =  top+4.2*bw
y2 = left+ 13.5*bh
DrawRRect  x1, y1, x2, y2



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// PARAMETERS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft = pleft
ptop = ptop + 3*bh

NVAR gtmax = root:I4P:I4Pquantification_sa:tmax	
Setvariable Quant_tmax,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Maximum thickness (�)",limits={0,inf,1.}
Setvariable Quant_tmax,help={"Enter the maximum thickness (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in)
Setvariable Quant_tmax,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gtmax

ptop += 1.5*bh
NVAR gdtmin = root:I4P:I4Pquantification_sa:dtmin
Setvariable Quant_dtmin,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Dichotomy accuracy (�)",limits={0.01,inf,0.01}
Setvariable Quant_dtmin,help={"Enter the accuracy of dichotomy (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in)
Setvariable Quant_dtmin,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gdtmin

SetDrawEnv linethick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 = top+5 + bw
y1= left-5 + 15*bh
x2 =  top+4.2*bw
y2 = left+ 18.*bh
DrawRRect  x1, y1, x2, y2


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FILM
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft = pleft+4.5*bw
ptop = top

NVAR gIa = root:I4P:I4Pquantification_sa:Ia
Setvariable Quant_Ia,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Film peak area",limits={0,inf,1}
Setvariable Quant_Ia,help={"Enter the film core-level area  (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Ia,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gIa

ptop += 1.5*bh
NVAR gTa = root:I4P:I4Pquantification_sa:Ta
Setvariable Quant_Ta,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Asorbate transmission function",limits={0,inf,0.001}
Setvariable Quant_Ta,help={"Enter the film core-level transmission function  (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Ta,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gTa

ptop += 1.5*bh
NVAR gSa = root:I4P:I4Pquantification_sa:Sa
Setvariable Quant_Sa,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Film photo-ionization cross-section",limits={0,inf,0.001}
Setvariable Quant_Sa,help={"Enter the film core-level photo-ionization cross-section (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Sa,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSa

ptop += 1.5*bh
NVAR gSEPaa = root:I4P:I4Pquantification_sa:SEPaa
Setvariable Quant_SEPaa,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title=" SEP film@film",limits={0,inf,0.001}
Setvariable Quant_SEPaa,help={"Enter the surface excitation parameter of the film photoelectron at film surface"},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEPaa,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEPaa

ptop += 1.5*bh
NVAR gEALbaa = root:I4P:I4Pquantification_sa:EALbaa
Setvariable Quant_EALbaa,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="EAL(bulk) (�) film@film",limits={0,inf,0.01}
Setvariable Quant_EALbaa,help={"Enter the bulk effective attenuation length of the film photoelectron in the film material  (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALbaa,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALbaa

ptop += 1.5*bh
NVAR gEALfaa = root:I4P:I4Pquantification_sa:EALfaa
Setvariable Quant_EALfaa,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="EAL(film) (�) film@film",limits={0,inf,0.01}
Setvariable Quant_EALfaa,help={"Enter the practical (film) effective attenuation length of the film photoelectron in the film material  (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALfaa,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALfaa

ptop += 1.5*bh
NVAR gNa = root:I4P:I4Pquantification_sa:Na	
Setvariable Quant_Na,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Film atomic concentration",limits={0,inf,0.001}
Setvariable Quant_Na,help={"Enter the film atomic concentration  (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Na,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gNa

SetDrawEnv linethick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 = top+5 + 4.5*bw
y1= left-5
x2 =  top+4.2*bw + 4.5*bw
y2 = left+10.5*bh
DrawRRect  x1, y1, x2, y2


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// GEOMETRY
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft = pleft
ptop = ptop + 3*bh

NVAR gAlpha = root:I4P:I4Pquantification_sa:Alpha	
Setvariable Quant_Alpha,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Sample normal / analyzer angle (deg)",limits={0,90,0.1}
Setvariable Quant_Alpha,help={"Enter the angle between sample normal angle and analyzer (deg)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Alpha,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gAlpha

ptop += 1.5*bh
NVAR gCov = root:I4P:I4Pquantification_sa:Cov	
Setvariable Quant_Cov,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Coverage",limits={0,1,0.01}
Setvariable Quant_Cov,help={"Enter the film partial coverage [0,1]."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Cov,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gCov

ptop += 1.5*bh
NVAR gtlayer = root:I4P:I4Pquantification_sa:tlayer	
Setvariable Quant_tlayer,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="Substrate overlayer thickness (�)",limits={0,inf,0.01}
Setvariable Quant_tlayer,help={"Enter the substrate overlayer thickness (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_tlayer,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gtlayer

ptop += 1.5*bh
NVAR gEALfsl = root:I4P:I4Pquantification_sa:EALfsl
Setvariable Quant_EALfsl,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SA,title="EAL(film) (�) substrate@layer",limits={0,inf,0.01}
Setvariable Quant_EALfsl,help={"Enter the practical (film) effective attenuation length of the substrate photoelectron in the overlayer material  (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALfsl,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALfsl


SetDrawEnv linethick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 = top+5 + 4.5*bw
y1= left-5 + 11.7*bh
x2 =  top+4.2*bw + 4.5*bw
y2 = left+ 18*bh
DrawRRect  x1, y1, x2, y2


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// OUTPUTS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft = pleft+4*bw
ptop = top + 6*bh

NVAR gtfilm = root:I4P:I4Pquantification_sa:tfilm
Setvariable Quant_tfilm,pos={pleft,ptop},size={3*bw,bh},noproc,title="Continuous film thickness (�)",limits={0,inf,0}
Setvariable Quant_tfilm,help={"Calculated continuous films thickness (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_tfilm,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, noedit = 1, value=gtfilm

ptop += 1.5*bh
NVAR gtpancake = root:I4P:I4Pquantification_sa:tpancake
Setvariable Quant_tpancake,pos={pleft,ptop},size={3*bw,bh},noproc,title="Pancake film thickness (�)",limits={0,inf,0}
Setvariable Quant_tpancake,help={"Calculated pancake films thickness (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_tpancake,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, noedit = 1, value=gtpancake

ptop += 1.5*bh
NVAR ghpancake = root:I4P:I4Pquantification_sa:hpancake
Setvariable Quant_hpancake,pos={pleft,ptop},size={3*bw,bh},noproc,title="Pancake heigth (�)",limits={0,inf,0}
Setvariable Quant_hpancake,help={"Calculated pancake heigth (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_hpancake,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, noedit = 1, value=ghpancake

ptop += 1.5*bh
NVAR gthemisphere = root:I4P:I4Pquantification_sa:themisphere
Setvariable Quant_themisphere,pos={pleft,ptop},size={3*bw,bh},noproc,title="Hemisphere film thickness (�)",limits={0,inf,0}
Setvariable Quant_themisphere,help={"Calculated hemisphere film thickness (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_themisphere,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, noedit = 1, value=gthemisphere

ptop += 1.5*bh
NVAR grhemisphere = root:I4P:I4Pquantification_sa:rhemisphere
Setvariable Quant_rhemisphere,pos={pleft,ptop},size={3*bw,bh},noproc,title="Hemisphere radius (�)",limits={0,inf,0}
Setvariable Quant_rhemisphere,help={"Calculated hemisphere radius (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_rhemisphere,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, noedit = 1, value=grhemisphere

ptop += 1.5*bh
NVAR gstoichiometry = root:I4P:I4Pquantification_sa:stoichiometry
Setvariable Quant_stoichiometry,pos={pleft,ptop},size={3*bw,bh},noproc,title="Stoichiometry",limits={0,inf,0}
Setvariable Quant_stoichiometry,help={"Calculated stoichiometry."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_stoichiometry,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, noedit = 1, value=gstoichiometry

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Set variables for quantification value
Function SetVarQuantPanel_SA(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

CalculateQuantPanel_SA()

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculation from panel parameters
Static Function CalculateQuantPanel_SA()

NVAR gIs = root:I4P:I4Pquantification_sa:Is
NVAR gTs = root:I4P:I4Pquantification_sa:Ts
NVAR gSs = root:I4P:I4Pquantification_sa:Ss
NVAR gSEPss = root:I4P:I4Pquantification_sa:SEPss
NVAR gSEPsl = root:I4P:I4Pquantification_sa:SEPsl
NVAR gEALbss = root:I4P:I4Pquantification_sa:EALbss
NVAR gEALfsa = root:I4P:I4Pquantification_sa:EALfsa
NVAR gNs = root:I4P:I4Pquantification_sa:Ns	

NVAR gIa = root:I4P:I4Pquantification_sa:Ia
NVAR gTa = root:I4P:I4Pquantification_sa:Ta
NVAR gSa = root:I4P:I4Pquantification_sa:Sa
NVAR gEALbaa = root:I4P:I4Pquantification_sa:EALbaa
NVAR gEALfaa = root:I4P:I4Pquantification_sa:EALfaa
NVAR gNa = root:I4P:I4Pquantification_sa:Na

NVAR gAlpha = root:I4P:I4Pquantification_sa:Alpha	
NVAR gCov = root:I4P:I4Pquantification_sa:Cov		
NVAR gtlayer = root:I4P:I4Pquantification_sa:tlayer	
NVAR gEALfsl = root:I4P:I4Pquantification_sa:EALfsl

NVAR gtfilm = root:I4P:I4Pquantification_sa:tfilm
NVAR gtpancake = root:I4P:I4Pquantification_sa:tpancake
NVAR ghpancake = root:I4P:I4Pquantification_sa:hpancake
NVAR gthemisphere = root:I4P:I4Pquantification_sa:themisphere
NVAR grhemisphere = root:I4P:I4Pquantification_sa:rhemisphere
NVAR gstoichiometry = root:I4P:I4Pquantification_sa:stoichiometry

NVAR gtmax = root:I4P:I4Pquantification_sa:tmax
NVAR gdtmin = root:I4P:I4Pquantification_sa:dtmin

NVAR pEALfaa = root:I4P:I4Pquantification_sa:pEALfaa
NVAR pEALfsa = root:I4P:I4Pquantification_sa:pEALfsa

// Projected mean free path
Variable CosAlpha
CosAlpha = cos(gAlpha/180*pi)
Variable pEALbss, pEALbaa, pEALfsl
pEALbss = gEALbss*CosAlpha
pEALfsa = gEALfsa*CosAlpha
pEALbaa = gEALbaa*CosAlpha
pEALfaa = gEALfaa*CosAlpha
pEALfsl  = gEALfsl*CosAlpha

// Corrected ratio of intensities to seek by dichotomy
Variable Ratio
if(gtlayer!=0)
	Ratio = gIs*exp(gSEPss)*exp(gSEPsl)/gIa
else
	Ratio = gIs/gIa*exp(gSEPss)
	gSEPsl = gSEPss
endif
Ratio *=  gNa*gTa*gSa*pEALbaa / (gNs*gTs*gSs*pEALbss) * Exp(gtlayer/pEALfsl) 
// Calculate the thickness by dichotomy within the different models
gtfilm             = I4PThickness_Dichotomy_SA(Ratio,gtmax,gdtmin,"film")
// Partial coverage calculation work only in non-normal emission
if(gAlpha==0) 
	gtpancake      = I4PThickness_Dichotomy_SA(Ratio,gtmax,gdtmin,"pancake") 
	gthemisphere = I4PThickness_Dichotomy_SA(Ratio,gtmax,gdtmin,"hemisphere") 
	ghpancake 	 = gtpancake/gCov
	grhemisphere = 1.5*gthemisphere/gCov
else
	gtpancake      = nan
	gthemisphere = nan
	ghpancake      = nan
	grhemisphere = nan
endif

// Calculate the stoichiometry (Ns/Na)
gstoichiometry = gIs/gIa *  gTa*gSa*pEALbaa / (gTs*gSs*pEALbss)
gstoichiometry = 1./(1.+gstoichiometry)

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4PThickness_Dichotomy_SA(Ratio,ttmax,dttmin,model) 

Variable Ratio,ttmax, dttmin
String model

Variable tt1, tt2, ttmid, dtt
Variable ratio1, ratio2, ratiomid

tt1 = dttmin
tt2 = ttmax
dtt = tt2-tt1

ratio1  = I4P_Ratio_SA(model,tt1) - Ratio
ratio2  = I4P_Ratio_SA(model,tt2) - Ratio

If(ratio1*ratio2>0) 
   // Print "==> No Solution in the dichotomy process for ", model
   // Actual returned value
   Return NaN
Endif

Do 
	dtt = tt2-tt1
	ttmid = (tt1+tt2)/2.0
	ratiomid = I4P_Ratio_SA(model,ttmid) - Ratio
	If(ratio1*ratiomid<0)
		tt2 = ttmid
		ratio2 = ratiomid
	Else
		tt1 = ttmid
		ratio1 = ratiomid
	Endif
While(dtt>dttmin)

Return ttmid

End 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4P_Ratio_SA(model,tt)

Variable tt
String model
Variable ratio_model

strswitch( model)
case "film":
	ratio_model  = I4P_Ratio_SA_film(tt)
break 
case "pancake":
	ratio_model  = I4P_Ratio_SA_pancake(tt)
break 
case "hemisphere":
	ratio_model  = I4P_Ratio_SA_hemisphere(tt) 
break
endswitch

Return ratio_model

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4P_Ratio_SA_film(tt)

Variable tt
NVAR pEALfsa = root:I4P:I4Pquantification_sa:pEALfsa
NVAR pEALfaa = root:I4P:I4Pquantification_sa:pEALfaa
NVAR gSEPsl = root:I4P:I4Pquantification_sa:SEPsl
NVAR gSEPsa = root:I4P:I4Pquantification_sa:SEPsa
NVAR gSEPaa = root:I4P:I4Pquantification_sa:SEPaa

Return exp(-tt/pEALfsa)*exp(-gSEPsl)*exp(-gSEPsa) / ( (1.-exp(-tt/pEALfaa)) *exp(-gSEPaa) )

End 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4P_Ratio_SA_pancake(tt)

Variable tt
NVAR pEALfsa = root:I4P:I4Pquantification_sa:pEALfsa
NVAR pEALfaa = root:I4P:I4Pquantification_sa:pEALfaa
NVAR gSEPsl = root:I4P:I4Pquantification_sa:SEPsl
NVAR gSEPsa = root:I4P:I4Pquantification_sa:SEPsa
NVAR gSEPaa = root:I4P:I4Pquantification_sa:SEPaa
NVAR  Cov = root:I4P:I4Pquantification_sa:Cov
Variable H

H = tt/Cov
Return ( (1.- Cov)*exp(-gSEPsl) + Cov*exp(-H/pEALfsa)*exp(-gSEPsl)*exp(-gSEPsa) ) /  ( (1.-exp(-H/pEALfaa)) * exp(-gSEPsa) * Cov )

End 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4P_Ratio_SA_hemisphere(tt)

Variable tt
NVAR pEALfsa = root:I4P:I4Pquantification_sa:pEALfsa
NVAR pEALfaa = root:I4P:I4Pquantification_sa:pEALfaa
NVAR gSEPsl = root:I4P:I4Pquantification_sa:SEPsl
NVAR gSEPsa = root:I4P:I4Pquantification_sa:SEPsa
NVAR gSEPaa = root:I4P:I4Pquantification_sa:SEPaa
NVAR  Cov = root:I4P:I4Pquantification_sa:Cov
Variable H
Variable AA, BB

H = 1.5*tt/Cov
AA = (1.  - Cov)*exp(-gSEPsl)  + 2.*Cov*(pEALfsa/H)^2 * (1. - (1.+H/pEALfsa)*exp(-H/pEALfsa)) * exp(-gSEPsl) *exp(-gSEPsa)
BB =  (1. - 2.*(pEALfaa/H)^2 + 2.*pEALfaa*(H+pEALfaa)/(H^2)*exp(-H/pEALfaa)) * Cov * exp(-gSEPaa)

Return AA/BB	

End 



