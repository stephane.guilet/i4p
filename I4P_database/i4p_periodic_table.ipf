// Procedure  Periodic Table Menu
// Author : David Niles and J. J. Weimer; Richard Knochenmuss Jan. 2017; modified by R. Lazzari for photoemission
// Usage : Periodic Table of Elements
// Graphically select any number of elements, the resulting list is processed separately.
// The selection list is: root:I4PPeriodicTable:ElemSelect
// see also waves AtNr and ElementSym in the same data folder.
// See the function SelectionActionProc() to perform action after element selection
// Create a periodic table panel from the Macros menu, or call PeriodicTableSelect#PeriodicTable()
// tested on Igor 7.02 and 6.37

#pragma rtGlobals=3	
#pragma ModuleName = periodic_table

// marked and ummarked button colors:
static constant unmarkR=65535  
static constant unmarkG=65535
static constant unmarkB=62500
static constant markR=32768
static constant markG=65280
static constant markB=0
static constant missR=65280
static constant missG=48896
static constant missB=55552


// panel background color:
static constant PTbgR=49152
static constant PTbgG=49152
static constant PTbgB=49152

static constant dfs=10 // element button default/base font size

static constant defaultscale=2  // default scale factor for panel. valid values: 1, 1.25, 1.5, 2, 2.5

static constant maxelem=104 // highest desired atomic nr+1

//************************************************************
//************************************************************
Function SelectionActionProc()  // consequences of element selection/deselection
	//
	// dosomething()  // PUT YOUR FUNCTION CALL HERE
	// NOTE:
	// If your function is outside this Independent Module, you will need to call it like this:
	//	 Execute "ProcGlobal#dosomething()"
	// or if it is in another Independent Module:
	//	 Execute "OtherModule#dosomething()"
	//
	// The selection list is: root:I4PPeriodicTable:ElemSelect
	// see also waves AtNr and ElementSym in the same data folder.
	i4pdatabase#PlotElementFromI4PDatabase()

end

//************************************************************
//Menu "Macros"
//	"Periodic Table",/Q, PeriodicTable()
//End

//************************************************************
// Periodic Table Initialize Globals Structure
// ptwidth: base width of panel
// ptheight: base height of panel
// left: base start for buttons
// top: base start for buttons
// scfstr: scale factors to change panel size

Static Structure PTPanelInitGlobals
	variable ptwidth
	variable ptheight
	variable left
	variable top
	string scfstr
	string xrsstr
	variable pangle
	variable pinfo
	variable psatellite
	variable pghost
	variable pauger
	variable ptransfunc
EndStructure

//************************************************************
// Periodic Table
// this is the starting point for creating the periodic table panel
Static Function PeriodicTableStart() : Panel

	if (!DataFolderExists("root:I4P:I4Pperiodictable:"))
		// new folder and display panel
		if (PTInitPanel())
			PTDisplayPanel()
		endif

	else
		// bring an existing panel forward
		if (strlen(WinList("PeriodicTablePanel", ";", "WIN:64"))!=0)
			DoWindow/F PeriodicTablePanel
		else
			NVAR gscfpN = root:I4P:I4Pperiodictable:scfpN
			PTDisplayPanel()
			PopupMenu PTresize mode=gscfpN
		endif

	endif

End

//************************************************************
Static Function PTInitGlobals(ptpig)
	STRUCT PTPanelInitGlobals &ptpig
	ptpig.ptwidth=260
	ptpig.ptheight=185
	ptpig.left=10
	ptpig.top=10
	ptpig.scfstr="\"1;1.25;1.5;2;2.5;\""
	ptpig.xrsstr="\"Al;Mg;He I\""
	ptpig.pangle= gPsi_Magic
	ptpig.pinfo= 0
	ptpig.psatellite= 0
	ptpig.pghost= 0
	ptpig.pauger= 1
	ptpig.ptransfunc= 1.0
End

//************************************************************
// PTInitPanel()
// initialize the periodic table panel folders and globals
Static Function PTInitPanel()
	
	// create new data folder for Periodic Table information
	string cdf = GetDataFolder(1)
	variable newpanel = 0
	
	if (!DataFolderExists("root:I4P:I4Pperiodictable"))
		NewDataFolder/O root:I4P
		NewDataFolder/O/S root:I4P:I4Pperiodictable
		
		// create the globals and set them
		variable/G scf=defaultscale, kill=1, scfpN = 1
		variable/G ptleft=0, pttop=0
		string /G pxrs = "Al"
		variable /G pangle =  gPsi_Magic
		variable /G pinfo =  0
		variable /G psatellite =  0		
		variable /G pghost =  0		
		variable /G pauger=  1	
		variable /G ptransfunc =  1.0
				
		make/N=(maxelem) AtNr
		AtNr=p
		AtNr[0]=NaN
		
		make/T/N=(maxelem) ElementSym
		ElementSym[0]= {"","H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y"}
		ElementSym[40]= {"Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir"}
		ElementSym[78]= {"Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr"}

		Make/N=(maxelem) ElemSelect, OldElemSelect
		ElemSelect=0
		OldElemSelect=0
		
		make/N=(maxelem,3) Col
		col[][0]=unmarkR
		col[][1]=unmarkG
		col[][2]=unmarkB
		
		make/T/N=(maxelem) ElementMiss
		ElementMiss[0]= {"","H","He","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""}
		ElementMiss[40]= {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""}
		ElementMiss[78]= {"","","","","","","Po","At","Rn","Fr","Ra","Ac","","Pa","","","","","","","","Es","Fm","Md","No","Lr"}
		variable k
		for(k=1;k<maxelem;k+=1)
			if(cmpstr(ElementMiss[k],"")!=0)
				col[k][0]=missR
				col[k][1]=missG
				col[k][2]=missB
			endif
		endfor
		
		newpanel = 1
	endif
	
	SetDataFolder cdf
	
	return newpanel
End

//************************************************************
// PTDisplayPanel()
// create the panel and element buttons
Static Function PTDisplayPanel()
	NVAR gscf		=  root:I4P:I4Pperiodictable:scf
	NVAR gptleft		=  root:I4P:I4Pperiodictable:ptleft
	NVAR gpttop		=  root:I4P:I4Pperiodictable:pttop
	NVAR gkill		=  root:I4P:I4Pperiodictable:kill
	SVAR gpxrs		=  root:I4P:I4Pperiodictable:pxrs
	NVAR gpangle	=  root:I4P:I4Pperiodictable:pangle	
	NVAR gpinfo		=  root:I4P:I4Pperiodictable:pinfo
	NVAR gpsatellite	=  root:I4P:I4Pperiodictable:psatellite
	NVAR gpghost	=  root:I4P:I4Pperiodictable:pghost
	NVAR gpauger	=  root:I4P:I4Pperiodictable:pauger
	NVAR gptransfunc=  root:I4P:I4Pperiodictable:ptransfunc	
	wave Col			=  root:I4P:I4Pperiodictable:Col
	
	STRUCT PTPanelInitGlobals ptpig	
	PTInitGlobals(ptpig)
	
	if (!gscf)
		gscf = 1
	endif
	
	variable ptwidth, ptheight,left,top
	variable bw=20*gscf,bh=15*gscf
	variable ptddwidth=gscf*120,ptddheight=11*bh
	
	string gscfstr=ptpig.scfstr
	string gxrsstr=ptpig.xrsstr
	gpangle=ptpig.pangle
	gpinfo=ptpig.pinfo
	gpsatellite=ptpig.psatellite
	gpghost=ptpig.pghost
	gpauger=ptpig.pauger
	gptransfunc=ptpig.ptransfunc
		
	ptwidth=gscf*(ptpig.ptwidth+120)
	ptheight=gscf*ptpig.ptheight
	left=gscf*ptpig.left
	top=gscf*ptpig.top
	
	string strgscf = num2str(gscf)
		
	NewPanel/K=(gkill)/W=(gptleft,gpttop,gptleft+ptwidth,gpttop+ptheight)
	ModifyPanel cbRGB=(PTbgR,PTbgG,PTbgB), fixedSize=1//,noEdit=1
	DoWindow/C/T PeriodicTablePanel,"I4P : PERIODIC TABLE OF XPS AND AUGER LINES"

	DefaultGUIControls/W=PeriodicTablePanel native
	
	SetDrawLayer UserBack
	
	// the element buttons	
	Button H0,pos={left,top},size={bw,bh},fColor=(col[1][0],col[1][1],col[1][2]),fsize=gscf*dfs,noproc,title="H"
	
	Button Li0,pos={left,top+bh},size={bw,bh},fColor=(col[3][0],col[3][1],col[3][2]),fsize=gscf*dfs,proc=PTGetState,title="Li"
	Button Na0,pos={left,top+2*bh},size={bw,bh},fColor=(col[11][0],col[11][1],col[11][2]),fsize=gscf*dfs,proc=PTGetState,title="Na"
	Button K0,pos={left,top+3*bh},size={bw,bh},fColor=(col[19][0],col[19][1],col[19][2]),fsize=gscf*dfs,proc=PTGetState,title="K"
	Button Rb0,pos={left,top+4*bh},size={bw,bh},fColor=(col[37][0],col[37][1],col[37][2]),fsize=gscf*dfs,proc=PTGetState,title="Rb"
	Button Cs0,pos={left,top+5*bh},size={bw,bh},fColor=(col[55][0],col[55][1],col[55][2]),fsize=gscf*dfs,proc=PTGetState,title="Cs"
	Button Fr0,pos={left,top+6*bh},size={bw,bh},fColor=(col[87][0],col[87][1],col[87][2]),fsize=gscf*dfs,noproc,title="Fr"

	Button Be0,pos={left+bw,top+bh},size={bw,bh},fColor=(col[4][0],col[4][1],col[4][2]),fsize=gscf*dfs,proc=PTGetState,title="Be"
	Button Mg0,pos={left+bw,top+2*bh},size={bw,bh},fColor=(col[12][0],col[12][1],col[12][2]),fsize=gscf*dfs,proc=PTGetState,title="Mg"
	Button Ca0,pos={left+bw,top+3*bh},size={bw,bh},fColor=(col[20][0],col[20][1],col[20][2]),fsize=gscf*dfs,proc=PTGetState,title="Ca"
	Button Sr0,pos={left+bw,top+4*bh},size={bw,bh},fColor=(col[38][0],col[38][1],col[38][2]),fsize=gscf*dfs,proc=PTGetState,title="Sr"
	Button Ba0,pos={left+bw,top+5*bh},size={bw,bh},fColor=(col[56][0],col[56][1],col[56][2]),fsize=gscf*dfs,proc=PTGetState,title="Ba"
	Button Ra0,pos={left+bw,top+6*bh},size={bw,bh},fColor=(col[88][0],col[88][1],col[88][2]),fsize=gscf*dfs,noproc,title="Ra"

	Button Sc0,pos={left+2*bw,top+3*bh},size={bw,bh},fColor=(col[21][0],col[21][1],col[21][2]),fsize=gscf*dfs,proc=PTGetState,title="Sc"
	Button Ti0,pos={left+3*bw,top+3*bh},fColor=(col[22][0],col[22][1],col[22][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Ti"
	Button V0,pos={left+4*bw,top+3*bh},fColor=(col[23][0],col[23][1],col[23][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="V"
	Button Cr0,pos={left+5*bw,top+3*bh},fColor=(col[24][0],col[24][1],col[24][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Cr"
	Button Mn0,pos={left+6*bw,top+3*bh},fColor=(col[24][0],col[25][1],col[25][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Mn"
	Button Fe0,pos={left+7*bw,top+3*bh},fColor=(col[26][0],col[26][1],col[26][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Fe"
	Button Co0,pos={left+8*bw,top+3*bh},fColor=(col[27][0],col[27][1],col[27][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Co"
	Button Ni0,pos={left+9*bw,top+3*bh},fColor=(col[28][0],col[28][1],col[28][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Ni"
	Button Cu0,pos={left+10*bw,top+3*bh},fColor=(col[29][0],col[29][1],col[29][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Cu"
	Button Zn0,pos={left+11*bw,top+3*bh},fColor=(col[30][0],col[30][1],col[30][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Zn"

	Button Y0,pos={left+2*bw,top+4*bh},fColor=(col[39][0],col[39][1],col[39][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Y"
	Button Zr0,pos={left+3*bw,top+4*bh},fColor=(col[40][0],col[40][1],col[40][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Zr"
	Button Nb0,pos={left+4*bw,top+4*bh},fColor=(col[41][0],col[41][1],col[41][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Nb"
	Button Mo0,pos={left+5*bw,top+4*bh},fColor=(col[42][0],col[42][1],col[42][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Mo"
	Button Tc0,pos={left+6*bw,top+4*bh},fColor=(col[43][0],col[43][1],col[43][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Tc"
	Button Ru0,pos={left+7*bw,top+4*bh},fColor=(col[44][0],col[44][1],col[44][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Ru"
	Button Rh0,pos={left+8*bw,top+4*bh},fColor=(col[45][0],col[45][1],col[45][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Rh"
	Button Pd0,pos={left+9*bw,top+4*bh},fColor=(col[46][0],col[46][1],col[46][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Pd"
	Button Ag0,pos={left+10*bw,top+4*bh},fColor=(col[47][0],col[47][1],col[47][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Ag"
	Button Cd0,pos={left+11*bw,top+4*bh},fColor=(col[48][0],col[48][1],col[48][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Cd"

	Button La0,pos={left+2*bw,top+5*bh},fColor=(col[57][0],col[57][1],col[57][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="La"
	Button Hf0,pos={left+3*bw,top+5*bh},fColor=(col[72][0],col[72][1],col[72][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Hf"
	Button Ta0,pos={left+4*bw,top+5*bh},fColor=(col[73][0],col[73][1],col[73][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Ta"
	Button W0,pos={left+5*bw,top+5*bh},fColor=(col[74][0],col[74][1],col[74][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="W"
	Button Re0,pos={left+6*bw,top+5*bh},fColor=(col[75][0],col[75][1],col[75][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Re"
	Button Os0,pos={left+7*bw,top+5*bh},fColor=(col[76][0],col[76][1],col[76][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Os"
	Button Ir0,pos={left+8*bw,top+5*bh},fColor=(col[77][0],col[77][1],col[77][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Ir"
	Button Pt0,pos={left+9*bw,top+5*bh},fColor=(col[78][0],col[78][1],col[78][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Pt"
	Button Au0,pos={left+10*bw,top+5*bh},fColor=(col[79][0],col[79][1],col[79][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Au"
	Button Hg0,pos={left+11*bw,top+5*bh},fColor=(col[80][0],col[80][1],col[80][2]),size={bw,bh},fsize=gscf*dfs,proc=PTGetState,title="Hg"

	Button Ac0,pos={left+2*bw,top+6*bh},size={bw,bh},fColor=(col[89][0],col[89][1],col[89][2]),fsize=gscf*dfs,noproc,title="Ac"

	Button He0,pos={left+17*bw,top},size={bw,bh},fColor=(col[2][0],col[2][1],col[2][2]),fsize=gscf*dfs,noproc,title="He"

	Button B0,pos={left+12*bw,top+bh},size={bw,bh},fColor=(col[5][0],col[5][1],col[5][2]),fsize=gscf*dfs,proc=PTGetState,title="B"
	Button C0,pos={left+13*bw,top+bh},size={bw,bh},fColor=(col[6][0],col[6][1],col[6][2]),fsize=gscf*dfs,proc=PTGetState,title="C"
	Button N0,pos={left+14*bw,top+bh},size={bw,bh},fColor=(col[7][0],col[7][1],col[7][2]),fsize=gscf*dfs,proc=PTGetState,title="N"
	Button O0,pos={left+15*bw,top+bh},size={bw,bh},fColor=(col[8][0],col[8][1],col[8][2]),fsize=gscf*dfs,proc=PTGetState,title="O"
	Button F0,pos={left+16*bw,top+bh},size={bw,bh},fColor=(col[9][0],col[9][1],col[9][2]),fsize=gscf*dfs,proc=PTGetState,title="F"
	Button Ne0,pos={left+17*bw,top+bh},size={bw,bh},fColor=(col[10][0],col[10][1],col[10][2]),fsize=gscf*dfs,proc=PTGetState,title="Ne"

	Button Al0,pos={left+12*bw,top+2*bh},size={bw,bh},fColor=(col[13][0],col[13][1],col[13][2]),fsize=gscf*dfs,proc=PTGetState,title="Al"
	Button Si0,pos={left+13*bw,top+2*bh},size={bw,bh},fColor=(col[14][0],col[14][1],col[14][2]),fsize=gscf*dfs,proc=PTGetState,title="Si"
	Button P0,pos={left+14*bw,top+2*bh},size={bw,bh},fColor=(col[15][0],col[15][1],col[15][2]),fsize=gscf*dfs,proc=PTGetState,title="P"
	Button S0,pos={left+15*bw,top+2*bh},size={bw,bh},fColor=(col[16][0],col[16][1],col[16][2]),fsize=gscf*dfs,proc=PTGetState,title="S"
	Button Cl0,pos={left+16*bw,top+2*bh},size={bw,bh},fColor=(col[17][0],col[17][1],col[17][2]),fsize=gscf*dfs,proc=PTGetState,title="Cl"
	Button Ar0,pos={left+17*bw,top+2*bh},size={bw,bh},fColor=(col[18][0],col[18][1],col[18][2]),fsize=gscf*dfs,proc=PTGetState,title="Ar"

	Button Ga0,pos={left+12*bw,top+3*bh},size={bw,bh},fColor=(col[31][0],col[31][1],col[31][2]),fsize=gscf*dfs,proc=PTGetState,title="Ga"
	Button Ge0,pos={left+13*bw,top+3*bh},size={bw,bh},fColor=(col[32][0],col[32][1],col[32][2]),fsize=gscf*dfs,proc=PTGetState,title="Ge"
	Button As0,pos={left+14*bw,top+3*bh},size={bw,bh},fColor=(col[33][0],col[33][1],col[33][2]),fsize=gscf*dfs,proc=PTGetState,title="As"
	Button Se0,pos={left+15*bw,top+3*bh},size={bw,bh},fColor=(col[34][0],col[34][1],col[34][2]),fsize=gscf*dfs,proc=PTGetState,title="Se"
	Button Br0,pos={left+16*bw,top+3*bh},size={bw,bh},fColor=(col[35][0],col[35][1],col[35][2]),fsize=gscf*dfs,proc=PTGetState,title="Br"
	Button Kr0,pos={left+17*bw,top+3*bh},size={bw,bh},fColor=(col[36][0],col[36][1],col[36][2]),fsize=gscf*dfs,proc=PTGetState,title="Kr"

	Button In0,pos={left+12*bw,top+4*bh},size={bw,bh},fColor=(col[49][0],col[49][1],col[49][2]),fsize=gscf*dfs,proc=PTGetState,title="In"
	Button Sn0,pos={left+13*bw,top+4*bh},size={bw,bh},fColor=(col[50][0],col[50][1],col[50][2]),fsize=gscf*dfs,proc=PTGetState,title="Sn"
	Button Sb0,pos={left+14*bw,top+4*bh},size={bw,bh},fColor=(col[51][0],col[51][1],col[51][2]),fsize=gscf*dfs,proc=PTGetState,title="Sb"
	Button Te0,pos={left+15*bw,top+4*bh},size={bw,bh},fColor=(col[52][0],col[52][1],col[52][2]),fsize=gscf*dfs,proc=PTGetState,title="Te"
	Button I0,pos={left+16*bw,top+4*bh},size={bw,bh},fColor=(col[53][0],col[53][1],col[53][2]),fsize=gscf*dfs,proc=PTGetState,title="I"
	Button Xe0,pos={left+17*bw,top+4*bh},size={bw,bh},fColor=(col[54][0],col[54][1],col[54][2]),fsize=gscf*dfs,proc=PTGetState,title="Xe"

	Button Tl0,pos={left+12*bw,top+5*bh},size={bw,bh},fColor=(col[81][0],col[81][1],col[81][2]),fsize=gscf*dfs,proc=PTGetState,title="Tl"
	Button Pb0,pos={left+13*bw,top+5*bh},size={bw,bh},fColor=(col[82][0],col[82][1],col[82][2]),fsize=gscf*dfs,proc=PTGetState,title="Pb"
	Button Bi0,pos={left+14*bw,top+5*bh},size={bw,bh},fColor=(col[83][0],col[83][1],col[83][2]),fsize=gscf*dfs,proc=PTGetState,title="Bi"
	Button Po0,pos={left+15*bw,top+5*bh},size={bw,bh},fColor=(col[84][0],col[84][1],col[84][2]),fsize=gscf*dfs,noproc,title="Po"
	Button At0,pos={left+16*bw,top+5*bh},size={bw,bh},fColor=(col[85][0],col[85][1],col[85][2]),fsize=gscf*dfs,noproc,title="At"
	Button Rn0,pos={left+17*bw,top+5*bh},size={bw,bh},fColor=(col[86][0],col[86][1],col[86][2]),fsize=gscf*dfs,noproc,title="Rn"

	Button Ce0,pos={left+4*bw,top+7.5*bh},size={bw,bh},fColor=(col[58][0],col[58][1],col[58][2]),fsize=gscf*dfs,proc=PTGetState,title="Ce"
	Button Pr0,pos={left+5*bw,top+7.5*bh},size={bw,bh},fColor=(col[59][0],col[59][1],col[59][2]),fsize=gscf*dfs,proc=PTGetState,title="Pr"
	Button Nd0,pos={left+6*bw,top+7.5*bh},size={bw,bh},fColor=(col[60][0],col[60][1],col[60][2]),fsize=gscf*dfs,proc=PTGetState,title="Nd"
	Button Pm0,pos={left+7*bw,top+7.5*bh},size={bw,bh},fColor=(col[61][0],col[61][1],col[61][2]),fsize=gscf*dfs,proc=PTGetState,title="Pm"
	Button Sm0,pos={left+8*bw,top+7.5*bh},size={bw,bh},fColor=(col[62][0],col[62][1],col[63][2]),fsize=gscf*dfs,proc=PTGetState,title="Sm"
	Button Eu0,pos={left+9*bw,top+7.5*bh},size={bw,bh},fColor=(col[63][0],col[63][1],col[63][2]),fsize=gscf*dfs,proc=PTGetState,title="Eu"
	Button Gd0,pos={left+10*bw,top+7.5*bh},size={bw,bh},fColor=(col[64][0],col[64][1],col[64][2]),fsize=gscf*dfs,proc=PTGetState,title="Gd"
	Button Tb0,pos={left+11*bw,top+7.5*bh},size={bw,bh},fColor=(col[65][0],col[65][1],col[65][2]),fsize=gscf*dfs,proc=PTGetState,title="Tb"
	Button Dy0,pos={left+12*bw,top+7.5*bh},size={bw,bh},fColor=(col[66][0],col[66][1],col[66][2]),fsize=gscf*dfs,proc=PTGetState,title="Dy"
	Button Ho0,pos={left+13*bw,top+7.5*bh},size={bw,bh},fColor=(col[67][0],col[67][1],col[67][2]),fsize=gscf*dfs,proc=PTGetState,title="Ho"
	Button Er0,pos={left+14*bw,top+7.5*bh},size={bw,bh},fColor=(col[68][0],col[68][1],col[68][2]),fsize=gscf*dfs,proc=PTGetState,title="Er"
	Button Tm0,pos={left+15*bw,top+7.5*bh},size={bw,bh},fColor=(col[69][0],col[69][1],col[69][2]),fsize=gscf*dfs,proc=PTGetState,title="Tm"
	Button Yb0,pos={left+16*bw,top+7.5*bh},size={bw,bh},fColor=(col[70][0],col[70][1],col[70][2]),fsize=gscf*dfs,proc=PTGetState,title="Yb"
	Button Lu0,pos={left+17*bw,top+7.5*bh},size={bw,bh},fColor=(col[71][0],col[71][1],col[71][2]),fsize=gscf*dfs,proc=PTGetState,title="Lu"
	
	Button Th0,pos={left+4*bw,top+8.5*bh},size={bw,bh},fColor=(col[90][0],col[90][1],col[90][2]),fsize=gscf*dfs,proc=PTGetState,title="Th"
	Button Pa0,pos={left+5*bw,top+8.5*bh},size={bw,bh},fColor=(col[91][0],col[91][1],col[91][2]),fsize=gscf*dfs,noproc,title="Pa"
	Button U0,pos={left+6*bw,top+8.5*bh},size={bw,bh},fColor=(col[92][0],col[92][1],col[92][2]),fsize=gscf*dfs,proc=PTGetState,title="U"
	Button Np0,pos={left+7*bw,top+8.5*bh},size={bw,bh},fColor=(col[93][0],col[93][1],col[93][2]),fsize=gscf*dfs,proc=PTGetState,title="Np"
	Button Pu0,pos={left+8*bw,top+8.5*bh},size={bw,bh},fColor=(col[94][0],col[94][1],col[94][2]),fsize=gscf*dfs,proc=PTGetState,title="Pu"
	Button Am0,pos={left+9*bw,top+8.5*bh},size={bw,bh},fColor=(col[95][0],col[95][1],col[95][2]),fsize=gscf*dfs,proc=PTGetState,title="Am"
	Button Cm0,pos={left+10*bw,top+8.5*bh},size={bw,bh},fColor=(col[96][0],col[96][1],col[96][2]),fsize=gscf*dfs,proc=PTGetState,title="Cm"
	Button Bk0,pos={left+11*bw,top+8.5*bh},size={bw,bh},fColor=(col[97][0],col[97][1],col[97][2]),fsize=gscf*dfs,proc=PTGetState,title="Bk"
	Button Cf0,pos={left+12*bw,top+8.5*bh},size={bw,bh},fColor=(col[98][0],col[98][1],col[98][2]),fsize=gscf*dfs,proc=PTGetState,title="Cf"
	Button Es0,pos={left+13*bw,top+8.5*bh},size={bw,bh},fColor=(col[99][0],col[99][1],col[99][2]),fsize=gscf*dfs,noproc,title="Es"
	Button Fm0,pos={left+14*bw,top+8.5*bh},size={bw,bh},fColor=(col[100][0],col[100][1],col[100][2]),fsize=gscf*dfs,noproc,title="Fm"
	Button Md0,pos={left+15*bw,top+8.5*bh},size={bw,bh},fColor=(col[101][0],col[101][1],col[101][2]),fsize=gscf*dfs,noproc,title="Md"
	Button No0,pos={left+16*bw,top+8.5*bh},size={bw,bh},fColor=(col[102][0],col[102][1],col[102][2]),fsize=gscf*dfs,noproc,title="No"
	Button Lr0,pos={left+17*bw,top+8.5*bh},size={bw,bh},fColor=(col[103][0],col[103][1],col[103][2]),fsize=gscf*dfs,noproc,title="Lr"

	// controls
	CheckBox PTinfo,pos={left+10*bw, top-0.1*bh},size={45*gscf,14*gscf},title="Print info.", noproc 
	CheckBox PTinfo,help={"Print the information from database."},fSize=gscf*dfs,fColor=(0,0,0)
	CheckBox PTinfo,bodywidth=90, valueBackColor=(65535,65535,65535), valueColor=(0,0,0), frame=1,disable=0,value=0,variable=gpinfo

	CheckBox PTghost,pos={left+0.25*bw,top+7.5*bh},size={45*gscf,14*gscf},title="Ghost lines.", proc=PTChangeGhost 
	CheckBox PTghost,help={"Plot ghost lines"},fSize=gscf*dfs,fColor=(0,0,0)
	CheckBox PTghost,bodywidth=90, valueBackColor=(65535,65535,65535), valueColor=(0,0,0), frame=1,disable=0,value=0,variable=gpghost

	CheckBox PTsatellite,pos={left+0.25*bw,top+8.5*bh},size={45*gscf,14*gscf},title="Satellites", proc=PTChangeSatellite
	CheckBox PTsatellite,help={"Plot source satellites."},fSize=gscf*dfs,fColor=(0,0,0)
	CheckBox PTsatellite,bodywidth=90, valueBackColor=(65535,65535,65535), valueColor=(0,0,0), frame=1,disable=0,value=0,variable=gpsatellite

	CheckBox PTauger,pos={left+0.25*bw,top+9.5*bh},size={45*gscf,14*gscf},title="Auger", proc=PTChangeAuger
	CheckBox PTauger,help={"Plot Auger lines."},fSize=gscf*dfs,fColor=(0,0,0)
	CheckBox PTauger,bodywidth=90, valueBackColor=(65535,65535,65535), valueColor=(0,0,0), frame=1,disable=0,value=1,variable=gpauger

	PopupMenu PTresize,pos={left+14*bw, top-0.1*bh},size={45*gscf,14*gscf},proc=PTResizePanel,title="Scale  "
	PopupMenu PTresize,help={"Use this to resize the panel."},fSize=gscf*dfs, fColor=(0,0,0)
	PopupMenu PTresize,mode=1,bodywidth=50,popvalue=strgscf,value=#gscfstr
	
	PopupMenu PTsource,pos={left+5.25*bw,top+10.25*bh},size={45*gscf,14*gscf},proc=PTChangeSource,title="X-ray source "
	PopupMenu PTsource,help={"Use this to select the X-ray source."},fSize=gscf*dfs, fColor=(0,0,0)
	PopupMenu PTsource,mode=1,bodywidth=70,popvalue=gpxrs,value=#gxrsstr	
	
	Setvariable PTangle,pos={left+10.25*bw,top+10.25*bh},size={45*gscf,14*gscf},title="Angle (deg) ",limits={0,180,0}, proc=PTGetAngle
	Setvariable PTangle,help={"Enter the angle between X-ray source and analyzer."},fSize=gscf*dfs,fColor=(0,0,0), format="%6.2f"
	Setvariable PTangle,bodywidth=80, valueBackColor=(65535,65535,65535), valueColor=(0,0,0), frame=1, disable=0, value=gpangle

	Setvariable PTtransfunc,pos={left+15.5*bw,top+10.25*bh},size={45*gscf,14*gscf},title="Trans.func. exp.",limits={0,100,0}, proc=PTGetTransFunc
	Setvariable PTtransfunc,help={"Enter the transmission function exponent."},fSize=gscf*dfs,fColor=(0,0,0), format="%4.2f"
	Setvariable PTtransfunc,bodywidth=60, valueBackColor=(65535,65535,65535), valueColor=(0,0,0), frame=1, disable=0, value=gptransfunc

	SetDrawLayer UserBack
	//DrawRect left+1,top+9.5*bh,left+50,top+9.5*bh+18  // was background for scale popup

	Button SelAllbutton,pos={left+1.5*bw, top-0.1*bh},size={30.00*gscf,14.00*gscf},proc=SelectAllButtonProc,title="All"
	Button SelAllbutton, fsize=gscf*dfs
	Button SelNonebutton,pos={left+3.5*bw, top-0.1*bh},size={32.00*gscf,14.00*gscf},proc=SelectNoneButtonProc,title="None"
	Button SelNonebutton, fsize=gscf*dfs
	Button SelFindPeaksbutton,pos={left+6.5*bw, top-0.1*bh},size={60.00*gscf,14.00*gscf},proc=SelectFindPeaksButtonProc,title="Find peaks"
	Button SelFindPeaksbutton, fsize=gscf*dfs

	
	TitleBox title0,pos={left+3*bw, top+1*bh},size={132.00,11.00},title="Shift+mouseover=select multi"
	TitleBox title0,frame=0,fColor=(26112,26112,26112),fsize=gscf*dfs
	TitleBox title1,pos={left+3*bw, top+1.7*bh},size={140.00,11.00},title="Ctrl+mouseover=Deselect multi"
	TitleBox title1,frame=0,fColor=(26112,26112,26112),fsize=gscf*dfs
	
End


//************************************************************
// Resize PT Panel
Function PTResizePanel(pa) : PopupMenuControl
	STRUCT WMPopupAction &pa
	
	NVAR gscf		=  root:I4P:I4Pperiodictable:scf
	NVAR gscfpN	=  root:I4P:I4Pperiodictable:scfpN
	
	// wait for mouse up event
	switch(pa.eventCode)
	case 2:

		// if no change in scale factor, no change in panel
		if (pa.popNum==gscfpN)
			return 0
		endif
		
		// store current states
		gscfpN = pa.popNum
		
		// redraw panel at new scale factor
		gscf = str2num(pa.popStr)
			
		KillWindow PeriodicTablePanel

		PTDisplayPanel()
		PopupMenu PTresize mode=gscfpN

	endswitch
End


//************************************************************
// Change Source PT Panel
Function PTChangeSource(pa) : PopupMenuControl
	STRUCT WMPopupAction &pa
	
	SVAR gpxrs =  root:I4P:I4Pperiodictable:pxrs
	
	// wait for mouse up event
	switch(pa.eventCode)
	case 2:
		// store current states
		gpxrs = pa.popStr
	endswitch
	
	SelectionActionProc()
	
End

//************************************************************
// Change Satellite PT Panel
Function PTChangeSatellite(pa) : CheckBoxControl
	STRUCT WMCheckboxAction &pa
	
	NVAR gpsatellite	=  root:I4P:I4Pperiodictable:psatellite
	
	// wait for mouse up event
	switch(pa.eventCode)
	case 2:
		// store current states
		gpsatellite = pa.checked
	endswitch
	SelectionActionProc()
	
End

//************************************************************
// Change Ghost PT Panel
Function PTChangeGhost(pa) : CheckBoxControl
	STRUCT WMCheckboxAction &pa
	
	NVAR gpghost=  root:I4P:I4Pperiodictable:pghost
	
	// wait for mouse up event
	switch(pa.eventCode)
	case 2:
		// store current states
		gpghost = pa.checked
	endswitch
	SelectionActionProc()
	
End

//************************************************************
// Change Auger PT Panel
Function PTChangeAuger(pa) : CheckBoxControl
	STRUCT WMCheckboxAction &pa
	
	NVAR gpauger=  root:I4P:I4Pperiodictable:pauger
	
	// wait for mouse up event
	switch(pa.eventCode)
	case 2:
		// store current states
		gpauger = pa.checked
	endswitch
	SelectionActionProc()

End



//************************************************************
// Read the angle parameter
Function PTGetAngle(ctrlName,varNum,varStr,varName) : SetVariableControl
	String ctrlName
	Variable varNum	// value of variable as number
	String varStr		// value of variable as string
	String varName	// name of variable
	
	NVAR gpangle =  root:I4P:I4Pperiodictable:pangle	
	
	gpangle = varNum
	
	SelectionActionProc()

End

//************************************************************
// Read the transmission function exponent
Function PTGetTransFunc(ctrlName,varNum,varStr,varName) : SetVariableControl
	String ctrlName
	Variable varNum	// value of variable as number
	String varStr		// value of variable as string
	String varName	// name of variable
	
	NVAR gptransfunc =  root:I4P:I4Pperiodictable:ptransfunc
	
	gptransfunc = varNum
	
	SelectionActionProc()

End

//************************************************************
Function GetAtNr(Estr)
	string Estr
	wave AtNr=  root:I4P:I4Pperiodictable:AtNr
	wave/T ElementSym=  root:I4P:I4Pperiodictable:ElementSym
	variable ii
	
	for (ii=0; ii<maxelem; ii+=1)
		if (cmpstr(Estr,Elementsym[ii])==0)
			return AtNr[ii]
		endif
	endfor

end


//************************************************************
Function SelectAllButtonProc(ba) : ButtonControl
	STRUCT WMButtonAction &ba

	switch( ba.eventCode )
		case 2: // mouse up
			wave Col				=  root:I4P:I4Pperiodictable:Col
			wave ElemSelect		=  root:I4P:I4Pperiodictable:ElemSelect
			wave/T ElementSym	=  root:I4P:I4Pperiodictable:ElementSym
			wave/T ElementMiss	=  root:I4P:I4Pperiodictable:ElementMiss
			string str
			variable ii

			for (ii=1; ii<maxelem;ii+=1)
				ElemSelect[ii]=1
				str=ElementSym[ii] +"0"
				if(cmpstr(ElementSym[ii],ElementMiss[ii])==0)
					col[ii][0]=missR
					col[ii][1]=missG
					col[ii][2]=missB
					Button $str,fColor=(col[ii][0],col[ii][1],col[ii][2])
				else
					col[ii][0]=markR
					col[ii][1]=markG
					col[ii][2]=markB
					Button $str,fColor=(col[ii][0],col[ii][1],col[ii][2])
				endif				
				SelectionActionProc()
			endfor
			break
		case -1: // control being killed
			break
	endswitch

End

//************************************************************
Function SelectNoneButtonProc(ba) : ButtonControl
	STRUCT WMButtonAction &ba

	switch( ba.eventCode )
		case 2: // mouse up
			wave Col			=  root:I4P:I4Pperiodictable:Col
			wave ElemSelect		=  root:I4P:I4Pperiodictable:ElemSelect
			wave/T ElementSym	=  root:I4P:I4Pperiodictable:ElementSym
			wave/T ElementMiss	=  root:I4P:I4Pperiodictable:ElementMiss
			string str
			variable ii

			for (ii=1; ii<maxelem;ii+=1)
				ElemSelect[ii]=0
				str=ElementSym[ii] +"0"
				if(cmpstr(ElementSym[ii],ElementMiss[ii])==0)
					col[ii][0]=missR
					col[ii][1]=missG
					col[ii][2]=missB
					Button $str,fColor=(col[ii][0],col[ii][1],col[ii][2])
				else
					col[ii][0]=unmarkR
					col[ii][1]=unmarkG
					col[ii][2]=unmarkB
					Button $str,fColor=(col[ii][0],col[ii][1],col[ii][2])
				endif
				SelectionActionProc()
			endfor
			break
		case -1: // control being killed
			break
	endswitch

End


//************************************************************
Function SelectFindPeaksButtonProc(ba) : ButtonControl
	STRUCT WMButtonAction &ba

	switch( ba.eventCode )
		case 2: // mouse up
			BE2Peaks(dBE=1.5)
			break
		case -1: // control being killed
			break
	endswitch

End



//************************************************************
// Periodic Table Get State
// this button procedure is called whenever an element button
// is entered, left, or pushed
Function PTGetState(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	wave Col				=  root:I4P:I4Pperiodictable:Col
	wave ElemSelect		=  root:I4P:I4Pperiodictable:ElemSelect
	wave OldElemSelect	=  root:I4P:I4Pperiodictable:OldElemSelect
	
	string str
	variable atomnr

	switch(bs.eventCode)
//		case -1:  // being killed
//		break
//		case 1:  // mouse down
//		break
		case	2: // mouse up
			str=bs.ctrlname
			//str=trimstring(removeending(str))
			str=removeending(str)
			atomnr=GetAtNr(str)
			if (ElemSelect[atomnr]==0)
				ElemSelect[atomnr]=1
				str=bs.ctrlname
				col[atomnr][0]=markR
				col[atomnr][1]=markG
				col[atomnr][2]=markB
				//Button $str,fColor=(col[atomnr][0],col[atomnr][1],col[atomnr][2])
				Button $str,fColor=(markR,markG,markB)
			else
				ElemSelect[atomnr]=0
				str=bs.ctrlname
				col[atomnr][0]=unmarkR
				col[atomnr][1]=unmarkG
				col[atomnr][2]=unmarkB
				Button $str,fColor=(unmarkR,unmarkG,unmarkB)
			endif
			OldElemSelect=ElemSelect
			SelectionActionProc()
			
		break
//		case 3: //Mouse up outside control
//		break
//		case 4: // mouse moved
//		break
		case 5: // mouse enter
			str=bs.ctrlname
			//str=trimstring(removeending(str))
			str=removeending(str)
			atomnr=GetAtNr(str)
			if (bs.eventmod==2)  // shift modifier
				ElemSelect[atomnr]=1
				str=bs.ctrlname
				col[atomnr][0]=markR
				col[atomnr][1]=markG
				col[atomnr][2]=markB
				Button $str,fColor=(markR,markG,markB)
			elseif ((bs.eventmod==8)||(bs.eventmod==16)) // ctrl modifier, different on mac,win
				ElemSelect[atomnr]=0
				str=bs.ctrlname
				col[atomnr][0]=unmarkR
				col[atomnr][1]=unmarkG
				col[atomnr][2]=unmarkB
				Button $str,fColor=(unmarkR,unmarkG,unmarkB)
			endif
			
			if (PTcheckListChange()) // selection may not have changed, if mouse only moved
				OldElemSelect=ElemSelect
				SelectionActionProc()
			endif

		break
//		case 6:  // mouse leave
//		break
	endswitch

end

//************************************************************
Function PTcheckListChange()
	wave ElemSelect		=  root:I4P:I4Pperiodictable:ElemSelect
	wave OldElemSelect	=  root:I4P:I4Pperiodictable:OldElemSelect
	
	matrixop/O chgwv=sum(equal(ElemSelect,OldElemSelect))
	variable check_chgwv = chgwv[0]
	killwaves /z chgwv

	if (check_chgwv==numpnts(ElemSelect))
		return 0
	else
		return 1
	endif
end