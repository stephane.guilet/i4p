// Function :  I4PDatabaseYL
// Author : R�mi Lazzari, lazzari@insp.jussieu.fr
// Date : 2019
// Usage :  database from J.J. Yeh and I. Lindau
// (binding energies, photoionization cross section, asymmetry parameter)

#pragma rtGlobals=3		


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Structure to store all the informations for each chemical element : Yeh and Lindau
//  Atomic subshell photoionization cross sections and assymetry parameters: 1< Z <300 
//  J.J. Yeh and I. Lindau, Atomic data and nuclear data tables 32 (1985) 1-155
// Unit : Mbarn
Structure I4PElementYL
	string na			// element name 
	string sy			// element symbol
	string nu			// element number
	string cl			// core-level label
	string be			// binding energy (eV)		
	string picsHeI	// PICS at He I 
	string picsMg	// PICS at Mg-Ka 
	string picsAl		// PICS at Al-Ka 
	string asymHeI	// Asymmetry at He I 
	string asymMg	// Asymmetry at Mg-Ka 
	string asymAl	// Asymmetry at Al-Ka 
EndStructure


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// I4P database 
// Atomic subshell photoionization cross sections and assymetry parameters: 1< Z <300 
//  J.J. Yeh and I. Lindau, Atomic data and nuclear data tables 32 (1985) 1-155

Function I4PDatabaseYL(TagElement,Elmt)

string TagElement
STRUCT I4PElementYL &Elmt

strswitch(TagElement)

//---------------------------------------------------
case "H":
Elmt.na			= "hydrogen"
Elmt.sy			= "H"
Elmt.nu			= "1"		
Elmt.cl			= "1s"
Elmt.be	 		= "13.6"
Elmt.picsHeI 	= "1.888"
Elmt.picsMg 		= "0.460e-5"
Elmt.picsAl 		= "0.20e-5"
Elmt.asymHeI	= "2"
Elmt.asymMg	= "2"
Elmt.asymAl		= "2"
break
//---------------------------------------------------
case "He":
Elmt.na			= "helium"
Elmt.sy			= "He"
Elmt.nu			= "2"		
Elmt.cl			= "1s"
Elmt.be	 		= "23.4"
Elmt.picsHeI 	= "nan"
Elmt.picsMg 		= "0.18e-3"
Elmt.picsAl 		= "0.11e-3"
Elmt.asymHeI	= "2"
Elmt.asymMg	= "2"
Elmt.asymAl		= "2"
break
//---------------------------------------------------
case "Li":
Elmt.na			= "lithium"
Elmt.sy			= "Li"
Elmt.nu			= "3"		
Elmt.cl			= "1s;2s"
Elmt.be	 		= "59.8;5.5"
Elmt.picsHeI 	= "nan;0.4806"
Elmt.picsMg 		= "0.13e-2;0.17e-4"
Elmt.picsAl 		= "0.79e-3;0.86e-5"
Elmt.asymHeI	= "nan;2"
Elmt.asymMg	= "2;2"
Elmt.asymAl		= "2;2"
break
//---------------------------------------------------
case "Be":
Elmt.na			= "beryllium"
Elmt.sy			= "Be"
Elmt.nu			= "4"		
Elmt.cl			= "1s;2s"
Elmt.be	 		= "118.3;8.2"
Elmt.picsHeI 	= "nan;1.535"
Elmt.picsMg 		= "0.44e-2;0.15e-3"
Elmt.picsAl 		= "0.26e-2;0.87e-4"
Elmt.asymHeI	= "nan;2"
Elmt.asymMg	= "2;2"
Elmt.asymAl		= "2;2"
break
//---------------------------------------------------
case "B":
Elmt.na			= "boron"
Elmt.sy			= "B"
Elmt.nu			= "5"		
Elmt.cl			= "1s;2s;2p"
Elmt.be	 		= "195.5;12.6;6.7"
Elmt.picsHeI 	= "nan;1.74;1.934"
Elmt.picsMg 		= "0.10e-1;0.49e-3;0.57e-5"
Elmt.picsAl 		= "0.66e-2;0.28e-3;0.24e-5"
Elmt.asymHeI	= "nan;2;1.474"
Elmt.asymMg	= "2;2;0.5124"
Elmt.asymAl		= "2;2;-0.2882"
break
//---------------------------------------------------
case "C":
Elmt.na			= "carbon"
Elmt.sy			= "C"
Elmt.nu			= "6"		
Elmt.cl			= "1s;2s;2p"
Elmt.be	 		= "290.9;17.5;9"
Elmt.picsHeI 	= "nan;1.230;6.128"
Elmt.picsMg 		= "0.22e-1;0.10e-2;0.56e-4"
Elmt.picsAl 		= "0.13e-1;0.66e-3;0.10e-4"
Elmt.asymHeI	= "nan;2;1.308"
Elmt.asymMg	= "2;2;1.070"
Elmt.asymAl		= "2;2;-0.5547"
break
//---------------------------------------------------
case "N":
Elmt.na			= "nitrogen"
Elmt.sy			= "N"
Elmt.nu			= "7"		
Elmt.cl			= "1s;2s;2p"
Elmt.be	 		= "404.6;23.1;11.5"
Elmt.picsHeI 	= "nan;nan;9.688"
Elmt.picsMg 		= "0.39e-1;0.19e-2;0.18e-3"
Elmt.picsAl 		= "0.24e-1;0.11e-2;0.72e-4"
Elmt.asymHeI	= "nan;nan;1.072"
Elmt.asymMg	= "2;2;0.6521"
Elmt.asymAl		= "2;2;0.1659"
break
//---------------------------------------------------
case "O":
Elmt.na			= "oxygen"
Elmt.sy			= "O"
Elmt.nu			= "8"		
Elmt.cl			= "1s;2s;2p"
Elmt.be	 		= "536.8;29.2;14.2"
Elmt.picsHeI 	= "nan;nan;10.67"
Elmt.picsMg 		= "0.63e-1;0.29e-2;0.50e-3"
Elmt.picsAl 		= "0.40e-1;0.19e-2;0.24e-3"
Elmt.asymHeI	= "nan;nan;0.7365"
Elmt.asymMg	= "2;2;0.5819"
Elmt.asymAl		= "2;2;0.5193"
break
//---------------------------------------------------
case "F":
Elmt.na			= "fluorine"
Elmt.sy			= "F"
Elmt.nu			= "9"		
Elmt.cl			= "1s;2s;2p"
Elmt.be	 		= "687.6;35.8;17"
Elmt.picsHeI 	= "nan;nan;9.305"
Elmt.picsMg 		= "0.94e-1;0.43e-2;0.11e-2"
Elmt.picsAl 		= "0.60e-1;0.28e-2;0.68e-3"
Elmt.asymHeI	= "nan;nan;0.2535"
Elmt.asymMg	= "2;2;0.6751"
Elmt.asymAl		= "2;2;0.6257"
break
//---------------------------------------------------
case "Ne":
Elmt.na			= "neon"
Elmt.sy			= "Ne"
Elmt.nu			= "10"		
Elmt.cl			= "1s;2s;2p"
Elmt.be	 		= "857;43.1;20"
Elmt.picsHeI 	= "nan;nan;6.514"
Elmt.picsMg 		= "0.1328;0.62e-2;0.25e-2"
Elmt.picsAl 		= "0.86e-1;0.40e-2;0.13e-2"
Elmt.asymHeI	= "nan;nan;-0.3960"
Elmt.asymMg	= "2;2;0.7645"
Elmt.asymAl		= "2;2;0.6958"
break
//---------------------------------------------------
case "Na":
Elmt.na			= "sodium"
Elmt.sy			= "Na"
Elmt.nu			= "11"		
Elmt.cl			= "1s;2s;2p;3s"
Elmt.be	 		= "1062.2;64.3;36.3;5.1"
Elmt.picsHeI	 	= "nan;nan;nan;0.1014"
Elmt.picsMg 		= "0.1781;0.85e-2;0.46e-2;0.14e-3"
Elmt.picsAl 		= "0.1165;0.58e-2;0.25e-2;0.97e-4"
Elmt.asymHeI	= "nan;nan;nan;2"
Elmt.asymMg	= "2;2;0.8701;2"
Elmt.asymAl		= "2;2;0.7882;2"
break
//---------------------------------------------------
case "Mg":
Elmt.na			= "magnesium"
Elmt.sy			= "Mg"
Elmt.nu			= "12"		
Elmt.cl			= "1s;2s;2p;3s"
Elmt.be	 		= "1291.9;89.1;56.4;6.9"
Elmt.picsHeI	 	= "nan;nan;nan;0.273"
Elmt.picsMg 		= "nan;0.11e-1;0.77e-2;0.58e-3"
Elmt.picsAl 		= "0.1524;0.77e-2;0.46e-2;0.38e-3"
Elmt.asymHeI	= "nan;nan;nan;2"
Elmt.asymMg	= "2;2;0.9517;2"
Elmt.asymAl		= "2;2;0.8688;2"
break
//---------------------------------------------------
case "Al":
Elmt.na			= "aluminium"
Elmt.sy			= "Al"
Elmt.nu			= "13"		
Elmt.cl			= "2s;2p;3s;3p"
Elmt.be	 		= "118.6;80.9;10.1;4.9"
Elmt.picsHeI 	= "nan;nan;0.3431;0.88e-1"
Elmt.picsMg 		= "0.15e-1;0.12e-1;0.11e-2;0.74e-4"
Elmt.picsAl 		= "0.1e-1;0.72e-2;0.78e-3;0.59e-4"
Elmt.asymHeI	= "nan;nan;2;-0.2642"
Elmt.asymMg	= "2;1.033;2;0.9633"
Elmt.asymAl		= "2;0.9535;2;0.8909"
break
//---------------------------------------------------
case "Si":
Elmt.na			= "silicon"
Elmt.sy			= "Si"
Elmt.nu			= "14"		
Elmt.cl			= "2s;2p;3s;3p"
Elmt.be	 		= "150.8;108.2;13.6;6.5"
Elmt.picsHeI 	= "nan;nan;0.2880;0.3269"
Elmt.picsMg 		= "0.18e-1;0.19e-1;0.16e-2;0.35e-3"
Elmt.picsAl 		= "0.13e-1;0.11e-1;0.10e-2;0.17e-3"
Elmt.asymHeI	= "nan;nan;2;0.6237"
Elmt.asymMg	= "2;1.105;2;1.068"
Elmt.asymAl		= "2;0.9535;2;0.8909"
break
//---------------------------------------------------
case "P":
Elmt.na			= "phosphrous"
Elmt.sy			= "P"
Elmt.nu			= "15"		
Elmt.cl			= "2s;2p;3s;3p"
Elmt.be	 		= "186.2;138.5;17.1;8.3"
Elmt.picsHeI 	= "nan;nan;0.1714;1.232"
Elmt.picsMg 		= "0.23e-1;0.27e-1;0.22e-2;0.88e-3"
Elmt.picsAl 		= "0.16e-1;0.16e-1;0.14e-2;0.50e-3"
Elmt.asymHeI	= "nan;nan;2;1.582"
Elmt.asymMg	= "2;1.169;2;1.131"
Elmt.asymAl		= "2;1.092;2;1.047"
break
//---------------------------------------------------
case "S":
Elmt.na			= "sulfur"
Elmt.sy			= "S"
Elmt.nu			= "16"		
Elmt.cl			= "2s;2p;3s;3p"
Elmt.be	 		= "224.6;171.8;20.8;10.3"
Elmt.picsHeI 	= "nan;nan;nan;4.333"
Elmt.picsMg 		= "0.27e-1;0.38e-1;0.28e-2;0.17e-2"
Elmt.picsAl 		= "0.19e-1;0.22e-1;0.19e-2;0.10e-2"
Elmt.asymHeI	= "nan;nan;2;1.852"
Elmt.asymMg	= "2;1.225;2;1.186"
Elmt.asymAl		= "2;1.153;2;1.108"
break
//---------------------------------------------------
case "Cl":
Elmt.na			= "chlorine"
Elmt.sy			= "Cl"
Elmt.nu			= "17"		
Elmt.cl			= "2s;2p;3s;3p"
Elmt.be	 		= "266.2;208.2;24.7;12.3"
Elmt.picsHeI 	= "nan;nan;nan;13.84"
Elmt.picsMg 		= "0.32e-1;0.52e-1;0.36e-2;0.33e-2"
Elmt.picsAl 		= "0.22e-1;0.31e-1;0.25e-2;0.19e-2"
Elmt.asymHeI	= "nan;nan;nan;1.776"
Elmt.asymMg	= "2;1.277;2;1.236"
Elmt.asymAl		= "2;1.207;2;1.153"
break
//---------------------------------------------------
case "Ar":
Elmt.na			= "argon"
Elmt.sy			= "Ar"
Elmt.nu			= "18"		
Elmt.cl			= "2s;2p;3s;3p"
Elmt.be	 		= "311.1;247.7;28.7;14.5"
Elmt.picsHeI 	= "nan;nan;nan;38.01"
Elmt.picsMg 		= "0.37e-1;0.69e-1;0.43e-2;0.51e-2"
Elmt.picsAl 		= "0.26e-1;0.41e-1;0.30e-2;0.34e-2"
Elmt.asymHeI	= "nan;nan;nan;1.542"
Elmt.asymMg	= "2;1.321;2;1.272"
Elmt.asymAl		= "2;1.256;2;1.205"
break
//---------------------------------------------------
case "K":
Elmt.na			= "potassium"
Elmt.sy			= "K"
Elmt.nu			= "19"		
Elmt.cl			= "2s;2p;3s;3p"
Elmt.be	 		= "368.2;299.4;40.2;23.6"
Elmt.picsHeI 	= "nan;nan;nan;nan"
Elmt.picsMg 		= "0.43e-1;0.89e-1;0.54e-2;0.78e-2"
Elmt.picsAl 		= "0.30e-1;0.53e-1;0.38e-2;0.49e-2"
Elmt.asymHeI	= "nan;nan;nan;nan"
Elmt.asymMg	= "2;1.358;2;1.311"
Elmt.asymAl		= "2;1.299;2;1.244"
break
//---------------------------------------------------
case "Ca":
Elmt.na			= "calcium"
Elmt.sy			= "Ca"
Elmt.nu			= "20"		
Elmt.cl			= "2s;2p;3s;3p;4s"
Elmt.be	 		= "430.3;356.2;52.7;33.8;5.4"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.1976"
Elmt.picsMg 		= "0.49e-1;0.1132;0.68e-2;0.11e-1;0.49e-3"
Elmt.picsAl 		= "0.35e-1;0.68e-1;0.47e-2;0.69e-2;0.31e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;2"
Elmt.asymMg	= "2;1.391;2;1.35;2"
Elmt.asymAl		= "2;1.337;2;1.286;2"
break
//---------------------------------------------------
case "Sc":
Elmt.na			= "scandium"
Elmt.sy			= "Sc"
Elmt.nu			= "21"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "489.4;409.9;60.3;39.2;5.9;7.2"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.1998;3.134"
Elmt.picsMg 		= "0.54e-1;0.1419;0.78e-2;0.14e-1;0.63e-3;0.10e-3"
Elmt.picsAl 		= "0.39e-1;0.86e-1;0.55e-2;0.89e-2;0.46e-3;0.53e-4"
Elmt.asymHeI	= "nan;nan;nan;nan;2;0.2578"
Elmt.asymMg	= "2;1.417;2;1.386;2;0.7405"
Elmt.asymAl		= "2;1.370;2;1.324;2;0.6818"
break
//---------------------------------------------------
case "Ti":
Elmt.na			= "titanium"
Elmt.sy			= "Ti"
Elmt.nu			= "22"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "551.3;466.4;67.8;44.6;6.2;8.5"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.1951;5.074"
Elmt.picsMg 		= "0.60e-1;0.1746;0.91e-2;0.17e-1;0.69e-3;0.34e-3"
Elmt.picsAl 		= "0.44e-1;0.1069;0.64e-2;0.11e-1;0.50e-3;0.17e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;2;0.1380"
Elmt.asymMg	= "2;1.436;2;1.412;2;0.7907"
Elmt.asymAl		= "2;1.397;2;1.353;2;0.7152"
break
//---------------------------------------------------
case "V":
Elmt.na			= "vanadium"
Elmt.sy			= "V"
Elmt.nu			= "23"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "616.2;525.8;75.3;49.9;6.6;9.8"
Elmt.picsHeI	 	= "nan;nan;nan;nan;0.1845;5.813"
Elmt.picsMg 		= "0.66e-1;0.2117;0.10e-1;0.21e-1;0.76e-3;0.85e-3"
Elmt.picsAl 		= "0.48e-1;0.1308;0.73e-2;0.13e-1;0.51e-3;0.42e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;2;0.25e-1"
Elmt.asymMg	= "2;1.449;2;1.441;2;0.8252"
Elmt.asymAl		= "2;1.419;2;1.386;2;0.7706"
break
//---------------------------------------------------
case "Cr":
Elmt.na			= "chromium"
Elmt.sy			= "Cr"
Elmt.nu			= "24"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "677.9;582;77.7;50.2;5.9;6.5"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.63e-1;9.230"
Elmt.picsMg 		= "0.72e-1;0.2540;0.11e-1;0.24e-1;0.31e-3;0.16e-2"
Elmt.picsAl 		= "0.53e-1;0.1577;0.81e-2;0.15e-1;0.18e-3;0.88e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;2;0.88e-1"
Elmt.asymMg	= "2;1.456;2;1.464;2;0.8586"
Elmt.asymAl		= "2;1.435;2;1.410;2;0.8007"
break
//---------------------------------------------------
case "Mn":
Elmt.na			= "manganese"
Elmt.sy			= "Mn"
Elmt.nu			= "25"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "755.2;653.7;90.9;60.9;7.1;12.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.1532;5.344"
Elmt.picsMg 		= "0.77e-1;0.3011;0.12e-1;0.29e-1;0.92e-3;0.26e-2"
Elmt.picsAl 		= "0.57e-1;0.1878;0.91e-2;0.19e-1;0.58e-3;0.14e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;2;-0.1373"
Elmt.asymMg	= "2;1.457;2;1.484;2;0.8998"
Elmt.asymAl		= "2;1.447;2;1.437;2;0.8312"
break
//---------------------------------------------------
case "Fe":
Elmt.na			= "iron"
Elmt.sy			= "Fe"
Elmt.nu			= "26"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "829.3;722.2;98.2;66.6;7.4;13.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.1349;4.833"
Elmt.picsMg 		= "0.83e-1;0.3529;0.13e-1;0.34e-1;0.91e-3;0.45e-2"
Elmt.picsAl 		= "0.62e-1;0.2216;0.10e-1;0.22e-1;0.70e-3;0.22e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;2;-0.1604"
Elmt.asymMg	= "2;1.449;2;1.501;2;0.9315"
Elmt.asymAl		= "2;1.453;2;1.458;2;0.8663"
break
//---------------------------------------------------
case "Co":
Elmt.na			= "colbalt"
Elmt.sy			= "Co"
Elmt.nu			= "27"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "906.7;793.9;107.2;72.4;7.7;14.2"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.1158;4.356"
Elmt.picsMg 		= "0.88e-1;0.4090;0.15e-1;0.39e-1;0.95e-3;0.67e-2"
Elmt.picsAl 		= "0.67e-1;0.2591;0.11e-1;0.26e-1;0.71e-3;0.37e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;2;-0.1328"
Elmt.asymMg	= "2;1.430;2;1.519;2;0.9569"
Elmt.asymAl		= "2;1.453;2;1.479;2;0.9001"
break
//---------------------------------------------------
case "Ni":
Elmt.na			= "nickel"
Elmt.sy			= "Ni"
Elmt.nu			= "28"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "987.3;868.8;115.6;78.3;7.9;15.2"
Elmt.picsHeI	 	= "nan;nan;nan;nan;0.93e-1;3.984"
Elmt.picsMg 		= "0.93e-1;0.4691;0.16e-1;0.45e-1;0.10e-2;0.10e-1"
Elmt.picsAl 		= "0.71e-1;0.2998;0.12e-1;0.29e-1;0.83e-3;0.59e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;2;-0.27e-1"
Elmt.asymMg	= "2;1.396;2;1.536;2;1.035"
Elmt.asymAl		= "2;1.446;2;1.493;2;0.9599"
break
//---------------------------------------------------
case "Cu":
Elmt.na			= "copper"
Elmt.sy			= "Cu"
Elmt.nu			= "29"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "1063.3;939.1;117.5;77.7;6.9;10.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.36e-1;7.553"
Elmt.picsMg 		= "0.98e-1;0.5345;0.17e-1;0.50e-1;0.42e-3;0.21e-1"
Elmt.picsAl 		= "0.75e-1;0.3438;0.13e-1;0.33e-1;0.27e-3;0.12e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;2;-0.1746"
Elmt.asymMg	= "2;1.343;2;1.548;2;1.116"
Elmt.asymAl		= "2;1.429;2;1.510;2;1.019"
break
//---------------------------------------------------
case "Zn":
Elmt.na			= "zinc"
Elmt.sy			= "Zn"
Elmt.nu			= "30"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "1158.1;1027.9;133.2;90.6;8.4;17.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.57e-1;3.572"
Elmt.picsMg 		= "0.1036;0.6057;0.19e-1;0.56e-1;0.11e-2;0.21e-1"
Elmt.picsAl 		= "0.79e-1;0.3907;0.14e-1;0.37e-1;0.78e-3;0.12e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;2;0.2692"
Elmt.asymMg	= "2;1.254;2;1.559;2;1.036"
Elmt.asymAl		= "2;1.400;2;1.526;2;0.9684"
break
//---------------------------------------------------
case "Ga":
Elmt.na			= "gallium"
Elmt.sy			= "Ga"
Elmt.nu			= "31"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d"
Elmt.be	 		= "1260.4;1124.2;152.9;107.4;11.4;27.8"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.48e-1;nan"
Elmt.picsMg 		= "nan;0.6851;0.21e-1;0.64e-1;0.17e-2;0.26e-1"
Elmt.picsAl 		= "0.83e-1;0.4412;0.15e-1;0.43e-1;0.12e-2;0.14e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;2;nan"
Elmt.asymMg	= "2;1.089;2;1.572;2;1.060"
Elmt.asymAl		= "2;1.352;2;1.537;2;1.002"
break
//---------------------------------------------------
case "Ge":
Elmt.na			= "germanium"
Elmt.sy			= "Ge"
Elmt.nu			= "32"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d;4p"
Elmt.be	 		= "1367.4;1225.1;173.7;125.1;14.4;39.3;6.4"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.22e-1;nan;1.528"
Elmt.picsMg 		= "nan;0.7071;0.22e-1;0.71e-1;0.19e-2;0.34e-1;0.11e-2"
Elmt.picsAl 		= "0.88e-1;0.4956;0.16e-1;0.48e-1;0.15e-2;0.19e-1;0.74e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;2;nan;1.888"
Elmt.asymMg	= "nan;0.6629;2;1.578;2;1.083;1.604"
Elmt.asymAl		= "2;1.274;2;1.552;2;1.031;1.522"
break
//---------------------------------------------------
case "As":
Elmt.na			= "arsenic"
Elmt.sy			= "As"
Elmt.nu			= "33"		
Elmt.cl			= "2s;2p;3s;3p;4s;3d;4p"
Elmt.be	 		= "1479.2;1330.9;195.6;144.0;17.4;52.0;7.9"
Elmt.picsHeI 	= "nan;nan;nan;nan;0.24e-2;nan;3.856"
Elmt.picsMg 		= "nan;nan;0.24e-1;0.79e-1;0.24e-2;0.44e-1;0.23e-2"
Elmt.picsAl 		= "0.86e-1;0.558;1.0.18e-1;0.54e-1;0.18e-2;0.25e-1;0.17e-2"
Elmt.asymHeI	= "2;nan;nan;nan;2;nan;1.878"
Elmt.asymMg	= "2;nan;2;1.587;2;1.105;1.589"
Elmt.asymAl		= "2;1.131;2;1.562;2;1.055;1.575"
break
//---------------------------------------------------
case "Se":
Elmt.na			= "selenium"
Elmt.sy			= "Se"
Elmt.nu			= "34"		
Elmt.cl			= "2p;3s;3p;4s;3d;4p"
Elmt.be	 		= "1441.4;218.7;164;20.3;65.8;9.5"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;8.057"
Elmt.picsMg 		= "nan;0.26e-1;0.88e-1;0.30e-2;0.55e-1;0.38e-2"
Elmt.picsAl 		= "0.5917;0.19e-1;0.60e-1;0.21e-2;0.31e-1;0.26e-2"
Elmt.asymHeI	= "nan;nan;nan;2;nan;1.844"
Elmt.asymMg	= "nan;2;1.595;2;1.124;1.580"
Elmt.asymAl		= "0.7728;2;1.571;2;1.069;1.566"
break
//---------------------------------------------------
case "Br":
Elmt.na			= "bromium"
Elmt.sy			= "Br"
Elmt.nu			= "35"		
Elmt.cl			= "3s;3p;4s;3d;4p;4d"
Elmt.be	 		= "243;185.2;23.4;80.6;11.2;nan"
Elmt.picsHeI 	= "nan;nan;nan;nan;15.57;nan"
Elmt.picsMg 		= "0.28e-1;0.97e-1;0.34e-2;0.67e-1;0.63e-2;nan"
Elmt.picsAl 		= "0.20e-1;0.67e-1;0.25e-2;0.39e-1;0.45e-2;nan"
Elmt.asymHeI	= "nan;nan;nan;nan;1.783;nan"
Elmt.asymMg	= "2;1.600;2;1.134;1.616;nan"
Elmt.asymAl		= "2;1.580;2;1.094;1.595;nan"
break
//---------------------------------------------------
case "Kr":
Elmt.na			= "krypton"
Elmt.sy			= "Kr"
Elmt.nu			= "36"		
Elmt.cl			= "3s;3p;4s;3d;4p"
Elmt.be	 		= "268.4;207.5;26.5;96.6;13.0"
Elmt.picsHeI 	= "nan;nan;nan;nan;28.51"
Elmt.picsMg 		= "0.30e-1;0.1063;0.39e-2;0.83e-1;0.95e-2"
Elmt.picsAl 		= "0.22e-1;0.74e-1;0.29e-2;0.47e-1;0.63e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;1.686"
Elmt.asymMg	= "2;1.605;2;1.153;1.634"
Elmt.asymAl		= "2;1.588;2;1.106;1.586"
break
//---------------------------------------------------
case "Rb":
Elmt.na			= "rubidium"
Elmt.sy			= "Rb"
Elmt.nu			= "37"		
Elmt.cl			= "3s;3p;4s;3d;4p"
Elmt.be	 		= "302.1;237.9;35.5;120.7;20.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan"
Elmt.picsMg 		= "0.32e-1;0.1162;0.45e-2;0.99e-1;0.12e-1"
Elmt.picsAl 		= "0.23e-1;0.81e-1;0.33e-2;0.58e-1;0.85e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan"
Elmt.asymMg	= "2;1.610;2;1.164;1.633"
Elmt.asymAl		= "2;1.593;2;1.124;1.614"
break
//---------------------------------------------------
case "Sr":
Elmt.na			= "strontium"
Elmt.sy			= "Sr"
Elmt.nu			= "38"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s"
Elmt.be	 		= "337.7;270.3;45.0;146.6;27.7;5"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.1036"
Elmt.picsMg 		= "0.34e-1;0.1262;0.53e-2;0.1186;0.14e-1;0.43e-3"
Elmt.picsAl 		= "0.25e-1;0.89e-1;0.39e-2;0.69e-1;0.10e-1;0.30e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2"
Elmt.asymMg	= "2;1.612;2;1.177;1.637;2"
Elmt.asymAl		= "2;1.600;2;1.138;1.614;2"
break
//---------------------------------------------------
case "Y":
Elmt.na			= "yttrium"
Elmt.sy			= "Y"
Elmt.nu			= "39"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d"
Elmt.be	 		= "371.2;300.5;51.5;170.5;32.6;5.5;5.6"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.1116;4.795"
Elmt.picsMg 		= "0.36e-1;0.1363;0.60e-2;0.1400;0.17e-1;0.61e-3;0.72e-3"
Elmt.picsAl 		= "0.27e-1;0.96e-1;0.45e-2;0.82e-1;0.12e-1;0.48e-3;0.45e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;0.6218"
Elmt.asymMg	= "2;1.613;2;1.185;1.645;2;1.178"
Elmt.asymAl		= "2;1.605;2;1.155;1.621;2;1.187"
break
//---------------------------------------------------
case "Zr":
Elmt.na			= "zirconium"
Elmt.sy			= "Zr"
Elmt.nu			= "40"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d"
Elmt.be	 		= "405.5;331.4;57.6;194.8;37.1;5.9;7.0"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.1139;12.18"
Elmt.picsMg 		= "0.38e-1;0.1464;0.68e-2;0.1633;0.19e-1;0.65e-3;0.19e-2"
Elmt.picsAl 		= "0.28e-1;0.1049;0.50e-2;0.96e-1;0.14e-1;0.46e-3;0.11e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;0.4928"
Elmt.asymMg	= "2;1.613;2;1.194;1.653;2;1.217"
Elmt.asymAl		= "2;1.608;2;1.166;1.635;2;1.155"
break
//---------------------------------------------------
case "Nb":
Elmt.na			= "niobium"
Elmt.sy			= "Nb"
Elmt.nu			= "41"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d"
Elmt.be	 		= "436.7;359.4;60.3;216.3;38.5;5.5;6.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.4e-1;22.10"
Elmt.picsMg 		= "0.41e-1;0.1572;0.74e-2;0.1900;0.22e-1;0.24e-3;0.46e-2"
Elmt.picsAl 		= "0.30e-1;0.1130;0.54e-2;0.1124;0.15e-1;0.18e-3;0.26e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;0.4174"
Elmt.asymMg	= "2;1.612;2;1.203;1.661;2;1.249"
Elmt.asymAl		= "2;1.611;2;1.175;1.644;2;1.165"
break
//---------------------------------------------------
case "Mo":
Elmt.na			= "molybdenum"
Elmt.sy			= "Mo"
Elmt.nu			= "42"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d"
Elmt.be	 		= "472.3;391.6;66;242;42.5;5.7;7.2"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.4e-1;26.27"
Elmt.picsMg 		= "0.43e-1;0.1683;0.82e-2;0.2186;0.24e-1;0.27e-3;0.74e-2"
Elmt.picsAl 		= "0.32e-1;0.1211;0.60e-2;0.1303;0.17e-1;0.23e-3;0.46e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;0.2878"
Elmt.asymMg	= "2;1.611;2;1.208;1.667;2;1.254"
Elmt.asymAl		= "2;1.612;2;1.185;1.649;2;1.220"
break
//---------------------------------------------------
case "Tc":
Elmt.na			= "technetium"
Elmt.sy			= "Tc"
Elmt.nu			= "43"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d"
Elmt.be	 		= "513.2;429.1;75.5;273.0;50.3;6.8;11.2"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.1024;22.15"
Elmt.picsMg 		= "0.45e-1;0.1792;0.89e-2;0.2500;0.27e-1;0.85e-3;0.98e-2"
Elmt.picsAl 		= "0.34e-1;0.1298;0.66e-2;0.1502;0.20e-1;0.58e-3;0.60e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;0.59e-1"
Elmt.asymMg	= "2;1.606;2;1.210;1.672;2;1.257"
Elmt.asymAl		= "2;1.613;2;1.195;1.656;2;1.202"
break
//---------------------------------------------------
case "Ru":
Elmt.na			= "ruthenium"
Elmt.sy			= "Ru"
Elmt.nu			= "44"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d"
Elmt.be	 		= "546.3;458.8;77.2;296.1;50.6;6.0;9.3"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.36e-1;25.63"
Elmt.picsMg 		= "0.47e-1;0.1899;0.96e-2;0.2836;0.30e-1;0.36e-3;0.15e-1"
Elmt.picsAl 		= "0.36e-1;0.1387;0.71e-2;0.1717;0.21e-1;0.22e-3;0.91e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;0.16e-1"
Elmt.asymMg	= "2;1.601;2;1.210;1.677;2;1.271"
Elmt.asymAl		= "2;1.613;2;1.201;1.661;2;1.200"
break
//---------------------------------------------------
case "Rh":
Elmt.na			= "rhodium"
Elmt.sy			= "Rh"
Elmt.nu			= "45"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d"
Elmt.be	 		= "584.9;493.9;82.8;324.5;54.7;6.2;10.4"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.34e-1;22.75"
Elmt.picsMg 		= "0.50e-1;0.2009;0.10e-1;0.3210;0.32e-1;0.34e-3;0.20e-1"
Elmt.picsAl 		= "0.38e-1;0.1475;0.76e-2;0.1949;0.23e-1;0.23e-3;0.12e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;-0.1245"
Elmt.asymMg	= "2;1.593;2;1.210;1.677;2;1.265"
Elmt.asymAl		= "2;1.611;2;1.205;1.664;2;1.220"
break
//---------------------------------------------------
case "Pd":
Elmt.na			= "palladium"
Elmt.sy			= "Pd"
Elmt.nu			= "46"		
Elmt.cl			= "3s;3p;4s;3d;4p;4d"
Elmt.be	 		= "519.8;525.4;84.2;349.3;54.6;8.0"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;26.05"
Elmt.picsMg 		= "0.52e-1;0.2118;0.11e-1;0.3617;0.34e-1;0.26e-1"
Elmt.picsAl 		= "0.39e-1;0.1564;0.81e-2;0.2197;0.25e-1;0.16e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;-0.88e-1"
Elmt.asymMg	= "2;1.584;2;1.208;1.681;1.255"
Elmt.asymAl		= "2;1.607;2;1.207;1.670;1.222"
break
//---------------------------------------------------
case "Ag":
Elmt.na			= "silver"
Elmt.sy			= "Ag"
Elmt.nu			= "47"		
Elmt.cl			= "3s;3p;4s;3d;4p;4d;5s"
Elmt.be	 		= "665.1;567.2;94.2;384.4;62.9;12.6;6.4"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;16.62;0.30e-1"
Elmt.picsMg 		= "0.54e-1;0.2225;0.12e-1;0.4052;0.38e-1;0.33e-1;0.35e-3"
Elmt.picsAl 		= "0.41e-1;0.1651;0.88e-2;0.2474;0.27e-1;0.21e-1;0.29e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;-0.3695;2"
Elmt.asymMg	= "2;1.571;2;1.203;1.688;1.275;2"
Elmt.asymAl		= "2;1.602;2;1.209;1.673;1.264;2"
break
//---------------------------------------------------
case "Cd":
Elmt.na			= "cadnium"
Elmt.sy			= "Cd"
Elmt.nu			= "48"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d"
Elmt.be	 		= "712.8;611.4;105.4;421.8;72.3;7.7;18.3"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.65e-1;nan"
Elmt.picsMg 		= "0.57e-1;0.2335;0.12e-1;0.4519;0.41e-1;0.98e-3;0.41e-1"
Elmt.picsAl 		= "0.43e-1;0.1740;0.96e-2;0.2776;0.30e-1;0.70e-3;0.26e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan"
Elmt.asymMg	= "2;1.557;2;1.195;1.688;2;1.287"
Elmt.asymAl		= "2;1.595;2;1.210;1.681;2;1.270"
break
//---------------------------------------------------
case "In":
Elmt.na			= "indium"
Elmt.sy			= "In"
Elmt.nu			= "49"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d"
Elmt.be	 		= "764.2; 659.3;119.0;462.9;84.1;10.1;26.2"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.72e-1;nan"
Elmt.picsMg 		= "0.59e-1;0.2442;0.14e-1;0.5017;0.45e-1;0.13e-2;0.49e-1"
Elmt.picsAl 		= "0.45e-1;0.1830;0.10e-1;0.3098;0.32e-1;0.10e-2;0.31e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan"
Elmt.asymMg	= "2;1.540;2;1.184;1.693;2;1.295"
Elmt.asymAl		= "2;1.587;2;1.208;1.680;2;1.270"
break
//---------------------------------------------------
case "Sn":
Elmt.na			= "tin"
Elmt.sy			= "Sn"
Elmt.nu			= "50"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p"
Elmt.be	 		= "817.2;708.7;132.8;505.5;96.1;12.5;34.4;5.9"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.65e-1;nan;1.173"
Elmt.picsMg 		= "0.61e-1;0.2550;0.15e-1;0.5551;0.49e-1;0.16e-2;0.57e-1;0.10e-2"
Elmt.picsAl 		= "0.47e-1;0.1920;0.11e-1;0.3442;0.35e-1;0.12e-2;0.37e-1;0.77e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;1.831"
Elmt.asymMg	= "2;1.519;2;1.170;1.694;2;1.304;1.708"
Elmt.asymAl		= "2;1.576;2;1.203;1.685;2;1.279;1.685"
break
//---------------------------------------------------
case "Sb":
Elmt.na			= "antimony"
Elmt.sy			= "Sb"
Elmt.nu			= "51"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p"
Elmt.be	 		= "872.0;759.9;147.1;549.9;108.5;14.8;42.9;7.3"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.43e-1;nan;2.585"
Elmt.picsMg 		= "0.64e-1;0.2652;0.16e-1;0.6116;0.53e-1;0.19e-2;0.66e-1;0.21e-2"
Elmt.picsAl 		= "0.49e-1;0.2010;0.11e-1;0.3810;0.39e-1;0.14e-2;0.43e-1;0.16e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;1.852"
Elmt.asymMg	= "2;1.492;2;1.152;1.695;2;1.310;1.718"
Elmt.asymAl		= "2;1.564;2;1.195;1.690;2;1.288;1.717"
break
//---------------------------------------------------
case "Te":
Elmt.na			= "tellurium"
Elmt.sy			= "Te"
Elmt.nu			= "52"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p"
Elmt.be	 		= "928.5;812.8;161.9;595.9;121.4;17.1;52.0;8.6"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.22e-1;nan;4.758"
Elmt.picsMg 		= "0.66e-1;0.2750;0.17e-1;0.6720;0.57e-1;0.22e-2;0.76e-1;0.35e-2"
Elmt.picsAl 		= "0.51e-1;0.2099;0.12e-1;0.4205;0.42e-1;0.17e-2;0.49e-1;0.26e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;1.870"
Elmt.asymMg	= "2;1.460;2;1.131;1.697;2;1.317;1.718"
Elmt.asymAl		= "2;1.548;2;1.185;1.691;2;1.295;1.709"
break
//---------------------------------------------------
case "I":
Elmt.na			= "iodine"
Elmt.sy			= "I"
Elmt.nu			= "53"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p"
Elmt.be	 		= "986.8;867.5;177.2;643.7;134.7;19.4;61.5;10.0"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;7.985"
Elmt.picsMg 		= "0.68e-1;0.2847;0.18e-1;0.7359;0.62e-1;0.25e-2;0.87e-1;0.54e-2"
Elmt.picsAl 		= "0.53e-1;0.2186;0.13e-1;0.4620;0.45e-1;0.19e-2;0.56e-1;0.38e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;1.882"
Elmt.asymMg	= "2;1.421;2;1.106;1.697;2;1.322;1.722"
Elmt.asymAl		= "2;1.530;2;1.172;1.694;2;1.301;1.703"
break
//---------------------------------------------------
case "Xe":
Elmt.na			= "xenon"
Elmt.sy			= "Xe"
Elmt.nu			= "54"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p"
Elmt.be	 		= "1046.8;923.9;193.0;693.2;148.6;21.8;71.5;11.4"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;12.87"
Elmt.picsMg 		= "0.70e-1;0.2939;0.19e-1;0.8023;0.66e-1;0.29e-2;0.98e-1;0.76e-2"
Elmt.picsAl 		= "0.55e-1;0.2273;0.14e-1;0.5067;0.48e-1;0.22e-2;0.64e-1;0.51e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;1.882"
Elmt.asymMg	= "2;1.370;2;1.075;1.697;2;1.327;1.727"
Elmt.asymAl		= "2;1.507;2;1.157;1.696;2;1.313;1.695"
break
//---------------------------------------------------
case "Cs":
Elmt.na			= "cesium"
Elmt.sy			= "Cs"
Elmt.nu			= "55"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p"
Elmt.be	 		= "1114.3;987.3;214.9;750.2;168.5;28.8;87.6;17.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;18.31"
Elmt.picsMg 		= "0.73e-1;0.3037;0.20e-1;0.8717;0.71e-1;0.34e-2;0.1096;0.93e-2"
Elmt.picsAl 		= "0.56e-1;0.2357;0.15e-1;0.5539;0.52e-1;0.26e-2;0.72e-1;0.68e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;1.948"
Elmt.asymMg	= "2;1.302;2;1.037;1.697;2;1.328;1.718"
Elmt.asymAl		= "2;1.478;2;1.138;1.698;2;1.315;1.713"
break
//---------------------------------------------------
case "Ba":
Elmt.na			= "baryum"
Elmt.sy			= "Ba"
Elmt.nu			= "56"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p;6s"
Elmt.be	 		= "1184;1053.8;237.6;809.2;189.3;35.9;104.5;22.9;4.5"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;0.1072"
Elmt.picsMg 		= "0.75e-1;0.3128;0.22e-1;0.9444;0.75e-1;0.40e-2;0.1220;0.11e-1;0.30e-3"
Elmt.picsAl 		= "0.58e-1;0.2440;0.16e-1;0.6036;0.56e-1;0.29e-2;0.80e-1;0.81e-2;0.23e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;2"
Elmt.asymMg	= "2;1.208;2;0.9915;1.697;2;1.332;1.716;2"
Elmt.asymAl		= "2;1.442;2;1.115;1.699;2;1.321;1.714;2"
break
//---------------------------------------------------
case "La":
Elmt.na			= "lanthanum"
Elmt.sy			= "La"
Elmt.nu			= "57"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p;6s;5d"
Elmt.be	 		= "1252.7;1118.8;258.1;867.2;207.8;40.5;119.1;26.4;4.9;6.2"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;0.1191;4.085"
Elmt.picsMg 		= "nan;0.3210;0.23e-1;1.020;0.80e-1;0.45e-2;0.1343;0.12e-1;0.44e-3;0.11e-2"
Elmt.picsAl 		= "0.60e-1;0.2522;0.17e-1;0.6559;0.59e-1;0.33e-2;0.89e-1;0.93e-2;0.29e-3;0.76e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;2;0.8132"
Elmt.asymMg	= "2;1.066;2;0.9365;1.695;2;1.331;1.720;2;1.368"
Elmt.asymAl		= "2;1.397;2;1.087;1.700;2;1.326;1.716;2;1.368"
break
//---------------------------------------------------
case "Ce":
Elmt.na			= "cerium"
Elmt.sy			= "Ce"
Elmt.nu			= "58"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "1302.0;1164.5;261.2;906.2;209.2;38.4;117.3;24.3;4.6;10.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;0.1013;0.6255"
Elmt.picsMg 		= "nan;0.3288;0.24e-1;1.100;0.83e-1;0.43e-2;0.1416;0.12e-1;0.47e-3;0.39e-2"
Elmt.picsAl 		= "0.62e-1;0.2601;0.17e-1;0.7116;0.61e-1;0.31e-2;0.95e-1;0.89e-2;0.31e-3;0.22e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;2;1.457"
Elmt.asymMg	= "2;0.8867;2;0.8786;1.694;2;1.330;1.723;2;1.027"
Elmt.asymAl		= "2;1.347;2;1.056;1.700;2;1.329;1.718;2;1.021"
break
//---------------------------------------------------
case "Pr":
Elmt.na			= "praseodynium"
Elmt.sy			= "Pr"
Elmt.nu			= "59"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "1362.1;1220.9;272.7;955.7;219.0;39.6;123.5;25.0;4.6;11.0"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;0.98e-1;0.6811"
Elmt.picsMg 		= "nan;0.3317;0.25e-1;1.183;0.87e-1;0.45e-2;0.1518;0.12e-1;0.50e-3;0.71e-2"
Elmt.picsAl 		= "0.64e-1;0.2681;0.18e-1;0.7688;0.64e-1;0.33e-2;0.1028;0.93e-2;0.38e-3;0.40e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;2;1.656"
Elmt.asymMg	= "nan;0.4840;2;0.8073;1.692;2;1.329;1.726;2;1.033"
Elmt.asymAl		= "2;1.281;2;1.018;1.699;2;1.331;1.721;2;1.027"
break
//---------------------------------------------------
case "Nd":
Elmt.na			= "neodynium"
Elmt.sy			= "Nd"
Elmt.nu			= "60"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "1423;1278.2;284.3;1006.0;228.6;40.7;129.6;25.6;4.7;11.8"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;0.95e-1;0.6961"
Elmt.picsMg 		= "nan;nan;0.26e-1;1.269;0.90e-1;0.46e-2;0.1620;0.13e-1;0.41e-3;0.11e-1"
Elmt.picsAl 		= "0.65e-1;0.2757;0.19e-1;0.8284;0.67e-1;0.34e-2;0.1103;0.97e-2;0.34e-3;0.63e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;2;1.804"
Elmt.asymMg	= "nan;nan;2;0.7243;1.690;2;1.327;1.729;2;1.041"
Elmt.asymAl		= "2;1.192;2;0.9739;1.698;2;1.332;1.724;2;1.026"
break
//---------------------------------------------------
case "Pm":
Elmt.na			= "promethium"
Elmt.sy			= "Pm"
Elmt.nu			= "61"		
Elmt.cl			= "3s;3p;4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "1484.9;1336.3;295.8;1057.2;238.3;41.8;135.7;26.2;4.8;12.5"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;0.92e-1;0.7066"
Elmt.picsMg 		= "nan;nan;0.27e-1;1.359;0.94e-1;0.48e-2;0.1721;0.13e-1;0.36e-3;0.16e-1"
Elmt.picsAl 		= "0.68e-1;0.2826;0.20e-1;0.8900;0.70e-1;0.35e-2;0.1179;0.10e-1;0.28e-3;0.93e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;2;1.888"
Elmt.asymMg	= "nan;nan;2;0.6284;1.687;2;1.324;1.729;2;1.042"
Elmt.asymAl		= "2;1.064;2;0.9222;1.697;2;1.332;1.726;2;1.028"
break
//---------------------------------------------------
case "Sm":
Elmt.na			= "samarium"
Elmt.sy			= "Sm"
Elmt.nu			= "62"		
Elmt.cl			= "3p;4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "1395.4;307.3;1109.3;248.0;42.8;141.7;26.7;4.8;13.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;0.89e-1;0.7216"
Elmt.picsMg 		= "nan;0.28e-1;1.456;0.97e-1;0.50e-2;0.1831;0.14e-1;0.39e-3;0.23e-1"
Elmt.picsAl 		= "0.2886;0.21e-1;0.9547;0.73e-1;0.37e-2;0.1249;0.10e-1;0.29e-3;0.12e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;2;1.900"
Elmt.asymMg	= "nan;2;0.5246;1.684;2;1.324;1.730;2;1.048"
Elmt.asymAl		= "0.8628;2;0.8627;1.697;2;1.329;1.729;2;1.028"
break
//---------------------------------------------------
case "Eu":
Elmt.na			= "europium"
Elmt.sy			= "Eu"
Elmt.nu			= "63"		
Elmt.cl			= "3p;4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "1455.5;319;1162.4;257.7;43.9;147.8;27.3;4.9;13.7"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;0.85e-1;0.7484"
Elmt.picsMg 		= "nan;0.29e-1;1.562;0.1010;0.51e-2;0.1931;0.14e-1;0.48e-3;0.31e-1"
Elmt.picsAl 		= "0.2912;0.21e-1;1.022;0.75e-1;0.38e-2;0.1326;0.10e-1;0.35e-3;0.17e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;2;1.853"
Elmt.asymMg	= "nan;2;0.4351;1.681;2;1.320;1.731;2;1.054"
Elmt.asymAl		= "0.4382;2;0.7929;1.695;2;1.328;1.731;2;1.030"
break
//---------------------------------------------------
case "Gd":
Elmt.na			= "gadolinium"
Elmt.sy			= "Gd"
Elmt.nu			= "64"		
Elmt.cl			= "4s;3d;4p;5s;4d;5p;6s;5d;4f"
Elmt.be	 		= "339.5;1226.2;276.2;48.5;162.3;30.9;5.3;5.9;21.6"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;0.94e-1;4.708;nan"
Elmt.picsMg 		= "0.30e-1;1.478;0.1051;0.55e-2;0.2054;0.15e-1;0.67e-3;0.12e-2;0.38e-1"
Elmt.picsAl 		= "0.22e-1;1.091;0.79e-1;0.41e-2;0.1422;0.11e-1;0.43e-3;0.94e-3;0.21e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;2;0.6297;nan"
Elmt.asymMg	= "2;0.6229;1.677;2;1.315;1.729;2;1.348;1.056"
Elmt.asymAl		= "2;0.7064;1.694;2;1.328;1.731;2;1.388;1.039"
break
//---------------------------------------------------
case "Tb":
Elmt.na			= "terbium"
Elmt.sy			= "Tb"
Elmt.nu			= "65"		
Elmt.cl			= "4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "342.4;1271.3;277.2;45.9;159.9;28.3;4.9;14.6"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;0.79e-1;0.8355"
Elmt.picsMg 		= "0.30e-1;nan;0.1074;0.53e-2;0.2128;0.15e-1;0.49e-3;0.52e-1"
Elmt.picsAl 		= "0.23e-1;1.167;0.81e-1;0.40e-2;0.1482;0.11e-1;0.36e-3;0.29e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;2;1.655"
Elmt.asymMg	= "2;nan;1.673;2;1.309;1.720;2;1.053"
Elmt.asymAl		= "2;0.6212;1.691;2;1.325;1.733;2;1.034"
break
//---------------------------------------------------
case "Dy":
Elmt.na			= "dysprosium"
Elmt.sy			= "Dy"
Elmt.nu			= "66"		
Elmt.cl			= "4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "354.2;1327.3;287.1;46.9;165.9;28.8;5;14.9"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;0.75e-1;0.8948"
Elmt.picsMg 		= "0.31e-1;nan;0.1101;0.55e-2;0.2224;0.15e-1;0.44e-3;0.66e-1"
Elmt.picsAl 		= "0.24e-1;1.245;0.84e-1;0.41e-2;0.1560;0.11e-1;0.31e-3;0.37e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;2;1.535"
Elmt.asymMg	= "2;nan;1.668;2;1.303;1.728;2;1.054"
Elmt.asymAl		= "2;0.5232;1.690;2;1.323;1.733;2;1.038"
break
//---------------------------------------------------
case "Ho":
Elmt.na			= "holmium"
Elmt.sy			= "Ho"
Elmt.nu			= "67"		
Elmt.cl			= "4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "366.1;1384.2;297.1;47.9;172.1;29.3;5;15.3"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;0.72e-1;0.9610"
Elmt.picsMg 		= "0.32e-1;nan;0.1130;0.56e-2;0.2310;0.15e-1;0.39e-3;0.82e-1"
Elmt.picsAl 		= "0.24e-1;1.327;0.86e-1;0.42e-2;0.1646;0.11e-1;0.27e-3;0.46e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;2;1.420"
Elmt.asymMg	= "2;nan;1.663;2;1.294;1.726;2;1.055"
Elmt.asymAl		= "2;0.4407;1.687;2;1.322;1.732;2;1.042"
break
//---------------------------------------------------
case "Er":
Elmt.na			= "erbium"
Elmt.sy			= "Er"
Elmt.nu			= "68"		
Elmt.cl			= "4s;3d;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "378.2;1442.1;307.1;48.9;178.2;29.8;5.1;15.5"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;0.69e-1;1.034"
Elmt.picsMg 		= "0.33e-1;nan;0.1160;0.58e-2;0.2404;0.15e-1;0.41e-3;0.1007"
Elmt.picsAl 		= "0.25e-1;1.353;0.89e-1;0.43e-2;0.1721;0.11e-1;0.31e-3;0.56e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;2;1.311"
Elmt.asymMg	= "2;nan;1.658;2;1.287;1.723;2;1.055"
Elmt.asymAl		= "2;0.5227;1.684;2;1.318;1.730;2;1.045"
break
//---------------------------------------------------
case "Tm":
Elmt.na			= "thullium"
Elmt.sy			= "Tm"
Elmt.nu			= "69"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "390.3;317.2;49.9;184.4;30.3;5.1;15.8"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.66e-1;1.112"
Elmt.picsMg 		= "0.34e-1;0.1189;0.59e-2;0.2497;0.15e-1;0.47e-3;0.1217"
Elmt.picsAl 		= "0.26e-1;0.91e-1;0.44-2;0.1797;0.11e-1;0.37e-3;0.69e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;1.214"
Elmt.asymMg	= "2;1.653;2;1.280;1.721;2;1.053"
Elmt.asymAl		= "2;1.681;2;1.314;1.729;2;1.049"
break
//---------------------------------------------------
case "Yb":
Elmt.na			= "ytterbium"
Elmt.sy			= "Yb"
Elmt.nu			= "70"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f"
Elmt.be	 		= "402.6;327.5;50.9;190.7;30.8;5.2;16"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.63e-1;1.190"
Elmt.picsMg 		= "0.35e-1;0.1217;0.60e-2;0.2587;0.15e-1;0.53e-3;0.1449"
Elmt.picsAl 		= "0.26e-1;0.93e-1;0.45-2;0.1871;0.12e-1;0.39e-3;0.82e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;1.123"
Elmt.asymMg	= "2;1.647;2;1.271;1.718;2;1.046"
Elmt.asymAl		= "2;1.677;2;1.308;1.727;2;1.045"
break

//---------------------------------------------------
case "Lu":
Elmt.na			= "lutetium"
Elmt.sy			= "Lu"
Elmt.nu			= "71"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d"
Elmt.be	 		= "424.6;347.3;56.1;206.3;34.8;5.7;26.4;5.3"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.68e-1;nan;4.505"
Elmt.picsMg 		= "0.36e-1;0.1252;0.64e-2;0.2699;0.17e-1;0.56e-3;0.1670;0.12e-2"
Elmt.picsAl 		= "0.27e-1;0.96e-1;0.48-2;0.1960;0.13e-1;0.46e-3;0.96e-1;0.97e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;0.5317"
Elmt.asymMg	= "2;1.640;2;1.263;1.719;2;1.039;1.316"
Elmt.asymAl		= "2;1.673;2;1.303;1.727;2;1.043;1.376"
break
//---------------------------------------------------
case "Hf":
Elmt.na			= "hafnium"
Elmt.sy			= "Hf"
Elmt.nu			= "72"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d"
Elmt.be	 		= "447.3;367.8;61.1;225.5;38.6;6.0;33.8;6.7"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.70e-1;nan;11.05"
Elmt.picsMg 		= "0.37e-1;0.1289;0.69e-2;0.2819;0.18e-1;0.65e-3;0.1922;0.33e-2"
Elmt.picsAl 		= "0.28e-1;0.99e-1;0.52e-2;0.2049;0.14e-1;0.47e-3;0.1108;0.24e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;0.4461"
Elmt.asymMg	= "2;1.633;2;1.255;1.716;2;1.039;1.359"
Elmt.asymAl		= "2;1.668;2;1.295;1.726;2;1.040;1.371"
break
//---------------------------------------------------
case "Ta":
Elmt.na			= "tantalum"
Elmt.sy			= "Ta"
Elmt.nu			= "73"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d"
Elmt.be	 		= "470.7;389;66;239.3;42.4;6.3;43.5;8.0"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.69e-1;nan;18.44"
Elmt.picsMg 		= "0.38e-1;0.1326;0.74e-2;0.2930;0.20e-1;0.64e-3;0.2194;0.59e-2"
Elmt.picsAl 		= "0.29e-1;0.1026;0.55e-2;0.2145;0.15e-1;0.54e-3;0.1274;0.42e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;0.3480"
Elmt.asymMg	= "2;1.265;2;1.244;1.716;2;1.039;1.340"
Elmt.asymAl		= "2;1.663;2;1.290;1.727;2;1.043;1.358"
break
//---------------------------------------------------
case "W":
Elmt.na			= "tungsten"
Elmt.sy			= "W"
Elmt.nu			= "74"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d"
Elmt.be	 		= "494.8;410.9;70.9;256.8;46.1;6.6;53.9;9.3"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.65e-1;nan;24.83"
Elmt.picsMg 		= "0.39e-1;0.1363;0.78e-2;0.3045;0.22e-1;0.74e-3;0.2463;0.88e-2"
Elmt.picsAl 		= "0.30e-1;0.1057;0.59e-2;0.2243;0.16e-1;0.54e-3;0.1449;0.66e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;0.2389"
Elmt.asymMg	= "2;1.617;2;1.233;1.714;2;1.029;1.325"
Elmt.asymAl		= "2;1.658;2;1.283;1.726;2;1.045;1.349"
break
//---------------------------------------------------
case "Re":
Elmt.na			= "rhenium"
Elmt.sy			= "Re"
Elmt.nu			= "75"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d"
Elmt.be	 		= "519.5;433.3;75.8;274.8;49.8;6.8;64.8;10.6"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.62e-1;nan;28.08"
Elmt.picsMg 		= "0.40e-1;0.1401;0.83e-2;0.3151;0.23e-1;0.82e-3;0.2776;0.12e-1"
Elmt.picsAl 		= "0.31e-1;0.1090;0.63e-2;0.2346;0.17e-1;0.54e-3;0.1631;0.91e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;0.1178"
Elmt.asymMg	= "2;1.608;2;1.220;1.714;2;1.029;1.333"
Elmt.asymAl		= "2;1.652;2;1.278;1.727;2;1.041;1.342"
break
//---------------------------------------------------
case "Os":
Elmt.na			= "osmium"
Elmt.sy			= "Os"
Elmt.nu			= "76"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d"
Elmt.be	 		= "544.9;456.4;80.7;293.4;53.4;7.0;76.3;11.9"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.58e-1;nan;27.49"
Elmt.picsMg 		= "0.41e-1;0.1439;0.88e-2;0.3270;0.25e-1;0.77e-3;0.3095;0.17e-1"
Elmt.picsAl 		= "0.32e-1;0.1123;0.66e-2;0.2443;0.19e-1;0.59e-3;0.1833;0.12e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;-0.86e-2"
Elmt.asymMg	= "2;1.599;2;1.208;1.712;2;1.023;1.343"
Elmt.asymAl		= "2;1.646;2;1.270;1.725;2;1.040;1.339"
break
//---------------------------------------------------
case "Ir":
Elmt.na			= "iridium"
Elmt.sy			= "Ir"
Elmt.nu			= "77"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d"
Elmt.be	 		= "570.8;480.1;85.6;312.6;57.1;7.2;88.4;13.3"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.53e-1;nan;24.29"
Elmt.picsMg 		= "0.43e-1;0.1477;0.93e-2;0.3389;0.27e-1;0.86e-3;0.3438;0.22e-1"
Elmt.picsAl 		= "0.33e-1;0.1155;0.70e-2;0.2542;0.20e-1;0.58e-3;0.2043;0.16e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;-0.1215"
Elmt.asymMg	= "2;1.588;2;1.194;1.711;2;1.018;1.336"
Elmt.asymAl		= "2;1.640;2;1.261;1.727;2;1.037;1.345"
break
//---------------------------------------------------
case "Pt":
Elmt.na			= "platinum"
Elmt.sy			= "Pt"
Elmt.nu			= "78"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d"
Elmt.be	 		= "592.9;499.9;86.4;327.8;56.9;6.4;96.5;11.4"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.19e-1;nan;29.46"
Elmt.picsMg 		= "0.44e-1;0.1514;0.98e-2;0.3508;0.28e-1;0.34e-3;0.3792;0.29e-1"
Elmt.picsAl 		= "0.34e-1;0.1190;0.74e-2;0.2642;0.21e-1;0.28e-3;0.2270;0.21e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;-0.1833"
Elmt.asymMg	= "2;1.577;2;1.180;1.710;2;1.012;1.336"
Elmt.asymAl		= "2;1.633;2;1.251;1.726;2;1.035;1.342"
break
//---------------------------------------------------
case "Au":
Elmt.na			= "gold"
Elmt.sy			= "Au"
Elmt.nu			= "79"		
Elmt.cl			= "4s;4p;5s;4d;5p;4f;5d;6s"
Elmt.be	 		= "619.8;524.4;91.1;347.8;60.4;109.4;12.5;6.5"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;25.95;0.19e-1"
Elmt.picsMg 		= "0.45e-1;0.1554;0.10e-1;0.3629;0.30e-1;0.4176;0.35e-1;0.33e-3"
Elmt.picsAl 		= "0.35e-1;0.1223;0.77e-2;0.2739;0.22e-1;0.2511;0.26e-1;0.29e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;-0.3011;2"
Elmt.asymMg	= "2;1.565;2;1.165;1.708;1.006;1.330;2"
Elmt.asymAl		= "2;1.626;2;1.241;1.725;1.032;1.348;2"
break
//---------------------------------------------------
case "Hg":
Elmt.na			= "mercury"
Elmt.sy			= "Hg"
Elmt.nu			= "80"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d"
Elmt.be	 		= "652.1;554.4;100.3;373.2;68.2;7.7;127.7;17.3"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.39e-1;nan;13.25"
Elmt.picsMg 		= "0.46e-1;0.1593;0.10e-1;0.3747;0.31e-1;0.99e-3;0.4584;0.40e-1"
Elmt.picsAl 		= "0.36e-1;0.1256;0.83e-2;0.2843;0.24e-1;0.70e-3;0.2767;0.30e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;-0.28e-1"
Elmt.asymMg	= "2;1.551;2;1.149;1.706;2;0.9997;1.318"
Elmt.asymAl		= "2;1.618;2;1.231;1.723;2;1.028;1.357"
break
//---------------------------------------------------
case "Tl":
Elmt.na			= "thallium"
Elmt.sy			= "Tl"
Elmt.nu			= "81"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p"
Elmt.be	 		= "687.2;587.1;111.6;401.3;78.0;9.9;148.7;24.0;4.6"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.43e-1;nan;nan;0.4181"
Elmt.picsMg 		= "0.48e-1;0.1633;0.11e-1;0.3865;0.34e-1;0.12e-2;0.5008;0.47e-1;0.28e-3"
Elmt.picsAl 		= "0.37e-1;0.1288;0.87e-2;0.2949;0.26e-1;0.91e-3;0.3040;0.35e-1;0.21e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;nan;1.835"
Elmt.asymMg	= "2;1.536;2;1.131;1.703;2;0.9916;1.326;1.698"
Elmt.asymAl		= "2;1.608;2;1.219;1.724;2;1.024;1.356;1.707"
break
//---------------------------------------------------
case "Pb":
Elmt.na			= "lead"
Elmt.sy			= "Pb"
Elmt.nu			= "82"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p"
Elmt.be	 		= "723.1;620.6;122.9;430.1;87.9;12.1;170.3;30.7;5.8"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.40e-1;nan;nan;1.295"
Elmt.picsMg 		= "0.49e-1;0.1674;0.12e-1;0.3980;0.36e-1;0.14e-2;0.5452;0.52e-1;0.95e-3"
Elmt.picsAl 		= "0.38e-1;0.1322;0.92e-2;0.3052;0.27e-1;0.11e-2;0.3330;0.39e-1;0.61e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;nan;1.841"
Elmt.asymMg	= "2;1.519;2;1.111;1.702;2;0.9820;1.319;1.734"
Elmt.asymAl		= "2;1.598;2;1.206;1.722;2;1.020;1.352;1.718"
break
//---------------------------------------------------
case "Bi":
Elmt.na			= "bismuth"
Elmt.sy			= "Bi"
Elmt.nu			= "83"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p"
Elmt.be	 		= "759.8;654.9;134.3;459.7;97.9;14.2;192.8;37.6;7.0"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.27e-1;nan;nan;2.692"
Elmt.picsMg 		= "0.50e-1;0.1715;0.13e-1;0.4093;0.38e-1;0.17e-2;0.5929;0.58e-1;0.16e-2"
Elmt.picsAl 		= "0.39e-1;0.1357;0.97e-2;0.3157;0.29e-1;0.13e-2;0.3632;0.43e-1;0.14e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;nan;1.855"
Elmt.asymMg	= "2;1.501;2;1.089;1.701;2;0.9738;1.315;1.722"
Elmt.asymAl		= "2;1.587;2;1.193;1.720;2;1.014;1.351;1.747"
break
//---------------------------------------------------
case "Po":
Elmt.na			= "polonium"
Elmt.sy			= "Po"
Elmt.nu			= "84"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p"
Elmt.be	 		= "797.4;690.1;146.1;490.2;108.2;16.2;216.2;44.8;8.2"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;0.14e-1;nan;nan;4.721"
Elmt.picsMg 		= "0.52e-1;0.1756;0.13e-1;0.4201;0.41e-1;0.21e-2;0.6431;0.63e-1;0.29e-2"
Elmt.picsAl 		= "0.40e-1;0.1392;0.10e-1;0.3262;0.31e-1;0.14e-2;0.3952;0.48e-1;0.22e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;nan;1.869"
Elmt.asymMg	= "2;1.481;2;1.065;1.698;2;0.9650;1.312;1.727"
Elmt.asymAl		= "2;1.575;2;1.178;1.721;2;1.008;1.348;1.737"
break
//---------------------------------------------------
case "At":
Elmt.na			= "astatine"
Elmt.sy			= "At"
Elmt.nu			= "85"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p"
Elmt.be	 		= "835.9;726.2;158.1;521.6;118.7;18.3;240.5;52.2;9.4"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;7.513"
Elmt.picsMg 		= "0.53e-1;0.1798;0.14e-1;0.4311;0.43e-1;0.23e-2;0.6948;0.69e-1;0.41e-2"
Elmt.picsAl 		= "0.41e-1;0.1426;0.10e-1;0.3364;0.33e-1;0.16e-2;0.4292;0.52e-1;0.31e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;nan;1.881"
Elmt.asymMg	= "2;1.458;2;1.040;1.697;2;0.9539;1.310;1.716"
Elmt.asymAl		= "2;1.561;2;1.162;1.718;2;1.001;1.346;1.730"
break
//---------------------------------------------------
case "Rn":
Elmt.na			= "radon"
Elmt.sy			= "Rn"
Elmt.nu			= "86"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p"
Elmt.be	 		= "875.4;763.2;170.3;553.8;129.5;20.3;265.6;59.9;10.7"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;11.37"
Elmt.picsMg 		= "0.54e-1;0.1838;0.15e-1;0.4422;0.46e-1;0.24e-2;0.7477;0.75e-1;0.55e-2"
Elmt.picsAl 		= "0.42e-1;0.1462;0.11e-1;0.3466;0.35e-1;0.19e-2;0.4651;0.56e-1;0.41e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;2;nan;nan;1.889"
Elmt.asymMg	= "2;1.433;2;1.013;1.695;2;0.9410;1.310;1.721"
Elmt.asymAl		= "2;1.547;2;1.145;1.717;2;0.9952;1.344;1.732"
break
//---------------------------------------------------
case "Fr":
Elmt.na			= "francium"
Elmt.sy			= "Fr"
Elmt.nu			= "87"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p"
Elmt.be	 		= "920.7;806.1;187.8;592.0;145.4;26.6;296.6;72.8;15.7"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;15.27"
Elmt.picsMg 		= "0.55e-1;0.1877;0.16e-1;0.4530;0.48e-1;0.30e-2;0.8048;0.82e-1;0.73e-2"
Elmt.picsAl 		= "0.43e-1;0.1497;0.12e-1;0.3564;0.37e-1;0.22e-2;0.5021;0.61e-1;0.52e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;1.968"
Elmt.asymMg	= "2;1.403;2;0.9816;1.691;2;0.9283;1.304;1.728"
Elmt.asymAl		= "2;1.531;2;1.125;1.717;2;0.9864;1.348;1.729"
break
//---------------------------------------------------
case "Ra":
Elmt.na			= "radium"
Elmt.sy			= "Ra"
Elmt.nu			= "88"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s"
Elmt.be	 		= "967.1;850.1;205.7;631.1;161.8;32.7;328.6;86.0;20.8;4.2"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.84e-1"
Elmt.picsMg 		= "0.57e-1;0.1916;0.16e-1;0.4635;0.51e-1;0.34e-2;0.8640;0.88e-1;0.83e-2;0.35e-3"
Elmt.picsAl 		= "0.44e-1;0.1533;0.12e-1;0.3665;0.39e-1;0.24e-2;0.5410;0.66e-1;0.63e-2;0.18e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2"
Elmt.asymMg	= "2;1.368;2;0.9479;1.688;2;0.9145;1.301;1.717;2"
Elmt.asymAl		= "2;1.512;2;1.104;1.715;2;0.9769;1.344;1.741;2"
break
//---------------------------------------------------
case "Ac":
Elmt.na			= "actinium"
Elmt.sy			= "Ac"
Elmt.nu			= "89"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;6d"
Elmt.be	 		= "1012.1;892.6;221.5;668.9;176.1;36.7;359.2;97.2;23.8;4.6;5.8"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.97e-1;3.528"
Elmt.picsMg 		= "0.58e-1;0.1954;0.17e-1;0.4739;0.54e-1;0.37e-2;0.9260;0.94e-1;0.94e-2;0.46e-3;0.86e-3"
Elmt.picsAl 		= "0.45e-1;0.1570;0.13e-1;0.3766;0.41e-1;0.27e-2;0.5814;0.71e-1;0.72e-2;0.28e-3;0.72e-3"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;0.8359"
Elmt.asymMg	= "2;1.329;2;0.9112;1.687;2;0.8997;1.295;1.721;2;1.334"
Elmt.asymAl		= "2;1.492;2;1.081;1.712;2;0.9664;1.341;1.741;2;1.388"
break
//---------------------------------------------------
case "Th":
Elmt.na			= "thorium"
Elmt.sy			= "Th"
Elmt.nu			= "90"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;6d"
Elmt.be	 		= "1057.8;935.8;237.3;707.2;190.4;40.5;390.4;108.4;26.7;5.0;7.0"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.1008;8.896"
Elmt.picsMg 		= "0.59e-1;0.1993;0.18e-1;0.4843;0.57e-1;0.40e-2;0.9895;0.1012;0.10e-1;0.55e-3;0.22e-2"
Elmt.picsAl 		= "0.46e-1;0.1605;0.14e-1;0.3864;0.43e-1;0.29e-2;0.6243;0.76e-1;0.81e-2;0.37e-3;0.16e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;0.7955"
Elmt.asymMg	= "2;1.282;2;0.8708;1.683;2;0.8835;1.291;1.723;2;1.347"
Elmt.asymAl		= "2;1.469;2;1.056;1.711;2;0.9557;1.340;1.741;2;1.361"
break
//---------------------------------------------------
case "Pa":
Elmt.na			= "protactinium"
Elmt.sy			= "Pa"
Elmt.nu			= "91"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;6d;5f"
Elmt.be	 		= "1091.4;966.8;241.7;733.5;193.5;39.2;409.3;109.1;25.4;4.8;6.1;14.5"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.94e-1;3.872;0.6278"
Elmt.picsMg 		= "0.61e-1;0.2031;0.19e-1;0.4938;0.59e-1;0.40e-2;1.054;0.1051;0.10e-1;0.46e-3;0.93e-3;0.12e-1"
Elmt.picsAl 		= "0.47e-1;0.1639;0.14e-1;0.3961;0.45e-1;0.30e-2;0.6681;0.79e-1;0.78e-2;0.36e-3;0.75e-3;0.80e-2"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;0.8072;1.911"
Elmt.asymMg	= "2;1.235;2;0.8313;1.681;2;0.8675;1.287;1.718;2;1.317;1.052"
Elmt.asymAl		= "2;1.447;2;1.031;1.709;2;0.9451;1.338;1.739;2;1.390;1.087"
break
//---------------------------------------------------
case "U":
Elmt.na			= "uranium"
Elmt.sy			= "U"
Elmt.nu			= "92"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;6d;5f"
Elmt.be	 		= "1131.4;1004.3;251.6;766.1;202.0;40.3;434.7;114.8;26.1;4.8;6.1;16.1"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.92e-1;4.010;0.6423"
Elmt.picsMg 		= "0.62e-1;0.2069;0.20e-1;0.5029;0.61e-1;0.41e-2;1.121;0.1101;0.10e-1;0.43e-3;0.99e-3;0.20e-1"
Elmt.picsAl 		= "0.48e-1;0.1673;0.15e-1;0.4058;0.47e-1;0.30e-2;0.7138;0.83e-1;0.82e-2;0.35e-3;0.74e-3;0.13e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;0.7918;1.894"
Elmt.asymMg	= "2;1.1175;2;0.7855;1.677;2;0.8493;1.282;1.720;2;1.316;1.054"
Elmt.asymAl		= "2;1.420;2;1.003;1.708;2;0.9329;1.335;1.741;2;1.374;1.082"
break
//---------------------------------------------------
case "Np":
Elmt.na			= "neptunium"
Elmt.sy			= "Np"
Elmt.nu			= "93"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;6d;5f"
Elmt.be	 		= "1171.7;1042.1;261.3;799.0;210.3;41.4;460.4;120.4;26.7;4.9;6.2;17.6"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.90e-1;4.135;0.7113"
Elmt.picsMg 		= "0.63e-1;0.2106;0.21e-1;0.5116;0.63e-1;0.43e-2;1.188;0.1147;0.11e-1;0.40e-3;0.10e-2;0.29e-1"
Elmt.picsAl 		= "0.50e-1;0.1707;0.15e-1;0.4151;0.49e-1;0.32e-2;0.7617;0.87e-1;0.84e-2;0.32e-3;0.74e-3;0.19e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;0.7756;1.433"
Elmt.asymMg	= "2;1.102;2;0.7362;1.674;2;0.8299;1.277;1.718;2;1.320;1.050"
Elmt.asymAl		= "2;1.390;2;0.9738;1.706;2;0.9205;1.330;1.740;2;1.362;1.077"
break
//---------------------------------------------------
case "Pu":
Elmt.na			= "plutonium"
Elmt.sy			= "Pu"
Elmt.nu			= "94"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;5f"
Elmt.be	 		= "1205.8;1073.7;264.9;825.7;212.5;39.5;479.8;120.2;24.9;4.6;14.6"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.78e-1;1.473"
Elmt.picsMg 		= "0.65e-1;0.2140;0.21e-1;0.5196;0.66e-1;0.43e-2;1.259;0.1185;0.10e-1;0.32e-3;0.45e-1"
Elmt.picsAl 		= "0.51e-1;0.1739;0.16e-1;0.4247;0.50e-1;0.32e-2;0.8102;0.90e-1;0.80e-2;0.24e-3;0.30e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;1.877"
Elmt.asymMg	= "2;1.1022;2;0.6856;1.672;2;0.6105;1.272;1.714;2;1.040"
Elmt.asymAl		= "2;1.359;2;0.9334;1.705;2;0.9069;1.327;1.736;2;1.084"
break
//---------------------------------------------------
case "Am":
Elmt.na			= "xx"
Elmt.sy			= "Am"
Elmt.nu			= "95"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;5f"
Elmt.be	 		= "1246.6;1112.0;274.2;859.1;220.5;40.4;505.9;125.5;25.4;4.6;15.9"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.76e-1;1.448"
Elmt.picsMg 		= "0.65e-1;0.2174;0.22e-1;0.5275;0.68e-1;0.44e-2;1.331;0.1232;0.11e-1;0.33e-3;0.58e-1"
Elmt.picsAl 		= "0.52e-1;0.1772;0.16e-1;0.4234;0.52e-1;0.33e-2;0.8606;0.94e-1;0.82e-2;0.23e-3;0.39e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;1.696"
Elmt.asymMg	= "2;0.9081;2;0.6268;1.668;2;0.7887;1.266;1.713;2;1.044"
Elmt.asymAl		= "2;1.320;2;0.9084;1.702;2;0.8917;1.323;1.735;2;1.082"
break
//---------------------------------------------------
case "Cm":
Elmt.na			= "xx"
Elmt.sy			= "Cm"
Elmt.nu			= "96"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;6d;5f"
Elmt.be	 		= "1294.7;1157.5;289.9;899.6;234.7;44.2;539.2;136.7;28.4;5.0;6.2;22.0"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.85e-1;4.427;nan"
Elmt.picsMg 		= "nan;0.2208;0.23e-1;0.5352;0.71e-1;0.48e-2;1.405;0.1292;0.11e-1;0.45e-3;0.11e-2;0.65e-1"
Elmt.picsAl 		= "0.53e-1;0.1805;0.17e-1;0.4420;0.54e-1;0.36e-2;0.9125;0.99e-1;0.91e-2;0.31e-3;0.80e-3;0.44e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;0.7273;nan"
Elmt.asymMg	= "2;0.7301;2;0.5582;1.665;2;0.7647;1.258;1.714;2;1.332;1.032"
Elmt.asymAl		= "2;1.272;2;0.8688;1.699;2;0.8749;1.322;1.736;2;1.346;1.070"
break
//---------------------------------------------------
case "Bk":
Elmt.na			= "xx"
Elmt.sy			= "Bk"
Elmt.nu			= "97"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;6d;5f"
Elmt.be	 		= "1336.5;1196.7;299.3;933.9;242.7;45.1;566.2;142.1;28.9;5.0;6.2;23.4"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.84e-1;4.498;nan"
Elmt.picsMg 		= "nan;0.2241;0.24e-1;0.5425;0.73e-1;0.49e-2;1.481;0.1338;0.12e-1;0.49e-3;0.11e-2;0.80e-1"
Elmt.picsAl 		= "0.54e-1;0.1837;0.18e-1;0.4502;0.56e-1;0.36e-2;0.9663;0.1034;0.93e-2;0.34e-3;0.83e-3;0.54e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;0.7118;nan"
Elmt.asymMg	= "2;0.4745;2;0.4869;1.661;2;0.7402;1.252;1.712;2;1.332;1.030"
Elmt.asymAl		= "2;1.220;2;0.8273;1.697;2;0.8574;1.317;1.735;2;1.351;1.070"
break
//---------------------------------------------------
case "Cf":
Elmt.na			= "xx"
Elmt.sy			= "Cf"
Elmt.nu			= "98"		
Elmt.cl			= "4s;4p;5s;4d;5p;6s;4f;5d;6p;7s;6d;5f"
Elmt.be	 		= "1378.6;1236.3;308.8;968.6;250.8;46.0;593.5;147.4;29.4;5.1;6.1;24.7"
Elmt.picsHeI 	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;0.82e-1;4.555;nan"
Elmt.picsMg 		= "nan;0.2285;0.24e-1;0.5495;0.76e-1;0.50e-2;1.558;0.1364;0.12e-1;0.50e-3;0.10e-2;0.97e-1"
Elmt.picsAl 		= "0.55e-1;0.1869;0.16e-1;0.4583;0.58e-1;0.37e-2;1.021;0.1071;0.95e-2;0.36e-3;0.86e-3;0.66e-1"
Elmt.asymHeI	= "nan;nan;nan;nan;nan;nan;nan;nan;nan;2;0.6971;nan"
Elmt.asymMg	= "2;-0.20e-1;2;0.4094;1.657;2;0.7147;1.246;1.711;2;1.328;1.027"
Elmt.asymAl		= "2;1.157;2;0.7829;1.694;2;0.8393;1.313;1.734;2;1.360;1.069"
break
//---------------------------------------------------
default:
Elmt.na			= "Xxx"			
Elmt.sy			= "X"
Elmt.nu			= "0"		
Elmt.cl			= "xx"
Elmt.be	 		= "nan"
Elmt.picsHeI 	= "nan"
Elmt.picsMg 		= "nan"
Elmt.picsAl 		= "nan"
Elmt.asymHeI	= "nan"
Elmt.asymMg	= "nan"
Elmt.asymAl		= "nan"

endswitch

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// check database 
Function Check_I4PDatabaseYL()

//----list of chemical elements
variable maxelem = 104
make/T/o/N=(maxelem) ElementSym
ElementSym[0]= {"","H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y"}
ElementSym[40]= {"Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir"}
ElementSym[78]= {"Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs"}

//----list of chemical elements
variable kk, checkphi
STRUCT I4PElementYL Element

//---loop over elements and check
variable nit, nn
string cl
variable asy, be, item 
for(kk=1;kk<maxelem;kk+=1)
	checkphi = I4PDatabaseYL(ElementSym[kk],Element)
	if(cmpstr(ElementSym[kk],Element.sy)!=0 &&  checkphi!=0)
		print "ERROR Symbol : " + ElementSym[kk] + "  " + Element.sy
	endif
	if(kk!=str2num(Element.nu) &&  checkphi!=0)
		print "ERROR : " + num2str(kk) + "  " + Element.nu
	endif
	nit = 	itemsinlist(Element.be,";")
	if(  nit!= itemsinlist(Element.cl,";") &&  checkphi!=0)
		print "ERROR : CL/BE" + "  " + Element.sy
	endif	
	if(  nit!= itemsinlist(Element.picsHeI,";") &&  checkphi!=0)
		print "ERROR : PICS He/BE" + "  " + Element.sy
	endif		
	if(  nit!= itemsinlist(Element.picsMg,";") &&  checkphi!=0)
		print "ERROR : PICS Mg/BE" + "  " + Element.sy
	endif	
	if(  nit!= itemsinlist(Element.picsAl,";") &&  checkphi!=0)
		print "ERROR : PICS Al/BE" + "  " + Element.sy
	endif	
	if(  nit!= itemsinlist(Element.asymHeI,";") &&  checkphi!=0)
		print "ERROR : ASYM He/BE" + "  " + Element.sy
	endif	
	if(  nit!= itemsinlist(Element.asymMg,";") &&  checkphi!=0)
		print "ERROR : ASYM Mg/BE" + "  " + Element.sy
	endif	
	if(  nit!= itemsinlist(Element.asymAl,";") &&  checkphi!=0)
		print "ERROR : ASYM Al/BE" + "  " + Element.sy
	endif	
	for(nn=0;nn<nit;nn+=1)
		cl = stringfromlist(nn,Element.cl,";")
		if(cmpstr(cl[1],"s")==0)
			be   =str2num(stringfromlist(nn,Element.be,";"))
			asy = str2num(stringfromlist(nn,Element.asymHeI,";"))
			if(asy!=2 && be<=ghv_HeI)
				print "ERROR : ASYM He/s" + "  " + Element.sy
			endif
			asy = str2num(stringfromlist(nn,Element.asymMg,";"))
			if(asy!=2 && be<=ghv_Mg)
				print "ERROR : ASYM Mg/s" + "  " + Element.sy
			endif
			asy = str2num(stringfromlist(nn,Element.asymAl,";"))
			if(asy!=2 && be<=ghv_Al)
				print "ERROR : ASYM Al/s" + "  " + Element.sy
			endif				
		endif
	endfor
	for(nn=0;nn<nit;nn+=1)
		be   =  str2num(stringfromlist(nn,Element.be,";"))
		item =  str2num(stringfromlist(nn,Element.picsHeI,";"))
		if(numtype(item)!=2 && be>ghv_HeI)
			print "ERROR : PICS He/be" + "  " + Element.sy
		endif
		item =  str2num(stringfromlist(nn,Element.picsMg,";"))
		if(numtype(item)!=2 && be>ghv_Mg)
			print "ERROR : PICS Mg/be" + "  " + Element.sy
		endif
		item =  str2num(stringfromlist(nn,Element.picsAl,";"))		
		if(numtype(item)!=2 && be>ghv_Al)
			print "ERROR : PICS Al/be" + "  " + Element.sy
		endif			
	endfor
	
	//if(checkphi==0)
	//	print ElementSym[kk] + "    not available"
	//endif		
endfor

//---clean 
killwaves /z ElementSym

End