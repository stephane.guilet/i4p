// Module : quantification_ss
// Author : Remi Lazzari; lazzari@insp.jussieu.fr
// Date : February  3th, 2020
// Usage : quantification from damping of substrate line in a film


#pragma rtGlobals = 3		
#pragma ModuleName = quantification_ss


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start the Quantification panel
Static Function QuantificationStart_SS() : Panel

if (datafolderexists("root:I4P:I4Pquantification_ss")==0)
	QuantificationInit_SS()
	DisplayPanelQuantification_SS()
else
	// bring an existing panel forward
	if (strlen(WinList("QuantificationPanel_SS", ";", "WIN:64"))!=0)
		DoWindow/F QuantificationPanel_SS
	else
		DisplayPanelQuantification_SS()
	endif

endif
	
End

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialize the Quantification panel
Static Function QuantificationInit_SS()

string cdf = GetDataFolder(1)

newdatafolder /o root:I4P
newdatafolder /o/s  root:I4P:I4Pquantification_ss
 
/// PEAK 1 (The index a in the variables means adsorbate=film) 
//// Integrated intensities (Arb. units) = peak 1 of substrate
variable /G I1 = 1000
//// Transmission function  (Arb. units) = peak 1 of substrate
variable /G T1 = 1
//// Photoionization cross section  (Arb. units) of peak 1 
variable /G S1 = 1
//// SEP of peak 1 = substrate@substrate 
variable /G SEP1ss = 0	
//// SEP of peak 1 = substrate@layer
variable /G SEP1sl = 0	
//// SEP of peak 1 = substrate@film
variable /G SEP1sa = 0		
//// Effective attenuation length (A) = peak 1 of substrate in substrate
variable /G EALb1s = 20	
//// Effective attenuation length (A) = peak 1 of substrate in film
variable /G EALf1a = 20	
		
/// PEAK 2
//// Integrated intensities (Arb. units) = peak 2 of substrate
variable /G I2 = 2000
//// Transmission function  (Arb. units) = peak 2 of substrate
variable /G T2 =  1
//// Photoionization cros	s section  (Arb. units) of peak 2 
variable /G S2 = 1
//// SEP of peak 2 = substrate@substrate 
variable /G SEP2ss = 0	
//// SEP of peak 2 = substrate@layer
variable /G SEP2sl = 0	
//// SEP of peak 2 = substrate@film
variable /G SEP2sa = 0		
//// Effective attenuation length (A) = peak 2 of substrate  in substrate
variable /G EALb2s = 30
//// Effective attenuation length (A) = peak 2 of substrate in film
variable /G EALf2a = 30

// CALCULATION PARAMETERS	
/// Maximum thickness (A)
variable /G tmax = 100
/// Precision in dichotomy (A)
variable /G dtmin = 0.05	

/// GEOMETRY	
/// Sample normal/analyzer angle (deg)
variable /G Alpha = 0.
/// Coverage (be carefull if Alpha/=0 then Cov=1)
variable /G Cov = 0.5
// Thickness of the overlayer (A)
variable /G tlayer = 0
//// Effective attenuation length (A) = peak 1 of substrate in overlayer
variable /G EALf1l = 10
//// Effective attenuation length (A) = peak 2 of substrate in overlayer
variable /G EALf2l = 10	
		
/// OUTPUTS	
///  Film thickness in a continuous film model (A)
variable /G tfilm = nan
/// Film thickness in a pancake model (A) 
variable /G tpancake = nan
/// Heigth of a pancake (A)
variable /G hpancake = nan
/// Film thickness in a hemisphere model (A)
variable /G themisphere = nan
/// Radius or heigth in a hemisphere model (A)
variable /G rhemisphere = nan

// CALCULATION PARAMATERS
/// Projected EAL (�)
variable /G pEALb1s, pEALb2s, pEALf1a, pEALf2a, pEALf1l, pEALf2l

setdatafolder cdf
	
return 0

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Create Quantification panel 
Static Function  DisplayPanelQuantification_SS()

//--size and position of the panel
variable top = 10
variable left =	 10
variable bw = 100
variable bh =	20
variable pw = 90
variable dfs  = 16
variable ptop, pleft
variable x1, y1, x2, y2
//--background color
variable CbackR = 49152
variable CbackG = 49152
variable CbackB = 49152
//--value background color
variable CvalbackR = 65535
variable CvalbackG = 65535
variable CvalbackB = 65535
//--font color (input)
variable CfontR_in = 0
variable CfontG_in = 0
variable CfontB_in = 0
//--font color (output)
variable CfontR_out = 39168
variable CfontG_out = 0
variable CfontB_out = 15616
//--format
string fmtstr = "%7.3f"

//--create the panel
NewPanel/K=1/W=(top,left,top+12.75*bw,left+22*bh)
ModifyPanel cbRGB=(CbackR,CbackG,CbackB), fixedSize=1
DoWindow/C/T QuantificationPanel_SS,"I4P : QUANTIFICATION FROM SUBSTRATE LINE DAMPING"
DefaultGUIControls/W=QuantificationPanel_SS native


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PEAK 1
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft = left+bw
ptop = top

NVAR gI1 = root:I4P:I4Pquantification_ss:I1
Setvariable Quant_I1,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Area of peak 1",limits={0,inf,1}
Setvariable Quant_I1,help={"Enter the area of peak 1 (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_I1,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gI1

ptop += 1.5*bh
NVAR gT1 = root:I4P:I4Pquantification_ss:T1
Setvariable Quant_T1,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Transmission function of peak 1",limits={0,inf,0.001}
Setvariable Quant_T1,help={"Enter the transmission function of peak 1 (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_T1,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gT1

ptop += 1.5*bh
NVAR gS1 = root:I4P:I4Pquantification_ss:S1
Setvariable Quant_S1,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Photo-ionization cross-section of peak 1",limits={0,inf,0.001}
Setvariable Quant_S1,help={"Enter the photo-ionization cross-section of peak 1 (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_S1,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gS1

ptop += 1.5*bh
NVAR gSEP1ss = root:I4P:I4Pquantification_ss:SEP1ss
Setvariable Quant_SEP1ss,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title=" SEP peak 1@substrate",limits={0,inf,0.001}
Setvariable Quant_SEP1ss,help={"Enter the surface excitation parameter of the peak 1 at substrate surface."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEP1ss,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEP1ss

ptop += 1.5*bh
NVAR gSEP1sl = root:I4P:I4Pquantification_ss:SEP1sl
Setvariable Quant_SEP1sl,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title=" SEP peak 1@layer",limits={0,inf,0.001}
Setvariable Quant_SEP1sl,help={"Enter the surface excitation parameter of the peak 1 at layer surface."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEP1sl,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEP1sl

ptop += 1.5*bh
NVAR gSEP1sa = root:I4P:I4Pquantification_ss:SEP1sa
Setvariable Quant_SEP1sa,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title=" SEP peak 1@film",limits={0,inf,0.001}
Setvariable Quant_SEP1sa,help={"Enter the surface excitation parameter of the peak 1 at film surface."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEP1sa,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEP1sa

ptop += 1.5*bh
NVAR gEALb1s = root:I4P:I4Pquantification_ss:EALb1s
Setvariable Quant_EALb1s,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="EAL(bulk) (�) peak1@substrate",limits={0,inf,0.01}
Setvariable Quant_EALb1s,help={"Enter the bulk effective attenuation length of peak 1 in the substrate material (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALb1s,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALb1s

ptop += 1.5*bh
NVAR gEALf1a = root:I4P:I4Pquantification_ss:EALf1a
Setvariable Quant_EALf1a,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="EAL(film) (�) peak1@film",limits={0,inf,0.01}
Setvariable Quant_EALf1a,help={"Enter the practical (film) effective attenuation length of peak 1 in the film material (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALf1a,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALf1a

SetDrawEnv linethick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 = top+5
y1= left-5
x2 =  top+4.2*bw
y2 = left+ 12*bh
DrawRRect  x1, y1, x2, y2


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// PARAMETERS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft =  left+bw
ptop = ptop + 5*bh

NVAR gtmax = root:I4P:I4Pquantification_ss:tmax	
Setvariable Quant_tmax,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Maximum thickness (�)",limits={0,inf,1.}
Setvariable Quant_tmax,help={"Enter the maximum thickness (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in)
Setvariable Quant_tmax,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gtmax

ptop += 1.5*bh
NVAR gdtmin = root:I4P:I4Pquantification_ss:dtmin
Setvariable Quant_dtmin,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Dichotomy accuracy (�)",limits={0.01,inf,0.01}
Setvariable Quant_dtmin,help={"Enter the accuracy of dichotomy (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in)
Setvariable Quant_dtmin,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gdtmin

SetDrawEnv linethick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 = top+5 + bw
y1= left-5 + 15.2*bh
x2 =  top+4.2*bw
y2 = left+ 18.7*bh
DrawRRect  x1, y1, x2, y2


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PEAK 2
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft = left+5.5*bw
ptop = top

NVAR gI2 = root:I4P:I4Pquantification_ss:I2
Setvariable Quant_I2,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Area of peak 2",limits={0,inf,1}
Setvariable Quant_I2,help={"Enter the area of peak 2 (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_I2,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gI2

ptop += 1.5*bh
NVAR gT2 = root:I4P:I4Pquantification_ss:T2
Setvariable Quant_T2,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Transmission function of peak 2",limits={0,inf,0.001}
Setvariable Quant_T2,help={"Enter the transmission function of peak 2 (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_T2,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gT2

ptop += 1.5*bh
NVAR gS2 = root:I4P:I4Pquantification_ss:S2
Setvariable Quant_S2,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Photo-ionization cross-section of peak 2",limits={0,inf,0.001}
Setvariable Quant_S2,help={"Enter the photo-ionization cross-section of peak 2 (arb. units)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_S2,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gS2

ptop += 1.5*bh
NVAR gSEP2ss = root:I4P:I4Pquantification_ss:SEP2ss
Setvariable Quant_SEP2ss,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title=" SEP peak 2@substrate",limits={0,inf,0.001}
Setvariable Quant_SEP2ss,help={"Enter the surface excitation parameter of the peak 2 at substrate surface."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEP2ss,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEP2ss

ptop += 1.5*bh
NVAR gSEP2sl = root:I4P:I4Pquantification_ss:SEP2sl
Setvariable Quant_SEP2sl,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title=" SEP peak 2@layer",limits={0,inf,0.001}
Setvariable Quant_SEP2sl,help={"Enter the surface excitation parameter of the peak 2 at layer surface."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEP2sl,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEP2sl

ptop += 1.5*bh
NVAR gSEP2sa = root:I4P:I4Pquantification_ss:SEP2sa
Setvariable Quant_SEP2sa,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title=" SEP peak 2@film",limits={0,inf,0.001}
Setvariable Quant_SEP2sa,help={"Enter the surface excitation parameter of the peak 2 at film surface."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_SEP2sa,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gSEP2sa

ptop += 1.5*bh
NVAR gEALb2s = root:I4P:I4Pquantification_ss:EALb2s
Setvariable Quant_EALb2s,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="EAL(bulk) (�) peak2@substrate",limits={0,inf,0.01}
Setvariable Quant_EALb2s,help={"Enter the bulk effective attenuation length of peak 2 in the substrate material (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALb2s,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALb2s

ptop += 1.5*bh
NVAR gEALf2a = root:I4P:I4Pquantification_ss:EALf2a
Setvariable Quant_EALf2a,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="EAL(film) (�) peak2@film",limits={0,inf,0.01}
Setvariable Quant_EALf2a,help={"Enter the practical (film) effective attenuation length of peak 2 in the film material (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALf2a,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALf2a


SetDrawEnv linethick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 = top+5 + 4.5*bw
y1= left-5
x2 =  top+4.2*bw + 4.5*bw
y2 = left+ 12*bh
DrawRRect  x1, y1, x2, y2


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// GEOMETRY
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft = left+5.5*bw
ptop = ptop + 3*bh

NVAR gAlpha = root:I4P:I4Pquantification_ss:Alpha	
Setvariable Quant_Alpha,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Sample normal / analyzer angle (deg)",limits={0,90,0.1}
Setvariable Quant_Alpha,help={"Enter the angle between sample normal angle and analyzer (deg)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Alpha,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gAlpha

ptop += 1.5*bh
NVAR gCov = root:I4P:I4Pquantification_ss:Cov	
Setvariable Quant_Cov,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Coverage",limits={0,1,0.01}
Setvariable Quant_Cov,help={"Enter the film partial coverage [0,1]."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_Cov,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gCov

ptop += 1.5*bh
NVAR gtlayer = root:I4P:I4Pquantification_ss:tlayer	
Setvariable Quant_tlayer,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="Substrate overlayer thickness (�)",limits={0,inf,0.01}
Setvariable Quant_tlayer,help={"Enter the substrate overlayer thickness (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_tlayer,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gtlayer

ptop += 1.5*bh
NVAR gEALf1l = root:I4P:I4Pquantification_ss:EALf1l
Setvariable Quant_EALf1l,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="EAL(film) (�) peak 1@layer",limits={0,inf,0.01}
Setvariable Quant_EALf1l,help={"Enter the practical (film) effective attenuation length of peak 1 in the overlayer material (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALf1l,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALf1l

ptop += 1.5*bh
NVAR gEALf2l = root:I4P:I4Pquantification_ss:EALf2l
Setvariable Quant_EALf2l,pos={pleft,ptop},size={3*bw,bh},proc=SetVarQuantPanel_SS,title="EAL(film) (�) peak 2@layer",limits={0,inf,0.01}
Setvariable Quant_EALf2l,help={"Enter the practical (film) effective attenuation length of peak 2 in the overlayer material (�)."},fSize=dfs,fColor=(CfontR_in,CfontG_in,CfontB_in), format=fmtstr
Setvariable Quant_EALf2l,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, value=gEALf2l


SetDrawEnv linethick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 = top+5 + 4.5*bw
y1= left-5  + 13.2*bh
x2 =  top+4.2*bw + 4.5*bw
y2 = left+ 21*bh
DrawRRect  x1, y1, x2, y2


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// OUTPUTS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pleft = left+9.5*bw
ptop = top + 7*bh

NVAR gtfilm = root:I4P:I4Pquantification_ss:tfilm
Setvariable Quant_tfilm,pos={pleft,ptop},size={3*bw,bh},noproc,title="Continuous film thickness (�)",limits={0,inf,0}
Setvariable Quant_tfilm,help={"Calculated continuous films thickness (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_tfilm,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0,  noedit = 1, value=gtfilm

ptop += 1.5*bh
NVAR gtpancake = root:I4P:I4Pquantification_ss:tpancake
Setvariable Quant_tpancake,pos={pleft,ptop},size={3*bw,bh},noproc,title="Pancake film thickness (�)",limits={0,inf,0}
Setvariable Quant_tpancake,help={"Calculated pancake films thickness (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_tpancake,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, noedit = 1, value=gtpancake

ptop += 1.5*bh
NVAR ghpancake = root:I4P:I4Pquantification_ss:hpancake
Setvariable Quant_hpancake,pos={pleft,ptop},size={3*bw,bh},noproc,title="Pancake heigth (�)",limits={0,inf,0}
Setvariable Quant_hpancake,help={"Calculated pancake heigth (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_hpancake,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, noedit = 1, value=ghpancake

ptop += 1.5*bh
NVAR gthemisphere = root:I4P:I4Pquantification_ss:themisphere
Setvariable Quant_themisphere,pos={pleft,ptop},size={3*bw,bh},noproc,title="Hemisphere film thickness (�)",limits={0,inf,0}
Setvariable Quant_themisphere,help={"Calculated hemisphere film thickness (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_themisphere,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0, noedit = 1, value=gthemisphere

ptop += 1.5*bh
NVAR grhemisphere = root:I4P:I4Pquantification_ss:rhemisphere
Setvariable Quant_rhemisphere,pos={pleft,ptop},size={3*bw,bh},noproc,title="Hemisphere radius (�)",limits={0,inf,0}
Setvariable Quant_rhemisphere,help={"Calculated hemisphere radius (�)."},fSize=dfs,fColor=(CfontR_out,CfontG_out,CfontB_out), fstyle=1, format=fmtstr
Setvariable Quant_rhemisphere,bodywidth=pw, valueBackColor=(CvalbackR,CvalbackG,CvalbackB), valueColor=(0,0,0), frame=1, disable=0,  noedit = 1, value=grhemisphere


End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Set variables for quantification value
Function SetVarQuantPanel_SS(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

CalculateQuantPanel_SS()

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculation from panel parameters
Static Function CalculateQuantPanel_SS()

NVAR gI1 = root:I4P:I4Pquantification_ss:I1
NVAR gT1 = root:I4P:I4Pquantification_ss:T1
NVAR gS1 = root:I4P:I4Pquantification_ss:S1
NVAR gSEP1ss = root:I4P:I4Pquantification_ss:SEP1ss
NVAR gSEP1sl = root:I4P:I4Pquantification_ss:SEP1sl
NVAR gSEP1sa = root:I4P:I4Pquantification_ss:SEP1sa
NVAR gEALb1s = root:I4P:I4Pquantification_ss:EALb1s
NVAR gEALb2s = root:I4P:I4Pquantification_ss:EALb2s

NVAR gI2 = root:I4P:I4Pquantification_ss:I2
NVAR gT2 = root:I4P:I4Pquantification_ss:T2
NVAR gS2 = root:I4P:I4Pquantification_ss:S2
NVAR gSEP2ss = root:I4P:I4Pquantification_ss:SEP2ss
NVAR gSEP2sl = root:I4P:I4Pquantification_ss:SEP2sl
NVAR gSEP2sa = root:I4P:I4Pquantification_ss:SEP2sa
NVAR gEALf1a = root:I4P:I4Pquantification_ss:EALf1a
NVAR gEALf2a = root:I4P:I4Pquantification_ss:EALf2a

NVAR gAlpha = root:I4P:I4Pquantification_ss:Alpha	
NVAR gCov = root:I4P:I4Pquantification_ss:Cov		
NVAR gtlayer = root:I4P:I4Pquantification_ss:tlayer	
NVAR gEALf1l = root:I4P:I4Pquantification_ss:EALf1l
NVAR gEALf2l = root:I4P:I4Pquantification_ss:EALf2l

NVAR gtfilm = root:I4P:I4Pquantification_ss:tfilm
NVAR gtpancake = root:I4P:I4Pquantification_ss:tpancake
NVAR ghpancake = root:I4P:I4Pquantification_ss:hpancake
NVAR gthemisphere = root:I4P:I4Pquantification_ss:themisphere
NVAR grhemisphere = root:I4P:I4Pquantification_ss:rhemisphere

NVAR gtmax = root:I4P:I4Pquantification_ss:tmax
NVAR gdtmin = root:I4P:I4Pquantification_ss:dtmin

NVAR pEALb1s = root:I4P:I4Pquantification_ss:pEALb1s
NVAR pEALb2s = root:I4P:I4Pquantification_ss:pEALb2s
NVAR pEALf1a = root:I4P:I4Pquantification_ss:pEALf1a
NVAR pEALf2a = root:I4P:I4Pquantification_ss:pEALf2a
NVAR pEALf1l = root:I4P:I4Pquantification_ss:pEALf1l
NVAR pEALf2l = root:I4P:I4Pquantification_ss:pEALf2l

// Projected mean free path
Variable CosAlpha
CosAlpha = cos(gAlpha/180*pi)
pEALb1s = gEALb1s*CosAlpha
pEALb2s = gEALb2s*CosAlpha
pEALf1a = gEALf1a*CosAlpha
pEALf2a = gEALf2a*CosAlpha
pEALf1l = gEALf1l*CosAlpha
pEALf2l = gEALf2l*CosAlpha

// Corrected ratio of intensities to seek by dichotomy
Variable Ratio
if(gtlayer!=0)
	Ratio =  gI2*exp(gSEP2ss+gSEP2sl) / (gI1*exp(gSEP1ss+gSEP1sl))
else
	Ratio =  gI2*exp(gSEP2ss) / (gI1*exp(gSEP1ss))
	gSEP1sl = gSEP1ss
	gSEP2sl = gSEP2ss	
endif
Ratio *=   gT1*gS1*pEALb1s* Exp(-gtlayer/pEALf1l)  / (gT2*gS2*pEALb2s* Exp(-gtlayer/pEALf2l)) 
// Calculate the thickness by dichotomy within the different models
Variable Ratio_film
Ratio_film = Ratio*exp(gSEP2sa)/exp(gSEP1sa)
gtfilm  = pEALf1a*pEALf2a/(pEALf2a-pEALf1a) * ln(abs(Ratio_film))
if(gtfilm<0)
	gtfilm = nan
endif
// Partial coverage calculation works only in non-normal emission
if(gAlpha==0) 
	gtpancake      = I4PThickness_Dichotomy_SS(Ratio,gtmax,gdtmin,"pancake") 
	gthemisphere = I4PThickness_Dichotomy_SS(Ratio,gtmax,gdtmin,"hemisphere") 
	ghpancake 	 = gtpancake/gCov
	grhemisphere = 1.5*gthemisphere/gCov
else
	gtpancake      = nan
	gthemisphere = nan
	ghpancake 	= nan
	grhemisphere = nan
endif

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4PThickness_Dichotomy_SS(Ratio,ttmax,dttmin,model) 

Variable Ratio,ttmax, dttmin
String model

Variable tt1, tt2, ttmid, dtt
Variable ratio1, ratio2, ratiomid

tt1 = dttmin
tt2 = ttmax
dtt = tt2-tt1

ratio1  = I4P_Ratio_SS(model,tt1) - Ratio
ratio2  = I4P_Ratio_SS(model,tt2) - Ratio

If(ratio1*ratio2>0) 
   // Print "==> No Solution in the dichotomy process for ", model
   // Actual returned value
   Return NaN
Endif

Do 
	dtt = tt2-tt1
	ttmid = (tt1+tt2)/2.0
	ratiomid = I4P_Ratio_SS(model,ttmid) - Ratio
	If(ratio1*ratiomid<0)
		tt2 = ttmid
		ratio2 = ratiomid
	Else
		tt1 = ttmid
		ratio1 = ratiomid
	Endif
While(dtt>dttmin)

Return ttmid

End 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4P_Ratio_SS(model,tt)

Variable tt
String model
Variable ratio_model

strswitch( model)
case "pancake":
	ratio_model  = I4P_Ratio_SS_pancake(tt)
break 
case "hemisphere":
	ratio_model  = I4P_Ratio_SS_hemisphere(tt) 
break
endswitch

Return ratio_model

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4P_Ratio_SS_pancake(tt)

Variable tt
NVAR pEALf1a = root:I4P:I4Pquantification_ss:pEALf1a
NVAR pEALf2a = root:I4P:I4Pquantification_ss:pEALf2a
NVAR  Cov = root:I4P:I4Pquantification_ss:Cov
NVAR  gSEP1sl = root:I4P:I4Pquantification_ss:SEP1sl
NVAR  gSEP1sa = root:I4P:I4Pquantification_ss:SEP1sa
NVAR  gSEP2sl = root:I4P:I4Pquantification_ss:SEP2sl
NVAR  gSEP2sa = root:I4P:I4Pquantification_ss:SEP2sa
Variable H
Variable AA1, AA2

H = tt/Cov

AA1 = (1.- Cov)*exp(-gSEP1sl)+ Cov*exp(-H/pEALf1a)*exp(-gSEP1sl)*exp(-gSEP1sa)
AA2 = (1.- Cov)*exp(-gSEP2sl)+ Cov*exp(-H/pEALf2a)*exp(-gSEP2sl)*exp(-gSEP2sa)

Return AA2/AA1


End 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4P_Ratio_SS_hemisphere(tt)

Variable tt
NVAR pEALf1a = root:I4P:I4Pquantification_ss:pEALf1a
NVAR pEALf2a = root:I4P:I4Pquantification_ss:pEALf2a
NVAR  Cov = root:I4P:I4Pquantification_ss:Cov
NVAR  gSEP1sl = root:I4P:I4Pquantification_ss:SEP1sl
NVAR  gSEP1sa = root:I4P:I4Pquantification_ss:SEP1sa
NVAR  gSEP2sl = root:I4P:I4Pquantification_ss:SEP2sl
NVAR  gSEP2sa = root:I4P:I4Pquantification_ss:SEP2sa
Variable H
Variable AA1, AA2

H = 1.5*tt/Cov
AA1 = (1. - Cov)*exp(-gSEP1sl) + ( 2.*Cov*(pEALf1a/H)^2 * (1. - (1.+H/pEALf1a)*exp(-H/pEALf1a)))*exp(-gSEP1sl)*exp(-gSEP1sa)
AA2 = (1.  -Cov)*exp(-gSEP2sl) +  (2.*Cov*(pEALf2a/H)^2 * (1. - (1.+H/pEALf2a)*exp(-H/pEALf2a)))*exp(-gSEP2sl)*exp(-gSEP2sa)


Return AA2/AA1	

End 