// Function :  I4PDatabasePhi
// Author : R�mi Lazzari, lazzari@insp.jussieu.fr
// Date : 2019
// Usage :  database from  Physical Electronics, Inc. 1985
// (binding energies of core-levels, kinetic energies of Auger transitions)

#pragma rtGlobals=3		.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Structure to store all the information for each chemical element : Phi handbook
// Handbook of X-ray photoelectron spectroscopy
//J.F. Moulder, W.F. Stickle, P.E. Sobol and K.D. Bomben
// Physical Electronics, Inc. 1985
Structure I4PElementPhi
	string na			// element name 
	string sy			// element symbol
	string nu			// element number
	string cl			// core-level label
	string be			// binding energy (eV)	
	string auger		// Auger transition label
	string bea		// Auger binding energy at Al Ka (eV)
	string bem		// Auger binding energy at Mg Ka (eV)
	string ke			// kinetic energy (eV)		
EndStructure


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// I4P database 
// Handbook of X-ray photoelectron spectroscopy
//J.F. Moulder, W.F. Stickle, P.E. Sobol and K.D. Bomben
// Physical Electronics, Inc. 1985
Function I4PDatabasePhi(TagElement,Elmt)

string TagElement
STRUCT I4PElementPhi &Elmt

strswitch(TagElement)

//---------------------------------------------------
case "H":
Elmt.na			= "hydrogen"
Elmt.sy			= "H"
Elmt.nu			= "1"
Elmt.cl			= "none"
Elmt.be			= "nan"
Elmt.auger		= "none"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"
break
//---------------------------------------------------
case "He":
Elmt.na			= "helium"
Elmt.sy			= "He"
Elmt.nu			= "2"	
Elmt.cl			= "none"
Elmt.be			= "nan"
Elmt.auger		= "none"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"	
break
//---------------------------------------------------
case "Li":
Elmt.na			= "lithium"
Elmt.sy			= "Li"
Elmt.nu			= "3"	
Elmt.cl			= "1s"
Elmt.be			= "55.6"
Elmt.auger		= "none"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"		
break
//---------------------------------------------------
case "Be":
Elmt.na			= "beryllium"
Elmt.sy			= "Be"
Elmt.nu			= "4"	
Elmt.cl			= "1s"
Elmt.be			= "111.8"
Elmt.auger		= "KVV"
Elmt.bea		= "1384"	
Elmt.bem		= "1151"
Elmt.ke			= "102.6"
break
//---------------------------------------------------
case "B":
Elmt.na			= "boron"
Elmt.sy			= "B"
Elmt.nu			= "5"	
Elmt.cl			= "1s"
Elmt.be			= "189.4"
Elmt.auger		= "KLL"
Elmt.bea		= "1310"	
Elmt.bem		= "1077"
Elmt.ke			= "176.6"	
break
//---------------------------------------------------
case "C":
Elmt.na			= "carbon"
Elmt.sy			= "C"
Elmt.nu			= "6"	
Elmt.cl			= "1s"
Elmt.be			= "284.5"
Elmt.auger		= "KVV"
Elmt.bea		= "1223"	
Elmt.bem		= "990"
Elmt.ke			= "263.6"
break
//---------------------------------------------------
case "N":
Elmt.na			= "nitrogen"
Elmt.sy			= "N"
Elmt.nu			= "7"	
Elmt.cl			= "1s"
Elmt.be			= "398.1"
Elmt.auger		= "KLL"
Elmt.bea		= "1107"	
Elmt.bem		= "874"
Elmt.ke			= "379.6"
break
//---------------------------------------------------
case "O":
Elmt.na			= "oxygen"
Elmt.sy			= "O"
Elmt.nu			= "8"	
Elmt.cl			= "1s;2s"
Elmt.be			= "531;23"
Elmt.auger		= "KL1L1;KL1L23;KL23L23"
Elmt.bea		= "1013;999;978"	
Elmt.bem		= "780;766;745"
Elmt.ke			= "473.6;487.6;508.6"
break
//---------------------------------------------------
case "F":
Elmt.na			= "fluorine"
Elmt.sy			= "F"
Elmt.nu			= "9"	
Elmt.cl			= "1s;2s"
Elmt.be			= "684.9;30"
Elmt.auger		= "KL1L1;KL1L23;KL23L23"
Elmt.bea		= "877;858;832"
Elmt.bem		= "644;625;599"
Elmt.ke			= "609.6;628.6;654.6"	
break
//---------------------------------------------------
case "Ne":
Elmt.na			= "neon"
Elmt.sy			= "Ne"
Elmt.nu			= "10"	
Elmt.cl			= "1s;2s;2p"
Elmt.be			= "863.1;41;14"
Elmt.auger		= "KL1L1;KL1L23;KL23L23"
Elmt.bea		= "725;702;669"
Elmt.bem		= "492;469;436"
Elmt.ke			= "761.6;784.6;817.6"
break
//---------------------------------------------------
case "Na":
Elmt.na			= "sodium"
Elmt.sy			= "Na"
Elmt.nu			= "11"	
Elmt.cl			= "1s;2s;2p"
Elmt.be			= "1071.8;64;31"
Elmt.auger		= "KL1L1;KL1L23;KL23L23"
Elmt.bea		= "561;532;493"
Elmt.bem		= "328;299;260"
Elmt.ke			= "925.6;954.6;993.6"	
break
//---------------------------------------------------
case "Mg":
Elmt.na			= "magnesium"
Elmt.sy			= "Mg"
Elmt.nu			= "12"	
Elmt.cl			= "1s;2s;2p"
Elmt.be			= "1303;89;49.8"
Elmt.auger		= "KL1L1;KL1L23;KL23L23"
Elmt.bea		= "381;347;301"
Elmt.bem		= "148;114;68"
Elmt.ke			= "1105.6;1139.6;1185.6"	
break
//---------------------------------------------------
case "Al":
Elmt.na			= "aluminium"
Elmt.sy			= "Al"
Elmt.nu			= "13"	
Elmt.cl			= "2s;2p"
Elmt.be			= "118;72.9"
Elmt.auger		= "L23M1M23"
Elmt.bea		= "1419"	
Elmt.bem		= "1186"
Elmt.ke			= "67.6"
break
//---------------------------------------------------
case "Si":
Elmt.na			= "silicon"
Elmt.sy			= "Si"
Elmt.nu			= "14"	
Elmt.cl			= "2s;2p"
Elmt.be			= "151;99.3"
Elmt.auger		= "L23M23M23"
Elmt.bea		= "1394"	
Elmt.bem		= "1161"
Elmt.ke			= "92.6"
break
//---------------------------------------------------
case "P":
Elmt.na			= "phosphorus"
Elmt.sy			= "P"
Elmt.nu			= "15"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s"
Elmt.be			= "188;130.74;129.9;14"
Elmt.auger		= "L23M23M23"
Elmt.bea		= "1367"	
Elmt.bem		= "1134"
Elmt.ke			= "119.6"
break
//---------------------------------------------------
case "S":
Elmt.na			= "sulfur"
Elmt.sy			= "S"
Elmt.nu			= "16"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s"
Elmt.be			= "228;165.18;164;18"
Elmt.auger		= "L23M23M23"
Elmt.bea		= "1336"
Elmt.bem		= "1103"
Elmt.ke			= "150.6"
break
//---------------------------------------------------
case "Cl":
Elmt.na			= "chlorine"
Elmt.sy			= "Cl"
Elmt.nu			= "17"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "271;201.1;198.5;17;6"
Elmt.auger		= "L23M23M23"
Elmt.bea		= "1304"	
Elmt.bem		= "1071"
Elmt.ke			= "182.6"
break
//---------------------------------------------------
case "Ar":
Elmt.na			= "argon"
Elmt.sy			= "Ar"
Elmt.nu			= "18"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s"
Elmt.be			= "320;244.02;241.9;24"
Elmt.auger		= "L23M23M23"
Elmt.bea		= "1272"	
Elmt.bem		= "1039"
Elmt.ke			= "214.6"
break
//---------------------------------------------------
case "K":
Elmt.na			= "potassium"
Elmt.sy			= "K"
Elmt.nu			= "19"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "380;297.2;294.4;35;19"
Elmt.auger		= "L23M23M23"
Elmt.bea		= "1239"	
Elmt.bem		= "1006"
Elmt.ke			= "247.6"
break
//---------------------------------------------------
case "Ca":
Elmt.na			= "calcium"
Elmt.sy			= "Ca"
Elmt.nu			= "20"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "440;350.3;346.7;45;26"
Elmt.auger		= "L23M23M23"
Elmt.bea		= "1197"	
Elmt.bem		= "964"
Elmt.ke			= "289.6"
break
//---------------------------------------------------
case "Sc":
Elmt.na			= "scandium"
Elmt.sy			= "Sc"
Elmt.nu			= "21"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "499;403.47;398.6;51;29"
Elmt.auger		= "LM23M23;L3M23M45(1P)"
Elmt.bea		= "1149;1118"
Elmt.bem		= "916;885"
Elmt.ke			= "337.6;368.6"	
break
//---------------------------------------------------
case "Ti":
Elmt.na			= "titanium"
Elmt.sy			= "Ti"
Elmt.nu			= "22"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "561;460.27;454.1;59;33"
Elmt.auger		= "LM23M23;L3M23M45(1P)"
Elmt.bea		= "1098;1068"
Elmt.bem		= "865;835"
Elmt.ke			= "388.6;418.6"	
break
//---------------------------------------------------
case "V":
Elmt.na			= "vanadium"
Elmt.sy			= "V"
Elmt.nu			= "23"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "627;519.84;512.2;66;37"
Elmt.auger		= "L23M23M23;L3M23M44(1P);L3M45M45"
Elmt.bea		= "1048;1014;977"
 Elmt.bem		= "815;781;744"  
Elmt.ke			= "438.6;472.6;509.6"	
break
//---------------------------------------------------
case "Cr":
Elmt.na			= "chromium"
Elmt.sy			= "Cr"
Elmt.nu			= "24"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "696;583.6;574.4;75;43"
Elmt.auger		= "L23M23M23;L3M23M45(1P);L3M45M45"
Elmt.bea		= "997;959;917"
Elmt.bem		= "764;726;684"
Elmt.ke			= "489.6;527.6;569.6"	
break
//---------------------------------------------------
case "Mn":
Elmt.na			= "manganese"
Elmt.sy			= "Mn"
Elmt.nu			= "25"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "769;650.05;639;83;48"
Elmt.auger		= "L23M23M23;L3M23M45;L3M45M45"
Elmt.bea		= "944;900;852"
Elmt.bem		= "711;667;619"
Elmt.ke			= "542.6;586.6;634.6"	
break
//---------------------------------------------------
case "Fe":
Elmt.na			= "iron"
Elmt.sy			= "Fe"
Elmt.nu			= "26"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "845;720.1;707;92;53"
Elmt.auger		= "LM23M23;L3M23M45(1P);L3M45M45"
Elmt.bea		= "888;839;784"
Elmt.bem		= "655;606;551"
Elmt.ke			= "598.6;647.6;702.6"	
break
//---------------------------------------------------
case "Co":
Elmt.na			= "cobalt"
Elmt.sy			= "Co"
Elmt.nu			= "27"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "925;793.27;778.3;101;60"
Elmt.auger		= "L3M23M23;L2M23M23;L3M23M45(1P);L3M23M45(3P);L2M23M45(1P);L3M45M45"
Elmt.bea		= "838;831;777;771;713;698"
Elmt.bem		= "605;598;544;538;480;465"
Elmt.ke			= "648.6;655.6;709.6;715.6;773.6;788.6"	
break
//---------------------------------------------------
case "Ni":
Elmt.na			= "nickel"
Elmt.sy			= "Ni"
Elmt.nu			= "28"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p"
Elmt.be			= "1009;869.97;852.7;111;67"
Elmt.auger		= "L3M23M23;L2M23M23;L3M23M45(1P);L3M23M45(3P);L2M23M45(1P);L3M45M45"
Elmt.bea		= "778;772;712;706;641;624"
Elmt.bem		= "545;539;479;473;408;391"
Elmt.ke			= "708.6;714.6;774.6;780.6;845.6;862.6"	
break
//---------------------------------------------------
case "Cu":
Elmt.na			= "copper"
Elmt.sy			= "Cu"
Elmt.nu			= "29"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p1/2;3p3/2"
Elmt.be			= "1097;952.5;932.7;123;77;75"
Elmt.auger		= "L3M23M23;L2M23M23;L3M23M45(1P);L3M23M45(3P);L2M23M45(1P);L3M45M45;L2M45M45"
Elmt.bea		= "719;712;648;640;628;568;548"
Elmt.bem		= "486;479;415;407;395;335;315"
Elmt.ke			= "767.6;774.6;838.6;846.6;858.6;918.6;938.6"	
break
//---------------------------------------------------
case "Zn":
Elmt.na			= "zinc"
Elmt.sy			= "Zn"
Elmt.nu			= "30"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p1/2;3p3/2;3d"
Elmt.be			= "1195;1044.77;1021.8;140;91;89;10"
Elmt.auger		= "L3M23M23;L2M23M23;L3M23M45(1P);L3M23M45(3P);L2M23M45(1P);L3M45M45;L2M45M45"
Elmt.bea		= "660;652;582;573;559;495;472"
Elmt.bem		= "427;419;349;340;326;262;239"
Elmt.ke			= "826.6;834.6;904.6;913.6;927.6;991.6;1014.6"	
break
//---------------------------------------------------
case "Ga": 
Elmt.na			= "gallium"
Elmt.sy			= "Ga"
Elmt.nu			= "31"	
Elmt.cl			= "2s;2p1/2;2p3/2;3s;3p1/2;3p3/2;3d"
Elmt.be			= "1301;1143.54;1116.7;160;107;104;19"
Elmt.auger		= "L3M23M23;L2M23M23;L3M23M45(1P);L3M23M45(3P);L2M23M45(1P);L3M45M45;L2M45M45"
Elmt.bea		= "597;589;514;504;487;419;392"
Elmt.bem		= "364;356;281;271;254;186;159"
Elmt.ke			= "889.6;897.6;972.6;982.6;999.6;1067.6;1094.6"	
break
//---------------------------------------------------
case "Ge": 
Elmt.na			= "germanium"
Elmt.sy			= "Ge"
Elmt.nu			= "32"	
Elmt.cl			= "2p1/2;2p3/2;3s;3p1/2;3p3/2;3d"
Elmt.be			= "1248;1217;181;126;122;29.4"
Elmt.auger		= "L3M23M23;L2M23M23;L3M23M45(1P);L3M23M45(3P);L2M23M45(1P);L3M45M45;L2M45M45"
Elmt.bea		= "534;525;444;433;412;342;310"
Elmt.bem		= "301;292;211;200;179;109;77"
Elmt.ke			= "952.6;961.6;1042.6;1053.6;1074.6;1144.6;1176.6"	
break
//---------------------------------------------------
case "As": 
Elmt.na			= "arsenic"
Elmt.sy			= "As"
Elmt.nu			= "33"	
Elmt.cl			= "2p1/2;2p3/2;3s;3p1/2;3p3/2;3d3/2;3d5/2"
Elmt.be			= "1359;1324;205;146;141;42.29;41.6"
Elmt.auger		= "L3M23M45(1P);L3M23M45(3P);L2M23M45(1P);L3M45M45;L2M45M45"
Elmt.bea		= "371;360;336;262;226"
Elmt.bem		= "138;127;103;29;nan"
Elmt.ke			= "1115.6;1126.6;1150.6;1224.6;1260.6"	
break
//---------------------------------------------------
case "Se": 
Elmt.na			= "selenium"
Elmt.sy			= "Se"
Elmt.nu			= "34"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2"
Elmt.be			= "232;169;163;56.46;55.6"
Elmt.auger		= "L3M23M45(1P);L3M23M45(3P);L2M23M45(1P);L3M45M45;L2M45M45"
Elmt.bea		= "299;287;257;181;140"
Elmt.bem		= "66;54;24;nan;nan"
Elmt.ke			= "1187.6;1199.6;1229.6;1305.6;1346.6"	
break
//---------------------------------------------------
case "Br": 
Elmt.na			= "bromide"
Elmt.sy			= "Br"
Elmt.nu			= "35"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4d"
Elmt.be			= "256;189;182;69.85;68.8;15;5"
Elmt.auger		= "M23M45N23"
Elmt.bea		= "1386"	
Elmt.bem		= "1153"
Elmt.ke			= "100.6"
break
//---------------------------------------------------
case "Kr": 
Elmt.na			= "krypton"
Elmt.sy			= "Kr"
Elmt.nu			= "36"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "287;216;208;88.23;87;21;8"
Elmt.auger		= "xx"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"	
break
//---------------------------------------------------
case "Rb": 
Elmt.na			= "rubidium"
Elmt.sy			= "Rb"
Elmt.nu			= "37"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "325;249;240;112.99;111.5;31;16"
Elmt.auger		= "M23M45N23"
Elmt.bea		= "1385"	
Elmt.bem		= "1152"
Elmt.ke			= "101.6"
break
//---------------------------------------------------
case "Sr": 
Elmt.na			= "strontium"
Elmt.sy			= "Sr"
Elmt.nu			= "38"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "360;281;270;136.09;134.3;39;21"
Elmt.auger		= "xx"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"	
break
//---------------------------------------------------
case "Y": 
Elmt.na			= "yttrium"
Elmt.sy			= "Y"
Elmt.nu			= "39"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "394;311;299;158.05;156;45;24"
Elmt.auger		= "M45N23V"
Elmt.bea		= "1356"
Elmt.bem		= "1123"
Elmt.ke			= "130.6"
break
//---------------------------------------------------
case "Zr": 
Elmt.na			= "zirconium"
Elmt.sy			= "Zr"
Elmt.nu			= "40"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "430;343;330;181.33;178.9;51;28"
Elmt.auger		= "M45N23N23;M45N23V;M45N45N45"
Elmt.bea		= "1393;1368;1337"
Elmt.bem		= "1160;1135;1104"
Elmt.ke			= "93.6;118.6;149.6"
break
//---------------------------------------------------
case "Nb": 
Elmt.na			= "niobium"
Elmt.sy			= "Nb"
Elmt.nu			= "41"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "467;376;361;205.12;202.4;56;31"
Elmt.auger		= "M45N23V;M45VV"
Elmt.bea		= "1319;1287"
Elmt.bem		= "1086;1054"
Elmt.ke			= "167.6;199.6"
break
//---------------------------------------------------
case "Mo": 
Elmt.na			= "molybdenum"
Elmt.sy			= "Mo"
Elmt.nu			= "42"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "506;412;394;231.13;228;63;36"
Elmt.auger		= "M45N23V;M45VV"
Elmt.bea		= "1299;1264"
Elmt.bem		= "1066;1031"
Elmt.ke			= "187.6;222.6"
break
//---------------------------------------------------
case "Tc": 
Elmt.na			= "xx"
Elmt.sy			= "Tc"
Elmt.nu			= "43"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "544;445;425;257;253;68;39"
Elmt.auger		= "M45N23V;M45VV"
Elmt.bea		= "1280;1241"
Elmt.bem		= "1047;1008"
Elmt.ke			= "206.6;245.6"
break
//---------------------------------------------------
case "Ru": 
Elmt.na			= "ruthenium"
Elmt.sy			= "Ru"
Elmt.nu			= "44"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "586;484;462;284.27;280.1;75;43"
Elmt.auger		= "M45N23V;M45VV"
Elmt.bea		= "1256;1212"
Elmt.bem		= "1023;979"
Elmt.ke			= "230.6;274.6"
break
//---------------------------------------------------
case "Rh": 
Elmt.na			= "rhodium"
Elmt.sy			= "Rh"
Elmt.nu			= "45"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "629;521;497;311.94;307.2;81;48"
Elmt.auger		= "M45N23V;M45VV"
Elmt.bea		= "1234;1185"
Elmt.bem		= "1001;952"
Elmt.ke			= "252.6;301.6"
break
//---------------------------------------------------
case "Pd": 
Elmt.na			= "palladium"
Elmt.sy			= "Pd"
Elmt.nu			= "46"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "671;560;533;340.36;335.1;88;52"
Elmt.auger		= "M45N23V;M45VV"
Elmt.bea		= "1211;1159"
Elmt.bem		= "978;926"
Elmt.ke			= "275.6;327.6"
break
//---------------------------------------------------
case "Ag": 
Elmt.na			= "silver"
Elmt.sy			= "Ag"
Elmt.nu			= "47"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p"
Elmt.be			= "719;604;573;374.3;368.3;98;60"
Elmt.auger		= "M45N23V;M5VV;M4VV"
Elmt.bea		= "1191;1135;1129"
Elmt.bem		= "958;902;896"
Elmt.ke			= "295.6;351.6;357.6"
break
//---------------------------------------------------
case "Cd": 
Elmt.na			= "cadmium"
Elmt.sy			= "Cd"
Elmt.nu			= "48"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p;4d"
Elmt.be			= "772;652;618;411.84;405.1;110;69;11"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "1110;1103"
Elmt.bem		= "877;870"
Elmt.ke			= "376.6;383.6"
break
//---------------------------------------------------
case "In": 
Elmt.na			= "indium"
Elmt.sy			= "In"
Elmt.nu			= "49"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p;4d"
Elmt.be			= "828;703;665;451.44;443.9;123;78;17"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "1084;1076"
Elmt.bem		= "851;843"
Elmt.ke			= "402.6;410.6"
break
//---------------------------------------------------
case "Sn": 
Elmt.na			= "tin"
Elmt.sy			= "Sn"
Elmt.nu			= "50"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p;4d"
Elmt.be			= "885;757;715;493.41;485;137;89;25"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "1058;1049"
 Elmt.bem		= "825;816"
Elmt.ke			= "428.6;437.6"
break
//---------------------------------------------------
case "Sb": 
Elmt.na			= "antimony"
Elmt.sy			= "Sb"
Elmt.nu			= "51"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p;4d"
Elmt.be			= "944;813;767;537.64;528.3;153;99;33"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "1032;1022"
Elmt.bem		= "799;789"
Elmt.ke			= "454.6;464.6"
break
//---------------------------------------------------
case "Te": 
Elmt.na			= "tellurium"
Elmt.sy			= "Te"
Elmt.nu			= "52"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p;4d3/2;4d5/2;5s"
Elmt.be			= "1009;871;820;583.49;573.1;170;111;42;41;12"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "1005;995"
Elmt.bem		= "772;762"
Elmt.ke			= "481.6;491.6"
break
//---------------------------------------------------
case "I": 
Elmt.na			= "iodine"
Elmt.sy			= "I"
Elmt.nu			= "53"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p;4d3/2;4d5/2;5s"
Elmt.be			= "1071;930;875;630.8;619.3;187;123;51;49;18"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "982;971"
Elmt.bem		= "749;738"
Elmt.ke			= "504.6;515.6"
break
//---------------------------------------------------
case "Xe": 
Elmt.na			= "xenon"
Elmt.sy			= "Xe"
Elmt.nu			= "54"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p;4d3/2;4d5/2;5s"
Elmt.be			= "1141;996;934;682.37;669.7;207;139;63;61;17"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "955;942"
Elmt.bem		= "722;709"
Elmt.ke			= "531.6;544.6"
break
//---------------------------------------------------
case "Cs": 
Elmt.na			= "cesium"
Elmt.sy			= "Cs"
Elmt.nu			= "55"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p1/2;4p3/2;4d3/2;4d5/2;5s"
Elmt.be			= "1219;1069;1002;740.34;726.4;234;173;161;80;77;25"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "931;918"
Elmt.bem		= "698;685"
Elmt.ke			= "555.6;568.6"
break
//---------------------------------------------------
case "Ba": 
Elmt.na			= "baryum"
Elmt.sy			= "Ba"
Elmt.nu			= "56"	
Elmt.cl			= "3s;3p1/2;3p3/2;3d3/2;3d5/2;4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;5p"
Elmt.be			= "1292;1138;1064;795.93;780.6;254;193;179;93;90;31;15"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "900;886"
Elmt.bem		= "667;653"
Elmt.ke			= "586.6;600.6"
break
//---------------------------------------------------
case "La": 
Elmt.na			= "lanthanum"
Elmt.sy			= "La"
Elmt.nu			= "57"	
Elmt.cl			= "3p1/2;3p3/2;3d3/2;3d5/2;4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;5p"
Elmt.be			= "1208;1128;852.58;835.8;275;213;197;106;103;37;17"
Elmt.auger		= "M5N45N45;M4N45N45"
Elmt.bea		= "867;854"
Elmt.bem		= "634;621"
Elmt.ke			= "619.6;632.6"
break
//---------------------------------------------------
case "Ce": 
Elmt.na			= "cerium"
Elmt.sy			= "Ce"
Elmt.nu			= "58"	
Elmt.cl			= "3p1/2;3p3/2;3d3/2;3d5/2;4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;5p"
Elmt.be			= "1272;1184;901.9;883.8;290;223;207;112;109;36;18"
Elmt.auger		= "M45N45N45"
Elmt.bea		= "833"
Elmt.bem		= "600"
Elmt.ke			= "653.6"
break
//---------------------------------------------------
case "Pr": 
Elmt.na			= "praseodynium"
Elmt.sy			= "Pr"
Elmt.nu			= "59"	
Elmt.cl			= "3p1/2;3p3/2;3d3/2;3d5/2;4s;4p1/2;4p3/2;4d;5s;5p"
Elmt.be			= "1339;1242;952.2;931.8;305;234;218;115;38;18"
Elmt.auger		= "M45N45N45"
Elmt.bea		= "797"
Elmt.bem		= "564"
Elmt.ke			= "689.6"
break
//---------------------------------------------------
case "Nd": 
Elmt.na			= "neodynium"
Elmt.sy			= "Nd"
Elmt.nu			= "60"	
Elmt.cl			= "3p3/2;3d3/2;3d5/2;4s;4p1/2;4p3/2;4d;5s;5p"
Elmt.be			= "1301;1003.4;980.8;320;245;228;121;39;19"
Elmt.auger		= "M45N45N45"
Elmt.bea		= "758"
Elmt.bem		= "525"
Elmt.ke			= "728.6"
break
//---------------------------------------------------
case "Pm": 
Elmt.na			= "xx"
Elmt.sy			= "Pm"
Elmt.nu			= "61"	
Elmt.cl			= "3d3/2;3d5/2;4s;4p1/2;4p3/2;4d;5s;5p"
Elmt.be			= "1060;1034;337;264;242;129;38;22"
Elmt.auger		= "M45N45N45"
Elmt.bea		= "714"
Elmt.bem		= "'481"
Elmt.ke			= "772.6"
break
//---------------------------------------------------
case "Sm": 
Elmt.na			= "samarium"
Elmt.sy			= "Sm"
Elmt.nu			= "62"	
Elmt.cl			= "3d3/2;3d5/2;4s;4p1/2;4p3/2;4d;5s;5p"
Elmt.be			= "1107.9;1081.1;349;283;250;129;41;19"
Elmt.auger		= "M45N45N45"
Elmt.bea		= "682"
Elmt.bem		= "449"
Elmt.ke			= "804.6"
break
//---------------------------------------------------
case "Eu": 
Elmt.na			= "europium"
Elmt.sy			= "Eu"
Elmt.nu			= "63"	
Elmt.cl			= "3d3/2;3d5/2;4s;4p1/2;4p3/2;4d;5s;5p"
Elmt.be			= "1155.4;1125.6;363;289;255;128;39;19"
Elmt.auger		= "M45N45N45"
Elmt.bea		= "637"
Elmt.bem		= "404"
Elmt.ke			= "849.6"
break
//---------------------------------------------------
case "Gd": 
Elmt.na			= "gadolinium"
Elmt.sy			= "Gd"
Elmt.nu			= "64"	
Elmt.cl			= "3d3/2;3d5/2;4s;4p1/2;4p3/2;4d;5s;5p;4f"
Elmt.be			= "1218;1186;378;291;272;140.4;43;21;8"
Elmt.auger		= "M45N45N45"
Elmt.bea		= "602"
Elmt.bem		= "369"
Elmt.ke			= "884.6"
break
//---------------------------------------------------
case "Tb": 
Elmt.na			= "terbium"
Elmt.sy			= "Tb"
Elmt.nu			= "65"	
Elmt.cl			= "3d3/2;3d5/2;4s;4p1/2;4p3/2;4d;5s;5p;4f"
Elmt.be			= "1276;1241;396;322;285;146.0;45;22;8"
Elmt.auger		= "M45N45N45;M45N45V;M5VV;M4VV"
Elmt.bea		= "559;411;260;230"
Elmt.bem		= "326;178;27;nan"
Elmt.ke			= "927.6;1075.6;1226.6;1256.6"
break
//---------------------------------------------------
case "Dy": 
Elmt.na			= "dysprosium"
Elmt.sy			= "Dy"
Elmt.nu			= "66"	
Elmt.cl			= "3d3/2;3d5/2;4s;4p1/2;4p3/2;4d;5s;5p;4f"
Elmt.be			= "1333;1296;417;337;297;152.4;48;23;8"
Elmt.auger		= "M45N45N45;M45N45V"
Elmt.bea		= "526;368"
Elmt.bem		= "293;135"
Elmt.ke			= "960.6;1118.6"
break
//---------------------------------------------------
case "Ho": 
Elmt.na			= "holmium"
Elmt.sy			= "Ho"
Elmt.nu			= "67"	
Elmt.cl			= "3d3/2;3d5/2;4s;4p1/2;4p3/2;4d;5s;5p1/2;5p5/2;4f"
Elmt.be			= "1393;1352;435;353;309;159.6;49;30;24;9"
Elmt.auger		= "M45N45N45;M45N45V;M4VV"
Elmt.bea		= "488;314;117"
Elmt.bem		= "255;81;nan"
Elmt.ke			= "998.6;1172.6;1369.6"
break
//---------------------------------------------------
case "Er": 
Elmt.na			= "erbium"
Elmt.sy			= "Er"
Elmt.nu			= "68"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d;5s;5p1/2;5p5/2;4f"
Elmt.be			= "451;368;321;167.3;52;31;24;9"
Elmt.auger		= "M45N45N45;M45N45V;M5VV;M4VV"
Elmt.bea		= "440;273;98;56"
Elmt.bem		= "207;40;nan;nan"
Elmt.ke			= "1046.6;1213.6;1388.6;1430.6"
break
//---------------------------------------------------
case "Tm": 
Elmt.na			= "thullium"
Elmt.sy			= "Tm"
Elmt.nu			= "69"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d;5s;5p1/2;5p5/2;4f"
Elmt.be			= "470;384;333;175.4;53;32;25;8"
Elmt.auger		= "M45N45N45"
Elmt.bea		= "398"
Elmt.bem		= "165"
Elmt.ke			= "1088.6"
break
//---------------------------------------------------
case "Yb": 
Elmt.na			= "ytterbium"
Elmt.sy			= "Yb"
Elmt.nu			= "70"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d;5s;5p1/2;5p5/2;4f"
Elmt.be			= "482;389;341;182.4;51;30;24;3"
Elmt.auger		= "xx"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"
break
//---------------------------------------------------
case "Lu": 
Elmt.na			= "lutetium"
Elmt.sy			= "Lu"
Elmt.nu			= "71"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;5p1/2;5p5/2;4f5/2;4f7/2"
Elmt.be			= "509;413;360;206;196;57;34;27;8.8;7.3"
Elmt.auger		= "xx"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"
break
//---------------------------------------------------
case "Hf": 
Elmt.na			= "hafnium"
Elmt.sy			= "Hf"
Elmt.nu			= "72"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;5p1/2;5p5/2;4f5/2;4f7/2"
Elmt.be			= "534;437;380;222;211;63;38;30;16.01;14.3"
Elmt.auger		= "N5N67N7;N4N67N7"
Elmt.bea		= "1317;1306"
Elmt.bem		= "1084;1073"
Elmt.ke			= "169.6;180.6"
break
//---------------------------------------------------
case "Ta": 
Elmt.na			= "tantalum"
Elmt.sy			= "Ta"
Elmt.nu			= "73"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;5p1/2;5p5/2;4f5/2;4f7/2"
Elmt.be			= "563;463;401;238;226;69;43;33;23.81;21.9"
Elmt.auger		= "N5N67N7;N4N67N7"
Elmt.bea		= "1318;1306"
Elmt.bem		= "1085;1073"
Elmt.ke			= "168.6;180.6"
break
//---------------------------------------------------
case "W": 
Elmt.na			= "tungsten"
Elmt.sy			= "W"
Elmt.nu			= "74"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;5p1/2;5p5/2;4f5/2;4f7/2"
Elmt.be			= "594;491;424;256;243;75;47;37;33.58;31.4"
Elmt.auger		= "N5N67N7;N4N67N7"
Elmt.bea		= "1320;1307"
Elmt.bem		= "1087;1074"
Elmt.ke			= "166.6;179.6"
break
//---------------------------------------------------
case "Re": 
Elmt.na			= "rhenium"
Elmt.sy			= "Re"
Elmt.nu			= "75"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2"
Elmt.be			= "625;518;446;274;260;99;42.73;40.3"
Elmt.auger		= "N5N67N7;N4N67N7"
Elmt.bea		= "1322;1309"
Elmt.bem		= "1089;1076"
Elmt.ke			= "164.6;177.6"
break
//---------------------------------------------------
case "Os": 
Elmt.na			= "osmium"
Elmt.sy			= "Os"
Elmt.nu			= "76"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p"
Elmt.be			= "658;548;471;293;279;89;53.42;50.7;44"
Elmt.auger		= "N5N67N7;N4N67N7"
Elmt.bea		= "1326;1311"
Elmt.bem		= "1093;1078"
Elmt.ke			= "160.6;175.6"
break
//---------------------------------------------------
case "Ir": 
Elmt.na			= "iridium"
Elmt.sy			= "Ir"
Elmt.nu			= "77"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p"
Elmt.be			= "692;578;495;312;297;96;63.88;60.9;48"
Elmt.auger		= "N5N67N7;N4N67N7"
Elmt.bea		= "1329;1314"
Elmt.bem		= "1096;1081"
Elmt.ke			= "157.6;172.6"
break
//---------------------------------------------------
case "Pt": 
Elmt.na			= "platinum"
Elmt.sy			= "Pt"
Elmt.nu			= "78"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p"
Elmt.be			= "725;609;520;332;315;103;74.53;71.2;52"
Elmt.auger		= "N5N67N7;N4N67N7"
Elmt.bea		= "1334;1317"
Elmt.bem		= "1101;1084"
Elmt.ke			= "152.6;169.6"
break
//---------------------------------------------------
case "Au": 
Elmt.na			= "gold"
Elmt.sy			= "Au"
Elmt.nu			= "79"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p1/2;5p3/2"
Elmt.be			= "763;643;547;353;335;110;87.67;84;74;57"
Elmt.auger		= "N67O45O45;N5N6N67;N4N6N67;N5N67V"
Elmt.bea		= "1416;1342;1324;1247"
Elmt.bem		= "1183;1109;1091;1014"
Elmt.ke			= "70.6;144.6;162.6;239.6"
break
//---------------------------------------------------
case "Hg": 
Elmt.na			= "mercury"
Elmt.sy			= "Hg"
Elmt.nu			= "80"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p1/2;5p3/2;5d3/2;5d5/2"
Elmt.be			= "805;682;579;391;361;125;105.05;101;85;67;12;10"
Elmt.auger		= "N7O45O45;N5N7O;N4N6O"
Elmt.bea		= "1412;1246;1230"
Elmt.bem		= "1179;1013;997"
Elmt.ke			= "74.6;240.6;256.6"
break
//---------------------------------------------------
case "Tl": 
Elmt.na			= "thallium"
Elmt.sy			= "Tl"
Elmt.nu			= "81"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p1/2;5p3/2;5d3/2;5d5/2"
Elmt.be			= "847;720;610;406;385;133;122.12;117.7;95;74;15;13"
Elmt.auger		= "N7O45O45;N6O45O45;N5N7O5;N4N67O5"
Elmt.bea		= "1401;1399;1241;1222"
Elmt.bem		= "1168;1166;1008;989"
Elmt.ke			= "85.6;87.6;245.6;264.6"
break
//---------------------------------------------------
case "Pb": 
Elmt.na			= "lead"
Elmt.sy			= "Pb"
Elmt.nu			= "82"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p1/2;5p3/2;5d3/2;5d5/2"
Elmt.be			= "893;762;644;434;412;150;141.76;136.9;107;84;21;18"
Elmt.auger		= "N7O45O45;N6O45O45"
Elmt.bea		= "1394;1391"
Elmt.bem		= "1161;1158"
Elmt.ke			= "92.6;95.6"
break
//---------------------------------------------------
case "Bi": 
Elmt.na			= "bismuth"
Elmt.sy			= "Bi"
Elmt.nu			= "83"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p1/2;5p3/2;5d3/2;5d5/2"
Elmt.be			= "940;806;679;464;440;161;162.31;157;119;93;27;24"
Elmt.auger		= "N7O45O45;N6O45O45"
Elmt.bea		= "1387;1383"
Elmt.bem		= "1154;1150"
Elmt.ke			= "99.6;103.6"
break
//---------------------------------------------------
case "Th": 
Elmt.na			= "thorium"
Elmt.sy			= "Th"
Elmt.nu			= "90"	
Elmt.cl			= "4s;4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p1/2;5p3/2;5d3/2;5d5/2;6s;6p1/2;6p3/2"
Elmt.be			= "1330;1170;965;713;676;342.54;333.2;294;234;177;93;85;42;25;17"
Elmt.auger		= "N6O23V;N67O45O45;N7O4O5;N67O45V"
Elmt.bea		= "1419;1404;1335;1239"
Elmt.bem		= "1186;1171;1102;1006"
Elmt.ke			= "67.6;82.6;151.6;247.6"
break
//---------------------------------------------------
case "U": 
Elmt.na			= "uranium"
Elmt.sy			= "U"
Elmt.nu			= "92"	
Elmt.cl			= "4p1/2;4p3/2;4d3/2;4d5/2;5s;4f5/2;4f7/2;5p1/2;5p3/2;5d3/2;5d5/2;6s;6p1/2;6p3/2"
Elmt.be			= "1272;1043;779;736;388.19;377.3;322;260;195;103;94;44;26;17"
Elmt.auger		= "N6O23V;N7O23O5;N6O45O45;N67O45V"
Elmt.bea		= "1412;1396;1386;1204"
Elmt.bem		= "1179;1163;1153;971"
Elmt.ke			= "74.6;90.6;100.6;282.6"
break
//---------------------------------------------------
case "Np": 
Elmt.na			= "neptunium"
Elmt.sy			= "Np"
Elmt.nu			= "93"	
Elmt.cl			= "4p3/2;4d3/2;4d5/2;4f5/2;4f7/2;5p3/2;5d5/2;6p1/2;6p3/2"
Elmt.be			= "1086;816;771;414;402;206;101;29;18"
Elmt.auger		= "N67O45O45;N67O45V"
Elmt.bea		= "1297;1203"
Elmt.bem		= "1064;970"
Elmt.ke			= "189.6;283.6"
break
//---------------------------------------------------
case "Pu": 
Elmt.na			= "plutonium"
Elmt.sy			= "Pu"
Elmt.nu			= "94"	
Elmt.cl			= "4p1/2;4d3/2;4d5/2;4f5/2;4f7/2;5p3/2;5d5/2;6p1/2;6p3/2"
Elmt.be			= "1121;850;802;439;427;216;105;31;18"
Elmt.auger		= "xx"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"
break
//---------------------------------------------------
case "Am": 
Elmt.na			= "XX"
Elmt.sy			= "Am"
Elmt.nu			= "95"	
Elmt.cl			= "4d3/2;4d5/2;4f5/2;4f7/2;5s;5p3/2;5d3/2;5d5/2;6p1/2;6p3/2"
Elmt.be			= "883;832;463;449;351;216;119;109;31;18"
Elmt.auger		= "xx"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"
break
//---------------------------------------------------
case "Cm": 
Elmt.na			= "XX"
Elmt.sy			= "Cm"
Elmt.nu			= "96"	
Elmt.cl			= "4d3/2;4d5/2;4f5/2;4f7/2;5p3/2;5d5/2;6p1/2;6p3/2"
Elmt.be			= "919;865;487;473;232;113;32;18"
Elmt.auger		= "xx"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"
break
//---------------------------------------------------
case "Bk": 
Elmt.na			= "XX"
Elmt.sy			= "Bk"
Elmt.nu			= "97"	
Elmt.cl			= "4d3/2;4d5/2;4f5/2;4f7/2;5p3/2;5d5/2;6p1/2;6p3/2"
Elmt.be			= "958;901;514;498;246;120;34;18"
Elmt.auger		= "xx"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"
break
//---------------------------------------------------
case "Cf": 
Elmt.na			= "XX"
Elmt.sy			= "Cf"
Elmt.nu			= "98"	
Elmt.cl			= "4d3/2;4d5/2;4f5/2;4f7/2;5d5/2;6p1/2;6p3/2"
Elmt.be			= "994;933;541;523;124;35;19"
Elmt.auger		= "xx"
Elmt.bea		= "nan"
Elmt.bem		= "nan"
Elmt.ke			= "nan"
break
//---------------------------------------------------
default:
Elmt.na			= "xx"			
Elmt.sy			= "X"
Elmt.nu			= "0"	
Elmt.cl			= "xx"
Elmt.be			= "nan"
Elmt.auger		= "xx"
Elmt.bea		= "nan"	
Elmt.bem		= "nan"
Elmt.ke			= "nan"		

endswitch

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// check database 
Function Check_I4PDatabasePhi()

//----list of chemical elements
variable maxelem = 104
make /t/o/n=(maxelem) ElementSym
ElementSym[0]= {"","H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y"}
ElementSym[40]= {"Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir"}
ElementSym[78]= {"Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs"}

//----list of chemical elements
variable kk, checkphi
STRUCT I4PElementPhi Element

//---loop over elements and check
for(kk=1;kk<maxelem;kk+=1)
	checkphi = I4PDatabasePhi(ElementSym[kk],Element)
	if(cmpstr(ElementSym[kk],Element.sy)!=0 &&  checkphi!=0)
		print "ERROR Symbol : " + ElementSym[kk] + "  " + Element.sy
	endif
	if(kk!=str2num(Element.nu) &&  checkphi!=0)
		print "ERROR : " + num2str(kk) + "  " + Element.nu
	endif	
	if(itemsinlist(Element.be,";") != itemsinlist(Element.cl,";") &&  checkphi!=0)
		print "ERROR : CL/BE @" + Element.sy
	endif	
	if( itemsinlist(Element.ke,";") != itemsinlist(Element.auger,";") &&  checkphi!=0)
		print "ERROR : Auger/KE @" + Element.sy
	endif
	if(checkphi==0)
		print ElementSym[kk] + "    not available"
	endif		
endfor

//---clean 
killwaves /z ElementSym

End
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// From Auger binding energy at Al Ka to KE 
Function FromBEAuger2KE()

//----list of chemical elements
variable maxelem = 104
make/T/o/N=(maxelem) ElementSym
ElementSym[0]= {"","H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y"}
ElementSym[40]= {"Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir"}
ElementSym[78]= {"Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs"}

//----list of chemical elements
STRUCT I4PElementPhi Element

//---loop over elements and check
variable kk, checkphi, nn
variable bem
string str
for(kk=1;kk<maxelem;kk+=1)
	checkphi = I4PDatabasePhi(ElementSym[kk],Element)
	str = ""
	for(nn=0;nn< itemsinlist(Element.auger,";");nn+=1)
		bem = ghv_Mg - str2num(stringfromlist(nn, Element.ke, ";"))
		if(bem>0)
			str += num2str( bem ) + ";"
		else
			str += "nan;"
		endif
	endfor	
	print "==>",Element.sy,  Element.na
	print "Elmt.bem		= \"" +  str[0,strlen(str)-2] + "\""
	print " "
endfor

//---clean 
killwaves /z ElementSym


End