// Module : ip_mfp_database
// Author : R�mi Lazzari, remi.lazzari@insp.jussieu.fr
// Date : 2020
// Usage : database for calculating inelastic/transport mean-free paths of electrons using "universal" formulae
// Tanuma, S., Powell, C. & Penn, D. R. Calculations of Electron Inelastic Mean Free Paths. Surface and Interface Analysis 21 (1993) 165�176.
// Also see http://www.quases.com/products/quases-imfp-tpp2m
//  Jablonski, A., Universal quantification of elastic scattering effects in AES and XPS, Surf. Sci., 1996, 364, 380 - 395


#pragma rtGlobals= 3		
#pragma ModuleName = mfp_database

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Structure to store all the material informations required in formulae
structure StructIMFP
	string material		// material name
	variable rho		// bulk density (g/cm^3)
	variable Nv		// number of valence electrons per atom or molecule
	variable M		// molar mas (g/mol)
	variable Eg		// band-gap (eV)
	string  Z			// (atomic number; atomic fraction) according to chemical formula
	variable k1		// k1 (�) of Gries
	variable k2	 	// k2 of Gries
endstructure

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Data base of materials for mean free paths
Static Function Get_IMFP_Database(material,MATIMFP)

string material
struct StructIMFP &MATIMFP


strswitch(material)
//--SIMPLE ELEMENTS IN SOLID STATE
//--H : hydrogen
case "H":
	MATIMFP.material= "H"		
	MATIMFP.rho	= 0.088
	MATIMFP.Nv		= 1
	MATIMFP.M		= 1.008
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "1;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--He : helium
case "He":
	MATIMFP.material= "He"		
	MATIMFP.rho	= 0.214
	MATIMFP.Nv		= 2
	MATIMFP.M		= 4.0026
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "2;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Li : lithium
case "Li":
	MATIMFP.material= "Li"		
	MATIMFP.rho	= 0.535
	MATIMFP.Nv		= 1
	MATIMFP.M		= 6.94
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "3;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Be : beryllium
case "Be":
	MATIMFP.material= "Be"		
	MATIMFP.rho	= 1.848
	MATIMFP.Nv		= 2
	MATIMFP.M		= 9.0122
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "4;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--B : boron
case "B":
	MATIMFP.material= "B"		
	MATIMFP.rho	= 2.46
	MATIMFP.Nv		= 3
	MATIMFP.M		= 10.81
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "5;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--C : carbon
case "C":
	MATIMFP.material= "C"		
	MATIMFP.rho	= 2.267
	MATIMFP.Nv		= 4
	MATIMFP.M		= 12.011
	MATIMFP.Eg	= 5.4
	MATIMFP.Z		= "6;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--N : nitrogen
case "N":
	MATIMFP.material= "N"		
	MATIMFP.rho	= 1.026
	MATIMFP.Nv		= 5
	MATIMFP.M		= 14.007
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "7;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--O: oxygen
case "O":
	MATIMFP.material= "O"		
	MATIMFP.rho	= 1.495
	MATIMFP.Nv		= 6
	MATIMFP.M		= 15.999
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "8;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--F: fluorine
case "F":
	MATIMFP.material= "F"		
	MATIMFP.rho	= 1.700
	MATIMFP.Nv		= 7
	MATIMFP.M		= 18.998
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "9;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Ne: neon
case "Ne":
	MATIMFP.material= "Ne"		
	MATIMFP.rho	= 1.444
	MATIMFP.Nv		= 8
	MATIMFP.M		= 20.180
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "10;1"	
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Na : sodium
case "Na":
	MATIMFP.material= "Na"		
	MATIMFP.rho	= 0.968
	MATIMFP.Nv		= 1
	MATIMFP.M		= 22.990
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "11;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--Mg : magnesium
case "Mg":
	MATIMFP.material= "Mg"		
	MATIMFP.rho	= 1.738
	MATIMFP.Nv		= 2
	MATIMFP.M		= 24.305
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "12;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--Al : aluminum
case "Al":
	MATIMFP.material= "Al"		
	MATIMFP.rho	= 2.7
	MATIMFP.Nv		= 3
	MATIMFP.M		= 26.982
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "13;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--Si : silicon
case "Si":
	MATIMFP.material= "Si"		
	MATIMFP.rho	= 2.33
	MATIMFP.Nv		= 4
	MATIMFP.M		= 28.085
	MATIMFP.Eg	= 1.12
	MATIMFP.Z		= "14;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--P : phosphorous
case "P":
	MATIMFP.material= "P"		
	MATIMFP.rho	= 1.823
	MATIMFP.Nv		= 5
	MATIMFP.M		= 30.974
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "15;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--S : sulfur
case "S":
	MATIMFP.material= "S"		
	MATIMFP.rho	= 1.96
	MATIMFP.Nv		= 6
	MATIMFP.M		= 32.06
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "16;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--Cl : chlorine
case "Cl":
	MATIMFP.material= "Cl"		
	MATIMFP.rho	= 2.030
	MATIMFP.Nv		= 7
	MATIMFP.M		= 35.45
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "17;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--Ar : argon
case "Ar":
	MATIMFP.material= "Ar"		
	MATIMFP.rho	= 1.616
	MATIMFP.Nv		= 8
	MATIMFP.M		= 39.948
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "18;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--K : potassium
case "K":
	MATIMFP.material= "K"		
	MATIMFP.rho	= 0.856
	MATIMFP.Nv		= 1
	MATIMFP.M		= 39.098
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "19;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--Ca : calcium
case "Ca":
	MATIMFP.material= "Ca"		
	MATIMFP.rho	= 1.55
	MATIMFP.Nv		= 2
	MATIMFP.M		= 40.078
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "20;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--Sc : scandium
case "Sc":
	MATIMFP.material= "Sc"		
	MATIMFP.rho	= 2.985
	MATIMFP.Nv		= 3
	MATIMFP.M		= 44.956
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "21;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 			
break 
//--Ti : titanium
case "Ti":
	MATIMFP.material= "Ti"		
	MATIMFP.rho	= 4.507
	MATIMFP.Nv		= 4
	MATIMFP.M		= 47.867
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "22;1"
	MATIMFP.k1		= 0.02
	MATIMFP.k2		= 1.30 			
break 
//--V : vanadium
case "V":
	MATIMFP.material= "V"		
	MATIMFP.rho	= 6.11
	MATIMFP.Nv		= 5
	MATIMFP.M		= 50.942
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "23;1"
	MATIMFP.k1		= 0.02
	MATIMFP.k2		= 1.30 			
break 
//--Cr : chromium
case "Cr":
	MATIMFP.material= "Cr"		
	MATIMFP.rho	= 7.14
	MATIMFP.Nv		= 6
	MATIMFP.M		= 51.996
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "24;1"
	MATIMFP.k1		= 0.02
	MATIMFP.k2		= 1.30 	
break 
//--Mn : manganese
case "Mn":
	MATIMFP.material= "Mn"		
	MATIMFP.rho	= 7.47
	MATIMFP.Nv		= 7
	MATIMFP.M		= 54.938
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "25;1"
	MATIMFP.k1		= 0.02
	MATIMFP.k2		= 1.30 	
break 
//--Fe : iron
case "Fe":
	MATIMFP.material= "Fe"		
	MATIMFP.rho	= 7.874
	MATIMFP.Nv		= 8
	MATIMFP.M		= 55.845
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "26;1"
	MATIMFP.k1		= 0.02
	MATIMFP.k2		= 1.30 		
break 
//--Co : cobalt
case "Co":
	MATIMFP.material= "Co"		
	MATIMFP.rho	= 8.9
	MATIMFP.Nv		= 9
	MATIMFP.M		= 58.933
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "27;1"
	MATIMFP.k1		= 0.02
	MATIMFP.k2		= 1.30 	
break 
//--Ni : nickel
case "Ni":
	MATIMFP.material= "Ni"		
	MATIMFP.rho	= 8.908
	MATIMFP.Nv		= 10
	MATIMFP.M		= 58.693
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "28;1"
	MATIMFP.k1		= 0.02
	MATIMFP.k2		= 1.30 	
break 
//--Cu : copper
case "Cu":
	MATIMFP.material= "Cu"		
	MATIMFP.rho	= 8.92
	MATIMFP.Nv		= 11
	MATIMFP.M		= 63.546
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "29;1"
	MATIMFP.k1		= 0.02
	MATIMFP.k2		= 1.30 	
break 
//--Zn : zinc
case "Zn":
	MATIMFP.material= "Zn"		
	MATIMFP.rho	= 7.14
	MATIMFP.Nv		= 12
	MATIMFP.M		= 65.38
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "30;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Ga : gallium
case "Ga":
	MATIMFP.material= "Ga"		
	MATIMFP.rho	= 5.904
	MATIMFP.Nv		= 3
	MATIMFP.M		= 69.723
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "31;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Ge : germanium
case "Ge":
	MATIMFP.material= "Ge"		
	MATIMFP.rho	= 5.323
	MATIMFP.Nv		= 4
	MATIMFP.M		= 72.630
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "32;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--As : arsenide
case "As":
	MATIMFP.material= "As"		
	MATIMFP.rho	= 5.727
	MATIMFP.Nv		= 5
	MATIMFP.M		= 74.922
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "33;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Se : selenium
case "Se":
	MATIMFP.material= "Se"		
	MATIMFP.rho	= 4.819
	MATIMFP.Nv		= 6
	MATIMFP.M		= 78.971
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "34;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Br : bromium
case "Br":
	MATIMFP.material= "Br"		
	MATIMFP.rho	= 4.050
	MATIMFP.Nv		= 7
	MATIMFP.M		= 79.904
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "35;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Kr : krypton
case "Kr":
	MATIMFP.material= "Kr"		
	MATIMFP.rho	= 2.155
	MATIMFP.Nv		= 8
	MATIMFP.M		= 83.798
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "36;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Rb: rubidium
case "Rb":
	MATIMFP.material= "Rb"		
	MATIMFP.rho	= 1.532
	MATIMFP.Nv		= 1
	MATIMFP.M		= 85.468
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "37;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Sr: strontium
case "Sr":
	MATIMFP.material= "Sr"		
	MATIMFP.rho	= 2.63
	MATIMFP.Nv		= 2
	MATIMFP.M		= 87.62
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "38;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Y: yttrium
case "Y":
	MATIMFP.material= "Y"		
	MATIMFP.rho	= 4.472
	MATIMFP.Nv		= 3
	MATIMFP.M		= 88.906
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "39;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 		
break 
//--Zr: zirconium
case "Zr":
	MATIMFP.material= "Zr"		
	MATIMFP.rho	= 6.511
	MATIMFP.Nv		= 4
	MATIMFP.M		= 91.224
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "40;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.35 		
break
//--Nb: niobium
case "Nb":
	MATIMFP.material= "Nb"		
	MATIMFP.rho	= 8.57
	MATIMFP.Nv		= 5
	MATIMFP.M		= 92.906
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "41;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.35 		
break
//--Mo : molybdenum
case "Mo":
	MATIMFP.material= "Mo"		
	MATIMFP.rho	= 10.28
	MATIMFP.Nv		= 6
	MATIMFP.M		= 95.95
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "42;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.35 		
break 
//--Tc : technecium
case "Tc":
	MATIMFP.material= "Tc"		
	MATIMFP.rho	= 11.5
	MATIMFP.Nv		= 7
	MATIMFP.M		= 96.906
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "43;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.35 		
break 
//--Ru : ruthenium
case "Ru":
	MATIMFP.material= "Ru"		
	MATIMFP.rho	= 12.37
	MATIMFP.Nv		= 8
	MATIMFP.M		= 101.07
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "44;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.35 		
break 
//--Rh : rhodium
case "Rh":
	MATIMFP.material= "Rh"		
	MATIMFP.rho	= 12.45
	MATIMFP.Nv		= 9
	MATIMFP.M		= 102.91
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "45;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.35 		
break 
//--Pd : palladium
case "Pd":
	MATIMFP.material= "Pd"		
	MATIMFP.rho	= 12.023
	MATIMFP.Nv		= 10
	MATIMFP.M		= 106.42
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "46;1"	
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.35	
break 
//--Ag : silver
case "Ag":
	MATIMFP.material= "Ag"		
	MATIMFP.rho	= 10.49
	MATIMFP.Nv		= 11
	MATIMFP.M		= 107.87
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "47;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.35 		
break 
//--Cd : cadnium
case "Cd":
	MATIMFP.material= "Cd"		
	MATIMFP.rho	= 8.65
	MATIMFP.Nv		= 12
	MATIMFP.M		= 112.41
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "48;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--In : indium
case "In":
	MATIMFP.material= "In"		
	MATIMFP.rho	= 7.31
	MATIMFP.Nv		= 3
	MATIMFP.M		= 114.82
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "49;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Sn : tin
case "Sn":
	MATIMFP.material= "Sn"		
	MATIMFP.rho	= 7.31
	MATIMFP.Nv		= 4
	MATIMFP.M		= 118.71
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "50;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Sb : antimony
case "Sb":
	MATIMFP.material= "Sb"		
	MATIMFP.rho	= 6.697
	MATIMFP.Nv		= 5
	MATIMFP.M		= 121.76
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "51;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Te : tellurium
case "Te":
	MATIMFP.material= "Te"		
	MATIMFP.rho	= 6.24
	MATIMFP.Nv		= 6
	MATIMFP.M		= 127.6
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "52;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--I : iodine
case "I":
	MATIMFP.material= "I"		
	MATIMFP.rho	= 4.94
	MATIMFP.Nv		= 7
	MATIMFP.M		= 126.90
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "53;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Xe : xenon
case "Xe":
	MATIMFP.material= "Xe"		
	MATIMFP.rho	= 3.64
	MATIMFP.Nv		= 8
	MATIMFP.M		= 131.29
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "54;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Cs : cesium
case "Cs":
	MATIMFP.material= "Cs"		
	MATIMFP.rho	= 1.879
	MATIMFP.Nv		= 1
	MATIMFP.M		= 132.91
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "55;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Ba : baryum
case "Ba":
	MATIMFP.material= "Ba"		
	MATIMFP.rho	= 3.510
	MATIMFP.Nv		= 2
	MATIMFP.M		= 137.33
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "56;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--La : lanthanum
case "La":
	MATIMFP.material= "La"		
	MATIMFP.rho	= 6.146
	MATIMFP.Nv		= 3
	MATIMFP.M		= 138.91
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "57;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Ce : cerium
case "Ce":
	MATIMFP.material= "Ce"		
	MATIMFP.rho	= 6.689
	MATIMFP.Nv		= 9
	MATIMFP.M		= 140.12
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "58;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Pr : praseodynium
case "Pr":
	MATIMFP.material= "Pr"		
	MATIMFP.rho	= 6.64
	MATIMFP.Nv		= 9
	MATIMFP.M		= 140.91
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "59;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Nd : neodynium
case "Nd":
	MATIMFP.material= "Nd"		
	MATIMFP.rho	= 6.8
	MATIMFP.Nv		= 9
	MATIMFP.M		= 144.24
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "60;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Pm : promethium
case "Pm":
	MATIMFP.material= "Pm"		
	MATIMFP.rho	= 7.264
	MATIMFP.Nv		= 7
	MATIMFP.M		= 144.91
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "61;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Sm: samarium
case "Sm":
	MATIMFP.material= "Sm"		
	MATIMFP.rho	= 7.353
	MATIMFP.Nv		= 8
	MATIMFP.M		= 150.36
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "62;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Eu: europium
case "Eu":
	MATIMFP.material= "Eu"		
	MATIMFP.rho	= 5.244
	MATIMFP.Nv		= 9
	MATIMFP.M		= 151.96
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "63;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Gd: gadolinium
case "Gd":
	MATIMFP.material= "Gd"		
	MATIMFP.rho	= 7.901
	MATIMFP.Nv		= 9
	MATIMFP.M		= 157.25
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "64;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Tb: terbium
case "Tb":
	MATIMFP.material= "Tb"		
	MATIMFP.rho	= 8.219
	MATIMFP.Nv		= 9
	MATIMFP.M		= 158.93
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "65;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Dy: dysprosium
case "Dy":
	MATIMFP.material= "Dy"		
	MATIMFP.rho	= 8.551
	MATIMFP.Nv		= 9
	MATIMFP.M		= 162.5
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "66;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Ho: holmium
case "Ho":
	MATIMFP.material= "Ho"		
	MATIMFP.rho	= 8.795
	MATIMFP.Nv		= 9
	MATIMFP.M		= 164.93
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "67;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Er: erbium
case "Er":
	MATIMFP.material= "Er"		
	MATIMFP.rho	= 9.066
	MATIMFP.Nv		= 9
	MATIMFP.M		= 167.26
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "68;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break 
//--Tm: thullium
case "Tm":
	MATIMFP.material= "Tm"		
	MATIMFP.rho	= 9.321
	MATIMFP.Nv		= 9
	MATIMFP.M		= 168.93
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "69;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break
//--Yb: ytterbium
case "Yb":
	MATIMFP.material= "Yb"		
	MATIMFP.rho	= 6.57
	MATIMFP.Nv		= 8
	MATIMFP.M		= 173.05
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "70;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break
//--Lu: luthetium
case "Lu":
	MATIMFP.material= "Lu"		
	MATIMFP.rho	= 9.841
	MATIMFP.Nv		= 9
	MATIMFP.M		= 174.97
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "71;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10 
break
//--Hf: hafnium
case "Hf":
	MATIMFP.material= "Hf"		
	MATIMFP.rho	= 13.310
	MATIMFP.Nv		= 4
	MATIMFP.M		= 178.49
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "72;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.45		
break
//--Ta : tantalum
case "Ta":
	MATIMFP.material= "Ta"		
	MATIMFP.rho	= 16.65
	MATIMFP.Nv		= 5
	MATIMFP.M		= 180.95
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "73;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.45		
break
//--W : tungsten
case "W":
	MATIMFP.material= "W"		
	MATIMFP.rho	= 19.250
	MATIMFP.Nv		= 6
	MATIMFP.M		= 183.84
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "74;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.45		
break 
//--Re : rhenium
case "Re":
	MATIMFP.material= "Re"		
	MATIMFP.rho	= 21.020
	MATIMFP.Nv		= 7
	MATIMFP.M		= 186.21
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "75;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.45		
break 
//--Os : osmium
case "Os":
	MATIMFP.material= "Os"		
	MATIMFP.rho	= 22.610
	MATIMFP.Nv		= 8
	MATIMFP.M		= 190.23
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "76;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.45		
break 
//--Ir : iridium
case "Os":
	MATIMFP.material= "Ir"		
	MATIMFP.rho	= 22.650
	MATIMFP.Nv		= 9
	MATIMFP.M		= 192.22
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "77;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.45		
break 
//--Pt : platinum
case "Pt":
	MATIMFP.material= "Pt"		
	MATIMFP.rho	= 21.090
	MATIMFP.Nv		= 10
	MATIMFP.M		= 195.08
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "78;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.45		
break 
//--Au : gold
case "Au":
	MATIMFP.material= "Au"		
	MATIMFP.rho	= 19.3
	MATIMFP.Nv		= 11
	MATIMFP.M		= 196.97
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "79;1"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.45		
break 
//--Hg : mercury
case "Au":
	MATIMFP.material= "Hg"		
	MATIMFP.rho	= 14.190
	MATIMFP.Nv		= 12
	MATIMFP.M		= 200.59
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "80;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break 
//--Tl : thallium
case "Tl":
	MATIMFP.material= "Tl"		
	MATIMFP.rho	= 11.85
	MATIMFP.Nv		= 13
	MATIMFP.M		= 204.38
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "81;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Pb : lead
case "Tl":
	MATIMFP.material= "Pb"		
	MATIMFP.rho	= 11.340
	MATIMFP.Nv		= 4
	MATIMFP.M		= 207.2
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "82;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Bi : bismuth
case "Bi":
	MATIMFP.material= "Bi"		
	MATIMFP.rho	= 9.78
	MATIMFP.Nv		= 5
	MATIMFP.M		= 208.98
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "83;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Po : polonium
case "Po":
	MATIMFP.material= "Po"		
	MATIMFP.rho	= 9.196
	MATIMFP.Nv		= 30
	MATIMFP.M		= 208.98
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "84;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--At : astatine
case "At":
	MATIMFP.material= "At"		
	MATIMFP.rho	= 6.4
	MATIMFP.Nv		= 31
	MATIMFP.M		= 209.99
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "85;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Rn : radon
case "Rn":
	MATIMFP.material= "Rn"		
	MATIMFP.rho	= 4.4
	MATIMFP.Nv		= 32
	MATIMFP.M		= 222.02
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "86;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Fr : francium
case "Fr":
	MATIMFP.material= "Fr"		
	MATIMFP.rho	= 2.9
	MATIMFP.Nv		= 1
	MATIMFP.M		= 223.02
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "87;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Ra : radium
case "Ra":
	MATIMFP.material= "Ra"		
	MATIMFP.rho	= 5
	MATIMFP.Nv		= 2
	MATIMFP.M		= 226.03
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "88;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Ac : actinium
case "Ac":
	MATIMFP.material= "Ac"		
	MATIMFP.rho	= 10.07
	MATIMFP.Nv		= 3
	MATIMFP.M		= 227.03
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "89;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Th : thorium
case "Th":
	MATIMFP.material= "Th"		
	MATIMFP.rho	= 11.724
	MATIMFP.Nv		= 4
	MATIMFP.M		= 232.04
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "90;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Pa : praseoidynium
case "Pa":
	MATIMFP.material= "Pa"		
	MATIMFP.rho	= 15.370
	MATIMFP.Nv		= 3
	MATIMFP.M		= 231.04
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "91;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--U : uranium
case "U":
	MATIMFP.material= "U"		
	MATIMFP.rho	= 19.050
	MATIMFP.Nv		= 3
	MATIMFP.M		= 238.03
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "92;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Np : neptunium
case "Np":
	MATIMFP.material= "Np"		
	MATIMFP.rho	= 20.450
	MATIMFP.Nv		= 7
	MATIMFP.M		= 237.05
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "93;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Pu : plutonium
case "Pu":
	MATIMFP.material= "Pu"		
	MATIMFP.rho	= 19.816
	MATIMFP.Nv		= 8
	MATIMFP.M		= 244.06
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "94;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Am : americium
case "Am":
	MATIMFP.material= "Am"		
	MATIMFP.rho	= 13.780
	MATIMFP.Nv		= 9
	MATIMFP.M		= 243.06
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "95;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Cm : curium
case "Cm":
	MATIMFP.material= "Cm"		
	MATIMFP.rho	= 13.510
	MATIMFP.Nv		= 10
	MATIMFP.M		= 247.07
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "96;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Bk : berkelium
case "Bk":
	MATIMFP.material= "Bk"		
	MATIMFP.rho	= 14.78
	MATIMFP.Nv		= 11
	MATIMFP.M		= 247.07
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "97;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break
//--Cf : californium
case "Cf":
	MATIMFP.material= "Cf"		
	MATIMFP.rho	= 15.10
	MATIMFP.Nv		= 12
	MATIMFP.M		= 251.08
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "98;1"
	MATIMFP.k1		= 0.014
	MATIMFP.k2		= 1.10		
break

//--ALLOYS
//--Ni3Al : nickel aluminide
case "Ni3Al":
	MATIMFP.material= "Ni3Al"		
	MATIMFP.rho	= 7.16
	MATIMFP.Nv		= 33
	MATIMFP.M		= 203.06
	MATIMFP.Eg	= 0
	MATIMFP.Z		= "28;0.75;13;0.25"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30		
break

//--OXIDES
//--MgO : magnesium oxide
case "MgO":
	MATIMFP.material= "MgO"		
	MATIMFP.rho	= 3.58
	MATIMFP.Nv		= 8
	MATIMFP.M		= 40.311
	MATIMFP.Eg	= 9
	MATIMFP.Z		= "12;0.5;8;0.5"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30				
break  
//--Al2O3 : aluminium oxide
case "Al2O3":
	MATIMFP.material= "Al2O3"		
	MATIMFP.rho	= 3.99
	MATIMFP.Nv		= 24
	MATIMFP.M		= 101.96
	MATIMFP.Eg	= 9
	MATIMFP.Z		=  "13;0.4;8;0.6"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30		
break
//--SiO2 : silicon oxide
case "SiO2":
	MATIMFP.material= "SiO2"		
	MATIMFP.rho	= 2.65
	MATIMFP.Nv		= 16
	MATIMFP.M		= 60.084
	MATIMFP.Eg	= 9.1
	MATIMFP.Z		= "14;0.33333;8;0.66666"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30			
break  
//--CaO: calcium oxide
case "CaO":
	MATIMFP.material= "CaO"		
	MATIMFP.rho	= 3.35
	MATIMFP.Nv		= 8
	MATIMFP.M		= 56.079
	MATIMFP.Eg	= 7.7
	MATIMFP.Z		=  "20;0.5;8;0.5"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30		
break
//--TiO2 : titanium dioxide	
case "TiO2":
	MATIMFP.material= "TiO2"		
	MATIMFP.rho	= 4.26
	MATIMFP.Nv		= 16
	MATIMFP.M		= 79.898
	MATIMFP.Eg	= 3.2
	MATIMFP.Z		= "22;0.33333;8;0.66666"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30			
break 
//--V2O5 : vanadium dioxide	
case "TiO2":
	MATIMFP.material= "V2O5"		
	MATIMFP.rho	= 3.36
	MATIMFP.Nv		= 40
	MATIMFP.M		= 181.88
	MATIMFP.Eg	= 2.4
	MATIMFP.Z		= "23;0.28571;8;0.71429"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30			
break 
//--Cr2O3 : chromium oxide	
case "Cr2O3":
	MATIMFP.material= "Cr2O3"		
	MATIMFP.rho	= 5.21
	MATIMFP.Nv		= 30
	MATIMFP.M		= 151.99
	MATIMFP.Eg	= 3.2
	MATIMFP.Z		= "24;0.4;8;0.6"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30				
break 
//--Fe2O3 : iron oxide	
case "Fe2O3":
	MATIMFP.material= "Fe2O3"		
	MATIMFP.rho	= 5.24
	MATIMFP.Nv		= 34
	MATIMFP.M		= 159.69
	MATIMFP.Eg	= 2.2
	MATIMFP.Z		= "26;0.4;8;0.6"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30			
break 
//--Fe3O4 : iron oxide	
case "Fe3O4":
	MATIMFP.material= "Fe3O4"		
	MATIMFP.rho	= 5.18
	MATIMFP.Nv		= 48
	MATIMFP.M		= 231.53
	MATIMFP.Eg	= 2.2
	MATIMFP.Z		= "26;0.42857;8;0.5714"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30		
break 
//--NiO : Nickel oxide	
case "NiO":
	MATIMFP.material= "NiO"		
	MATIMFP.rho	= 7.45
	MATIMFP.Nv		= 16
	MATIMFP.M		= 74.69
	MATIMFP.Eg	= 3.4
	MATIMFP.Z		= "28;0.5;8;0.5"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30				
break 
//--ZnO : zinc oxide
case "ZnO":
	MATIMFP.material= "ZnO"		
	MATIMFP.rho	= 5.47
	MATIMFP.Nv		= 18
	MATIMFP.M		= 81.369
	MATIMFP.Eg	= 3.2
	MATIMFP.Z		= "30;0.5;8;0.5"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30			
break 
//--MoO3 : molybdenum oxide
case "MoO3":
	MATIMFP.material= "MoO3"		
	MATIMFP.rho	= 4.5
	MATIMFP.Nv		= 24
	MATIMFP.M		= 143.93
	MATIMFP.Eg	= 3
	MATIMFP.Z		= "42;0.25;8;0.75"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30			
break  
//--Ta2O5 : tantalum oxide
case "Ta2O5":
	MATIMFP.material= "Ta2O5"		
	MATIMFP.rho	= 8.74
	MATIMFP.Nv		= 40
	MATIMFP.M		= 441.89
	MATIMFP.Eg	= 4
	MATIMFP.Z		= "73;0.28571;8;0.71428"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30		
break

//--VARIOUS 
//--NaCl : sodium chlroride
case "NaCl":
	MATIMFP.material= "NaCl"		
	MATIMFP.rho	= 2.17
	MATIMFP.Nv		= 8
	MATIMFP.M		= 58.442
	MATIMFP.Eg	= 7.42
	MATIMFP.Z		= "11;0.5;17;0.5"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30				
break  
//--NaBr : sodium bromide
case "NaBr":
	MATIMFP.material= "NaBr"		
	MATIMFP.rho	= 3.2
	MATIMFP.Nv		= 8
	MATIMFP.M		= 102.89
	MATIMFP.Eg	= 7.1
	MATIMFP.Z		= "11;0.5;35;0.5"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30				
break  
//--KBr : potassium bromide
case "KBr":
	MATIMFP.material= "KBr"		
	MATIMFP.rho	= 2.75
	MATIMFP.Nv		= 8
	MATIMFP.M		= 119.01
	MATIMFP.Eg	= 7.6
	MATIMFP.Z		= "19;0.5;35;0.5"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30				
break  
//--Si3N4 : silicon nitride
case "Si3N4":
	MATIMFP.material= "Si3N4"		
	MATIMFP.rho	= 3.17
	MATIMFP.Nv		= 32
	MATIMFP.M		= 140.28
	MATIMFP.Eg	= 5.25
	MATIMFP.Z		= "14;0.42857;7;0.57143"
	MATIMFP.k1		= 0.019
	MATIMFP.k2		= 1.30				
break  

   
//--default value
default:							
	print "==> Material " + material + " not yet implemented !"				
	return 0
endswitch

//------- return check value = OK
return 1

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Data base of coefficients of TRMFP
// Transport cross section for electrons at energies of surface-sensitive spectroscopies
// A. Jablonski, Phys. Rev. B 58 (1998) 16470
Static Function Z2Aold(Z,AA)

variable Z
wave AA

// The waves
make /o/n=(6,96)/D AAm
make /o/n=(96)/D ZZ, AA0, AA1, AA2, AA3, AA4
AAm[][0] = {1,0.264249,-3.05110,12.4413,-26.7171,22.4159}
AAm[][1] = {2,-0.429336,3.94990,-13.9843,17.4198,-5.01816}
AAm[][2] = {3,-0.625441,6.24177,-23.9889,36.7366,-18.8995}
AAm[][3] = {4,-0.224636,2.63389,-12.1831,20.3588,-11.0168}
AAm[][4] = {5,0.290872,-2.34187,5.57294,-7.26493,4.67424}
AAm[][5] = {6,0.477867,-4.43493,14.0158,-21.8313,13.7141}
AAm[][6] = {7,0.276007,-2.95908,10.2746,-18.0938,12.5662}
AAm[][7] = {8,-0.219878,1.29078,-3.22671,0.800357,2.66689}
AAm[][8] = {9,-0.873345,7.08802,-22.3927,28.8433,-12.7240}
AAm[][9] = {10,-1.52678,12.9943,-42.3260,58.6682,-29.4797}
AAm[][10] = {11,-2.03847,17.7110,-58.5770,83.5229,-43.7744}
AAm[][11] = {12,-2.32696,20.4802,-68.5036,99.3210,-53.2438}
AAm[][12] = {13,-2.36798,21.0570,-71.1836,104.527,-56.9365}
AAm[][13] = {14,-2.16528,19.4689,-66.6748,99.1559,-54.8077}
AAm[][14] = {15,-1.72919,15.8022,-55.2392,83.5481,-47.0164}
AAm[][15] = {16,-1.08564,10.3010,-37.7273,59.0002,-34.2931}
AAm[][16] = {17,-0.294365,3.50192,-15.9343,28.1678,-18.1024}
AAm[][17] = {18,0.554684,-3.80408,7.53989,-5.16661,-0.499827}
AAm[][18] = {19,1.36620,-10.7878,29.9972,-37.1104,16.4133}
AAm[][19] = {20,2.06039,-16.7561,49.1847,-64.4144,30.8824}
AAm[][20] = {21,2.59837,-21.3675,63.9748,-85.4211,41.9915}
AAm[][21] = {22,2.95832,-24.4251,73.7010,-99.1262,49.1724}
AAm[][22] = {23,3.13929,-25.9155,78.2983,-105.396,52.3271}
AAm[][23] = {24,3.12749,-25.7230,77.4011,-103.718,51.1839}
AAm[][24] = {25,2.92367,-23.8690,71.1215,-94.3019,45.8758}
AAm[][25] = {26,2.53348,-20.4116,59.6770,-77.5019,36.6148}
AAm[][26] = {27,1.99040,-15.6493,44.0589,-54.7701,24.1926}
AAm[][27] = {28,1.36289,-10.1654,26.1230,-28.7229,9.98764}
AAm[][28] = {29,0.762533,-4.89141,8.77742,-3.38097,-3.92199}
AAm[][29] = {30,0.353047,-1.17353,-3.84319,15.6311,-14.6768}
AAm[][30] = {31,0.352863,-0.787299,-6.34942,21.0730,-18.6432}
AAm[][31] = {32,0.989649,-5.56677,6.77549,5.59955,-12.1679}
AAm[][32] = {33,2.37467,-16.4084,38.2344,-34.4050,6.56097}
AAm[][33] = {34,4.31661,-31.7992,83.5883,-93.2196,34.8134}
AAm[][34] = {35,6.48755,-49.1452,135.196,-160.913,67.7855}
AAm[][35] = {36,8.35183,-64.2035,180.550,-221.228,97.6187}
AAm[][36] = {37,9.60705,-74.5791,212.573,-264.917,119.804}
AAm[][37] = {38,10.1156,-79.1638,227.927,-287.527,132.122}
AAm[][38] = {39,9.91497,-78.2276,227.316,-289.824,134.859}
AAm[][39] = {40,9.12731,-72.7095,213.405,-275.123,129.532}
AAm[][40] = {41,7.89898,-63.7412,189.438,-247.503,118.037}
AAm[][41] = {42,6.35555,-52.3009,158.244,-210.556,102.064}
AAm[][42] = {43,4.62798,-39.3963,122.702,-167.898,83.2965}
AAm[][43] = {44,2.78359,-25.5614,84.3838,-121.567,62.7111}
AAm[][44] = {45,0.911013,-11.4801,45.2514,-74.0360,41.4658}
AAm[][45] = {46,-0.934796,2.40970,6.59973,-26.9895,20.3710}
AAm[][46] = {47,-2.74133,16.0057,-31.2567,19.1441,-0.355420}
AAm[][47] = {48,-4.35648,28.1678,-65.1468,60.4863,-18.9548}
AAm[][48] = {49,-5.89468,39.7164,-97.2390,99.5410,-36.4920}
AAm[][49] = {50,-7.28783,50.1416,-126.116,134.572,-52.1774}
AAm[][50] = {51,-8.56895,59.6812,-152.404,166.297,-66.3100}
AAm[][51] = {52,-9.74168,68.3310,-176.005,194.486,-78.7394}
AAm[][52] = {53,-10.5507,74.2249,-191.863,213.136,-86.8259}
AAm[][53] = {54,-11.2483,79.1889,-204.873,227.992,-93.0647}
AAm[][54] = {55,-11.6624,81.9828,-211.725,235.183,-95.7824}
AAm[][55] = {56,-11.6740,81.7139,-209.942,231.708,-93.6387}
AAm[][56] = {57,-11.2520,78.1721,-199.009,217.024,-86.4327}
AAm[][57] = {58,-10.3092,70.7364,-177.302,189.289,-73.4021}
AAm[][58] = {59,-8.83362,59.3698,-144.870,148.759,-54.7555}
AAm[][59] = {60,-6.80745,43.9958,-101.671,95.6075,-30.6831}
AAm[][60] = {61,-4.35730,25.6110,-50.6210,33.5916,-2.97981}
AAm[][61] = {62,-1.77710,6.43632,2.04718,-29.5968,24.8332}
AAm[][62] = {63,0.317198,-9.00175,44.0064,-79.2258,46.2429}
AAm[][63] = {64,1.70035,-19.0502,70.7758,-109.991,58.9435}
AAm[][64] = {65,2.28896,-23.2100,81.3506,-121.182,62.8753}
AAm[][65] = {66,2.39269,-23.8572,82.5319,-121.409,62.1076}
AAm[][66] = {67,2.30299,-23.2146,80.6402,-118.616,60.3682}
AAm[][67] = {68,2.07316,-21.7017,76.9147,-114.432,58.4639}
AAm[][68] = {69,1.54664,-18.1648,68.1962,-105.055,54.6965}
AAm[][69] = {70,0.754648,-12.7211,54.4770,-90.0551,48.6788}
AAm[][70] = {71,-0.348764,-5.00374,34.6520,-67.9457,39.6588}
AAm[][71] = {72,-1.93172,6.18507,5.54471,-35.0056,26.0069}
AAm[][72] = {73,-3.57936,17.9214,-25.2429,0.141212,11.3178}
AAm[][73] = {74,-5.25966,29.9566,-57.0068,36.6386,-4.03482}
AAm[][74] = {75,-6.81013,41.1348,-86.7097,70.9991,-18.5789}
AAm[][75] = {76,-8.19784,51.1951,-113.594,102.273,-31.8818}
AAm[][76] = {77,-9.42464,60.1481,-137.679,130.468,-43.9419}
AAm[][77] = {78,-10.4052,67.3701,-157.280,153.597,-53.8981}
AAm[][78] = {79,-11.1578,72.9901,-172.729,172.032,-61.9040}
AAm[][79] = {80,-11.6908,77.0577,-184.132,185.867,-67.9879}
AAm[][80] = {81,-11.9971,79.5223,-191.353,194.942,-72.0817}
AAm[][81] = {82,-12.1097,80.6164,-194.996,199.943,-74.4735}
AAm[][82] = {83,-12.0299,80.3590,-195.140,200.993,-75.2290}
AAm[][83] = {84,-11.7707,78.8358,-191.989,198.309,-74.4350}
AAm[][84] = {85,-11.3395,76.1169,-185.776,192.210,-72.2486}
AAm[][85] = {86,-10.8108,72.6933,-177.694,183.966,-69.1706}
AAm[][86] = {87,-10.1474,68.3644,-167.382,173.346,-65.1762}
AAm[][87] = {88,-9.42347,63.6088,-155.972,161.521,-60.7127}
AAm[][88] = {89,-8.61923,58.3281,-143.318,148.443,-55.8098}
AAm[][89] = {90,-7.76164,52.7081,-129.893,134.649,-50.6909}
AAm[][90] = {91,-6.91173,47.1745,-116.796,121.370,-45.8678}
AAm[][91] = {92,-6.07090,41.7323,-104.024,108.594,-41.3279}
AAm[][92] = {93,-5.26203,36.5482,-92.0249,96.8332,-37.2873}
AAm[][93] = {94,-4.48976,31.6361,-80.7824,86.0104,-33.6852}
AAm[][94] = {95,-3.73747,26.8784,-69.9833,75.7526,-30.3543}
AAm[][95] = {96,-2.98813,22.1421,-59.2422,65.5686,-27.0628}
AA0 = AAm[5][p]
AA1 = AAm[4][p]
AA2 = AAm[3][p]
AA3 = AAm[2][p]
AA4 = AAm[1][p]
ZZ 	= AAm[0][p]

// Interpolate them
make /d/o/n=1 Zi, Ai
Zi = Z
interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA0
AA[0] = Ai[0]
interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA1
AA[1]  = Ai[0]
interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA2
AA[2]  = Ai[0] 
interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA3
AA[3]  = Ai[0]
 interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA4
AA[4]  = Ai[0]
  
// Clean
killwaves /z AAm
killwaves /z  ZZ, AA0, AA1, AA2, AA3, AA4, Zi, Ai

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Data base of coefficients of TRMFP
// Improved algorithm for calculating transport cross sections of electrons with energies from 50 eV to 30 keV
// A. Jablonski and C.J. Powell  Phys. Rev. B 76 (2007) 085123
Static Function Z2Anew(Ek,Z,AA)

variable Ek, Z
wave AA

// The waves
make /o/n=(6,92)/D AAl, AAh
make /o/n=(92)/D ZZ, AA0, AA1, AA2, AA3, AA4


// Extract the coefficients
if(Ek<300)
	//- low energy 50 to 300 eV
	AAl[][0]  = {1,-0.546230,4.50248,-13.5889,12.1864,1.69745}
	AAl[][1]  = {2,-1.25914,12.0038,-43.1263,63.8542,-32.3512}
	AAl[][2]  = {3,0.904932,-7.31498,21.0091,-29.7511,18.1957}
	AAl[][3]  = {4,0.820848,-7.46632,24.3170,-38.3118,24.5885}
	AAl[][4]  = {5,-0.485772,3.10657,-7.54629,4.20442,3.25846}
	AAl[][5]  = {6,-1.63196,12.7472,-37.8854,46.6927,-19.2354}
	AAl[][6]  = {7,-1.96222,15.9729,-49.5042,65.1921,-30.3743}
	AAl[][7]  = {8,-1.20022,10.5063,-35.3291,49.8963,-25.0389}
	AAl[][8]  = {9,0.498921,-2.45254,1.21827,5.05328,-5.15648}
	AAl[][9]  = {10,2.59663,-18.7006,47.9032,-53.6018,21.7347}
	AAl[][10]  = {11,1.45382,-9.97443,23.2140,-22.9846,7.68602}
	AAl[][11]  = {12,1.07939,-7.24368,15.9844,-14.8674,4.48392}
	AAl[][12]  = {13,1.12533,-8.00526,19.6551,-21.9199,9.24374}
	AAl[][13]  = {14,1.08094,-7.90936,20.2712,-24.1321,11.1273}
	AAl[][14]  = {15,1.12801,-8.34732,21.8375,-26.6447,12.6160}
	AAl[][15]  = {16,1.62503,-12.0349,32.0727,-39.2168,18.3426}
	AAl[][16]  = {17,2.95835,-21.8931,59.2633,-72.3044,33.2604}
	AAl[][17]  = {18,5.26443,-38.9977,106.600,-130.128,59.4700}
	AAl[][18]  = {19,3.75956,-27.8519,75.9531,-93.1646,43.0303}
	AAl[][19]  = {20,1.83503,-13.2755,34.7374,-41.6661,19.0705}
	AAl[][20]  = {21,2.22769,-15.7802,40.2939,-46.3395,19.9189}
	AAl[][21]  = {22,2.95026,-20.8160,53.0667,-60.0385,24.9189}
	AAl[][22]  = {23,3.75527,-26.6009,68.2834,-77.1486,31.6353}
	AAl[][23]  = {24,7.13099,-51.8707,138.510,-162.662,69.8486}
	AAl[][24]  = {25,4.93030,-35.5302,93.0118,-106.294,43.5654}
	AAl[][25]  = {26,5.12951,-37.4368,99.1856,-114.371,47.0457}
	AAl[][26]  = {27,5.08605,-37.7263,101.400,-118.248,48.9066}
	AAl[][27]  = {28,4.89299,-37.0297,101.254,-119.718,49.9030}
	AAl[][28]  = {29,5.12504,-41.2464,119.459,-149.187,65.9697}
	AAl[][29]  = {30,4.55903,-36.0939,102.570,-125.238,53.4354}
	AAl[][30]  = {31,5.65853,-43.0268,118.739,-141.807,59.7450}
	AAl[][31]  = {32,9.08367,-66.6901,179.920,-212.026,89.9488}
	AAl[][32]  = {33,13.7227,-99.2595,265.553,-312.004,133.691}
	AAl[][33]  = {34,18.2977,-131.817,352.371,-414.848,179.354}
	AAl[][34]  = {35,21.2963,-153.905,413.329,-489.559,213.651}
	AAl[][35]  = {36,21.1832,-154.971,421.297,-505.213,223.401}
	AAl[][36]  = {37,9.64677,-76.1385,221.010,-281.597,131.207}
	AAl[][37]  = {38,1.41731,-19.4493,75.7801,-118.096,63.2610}
	AAl[][38]  = {39,-3.30678,12.6927,-5.60335,-27.4561,25.9406}
	AAl[][39]  = {40,-7.86376,43.3099,-82.2004,56.9424,-8.52403}
	AAl[][40]  = {41,-16.2267,98.4459,-217.602,203.680,-67.7396}
	AAl[][41]  = {42,-24.9870,156.848,-362.746,362.928,-132.775}
	AAl[][42]  = {43,-35.4733,226.810,-536.803,554.196,-211.069}
	AAl[][43]  = {44,-47.2864,305.808,-733.843,771.341,-300.251}
	AAl[][44]  = {45,-59.5524,388.194,-940.255,999.866,-394.563}
	AAl[][45]  = {46,-76.7294,506.392,-1243.61,1343.99,-540.174}
	AAl[][46]  = {47,-79.9843,527.668,-1295.23,1398.90,-561.711}
	AAl[][47]  = {48,-82.9318,545.744,-1335.99,1438.69,-575.753}
	AAl[][48]  = {49,-78.5304,513.240,-1246.93,1331.25,-527.567}
	AAl[][49]  = {50,-70.9070,459.425,-1105.42,1167.03,-456.612}
	AAl[][50]  = {51,-59.6936,382.290,-907.640,943.121,-362.272}
	AAl[][51]  = {52,-46.0501,289.910,-674.554,683.652,-254.894}
	AAl[][52]  = {53,-32.2150,197.180,-443.186,429.324,-151.180}
	AAl[][53]  = {54,-20.3801,118.287,-247.757,216.571,-65.5586}
	AAl[][54]  = {55,-8.43899,42.7910,-71.5121,37.2396,1.15410}
	AAl[][55]  = {56,-2.81789,8.24406,6.23498,-38.0796,27.3118}
	AAl[][56]  = {57,2.11220,-23.7018,82.8355,-118.362,58.1900}
	AAl[][57]  = {58,-8.51577,41.9702,-67.2088,31.6943,2.71769}
	AAl[][58]  = {59,-14.1279,76.5807,-146.280,110.999,-26.8220}
	AAl[][59]  = {60,-20.9305,118.984,-244.316,210.606,-64.4117}
	AAl[][60]  = {61,-28.3527,165.599,-352.967,321.941,-106.782}
	AAl[][61]  = {62,-35.7984,212.676,-463.453,435.938,-150.454}
	AAl[][62]  = {63,-42.7093,256.702,-567.529,544.066,-192.143}
	AAl[][63]  = {64,-43.3001,260.622,-577.214,554.558,-196.324}
	AAl[][64]  = {65,-53.1413,324.346,-730.039,715.338,-259.014}
	AAl[][65]  = {66,-56.0713,344.232,-779.688,769.360,-280.672}
	AAl[][66]  = {67,-57.2905,353.638,-805.441,799.347,-293.327}
	AAl[][67]  = {68,-56.8010,352.556,-807.240,805.198,-296.927}
	AAl[][68]  = {69,-54.6954,341.545,-786.331,788.145,-291.926}
	AAl[][69]  = {70,-51.1368,321.596,-744.962,750.440,-279.169}
	AAl[][70]  = {71,-45.7417,288.775,-670.826,676.657,-251.792}
	AAl[][71]  = {72,-41.8469,265.536,-619.359,626.419,-233.475}
	AAl[][72]  = {73,-38.0652,243.318,-570.976,580.035,-216.879}
	AAl[][73]  = {74,-33.7661,218.185,-516.542,528.191,-198.485}
	AAl[][74]  = {75,-28.7092,188.567,-452.270,466.876,-176.723}
	AAl[][75]  = {76,-22.9576,154.756,-378.606,396.329,-151.608}
	AAl[][76]  = {77,-16.7915,118.379,-299.061,319.885,-124.317}
	AAl[][77]  = {78,-10.7136,83.7326,-226.118,252.716,-101.530}
	AAl[][78]  = {79,-4.69271,48.0760,-147.945,177.525,-74.7314}
	AAl[][79]  = {80,0.00740570,18.7635,-80.3319,109.092,-48.9996}
	AAl[][80]  = {81,4.80512,-12.1092,-6.88241,32.4103,-19.2395}
	AAl[][81]  = {82,9.48573,-42.0312,63.8109,-40.8254,8.93192}
	AAl[][82]  = {83,14.1899,-71.8526,133.668,-112.529,36.2199}
	AAl[][83]  = {84,19.0410,-102.258,204.086,-183.931,63.0136}
	AAl[][84]  = {85,24.1436,-133.825,276.224,-256.034,89.6239}
	AAl[][85]  = {86,29.6246,-167.278,351.625,-330.279,116.549}
	AAl[][86]  = {87,30.7856,-173.510,363.218,-338.633,118.224}
	AAl[][87]  = {88,30.1334,-168.477,348.845,-320.701,110.053}
	AAl[][88]  = {89,29.6743,-164.421,336.270,-304.168,102.239}
	AAl[][89]  = {90,28.8902,-158.292,318.862,-282.747,92.6015}
	AAl[][90]  = {91,40.2924,-226.921,472.083,-432.878,146.930}
	AAl[][91]  = {92,47.2920,-268.612,564.011,-521.611,178.465}
	AA0 = AAl[5][p]
	AA1 = AAl[4][p]
	AA2 = AAl[3][p]
	AA3 = AAl[2][p]
	AA4 = AAl[1][p]
	ZZ 	= AAl[0][p]	
else
	//- high energy 300 to 30000 eV
	AAh[][0]  = {1,-0.303626,3.20749,-13.2553,19.6833,-8.37291}
	AAh[][1]  = {2,0.477448,-5.50093,23.1147,-47.6719,38.1332}
	AAh[][2]  = {3,0.147657,-1.63846,6.24500,-15.1307,14.7987}
	AAh[][3]  = {4,-0.0704158,0.965349,-5.36671,7.78514,-2.05384}
	AAh[][4]  = {5,-0.0892603,1.34610,-7.72596,13.7035,-7.31416}
	AAh[][5]  = {6,0.0534841,-0.0640774,-2.66783,6.03266,-3.30677}
	AAh[][6]  = {7,0.287128,-2.48086,6.56179,-9.29723,5.94458}
	AAh[][7]  = {8,0.540620,-5.12933,16.8055,-26.5945,16.6158}
	AAh[][8]  = {9,0.758683,-7.41405,25.6656,-41.5828,25.8606}
	AAh[][9]  = {10,0.905818,-8.95686,31.6346,-51.5994,31.9217}
	AAh[][10]  = {11,1.05418,-10.5718,38.1545,-63.1158,39.3813}
	AAh[][11]  = {12,1.12326,-11.3713,41.5626,-69.4241,43.6291}
	AAh[][12]  = {13,1.12529,-11.4801,42.3270,-71.2863,45.1151}
	AAh[][13]  = {14,1.06100,-10.8942,40.3773,-68.4662,43.5941}
	AAh[][14]  = {15,0.947888,-9.79383,36.4038,-62.1264,39.7907}
	AAh[][15]  = {16,0.802874,-8.35423,31.0829,-53.4182,34.4338}
	AAh[][16]  = {17,0.639789,-6.71864,24.9675,-43.2850,28.1228}
	AAh[][17]  = {18,0.468750,-4.99165,18.4614,-32.4153,21.2953}
	AAh[][18]  = {19,0.364564,-3.96911,14.7396,-26.4441,17.7120}
	AAh[][19]  = {20,0.275421,-3.09712,11.5831,-21.4214,14.7323}
	AAh[][20]  = {21,0.148850,-1.80972,6.69648,-13.1913,9.51556}
	AAh[][21]  = {22,0.0255774,-0.544631,1.84546,-4.92583,4.20651}
	AAh[][22]  = {23,-0.0927181,0.678496,-2.88384,3.20766,-1.07261}
	AAh[][23]  = {24,-0.260317,2.43173,-9.75284,15.1862,-8.95102}
	AAh[][24]  = {25,-0.319051,3.04107,-12.1144,19.2639,-11.6237}
	AAh[][25]  = {26,-0.431246,4.22080,-16.7602,27.4137,-17.0265}
	AAh[][26]  = {27,-0.545784,5.42877,-21.5328,35.8147,-22.6150}
	AAh[][27]  = {28,-0.664631,6.68419,-26.5020,44.5788,-28.4552}
	AAh[][28]  = {29,-0.895965,9.11202,-36.0517,61.2967,-39.4792}
	AAh[][29]  = {30,-0.920320,9.38633,-37.2060,63.4732,-41.0530}
	AAh[][30]  = {31,-0.885563,9.04952,-35.9933,61.5770,-39.9976}
	AAh[][31]  = {32,-0.843013,8.63096,-34.4576,59.1091,-38.5584}
	AAh[][32]  = {33,-0.798979,8.19576,-32.8508,56.5036,-37.0170}
	AAh[][33]  = {34,-0.758649,7.79726,-31.3786,54.1132,-35.6006}
	AAh[][34]  = {35,-0.725520,7.47133,-30.1790,52.1747,-34.4620}
	AAh[][35]  = {36,-0.701802,7.24066,-29.3397,50.8382,-33.6982}
	AAh[][36]  = {37,-0.562805,5.82378,-23.9276,41.6723,-27.9068}
	AAh[][37]  = {38,-0.430038,4.46842,-18.7393,32.8590,-22.3153}
	AAh[][38]  = {39,-0.357874,3.73495,-15.9406,28.1207,-19.3270}
	AAh[][39]  = {40,-0.312568,3.27693,-14.1998,25.1863,-17.4910}
	AAh[][40]  = {41,-0.336853,3.53322,-15.2067,26.9481,-18.6660}
	AAh[][41]  = {42,-0.334752,3.51717,-15.1598,26.8969,-18.6695}
	AAh[][42]  = {43,-0.348753,3.66534,-15.7405,27.9109,-19.3507}
	AAh[][43]  = {44,-0.378316,3.97197,-16.9258,29.9497,-20.6831}
	AAh[][44]  = {45,-0.423173,4.43401,-18.7030,32.9902,-22.6511}
	AAh[][45]  = {46,-0.556012,5.79189,-23.8979,41.8227,-28.3002}
	AAh[][46]  = {47,-0.558283,5.81839,-24.0069,42.0253,-28.4581}
	AAh[][47]  = {48,-0.554960,5.78711,-23.8916,41.8397,-28.3620}
	AAh[][48]  = {49,-0.523669,5.47097,-22.6889,39.8093,-27.0911}
	AAh[][49]  = {50,-0.500367,5.23458,-21.7833,38.2673,-26.1187}
	AAh[][50]  = {51,-0.486092,5.08825,-21.2133,37.2788,-25.4864}
	AAh[][51]  = {52,-0.482331,5.04676,-21.0342,36.9358,-25.2520}
	AAh[][52]  = {53,-0.490243,5.12160,-21.2887,37.3088,-25.4595}
	AAh[][53]  = {54,-0.510626,5.32056,-22.0054,38.4450,-26.1383}
	AAh[][54]  = {55,-0.446163,4.66104,-19.4677,34.1007,-23.3557}
	AAh[][55]  = {56,-0.388404,4.06790,-17.1745,30.1526,-20.8098}
	AAh[][56]  = {57,-0.378536,3.95770,-16.7054,29.2632,-20.1862}
	AAh[][57]  = {58,-0.561416,5.83540,-23.9361,41.6595,-28.1890}
	AAh[][58]  = {59,-0.659449,6.84010,-27.7948,48.2564,-32.4402}
	AAh[][59]  = {60,-0.761543,7.88819,-31.8282,55.1681,-36.9054}
	AAh[][60]  = {61,-0.866151,8.96411,-35.9778,62.2958,-41.5220}
	AAh[][61]  = {62,-0.971841,10.0537,-40.1906,69.5521,-46.2356}
	AAh[][62]  = {63,-1.07712,11.1423,-44.4132,76.8498,-50.9930}
	AAh[][63]  = {64,-1.03940,10.7850,-43.1538,74.9049,-49.8985}
	AAh[][64]  = {65,-1.27996,13.2536,-52.6586,91.2006,-60.4161}
	AAh[][65]  = {66,-1.37387,14.2411,-56.5548,98.0511,-64.9607}
	AAh[][66]  = {67,-1.46015,15.1580,-60.2094,104.541,-69.3082}
	AAh[][67]  = {68,-1.53662,15.9835,-63.5476,110.552,-73.3885}
	AAh[][68]  = {69,-1.60102,16.6958,-66.4925,115.962,-77.1298}
	AAh[][69]  = {70,-1.65100,17.2727,-68.9645,120.645,-80.4579}
	AAh[][70]  = {71,-1.48394,15.6682,-63.2201,111.576,-75.1486}
	AAh[][71]  = {72,-1.34268,14.3135,-58.3766,103.941,-70.6908}
	AAh[][72]  = {73,-1.21649,13.1037,-54.0521,97.1266,-66.7140}
	AAh[][73]  = {74,-1.10353,12.0220,-50.1896,91.0470,-63.1722}
	AAh[][74]  = {75,-1.00229,11.0544,-46.7418,85.6337,-60.0290}
	AAh[][75]  = {76,-0.910819,10.1830,-43.6479,80.7953,-57.2340}
	AAh[][76]  = {77,-0.826878,9.38691,-40.8347,76.4204,-54.7245}
	AAh[][77]  = {78,-0.877368,9.92174,-42.9553,80.1637,-57.2193}
	AAh[][78]  = {79,-0.805565,9.25159,-40.6302,76.6256,-55.2455}
	AAh[][79]  = {80,-0.592413,7.18873,-33.1664,64.6694,-48.1033}
	AAh[][80]  = {81,-0.312108,4.46036,-23.2308,48.6348,-38.4371}
	AAh[][81]  = {82,-0.0377581,1.79230,-13.5222,32.9750,-29.0000}
	AAh[][82]  = {83,0.226201,-0.773370,-4.18940,17.9241,-19.9295}
	AAh[][83]  = {84,0.475247,-3.19305,4.61014,3.73390,-11.3765}
	AAh[][84]  = {85,0.705882,-5.43304,12.7550,-9.40118,-3.45765}
	AAh[][85]  = {86,0.915442,-7.46774,20.1529,-21.3340,3.73879}
	AAh[][86]  = {87,1.15152,-9.80100,28.7997,-35.5701,12.5181}
	AAh[][87]  = {88,1.35327,-11.8034,36.2560,-47.9122,20.1743}
	AAh[][88]  = {89,1.50611,-13.3226,41.9251,-57.3222,26.0297}
	AAh[][89]  = {90,1.61978,-14.4590,46.1953,-64.4650,30.5105}
	AAh[][90]  = {91,1.65315,-14.7165,46.8753,-65.1159,30.6018}
	AAh[][91]  = {92,1.71202,-15.2566,48.7234,-67.9025,32.1524}
	AA0 = AAh[5][p]
	AA1 = AAh[4][p]
	AA2 = AAh[3][p]
	AA3 = AAh[2][p]
	AA4 = AAh[1][p]
	ZZ 	= AAh[0][p]
endif

// Interpolate them
make /d/o/n=1 Zi, Ai
Zi = Z
interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA0
AA[0] = Ai[0]
interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA1
AA[1]  = Ai[0]
interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA2
AA[2]  = Ai[0] 
interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA3
AA[3]  = Ai[0]
 interpolate2 /i=3/t=2/x=Zi/y=Ai ZZ, AA4
AA[4]  = Ai[0]
  
// Clean
killwaves /z AAl, AAh
killwaves /z  ZZ, AA0, AA1, AA2, AA3, AA4, Zi, Ai

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Data base of coefficients of TRMFP
// Photoelectron transport in the surface region of solids: universal analytical formalism for quantitative applications of electron spectroscopies
// J. Phys. D: Appl. Phys. 2015, 48,  075301
Static Function Z2B(Ek,Z,BB)

variable Ek,Z
wave BB

// The waves
make /o/n=(6,72)/D BBl, BBh
make /o/n=(72)/D ZZ, BB0, BB1, BB2, BB3, BB4

// Extract the coefficients
if(Ek<300)
	//- low energy 50 to 300 eV
	BBl[][0]  = {3,-0.0104338,0.229489,-1.94377,6.63091,-8.97187}
	BBl[][1]  = {4,-0.00372685,0.0695423,-0.542675,1.33736,-0.982736}
	BBl[][2]  = {5,-0.0308963,0.622136,-4.75238,15.6770,-19.0507}
	BBl[][3]  = {6,-0.00396601,0.0547396,-0.243147,-0.337161,2.19524}
	BBl[][4]  = {11,-0.0299394,0.634291,-5.14632,18.5442,-26.1108}
	BBl[][5]  = {12,-0.0306849,0.635715,-5.02886,17.6171,-23.9307}
	BBl[][6]  = {13,-0.0136067,0.277666,-2.20867,7.75227,-10.8534}
	BBl[][7]  = {14,-0.00513060,0.0998115,-0.806046,2.83641,-4.38533}
	BBl[][8]  = {15,-0.0140773,0.277047,-2.10685,7.00168,-9.32940}
	BBl[][9]  = {16,-0.0151113,0.297392,-2.25429,7.47543,-9.91555}
	BBl[][10]  = {19,0.0324771,-0.661585,4.94551,-16.2627,18.7542}
	BBl[][11]  = {20,0.00356187,-0.0693807,0.408996,-0.871633,-0.605533}
	BBl[][12]  = {21,0.00155480,-0.0547530,0.550397,-2.45628,3.31074}
	BBl[][13]  = {22,-0.0126058,0.237223,-1.71356,5.41039,-6.91944}
	BBl[][14]  = {23,-0.0378808,0.748019,-5.57766,18.4065,-23.1978}
	BBl[][15]  = {24,-0.00120980,0.00249795,0.0689474,-0.398085,-0.0463984}
	BBl[][16]  = {25,-0.00962525,0.174189,-1.24402,4.07734,-5.78585}
	BBl[][17]  = {26,-0.0334661,0.661428,-4.98847,16.9508,-22.5432}
	BBl[][18]  = {27,-0.0556170,1.07921,-7.89677,25.7713,-32.3394}
	BBl[][19]  = {28,-0.0582921,1.13257,-8.31099,27.3051,-34.6693}
	BBl[][20]  = {29,-0.0306127,0.560030,-3.91125,12.5160,-16.4966}
	BBl[][21]  = {30,-0.0142589,0.249947,-1.76712,6.22868,-10.3127}
	BBl[][22]  = {31,0.0426745,-0.873385,6.48862,-20.4910,21.7100}
	BBl[][23]  = {32,0.0363508,-0.723295,5.15283,-15.1979,13.6856}
	BBl[][24]  = {33,0.0972160,-1.97660,14.8424,-48.5597,56.9081}
	BBl[][25]  = {34,0.147254,-2.99145,22.5168,-74.1561,88.4170}
	BBl[][26]  = {37,0.160292,-3.36544,26.2354,-89.7346,111.813}
	BBl[][27]  = {38,0.0824084,-1.83184,15.0526,-54.1232,70.4365}
	BBl[][28]  = {39,0.0537647,-1.30343,11.5417,-44.4399,61.7528}
	BBl[][29]  = {40,-0.0157785,0.116291,0.719417,-7.98106,16.2535}
	BBl[][30]  = {41,-0.0959522,1.68938,-10.7479,28.7400,-27.1105}
	BBl[][31]  = {42,-0.133695,2.41733,-15.9511,45.0397,-45.9555}
	BBl[][32]  = {44,-0.268101,5.08685,-35.6917,109.390,-123.872}
	BBl[][33]  = {45,-0.347868,6.68457,-47.6128,148.629,-171.929}
	BBl[][34]  = {46,-0.461526,8.98274,-64.9176,206.118,-243.047}
	BBl[][35]  = {47,-0.516941,10.1258,-73.6902,235.740,-280.082}
	BBl[][36]  = {48,-0.524072,10.2220,-74.0488,235.758,-278.910}
	BBl[][37]  = {49,-0.519358,10.1271,-73.3561,233.615,-276.574}
	BBl[][38]  = {50,-0.507010,9.85023,-71.0424,225.052,-264.785}
	BBl[][39]  = {51,-0.381519,7.28500,-51.4807,159.189,-182.274}
	BBl[][40]  = {52,-0.306991,5.75613,-39.7809,119.647,-132.622}
	BBl[][41]  = {55,0.0471284,-1.27043,12.0272,-48.1165,67.5435}
	BBl[][42]  = {56,0.0810129,-1.85552,15.6444,-57.4413,76.0977}
	BBl[][43]  = {57,0.113981,-2.48980,20.1780,-71.7293,93.0508}
	BBl[][44]  = {58,0.0313629,-0.983089,10.1285,-42.8817,63.0813}
	BBl[][45]  = {59,-0.00513467,-0.314879,5.63446,-29.7544,49.0222}
	BBl[][46]  = {60,-0.0598100,0.714981,-1.53368,-7.93307,24.5069}
	BBl[][47]  = {62,-0.206909,3.54527,-21.7171,55.2311,-48.6810}
	BBl[][48]  = {63,-0.281170,4.97906,-31.9909,87.6581,-87.0598}
	BBl[][49]  = {64,-0.281435,5.00189,-32.2791,88.9155,-88.6444}
	BBl[][50]  = {65,-0.449447,8.31002,-56.4360,166.348,-180.580}
	BBl[][51]  = {66,-0.521431,9.77130,-67.4443,202.774,-225.167}
	BBl[][52]  = {67,-0.559899,10.5781,-73.6900,223.895,-251.419}
	BBl[][53]  = {68,-0.595402,11.3362,-79.6457,244.292,-277.122}
	BBl[][54]  = {69,-0.614548,11.7827,-83.3976,257.825,-294.877}
	BBl[][55]  = {70,-0.616102,11.8856,-84.6530,263.429,-303.632}
	BBl[][56]  = {71,-0.573951,11.1192,-79.5119,248.277,-286.708}
	BBl[][57]  = {72,-0.570629,11.1690,-80.8279,255.918,-299.989}
	BBl[][58]  = {73,-0.544310,10.7093,-77.8877,247.752,-291.614}
	BBl[][59]  = {74,-0.487029,9.63749,-70.4453,224.985,-265.552}
	BBl[][60]  = {75,-0.447095,8.91398,-65.5969,210.710,-249.890}
	BBl[][61]  = {76,-0.387886,7.82795,-58.2689,189.208,-226.711}
	BBl[][62]  = {77,-0.330386,6.75025,-50.7628,166.160,-200.373}
	BBl[][63]  = {78,-0.279431,5.82691,-44.5739,147.985,-180.693}
	BBl[][64]  = {79,-0.244271,5.20544,-40.5584,136.776,-169.364}
	BBl[][65]  = {80,-0.161144,3.55574,-28.3393,96.7173,-120.476}
	BBl[][66]  = {81,-0.0616630,1.55623,-13.5041,48.6839,-63.0815}
	BBl[][67]  = {82,-0.0102176,0.533361,-5.95260,24.1189,-33.3712}
	BBl[][68]  = {83,-0.0105232,0.580013,-6.64755,27.6422,-39.4006}
	BBl[][69]  = {90,0.172966,-3.08898,20.3828,-59.1574,63.3258}
	BBl[][70]  = {91,0.239767,-4.44183,30.5600,-92.8151,104.717}
	BBl[][71]  = {92,0.275429,-5.16597,36.0218,-110.938,127.095}
	BB0 = BBl[5][p]
	BB1 = BBl[4][p]
	BB2 = BBl[3][p]
	BB3 = BBl[2][p]
	BB4 = BBl[1][p]
	ZZ 	 = BBl[0][p]
else
	//- high energy 300 to 30000 eV
	BBh[][0]  = {3,-0.000136568,0.00625508,-0.0992549,-0.271204,0.915338}
	BBh[][1]  = {4,-0.000635992,0.0245901,-0.350334,1.23580,-1.58940}
	BBh[][2]  = {5,-0.000842780,0.0338556,-0.502572,2.32816,-3.81717}
	BBh[][3]  = {6,-0.000628812,0.0276269,-0.440442,2.10318,-3.80774}
	BBh[][4]  = {11,0.000836224,-0.0182506,0.0621377,-0.00720480,-1.23196}
	BBh[][5]  = {12,0.00114464,-0.0289963,0.198268,-0.743130,0.393844}
	BBh[][6]  = {13,0.00130765,-0.0347447,0.269098,-1.08588,1.13465}
	BBh[][7]  = {14,0.00133498,-0.0363267,0.295263,-1.24109,1.46282}
	BBh[][8]  = {15,0.00131294,-0.0366402,0.313029,-1.41056,1.84731}
	BBh[][9]  = {16,0.00121393,-0.0340349,0.288730,-1.30593,1.66297}
	BBh[][10]  = {19,0.000580830,-0.0139159,0.0472588,0.0460997,-1.33420}
	BBh[][11]  = {20,0.000505184,-0.0117620,0.0254963,0.135095,-1.32592}
	BBh[][12]  = {21,0.000625446,-0.0166234,0.0976114,-0.322370,-0.220276}
	BBh[][13]  = {22,0.000584718,-0.0152732,0.0779807,-0.163595,-0.516668}
	BBh[][14]  = {23,0.000610372,-0.0161721,0.0869271,-0.169289,-0.464858}
	BBh[][15]  = {24,0.000475843,-0.0114796,0.0230011,0.249075,-1.43169}
	BBh[][16]  = {25,0.000416911,-0.00948937,-0.00252269,0.402944,-1.76117}
	BBh[][17]  = {26,0.000350272,-0.00725271,-0.0322039,0.602338,-2.24485}
	BBh[][18]  = {27,0.000251664,-0.00391397,-0.0733290,0.820898,-2.68993}
	BBh[][19]  = {28,0.000135339,0.000160260,-0.127790,1.16102,-3.47813}
	BBh[][20]  = {29,-0.000115723,0.00893192,-0.243923,1.86419,-5.06120}
	BBh[][21]  = {30,-0.000503267,0.0224182,-0.417007,2.83572,-7.21741}
	BBh[][22]  = {31,-0.000540466,0.0238501,-0.439353,3.01327,-7.71668}
	BBh[][23]  = {32,-0.000787213,0.0326130,-0.552985,3.64407,-9.11578}
	BBh[][24]  = {33,-0.000657054,0.0281739,-0.497071,3.34533,-8.52482}
	BBh[][25]  = {34,-0.000947451,0.0384525,-0.630577,4.09524,-10.2347}
	BBh[][26]  = {37,-0.00102912,0.0417636,-0.681912,4.48205,-11.3746}
	BBh[][27]  = {38,-0.000658364,0.0289398,-0.516375,3.53942,-9.31848}
	BBh[][28]  = {39,-0.000332499,0.0175528,-0.368060,2.69355,-7.48835}
	BBh[][29]  = {40,0.0000656958,0.00343061,-0.183568,1.65560,-5.15635}
	BBh[][30]  = {41,0.000262099,-0.00371291,-0.0876335,1.10567,-3.95339}
	BBh[][31]  = {42,0.000220009,-0.00216694,-0.108447,1.22958,-4.24354}
	BBh[][32]  = {44,0.000198219,-0.00149555,-0.116052,1.27746,-4.36274}
	BBh[][33]  = {45,0.000113858,0.00146526,-0.154484,1.49852,-4.85347}
	BBh[][34]  = {46,-0.000146258,0.0104855,-0.271160,2.17077,-6.32677}
	BBh[][35]  = {47,-0.000101900,0.00913328,-0.254718,2.06741,-6.09358}
	BBh[][36]  = {48,-0.000465412,0.0220319,-0.425325,3.06501,-8.33008}
	BBh[][37]  = {49,-0.000399006,0.0194320,-0.388948,2.86591,-7.93286}
	BBh[][38]  = {50,-0.000452025,0.0216375,-0.420368,3.03927,-8.33900}
	BBh[][39]  = {51,-0.000533410,0.0243112,-0.454742,3.25873,-8.82393}
	BBh[][40]  = {52,-0.000663805,0.0290690,-0.518323,3.62438,-9.66636}
	BBh[][41]  = {55,-0.00131945,0.0523063,-0.825336,5.43490,-13.9308}
	BBh[][42]  = {56,-0.000671987,0.0290540,-0.515181,3.62218,-9.71549}
	BBh[][43]  = {57,-0.000353329,0.0174584,-0.358577,2.70145,-7.60216}
	BBh[][44]  = {58,-0.000919257,0.0379466,-0.632746,4.29062,-11.1696}
	BBh[][45]  = {59,-0.00117074,0.0467115,-0.747133,4.95809,-12.6347}
	BBh[][46]  = {60,-0.00142949,0.0557254,-0.864760,5.64468,-14.1379}
	BBh[][47]  = {62,-0.00200480,0.0757835,-1.12677,7.17489,-17.4901}
	BBh[][48]  = {63,-0.00275117,0.102306,-1.47825,9.23133,-22.1124}
	BBh[][49]  = {64,-0.00246348,0.0917281,-1.33532,8.41095,-20.2580}
	BBh[][50]  = {65,-0.00324146,0.119178,-1.69682,10.5160,-24.9135}
	BBh[][51]  = {66,-0.00350231,0.128245,-1.81510,11.2076,-26.4037}
	BBh[][52]  = {67,-0.00375903,0.137185,-1.93197,11.8902,-27.8587}
	BBh[][53]  = {68,-0.00414213,0.150651,-2.10935,12.9335,-30.1634}
	BBh[][54]  = {69,-0.00452894,0.164277,-2.28924,13.9942,-32.5124}
	BBh[][55]  = {70,-0.00535462,0.193888,-2.68600,16.3471,-37.8336}
	BBh[][56]  = {71,-0.00471067,0.171227,-2.38911,14.6415,-34.0913}
	BBh[][57]  = {72,-0.00392733,0.142753,-2.00426,12.3730,-28.9735}
	BBh[][58]  = {73,-0.00377466,0.137798,-1.94486,12.0634,-28.3889}
	BBh[][59]  = {74,-0.00348698,0.127586,-1.80990,11.2839,-26.6770}
	BBh[][60]  = {75,-0.00340964,0.125210,-1.78343,11.1616,-26.5001}
	BBh[][61]  = {76,-0.00316857,0.116372,-1.66297,10.4491,-24.8975}
	BBh[][62]  = {77,-0.00323887,0.119307,-1.70779,10.7481,-25.6644}
	BBh[][63]  = {78,-0.00362155,0.132792,-1.88546,11.7886,-27.9668}
	BBh[][64]  = {79,-0.00371000,0.136587,-1.94449,12.1866,-29.0046}
	BBh[][65]  = {80,-0.00378409,0.141385,-2.03655,12.8729,-30.9562}
	BBh[][66]  = {81,-0.00309462,0.114898,-1.65990,10.5617,-25.5235}
	BBh[][67]  = {82,-0.00265405,0.101073,-1.50065,9.75846,-24.1088}
	BBh[][68]  = {83,-0.00217872,0.0870326,-1.35170,9.08305,-23.2124}
	BBh[][69]  = {90,0.00111852,-0.0284192,0.158201,0.378282,-4.35326}
	BBh[][70]  = {91,0.00100171,-0.0263101,0.157725,0.242452,-3.65343}
	BBh[][71]  = {92,0.00108319,-0.0298543,0.213852,-0.133319,-2.69391}
	BB0 = BBh[5][p]
	BB1 = BBh[4][p]
	BB2 = BBh[3][p]
	BB3 = BBh[2][p]
	BB4 = BBh[1][p]
	ZZ 	 = BBh[0][p]
endif
	
// Interpolate them
make /d/o/n=1 Zi, Bi
Zi = Z
interpolate2 /i=3/t=2/x=Zi/y=Bi ZZ, BB0
BB[0] = Bi[0]
interpolate2 /i=3/t=2/x=Zi/y=Bi ZZ, BB1
BB[1]  = Bi[0]
interpolate2 /i=3/t=2/x=Zi/y=Bi ZZ, BB2
BB[2]  = Bi[0] 
interpolate2 /i=3/t=2/x=Zi/y=Bi ZZ, BB3
BB[3]  = Bi[0]
interpolate2 /i=3/t=2/x=Zi/y=Bi ZZ, BB4
BB[4]  = Bi[0]
  
// Clean
killwaves /z BBl, BBh
killwaves /z  ZZ, BB0, BB1, BB2, BB3, BB4, Zi, Bi

return 0

End




