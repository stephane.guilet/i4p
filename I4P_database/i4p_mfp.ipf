// Module : i4p_mfp
// Author : R�mi Lazzari, remi.lazzari@insp.jussieu.fr
// Date : 2019-2020
// Usage : calculating and plotting mean-free paths of electrons, effective attenuation lengths and surface exitation parameters
// Tanuma, S., Powell, C. & Penn, D. R. Calculations of Electron Inelastic Mean Free Paths. Surface and Interface Analysis 21 (1993) 165�176.
// Also see http://www.quases.com/products/quases-imfp-tpp2m
//  all Jablonski's work on inelastic scattering
// Pauly, N.; Tougaard, S. & Yubero, F., Theoretical determination of the surface excitation parameter from X-ray photoelectron spectroscopy, Surf. Interface Anal., 2006, 38, 672-675


#pragma rtGlobals= 3		
#pragma ModuleName = mfp_calc


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start the MFP panel
Static Function MFPStart() : Panel

if (datafolderexists("root:I4P:I4Pmfp")==0)
	// initialize all
	MFPInit()
	DisplayPanelMFP()
else
	// bring an existing panel forward
	if (strlen(winlist("MFPPanel", ";", "WIN:64"))!=0)
		dowindow/F MFPPanel
	else
		DisplayPanelMFP()
	endif

endif
	
End

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialize the MFP panel
Static Function MFPInit()

string saveDF = getdatafolder(1)

newdatafolder /o root:I4P
newdatafolder /o/S root:I4P:I4Pmfp
string /G list_xray_source = "\"Al;Mg;He I;He II\""
string /G list_choiceIMFP = "\"TPP2M non-relativistic;TPP2M relativistic;Gries\""
string /G list_choiceTRMFP = "\"database 1;database 2;database 3\""
string /G list_choiceCHANDRA = "\"integral;approximative 1;approximative 2;approximative 3;approximative 4\""	
string /G material = "Ag"
string /G xray_source = "Al"	
variable /G hv = ghv_Al
variable /G Md = 0.0973393
variable /G BE = 368.3
variable /G KE = 1118.3
variable /G Alpha = 0
variable /G Psi = 54.7	
variable /G Theta = Psi-Alpha
variable /G aBeta = 1.209		
variable /G Thick = 10	
variable /G IDlim = 95	
string /G  choiceIMFP = "TPP2M non-relativistic"
variable /G typeIMFP = 0
variable /G IMFP =   nan
string /G  choiceTRMFP = "database 2"
variable /G typeTRMFP = 2
variable /G TRMFP =    nan
variable /G ALBEDO =    nan
string /G  choiceCHANDRA = "approximative 3"	
variable /G typeCHANDRA = 3
variable /G MED = nan
variable /G EALb = nan
variable /G EALf =  nan
variable /G EALm =  nan
variable /G SEP =   nan

setdatafolder saveDF
	
return 0

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Create MFP panel 
Static Function  DisplayPanelMFP()

//--size and position of the panel
variable top = 10
variable left =	 10
variable bw = 85
variable bh =	40
variable dfs  = 16
variable x1, y1, x2, y2
//--background color
variable CbackR = 49152
variable CbackG = 49152
variable CbackB = 49152
//--font color
variable CfontR = 0
variable CfontG = 0
variable CfontB = 0
//--value background color
variable CvalR = 65535
variable CvalG = 65535
variable CvalB = 65535

//--global vriables
SVAR gmaterial 			= root:I4P:I4Pmfp:material
NVAR gMd				= root:I4P:I4Pmfp:Md
SVAR glist_xray_source	= root:I4P:I4Pmfp:list_xray_source
SVAR gxray_source		= root:I4P:I4Pmfp:xray_source
NVAR ghv				= root:I4P:I4Pmfp:hv
NVAR gKE				= root:I4P:I4Pmfp:KE
NVAR gBE				= root:I4P:I4Pmfp:BE
NVAR gAlpha				= root:I4P:I4Pmfp:Alpha
NVAR gPsi				= root:I4P:I4Pmfp:Psi
NVAR gTheta				= root:I4P:I4Pmfp:Theta
NVAR gaBeta			= root:I4P:I4Pmfp:aBeta
NVAR gThick				= root:I4P:I4Pmfp:Thick
NVAR gIDlim				= root:I4P:I4Pmfp:IDlim
SVAR glist_choiceIMFP	= root:I4P:I4Pmfp:list_choiceIMFP
SVAR gchoiceIMFP		= root:I4P:I4Pmfp:choiceIMFP
NVAR gIMFP				= root:I4P:I4Pmfp:IMFP
SVAR glist_choiceTRMFP	= root:I4P:I4Pmfp:list_choiceTRMFP
SVAR gchoiceTRMFP		= root:I4P:I4Pmfp:choiceTRMFP
NVAR gTRMFP			= root:I4P:I4Pmfp:TRMFP
NVAR gALBEDO			= root:I4P:I4Pmfp:ALBEDO
SVAR glist_choiceCHANDRA	= root:I4P:I4Pmfp:list_choiceCHANDRA
SVAR gchoiceCHANDRA	= root:I4P:I4Pmfp:choiceCHANDRA
NVAR gMED				= root:I4P:I4Pmfp:MED
NVAR gEALb				= root:I4P:I4Pmfp:EALb
NVAR gEALf				= root:I4P:I4Pmfp:EALf
NVAR gEALm			= root:I4P:I4Pmfp:EALm
NVAR gSEP				= root:I4P:I4Pmfp:SEP

//--create the panel
NewPanel/K=1/W=(top,left,top+10.3*bw,left+15*bh)
ModifyPanel cbRGB=(CbackR,CbackG,CbackB), fixedSize=1
DoWindow/C/T MFPPanel,"MEAN FREE PATHS, EFFECTIVE ATTENUATION LENGTHS, MEAN ESCAPE DEPTH, EMISSION DEPTH DISTRIBUTION FUNCTION"
DefaultGUIControls/W=MFPPanel native

///////////////////////////////////////////////////////////////////////////////////
// User data
Setvariable MFP_material,pos={left+bw,top+0.2*bh},size={3.5*bw,bh},proc=MFPGet_Material,title="Material"
Setvariable MFP_material,help={"Enter the type of material."},fSize=dfs, fColor=(CfontR,CfontG,CfontB)
Setvariable MFP_material,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gmaterial

Setvariable MFP_Md,pos={left+bw,top+1.2*bh},size={3.5*bw,bh},noproc,title="Molar density (mol/cm^3)",limits={0,inf,0}
Setvariable MFP_Md,help={"Molar density of the material (mol/cm^3)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.4f"
Setvariable MFP_Md,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gMd

PopupMenu MFP_xraysource,pos={left+bw,top+2.2*bh},size={3.5*bw,bh},proc=MFPChangeSource,title="X-ray source"
PopupMenu MFP_xraysource,help={"Select the type of X-ray source."},fSize=dfs, fColor=(CfontR,CfontG,CfontB)
PopupMenu MFP_xraysource,mode=1,bodywidth=2*bw,popvalue=gxray_source,value=#glist_xray_source

Setvariable MFP_KE,pos={left+bw,top+3.2*bh},size={3.5*bw,bh},proc=MFPGet_KE,title="Kinetic energy (eV)",limits={0,inf,1}
Setvariable MFP_KE,help={"Enter the kinetic energy of photoelectron (eV)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.2f"
Setvariable MFP_KE,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gKE

Setvariable MFP_BE,pos={left+bw,top+4.2*bh},size={3.5*bw,bh},proc=MFPGet_BE,title="Binding energy (eV)",limits={0,inf,1}
Setvariable MFP_BE,help={"Enter the binding energy of photoelectron (eV)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.2f"
Setvariable MFP_BE,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gBE

Setvariable MFP_Alpha,pos={left+bw,top+5.2*bh},size={3.5*bw,bh},proc=MFPGet_Alpha,title="Normal/Analyzer angle (deg)",limits={0,90,1}
Setvariable MFP_Alpha,help={"Enter the angle between sample normal and analyzer (deg)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.2f"
Setvariable MFP_Alpha,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gAlpha

Setvariable MFP_Psi,pos={left+bw,top+6.2*bh},size={3.5*bw,bh},proc=MFPGet_Psi,title="Analyzer/X-ray angle (deg)",limits={0,180,1}
Setvariable MFP_Psi,help={"Enter the angle between analyzer and X-ray source (deg)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.2f"
Setvariable MFP_Psi,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gPsi

Setvariable MFP_Theta,pos={left+bw,top+7.2*bh},size={3.5*bw,bh},proc=MFPGet_Theta,title="Normal/X-ray angle (deg)",limits={0,180,1}
Setvariable MFP_Theta,help={"Enter the angle between sample normal and X-ray source  (deg)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.2f"
Setvariable MFP_Theta,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gTheta

Setvariable MFP_aBeta,pos={left+bw,top+8.2*bh},size={3.5*bw,bh},proc=MFPGet_aBeta,title="Asymmetry facor",limits={0,2,0.01}
Setvariable MFP_aBeta,help={"Enter the asymmetry factor of core level."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.3f"
Setvariable MFP_aBeta,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gaBeta

Setvariable MFP_Thick,pos={left+bw,top+9.2*bh},size={3.5*bw,bh},proc=MFPGet_Thick,title="Thickness/Depth (�)",limits={0,inf,0.5}
Setvariable MFP_Thick,help={"Enter the film thickness or depth of the marker (�)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.2f"
Setvariable MFP_Thick,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gThick

Setvariable MFP_ID,pos={left+bw,top+10.2*bh},size={3.5*bw,bh},proc=MFPGet_ID,title="Information depth up to (%)",limits={0,100,0.1}
Setvariable MFP_ID,help={"Enter the definition of the information depth)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%7.3f"
Setvariable MFP_ID,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gIDlim


SetDrawEnv lineThick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 =top+0*bw
y1= left-5+0.2*bh
x2 =  top+4.8*bw
y2 = left+ 11*bh
DrawRRect  x1, y1, x2, y2


///////////////////////////////////////////////////////////////////////////////////
// IMFP/TRMFP/ALBEDAO
PopupMenu MFP_choiceIMFP,pos={left+5.2*bw,top+0.2*bh},size={4.5*bw,bh},proc=MFPChangechoiceIMFP,title="IMFP source"
PopupMenu MFP_choiceIMFP,help={"Select the type of IMFP source."},fSize=dfs, fColor=(CfontR,CfontG,CfontB)
PopupMenu MFP_choiceIMFP,mode=1,bodywidth=2*bw,popvalue=gchoiceIMFP,value=#glist_choiceIMFP

Setvariable MFP_I,pos={left+5.2*bw,top+1.2*bh},size={4.5*bw,bh},noproc,title="IMFP (�)",limits={0,inf,0}
Setvariable MFP_I,help={"Estimated inelastic mean free path (�)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.3f"
Setvariable MFP_I,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gIMFP

PopupMenu MFP_choiceTRMFP,pos={left+5.2*bw,top+2.2*bh},size={4.5*bw,bh},proc=MFPChangechoiceTRMFP,title="TRMFP source"
PopupMenu MFP_choiceTRMFP,help={"Select the type of TRMFP source."},fSize=dfs, fColor=(CfontR,CfontG,CfontB)
PopupMenu MFP_choiceTRMFP,mode=1,bodywidth=2*bw,popvalue=gchoiceTRMFP,value=#glist_choiceTRMFP

Setvariable MFP_TR,pos={left+5.2*bw,top+3.2*bh},size={4.5*bw,bh},noproc,title="TRMFP (�)",limits={0,inf,0}
Setvariable MFP_TR,help={"Calculated transport mean free path (�)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.3f"
Setvariable MFP_TR,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gTRMFP

Setvariable MFP_ALBEDO,pos={left+5.2*bw,top+4.2*bh},size={4.5*bw,bh},noproc,title="ALBEDO",limits={0,inf,0}
Setvariable MFP_ALBEDO,help={"Calculated albedo."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.4f"
Setvariable MFP_ALBEDO,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gALBEDO

SetDrawEnv lineThick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 =top+5.8*bw
y1= left-5 + 0.2*bh
x2 =  top+9.9*bw
y2 = left+ 5*bh
DrawRRect  x1, y1, x2, y2



///////////////////////////////////////////////////////////////////////////////////
// VARIOUS

PopupMenu MFP_choiceCHANDRA,pos={left+5.2*bw,top+6*bh},size={4.5*bw,bh},proc=MFPChangechoiceCHANDRA,title="Chandrasekhar function"
PopupMenu MFP_choiceCHANDRA,help={"Select the type of formula for Chandrasekhar function."},fSize=dfs, fColor=(CfontR,CfontG,CfontB)
PopupMenu MFP_choiceCHANDRA,mode=1,bodywidth=2*bw,popvalue=gchoiceCHANDRA,value=#glist_choiceCHANDRA

Setvariable MFP_MED,pos={left+5.2*bw,top+7*bh},size={4.5*bw,bh},noproc,title="MED (�)",limits={0,inf,0}
Setvariable MFP_MED,help={"Calculated mean escape depth (�)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.3f"
Setvariable MFP_MED,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gMED

Setvariable MFP_EALb,pos={left+5.2*bw,top+8*bh},size={4.5*bw,bh},noproc,title="EAL (bulk) (�)",limits={0,inf,0}
Setvariable MFP_EALb,help={"Calculated effective attenuation length of bulk (�)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.3f"
Setvariable MFP_EALb,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gEALb

Setvariable MFP_EALf,pos={left+5.2*bw,top+9*bh},size={4.5*bw,bh},noproc,title="Practical EAL (film) (�)",limits={0,inf,0}
Setvariable MFP_EALf,help={"Calculated practical effective attenuation length of film (�)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.3f"
Setvariable MFP_EALf,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gEALf

Setvariable MFP_EALm,pos={left+5.2*bw,top+10*bh},size={4.5*bw,bh},noproc,title="Practical EAL (marker) (�)",limits={0,inf,0}
Setvariable MFP_EALm,help={"Calculated practical effective attenuation length of marker (�)."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.3f"
Setvariable MFP_EALm,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gEALm

Setvariable MFP_SEP,pos={left+5.2*bw,top+11*bh},size={4.5*bw,bh},noproc,title="SEP",limits={0,inf,0}
Setvariable MFP_SEP,help={"Calculated surface excitation parameter."},fSize=dfs,fColor=(CfontR,CfontG,CfontB), format="%8.4f"
Setvariable MFP_SEP,bodywidth=2*bw, valueBackColor=(CvalR,CvalG,CvalB), valueColor=(0,0,0), frame=1, disable=0, value=gSEP

SetDrawEnv lineThick=2.5, linebgc= (0,0,0),fillfgc= (CbackR,CbackG,CbackB),fillbgc= (CbackR,CbackG,CbackB)
x1 =top+5.2*bw
y1= left-5 + 5.8*bh
x2 =  top+9.9*bw
y2 = left+ 11.8*bh
DrawRRect  x1, y1, x2, y2


///////////////////////////////////////////////////////////////////////////////////
// PLOT
Button MFP_plot_MFP,pos={left+1.7*bw,top+12.2*bh},size={3.1*bw,bh},fColor=(65535,65535,65535),fsize=dfs,proc=MFPButtonPlot_MFP,title="Plot mean free paths vs energy"
Button MFP_plot_MFP,help={"Plot mean free paths for the selected material versus energy."},fSize=dfs

Button MFP_plot_EAL,pos={left+4.9*bw,top+12.2*bh},size={3.1*bw,bh},fColor=(65535,65535,65535),fsize=dfs,proc=MFPButtonPlot_EAL,title="Plot practical EAL vs thickness"
Button MFP_plot_EAL,help={"Plot practical effective attenuation lengths for the selected material versus thickness."},fSize=dfs

Button MFP_plot_EDDF,pos={left+1.7*bw,top+13.4*bh},size={3.1*bw,bh},fColor=(65535,65535,65535),fsize=dfs,proc=MFPButtonPlot_EDDF,title="Plot EDDF vs depth"
Button MFP_plot_EDDF,help={"Plot emission depth distribution function versus depth."},fSize=dfs

Button MFP_plot_CF,pos={left+4.9*bw,top+13.4*bh},size={3.1*bw,bh},fColor=(65535,65535,65535),fsize=dfs,proc=MFPButtonPlot_CF,title="Plot CF vs depth"
Button MFP_plot_CF,help={"Plot CF function versus depth."},fSize=dfs

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Change X-ray source  MFPPanel
Function MFPChangeSource(pa) : PopupMenuControl

STRUCT WMPopupAction &pa

SVAR gxray_source		= root:I4P:I4Pmfp:xray_source
NVAR gKE				= root:I4P:I4Pmfp:KE
NVAR gBE				= root:I4P:I4Pmfp:BE
NVAR ghv				= root:I4P:I4Pmfp:hv

// wait for mouse up event
switch(pa.eventCode)
case 2:
	// store current states
	gxray_source = pa.popStr
	strswitch(gxray_source)
	case "Al":
		ghv = ghv_Al
	break
	case "Mg":
		ghv = ghv_Mg
	break
	case "He I":
		ghv = ghv_HeI
	break
	case "He II":	
		ghv = ghv_HeII
	break
	endswitch
	gKE = Max(ghv - gBE,0)	
break
endswitch

CalculateMFPPanel()
	
End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Change type of IMFP equation MFPPanel
Function MFPChangechoiceIMFP(pa) : PopupMenuControl

STRUCT WMPopupAction &pa

SVAR gchoiceIMFP		= root:I4P:I4Pmfp:choiceIMFP
NVAR gtypeIMFP			= root:I4P:I4Pmfp:typeIMFP

// wait for mouse up event
switch(pa.eventCode)
case 2:
	// store current states
	gchoiceIMFP = pa.popStr
	strswitch(gchoiceIMFP)
	case "TPP2M non-relativistic":
		gtypeIMFP = 0
	break
	case "TPP2M relativistic":
		gtypeIMFP = 1
	break
	case "Gries":
		gtypeIMFP = 2
	break
	endswitch
break
endswitch

CalculateMFPPanel()
	
End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Change type of TRMFP equation MFPPanel
Function MFPChangechoiceTRMFP(pa) : PopupMenuControl

STRUCT WMPopupAction &pa

SVAR gchoiceTRMFP	 = root:I4P:I4Pmfp:choiceTRMFP
NVAR gtypeTRMFP	= root:I4P:I4Pmfp:typeTRMFP

// wait for mouse up event
switch(pa.eventCode)
case 2:
	// store current states
	gchoiceTRMFP = pa.popStr
	strswitch(gchoiceTRMFP)
	case "database 1":
		gtypeTRMFP = 1
	break
	case "database 2":
		gtypeTRMFP= 2
	break
	case "database 3":
		gtypeTRMFP= 3
	break
	endswitch
break
endswitch

CalculateMFPPanel()
	
End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Change type of Chandrasekhar function MFPPanel
Function MFPChangechoiceCHANDRA(pa) : PopupMenuControl

STRUCT WMPopupAction &pa

SVAR gchoiceCHANDRA	 = root:I4P:I4Pmfp:choiceCHANDRA
NVAR gtypeCHANDRA	 = root:I4P:I4Pmfp:typeCHANDRA

// wait for mouse up event
switch(pa.eventCode)
case 2:
	// store current states
	gchoiceCHANDRA = pa.popStr
	strswitch(gchoiceCHANDRA)
	case "integral":
		gtypeCHANDRA = 0
	break
	case "approximative 1":
		gtypeCHANDRA= 1
	break
	case "approximative 2":
		gtypeCHANDRA= 2
	break
	case "approximative 3":
		gtypeCHANDRA= 3
	break
	case "approximative 4":
		gtypeCHANDRA= 4
	break
endswitch	
break
endswitch

CalculateMFPPanel()
	
End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read the material name
Function MFPGet_Material(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

SVAR gmaterial	= root:I4P:I4Pmfp:material

gmaterial = varStr
	
CalculateMFPPanel()

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read the kinetic energy
Function MFPGet_KE(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

NVAR gKE	= root:I4P:I4Pmfp:KE
NVAR gBE	= root:I4P:I4Pmfp:BE
NVAR ghv	= root:I4P:I4Pmfp:hv

gKE = Abs(varNum)
gBE = Max(ghv - gKE,0)	
	
CalculateMFPPanel()

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read the binding energy
Function MFPGet_BE(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

NVAR gKE	= root:I4P:I4Pmfp:KE
NVAR gBE	= root:I4P:I4Pmfp:BE
NVAR ghv	= root:I4P:I4Pmfp:hv

gBE = Min(Abs(varNum),ghv)
gKE = ghv - gBE

CalculateMFPPanel()

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read the angle between sample normal and analyzer angle (emission angle)
Function MFPGet_Alpha(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

NVAR gAlpha	= root:I4P:I4Pmfp:Alpha

gAlpha = Min(Max(Abs(varNum),0),90)

CalculateMFPPanel()

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read the angle between X-ray source and analyzer
Function MFPGet_Psi(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

NVAR gPsi	= root:I4P:I4Pmfp:Psi

gPsi = Min(Max(Abs(varNum),0),180)

CalculateMFPPanel()

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read the angle between X-ray source and analyzer
Function MFPGet_Theta(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

NVAR gTheta	= root:I4P:I4Pmfp:Theta

gTheta = Min(Max(Abs(varNum),0),180)

CalculateMFPPanel()

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read the asymmetry parameter of the core level
Function MFPGet_aBeta(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

NVAR gaBeta	= root:I4P:I4Pmfp:aBeta

gaBeta = Min(Max(Abs(varNum),0),2.0)

CalculateMFPPanel()

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read the film Thickness
Function MFPGet_Thick(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

NVAR gThick	= root:I4P:I4Pmfp:Thick

gThick = Max(Abs(gThick),0)

CalculateMFPPanel()

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read the information depth limit
Function MFPGet_ID(ctrlName,varNum,varStr,varName) : SetVariableControl

String ctrlName
Variable varNum	// value of variable as number
String varStr		// value of variable as string
String varName	// name of variable

NVAR gIDlim	= root:I4P:I4Pmfp:IDlim

gIDlim = Min(Max(Abs(gIDlim),0),100)

CalculateMFPPanel()

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function MFPButtonPlot_MFP(bs) : ButtonControl

STRUCT WMButtonAction &bs

SVAR gmaterial 	= root:I4P:I4Pmfp:material
NVAR gKE		= root:I4P:I4Pmfp:KE
NVAR gtypeIMFP	= root:I4P:I4Pmfp:typeIMFP
NVAR gtypeTRMFP= root:I4P:I4Pmfp:typeTRMFP
NVAR gtypeChandra = root:I4P:I4Pmfp:typeChandra
NVAR gAlpha		= root:I4P:I4Pmfp:Alpha
NVAR gPsi		= root:I4P:I4Pmfp:Psi
NVAR gTheta		= root:I4P:I4Pmfp:Theta
NVAR gaBeta	= root:I4P:I4Pmfp:aBeta
NVAR gThick		= root:I4P:I4Pmfp:Thick

switch(bs.eventCode)
case 2: // mouse up
	string saveDF = getdatafolder(1)
	setdatafolder root:I4P:I4Pmfp
	Plot_Material2MFPvsKE(gmaterial,50, Max(gKE,100), gKE/125,gAlpha, gPsi,gTheta,gaBeta, gThick,gtypeIMFP,gtypeTRMFP,gtypeChandra)
	setdatafolder saveDF
break
endswitch
	
end


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function MFPButtonPlot_EAL(bs) : ButtonControl

STRUCT WMButtonAction &bs

SVAR gmaterial 	= root:I4P:I4Pmfp:material
NVAR gKE		= root:I4P:I4Pmfp:KE
NVAR gtypeIMFP	= root:I4P:I4Pmfp:typeIMFP
NVAR gtypeTRMFP= root:I4P:I4Pmfp:typeTRMFP
NVAR gtypeChandra = root:I4P:I4Pmfp:typeChandra
NVAR gAlpha		= root:I4P:I4Pmfp:Alpha
NVAR gPsi		= root:I4P:I4Pmfp:Psi
NVAR gTheta		= root:I4P:I4Pmfp:Theta
NVAR gaBeta	= root:I4P:I4Pmfp:aBeta
NVAR gIDlim		= root:I4P:I4Pmfp:IDlim

switch(bs.eventCode)
case 2: // mouse up
	string saveDF = getdatafolder(1)
	setdatafolder root:I4P:I4Pmfp
	Plot_Material2EALvsThick(gmaterial,gKE,gAlpha, gPsi,gTheta,gaBeta,gtypeIMFP,gtypeTRMFP,gtypeChandra,gIDlim)
	setdatafolder saveDF
break
endswitch
	
end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function MFPButtonPlot_EDDF(bs) : ButtonControl

STRUCT WMButtonAction &bs

SVAR gmaterial 	= root:I4P:I4Pmfp:material
NVAR gKE		= root:I4P:I4Pmfp:KE
NVAR gtypeIMFP	= root:I4P:I4Pmfp:typeIMFP
NVAR gtypeTRMFP= root:I4P:I4Pmfp:typeTRMFP
NVAR gtypeChandra = root:I4P:I4Pmfp:typeChandra
NVAR gAlpha		= root:I4P:I4Pmfp:Alpha
NVAR gPsi		= root:I4P:I4Pmfp:Psi
NVAR gTheta		= root:I4P:I4Pmfp:Theta
NVAR gaBeta	= root:I4P:I4Pmfp:aBeta
NVAR gThick		= root:I4P:I4Pmfp:Thick

switch(bs.eventCode)
case 2: // mouse up
	string saveDF = getdatafolder(1)
	setdatafolder root:I4P:I4Pmfp
	Plot_Material2EDDFvsDepth(gmaterial, 0, gThick, gThick/50, gKE, gAlpha, gPsi, gTheta, gaBeta, gtypeIMFP,gtypeTRMFP,gtypeChandra)
	setdatafolder saveDF
break
endswitch
	
end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function MFPButtonPlot_CF(bs) : ButtonControl

STRUCT WMButtonAction &bs

SVAR gmaterial 	= root:I4P:I4Pmfp:material
NVAR gKE		= root:I4P:I4Pmfp:KE
NVAR gtypeIMFP	= root:I4P:I4Pmfp:typeIMFP
NVAR gtypeTRMFP= root:I4P:I4Pmfp:typeTRMFP
NVAR gtypeChandra = root:I4P:I4Pmfp:typeChandra
NVAR gAlpha		= root:I4P:I4Pmfp:Alpha
NVAR gPsi		= root:I4P:I4Pmfp:Psi
NVAR gTheta		= root:I4P:I4Pmfp:Theta
NVAR gaBeta	= root:I4P:I4Pmfp:aBeta
NVAR gThick		= root:I4P:I4Pmfp:Thick

switch(bs.eventCode)
case 2: // mouse up
	string saveDF = getdatafolder(1)
	setdatafolder root:I4P:I4Pmfp
	Plot_Material2CFvsDepth(gmaterial, 0, gThick, gThick/50, gKE, gAlpha, gPsi, gTheta, gaBeta, gtypeIMFP,gtypeTRMFP,gtypeChandra)
	setdatafolder saveDF
break
endswitch
	
end




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate all mean free paths, effective attenuation lengths and surface excitation parameter
Static Function CalculateMFPPanel()

SVAR gmaterial 	= root:I4P:I4Pmfp:material
NVAR gMd		= root:I4P:I4Pmfp:Md
NVAR gKE		= root:I4P:I4Pmfp:KE
NVAR gAlpha		= root:I4P:I4Pmfp:Alpha
NVAR gPsi		= root:I4P:I4Pmfp:Psi
NVAR gTheta		= root:I4P:I4Pmfp:Theta
NVAR gaBeta	= root:I4P:I4Pmfp:aBeta
NVAR gThick		= root:I4P:I4Pmfp:Thick
NVAR gtypeIMFP	= root:I4P:I4Pmfp:typeIMFP
NVAR gtypeTRMFP= root:I4P:I4Pmfp:typeTRMFP
NVAR gIMFP		= root:I4P:I4Pmfp:IMFP
NVAR gTRMFP	= root:I4P:I4Pmfp:TRMFP
NVAR gALBEDO	= root:I4P:I4Pmfp:ALBEDO
NVAR gtypeCHANDRA = root:I4P:I4Pmfp:typeCHANDRA
NVAR gMED		= root:I4P:I4Pmfp:MED
NVAR gEALb		= root:I4P:I4Pmfp:EALb
NVAR gEALf		= root:I4P:I4Pmfp:EALf
NVAR gEALm	= root:I4P:I4Pmfp:EALm
NVAR gSEP		= root:I4P:I4Pmfp:SEP

// IMFP 
Variable Molar_density
gIMFP = Material2IMFP(gtypeIMFP,gmaterial,gKE,Molar_density=Molar_density)
gMd = Molar_density
// TRMFP from Jablonski's formula
gTRMFP = Material2TRMFP(gtypeIMFP,gtypeTRMFP,gmaterial,gKE)
// ALBEDO
gALBEDO = gIMFP / (gIMFP + gTRMFP)
// MED
gMED = ip4_eal#Get_MED(gtypeCHANDRA,gIMFP,gALBEDO,gAlpha,gPsi,gTheta,gaBeta)
// EAL from bulk
gEALb = ip4_eal#Get_EALb(gtypeCHANDRA,gIMFP,gALBEDO,gAlpha,gPsi,gTheta,gaBeta)
// EAL film
gEALf = ip4_eal#Get_EALf(gtypeCHANDRA,gIMFP,gALBEDO,gAlpha,gPsi,gTheta,gaBeta,gThick)
// EAL marker
gEALm = ip4_eal#Get_EALm(gtypeCHANDRA,gIMFP,gALBEDO,gAlpha,gPsi,gTheta,gaBeta,gThick)
//SEP
gSEP =  Material2SEP(gmaterial,gKE,gAlpha)

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Inelastic mean-free path from the TPP-2M equation and material name
Function Material2IMFP(type,material,Ek,[Xray_source,Molar_density])

variable type
string material
variable Ek
string Xray_source
variable &Molar_density

struct StructIMFP MATIMFP
variable IMFP
variable checkMATIMFP
variable hv

//---IMFP option check
if(type!=0 && type!=1 && type!=2)
	print "==> IMFP option : " + num2str(type) +" not valid ! "
	return nan
endif

//---KE from BE
if(paramisdefault(Xray_source)==0)
	strswitch(Xray_source)	
	case "Al":	
		hv = ghv_Al
	break
	case "Mg":	
		hv = ghv_Mg
	break
	case "He I":	
		hv = ghv_HeI
	break
	case "He II":	
		hv = ghv_HeII
	break				
	default:							
		print "==> X-ray source " + Xray_source +" not valid ! "
		return nan
	endswitch
	Ek = hv - Ek
endif

//--extract information from database
checkMATIMFP =mfp_database#Get_IMFP_Database(material,MATIMFP)

//--if material exists, calculate IMFP
if(checkMATIMFP==1)
	IMFP = Get_IMFP(type,Ek,MATIMFP)
	if(paramisdefault(Molar_density)==0)
		Molar_density = MATIMFP.rho/MATIMFP.M
	endif
	return IMFP
else
	if(paramisdefault(Molar_density)==0)
		Molar_density = nan
	endif
	return nan
endif

return 0

End 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Inelastic mean-free path from predictive formuma
Static Function Get_IMFP(type,Ek,MATIMFP)

variable type, Ek
struct StructIMFP &MATIMFP
variable IMFP

switch(type)
//--TPP2M equation
case 0:
case 1:
	IMFP = Get_IMFP_TPP2M(type, Ek, MATIMFP.rho, MATIMFP.Nv, MATIMFP.M, MATIMFP.Eg)
break
//--Gries equation
case 2:
	IMFP = Get_IMFP_G1(Ek, MATIMFP.rho, MATIMFP.M,MATIMFP.Z,MATIMFP.k1,MATIMFP.k2)
break
endswitch 

return IMFP

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Inelastic mean-free path from the TPP-2M equation
// Calculations of electron inelastic mean free paths. X. Data for 41 elemental solids over the 50eV to 200keV range with the relativistic full Penn algorithm
// H. Shinotsuka, S. Tanuma, C. J. Powell and D. R. Penn,  Surf. Interface Anal. 2015, 47, 871�888
Static Function Get_IMFP_TPP2M(type, Ek, rho, Nv, M, Eg)

variable type   // 0 = non relativistic, 1= relativistic	
variable Ek	// outgoing photoelectron kinetic energy (eV)
variable rho	// bulk density  (g/cm^3)
variable Nv	// number of valence electrons per atom or molecule
variable M	// molar mass (g/mol)
variable Eg	// band-gap  (eV)

variable Ep // Plasmon energy
variable B // eV-1.A-1
variable U  
variable C // A-1
variable D // A-1
variable G // eV-1
variable IMFP  // In Angstroms
variable Alpha // relativistic correction


Ep = 28.816 * sqrt((Nv*rho) / M) 
//B = -0.0216 + (0.944 / sqrt(Ep^2 + Eg^2)) + 7.39e-4*rho //TPP-2
B = -0.10 + 0.944/sqrt(Ep^2 + Eg^2) + 0.069*rho^(0.1) //TPP-2M
//U =  Ep^2 / 829.4 
U = (Nv*rho)/M 
C =  1.97 - 0.91*U
D = 53.4 - 20.8 * U 
G = 0.191 / sqrt(rho)

if(type ==0)
	Alpha = 1
endif
if(type ==1)
	Alpha = (1. + 0.5*Ek/510999) / ((1. + Ek/510999)^2)
endif

IMFP = Alpha*Ek / (Ep^2 * ( B*ln(G*Alpha*Ek) - C/Ek + D/(Ek^2) ))

return IMFP

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Inelastic mean-free path from the Gries equation
// A Universal Predictive Equation for the Inelastic Mean Free Pathlengths of X-ray Photoelectrons and Auger Electron
// W. H. Gries, SURFACE AND INTERFACE ANALYSIS, VOL. 24, 38-50 (1996)
Static Function Get_IMFP_G1(Ek,rho,M,Z,k1,k2)

variable Ek	// outgoing photoelectron kinetic energy (eV)
variable rho	// bulk density  (g/cm^3)
variable M	// molar mass (g/mol)
string  Z	//  (Atomic number; atomic fraction) of each elements in the compound
variable k1, k2 // k1, k2 parameters of Gries

variable Zs  //effective atomic number 
variable Va // atomice volume (cm^3/mol)
variable IMFP  // In Angstroms


//--number of different atoms in the compound
variable nn  = floor(itemsinlist(Z)/2)
//--extract the atomic number and fraction of each element
make /d/o/n=(nn) Za, xf
variable kk
for(kk=0;kk<nn+1;kk+=2)
	Za[floor(kk/2)] = str2num(stringfromlist(kk,Z))
	xf[floor(kk/2)]  = str2num(stringfromlist(kk+1,Z))
endfor
//--effective atomic number 
Za = sqrt(Za)*xf
Zs = Sum(Za)
//--clean
killwaves /z Za, xf
//--atomic volume
Va  = M/rho

IMFP = k1*Va/Zs*Ek/(log(Ek)-k2)

return IMFP

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Transport mean-free path from material name
Function Material2TRMFP(typeIMFP,typeTRMFP,material,Ek,[Xray_source])

variable typeIMFP,typeTRMFP
string material
variable Ek
string Xray_source

struct StructIMFP MATIMFP
variable IMFP, TRMFP
variable checkMATIMFP
variable hv

//---IMFP option check
if(typeIMFP!=0 && typeIMFP!=1 && typeIMFP!=2)
	print "==> IMFP option : " + num2str(typeIMFP) +" not valid ! "
	return nan
endif
//---TRFMP  option check
if(typeTRMFP!=1 && typeTRMFP!=2 &&  typeTRMFP!=3)
	print "==> TRMFP option : " + num2str(typeTRMFP) +" not valid ! "
	return nan
endif

//---KE from BE
if(paramisdefault(Xray_source)==0)
	strswitch(Xray_source)	
	case "Al":	
		hv = ghv_Al
	break
	case "Mg":	
		hv = ghv_Mg
	break
	case "He I":	
		hv = ghv_HeI
	break
	case "He II":	
		hv = ghv_HeII
	break				
	default:							
		print "==> X-ray source " + Xray_source +" not valid ! "
		return nan
	endswitch
	Ek = hv - Ek
endif

//--extract information from database
checkMATIMFP = mfp_database#Get_IMFP_Database(material,MATIMFP)

//--if material exists, calculate TRMFP
if(checkMATIMFP==1)
	IMFP =  Get_IMFP(typeIMFP,Ek,MATIMFP)
	TRMFP = Get_TRMFP(typeTRMFP,Ek,IMFP,MATIMFP)
	return TRMFP
else
	return nan
endif

End 


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Transport mean free path
Static Function Get_TRMFP(type,Ek,IMFP,MATIMFP)

variable type, Ek, IMFP
struct StructIMFP &MATIMFP
variable TRSIG
variable nn, kk
variable Na
variable TRMFP
variable Zm

// Number of different atoms in the compound
nn = floor(itemsinlist(MATIMFP.Z)/2)
// Extract the atomic number and fraction of each
make /d/o/n=(nn) Za, xf
for(kk=0;kk<nn+1;kk+=2)
	Za[floor(kk/2)] = str2num(stringfromlist(kk,MATIMFP.Z))
	xf[floor(kk/2)]  = str2num(stringfromlist(kk+1,MATIMFP.Z))
endfor
// Atomic density in A-3
Na= (MATIMFP.rho/MATIMFP.M)*0.602214076 

// Average TRMFP

//===> From average cross section
// Improved algorithm for calculating transport cross sections of electrons with energies from 50 eV to 30 keV
// A. Jablonski and C.J. Powell  Phys. Rev. B 76 (2007) 085123
if(type==2)
	TRSIG = 0
	for(kk=0;kk<nn;kk+=1)
		TRSIG = TRSIG + xf[kk]*TRSIG_JABLONSKI(type,Ek,IMFP,Za[kk],Na)  // TRSIG does not depend on Na in this case
	endfor
	TRMFP= 1./(Na*TRSIG)
endif

//===> From average Z
//  Universal quantification of elastic scattering effects in AES and XPS,
// Surf. Sci., 1996, 364, 380 - 395
// Photoelectron transport in the surface region of solids: universal analytical formalism for quantitative applications of electron spectroscopies
// J. Phys. D: Appl. Phys. 2015, 48,  075301
if(type==1 || type==3)
	duplicate /o Za wZm
	wZm = Za*xf
	Zm = sum(wZm)
	killwaves /z wZm
	TRSIG = TRSIG_JABLONSKI(type,Ek,IMFP,Zm,Na)
	TRMFP= 1./(Na*TRSIG)
endif

//Clean
killwaves /z Za, xf

return TRMFP

End
 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Transport cross section 
Static Function TRSIG_JABLONSKI(type,Ek,IMFP,Zz,Na)

variable type, Ek, IMFP, Zz,Na
variable kk
variable TRSIG
variable Zeta = 0
variable TRMFP

//  Universal quantification of elastic scattering effects in AES and XPS,
// Surf. Sci., 1996, 364, 380 - 395
if(type==1)
	make /o/n=(4,4)/d AA
	AA[][0]  = {-15.4421,7.40525,-1.13552,6.47105e-2}
	AA[][1]  = {2.40871,-1.22881,0.201522,-1.10184e-2}
	AA[][2]  = {-8.46984e-2,4.44227e-2,-7.50284e-3,4.15715e-4}
	AA[][3] = {6.30016e-4,-3.34967e-4,5.74021e-5, -3.21438e-6}
	make /o/n=(4)/d GG, AAkk
	for(kk=0;kk<4;kk+=1)
		AAkk[] =  AA[kk][p]
		GG[kk] =  poly( AAkk, Zz )
	endfor
	Zeta = poly(GG, ln(Ek))
	Zeta = exp(Zeta)
	TRMFP = IMFP*Zeta
	TRSIG = 1./(Na*TRMFP)
	killwaves /Z AA, GG, AAkk
endif

// Improved algorithm for calculating transport cross sections of electrons with energies from 50 eV to 30 keV
// A. Jablonski and C.J. Powell  Phys. Rev. B 76 (2007) 085123
// Formula in :  J. Phys. D: Appl. Phys. 2015, 48,  075301
if(type==2)
	variable e0 = 0.154824*Ek/(Zz^(2./3.))
	variable sigtrB = 7.807390*Zz^(2./3.) * (ln(1.+e0) - e0/(1.+e0)) / (e0^2)
	make /o/n=(5)/d AA
	mfp_database#Z2Anew(Ek,Zz,AA)
	variable Ge0
	Ge0 = poly(AA, sqrt(ln(10*e0)) )
	Ge0 = e0*Exp(Ge0)
	TRSIG = sigtrB*Ge0
	killwaves /z AA
endif


// Photoelectron transport in the surface region of solids: universal analytical formalism for quantitative applications of electron spectroscopies
// J. Phys. D: Appl. Phys. 2015, 48,  075301
if(type==3)
	make /o/N=(5)/d BB
	mfp_database#Z2B(Ek,Zz,BB)
	Zeta = poly(BB, ln(Ek))
	Zeta = Exp(Zeta)
	Zeta = (1.-Zeta)/Zeta
	TRMFP = IMFP*Zeta
	TRSIG = 1./(Na*TRMFP)
	killwaves /z BB
endif

return TRSIG

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Surface excitation parameter from material name
Function Material2SEP(material,Ek,Alpha,[Xray_source])

string material
variable Ek, Alpha
string Xray_source

struct StructIMFP MATIMFP
variable SEP
variable checkMATIMFP
variable hv

//---KE from BE
if(paramisdefault(Xray_source)==0)
	strswitch(Xray_source)	
	case "Al":	
		hv = ghv_Al
	break
	case "Mg":	
		hv = ghv_Mg
	break
	case "He I":	
		hv = ghv_HeI
	break
	case "He II":	
		hv = ghv_HeII
	break				
	default:							
		print "==> X-ray source " + Xray_source +" not valid ! "
		return nan
	endswitch
	Ek = hv - Ek
endif

//--extract information from database
checkMATIMFP = mfp_database#Get_IMFP_Database(material,MATIMFP)

//--if material exists, calculate SEP
if(checkMATIMFP==1)
	SEP = Get_SEP(Ek,Alpha,MATIMFP)
	return SEP
else
	return nan
endif

End 


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Estimate of the surface excitation parameter
// Surface excitation parameter for selected polymers, N. Pauly and S. Tougaard Surf. Interface Anal. 2009, 41, 23�26
Static Function Get_SEP(KE,Alpha,MATIMFP)

variable KE, Alpha
struct StructIMFP &MATIMFP
variable a, Ep
variable SEP

Ep = 28.816 * sqrt((MATIMFP.Nv*MATIMFP.rho) / MATIMFP.M)
a = 0.039*Ep + 0.4 + 0.22*MATIMFP.Eg
SEP = 0.173*a*sqrt(KE)*cos(Alpha/180*pi) + 1.
SEP = 1./SEP

return SEP

End 


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Plot of mean-free paths of material
Function Plot_Material2MFPvsKE(material, Emin, Emax, Estep, Alpha, Psi, Theta, aBeta, Thick, typeIMFP, typeTRMFP,typeChandra)

string material
variable Emin, Emax, Estep, Alpha, Psi, Theta, aBeta, Thick, typeIMFP,typeTRMFP,typeChandra

struct StructIMFP MATIMFP
variable checkMATIMFP

//--extract information from database
checkMATIMFP = mfp_database#Get_IMFP_Database(material,MATIMFP)
//--if material exists, calculate and plot IMFP 
if(checkMATIMFP!=1)
	return 0
endif

//-setup
string nIMFP = "IMFP_"+material
variable np = (Emax - Emin ) / Estep + 1
make /d/o/n=(np)  $nIMFP
wave /d wIMFP = $nIMFP
setscale/P x Emin, Estep, "eV", wIMFP
setscale d 0, 10000, "�",  wIMFP	
	
//--the others
string nTRMFP	= "TRMFP_"	+material
string nALBEDO	= "ALBEDO_"+material
string nMED		= "MED_"	+material
string nEALb		= "EALb_"	+material	
string nEALf		= "EALf_"	+material	
string nEALm		= "EALlm_"	+material	
string nSEP		= "SEP_"	+material	
duplicate /o wIMFP  $nTRMFP, $nALBEDO, $nMED, $nEALb, $nEALf,  $nEALm, $nSEP
wave /d wTRMFP		= $nTRMFP
wave /d wALBEDO	= $nALBEDO
setscale d 0, 10000, "", wALBEDO	
wave /d wMED		= $nMED
wave /d wEALb		= $nEALb
wave /d wEALf		= $nEALf
wave /d wEALm		= $nEALm
wave /d wSEP		= $nSEP
setscale d 0, 10000, "",  wSEP	


//--calculate IMFPs
wIMFP =  Get_IMFP(typeIMFP,x,MATIMFP)
//--calculate TRMFPs	
wTRMFP = Get_TRMFP(typeTRMFP,x,wIMFP,MATIMFP)
//-- calculate ALBEDO
wALBEDO = wIMFP/(wIMFP+ wTRMFP)
//-- calculate MED	
wMED = ip4_eal#Get_MED(typeChandra,wIMFP,wALBEDO,Alpha,Psi,Theta,aBeta)
//-- calculate EALb	
wEALb = ip4_eal#Get_EALb(typeChandra,wIMFP,wALBEDO,Alpha,Psi,Theta,aBeta)
//-- calculate EALf	
wEALf = ip4_eal#Get_EALf(typeChandra,wIMFP,wALBEDO,Alpha,Psi,Theta,aBeta,Thick)	
//-- calculate EALm	
wEALm = ip4_eal#Get_EALm(typeChandra,wIMFP,wALBEDO,Alpha,Psi,Theta,aBeta,Thick)	
//-calculate SEP
wSEP = Get_SEP(x,Alpha,MATIMFP)      

//--display graph
display wIMFP, wTRMFP, wMED, wEALb, wEALf, wEALm
appendtograph/R wSEP
appendToGraph/L=VertCrossing wALBEDO
modifygraph standoff=0
modifygraph tick=2,minor=1
modifygraph mirror(bottom)=2
label left "\\K(65280,0,0)\\Z18Mean free path, effective attenuation length, mean escape depth (�)"
label bottom "\\Z20Kinetic energy (eV)"
label right "\\K(0,0,65280)\\Z20Surface Excitation Parameter"
label vertcrossing "\\K(0,65280,0)\\Z20Albedo "
modifygraph fSize=16
modifygraph mode=3
modifygraph rgb($nIMFP)=(65280,0,0), marker($nIMFP)=16
modifygraph rgb($nTRMFP)=(65280,0,0), marker($nTRMFP)=19
modifygraph rgb($nALBEDO)=(0,65280,0), marker($nALBEDO)=17
modifygraph rgb($nMED)=(65280,0,0), marker($nMED)=6
modifygraph rgb($nEALb)=(65280,0,0), marker($nEALb)=8
modifygraph rgb($nEALf)=(65280,0,0), marker($nEALf)=5
modifygraph rgb($nEALm)=(65280,0,0), marker($nEALm)=7	
modifygraph rgb($nSEP)=(0,0,65280), marker($nSEP)=22	
modifygraph msize=4
modifygraph mode($nIMFP)=4,mskip($nIMFP)=4
modifygraph mode($nTRMFP)=4,mskip($nTRMFP)=4
modifygraph mode($nALBEDO)=4,mskip($nALBEDO)=4
modifygraph mode($nMED)=4,mskip($nMED)=4
modifygraph mode($nEALb)=4,mskip($nEALb)=4
modifygraph mode($nEALf)=4,mskip($nEALf)=4
modifygraph mode($nEALm)=4,mskip($nEALm)=4		
modifygraph mode($nSEP)=4,mskip($nSEP)=4	
setaxis bottom 0,*
setaxis left 0,*
setaxis right 0,1
setaxis vertcrossing 0,1
modifygraph width={Aspect,1}
modifygraph axRGB(left)=(65280,0,0), tlblRGB(left)=(65280,0,0), alblRGB(left)=(65280,0,0)
modifygraph axRGB(right)=(0,0,65280), tlblRGB(right)=(0,0,65280), alblRGB(right)=(0,0,65280)
modifygraph axRGB(vertcrossing)=(0,65280,0), tlblRGB(vertcrossing)=(0,65280,0), alblRGB(vertcrossing)=(0,65280,0)
string slegend 
slegend   =  "\\Z16 Material :  " + material + "\r"
slegend +=   "\\s("+nIMFP+") IMFP\r"
slegend +=  "\\s("+nTRMFP+") TRMFP\r"
slegend +=  "\\s("+nALBEDO+") Albedo\r"
slegend +=  "\\s("+nMED+") MED\r"
slegend +=  "\\s("+nEALb+") Bulk EAL\r"	
slegend +=  "\\s("+nEALf+") Film EAL\r"	
slegend +=  "\\s("+nEALm+") Marker EAL\r"			
slegend +=  "\\s("+nSEP+") SEP(\\F'Symbol'q\\F'Arial'="+num2str(Alpha)+"�)"
legend/C/N=text0/J slegend
dowindow/T kwTopWin,"Mean free paths, effective attenuation lengths, mean escape depth and surface excitation parameter of " + material
 
//--Add notes to the waves
string snote
snote = "# Material : "  + material + "\r"
snote += "# Bulk density (g/cm^3) : "		+ num2str(MATIMFP.rho)  + "\r"
snote += "# Number of valence electrons per atom or molecule : "  + num2str(MATIMFP.Nv)	  + "\r"
snote += "# Molar mas (g/mol) : " 			+ num2str(MATIMFP.M)  + "\r"
snote += "# Band gap (eV) : " 			+ num2str(MATIMFP.Eg)  + "\r"
snote += "# Atomic number / Fraction : " 	+ MATIMFP.Z	  + "\r"
snote += "# k1-Gries parameter (�-1)  : " 	+ num2str(MATIMFP.k1)  + "\r"
snote += "# k2-Gries parameter  : " 		+ num2str(MATIMFP.k2)  + "\r"	
snote += "# Normal/Analyzer angle (deg) : "  + num2str(Alpha)  + "\r"
snote += "# Analyzer/X-ray angle (deg) : "  	+ num2str(Psi)  + "\r"
snote += "# Normal/X-ray angle (deg) : "  	+ num2str(Theta)  + "\r"	 
snote += "# Asymmetry factor : "			+ num2str(aBeta)  + "\r"	
snote += "# Film thickness (�) : "			+ num2str(Thick)  + "\r"	
snote += "# IMFP source : " 				+ num2str(typeIMFP)  + "\r"
snote += "# TRMFP source : "			+ num2str(typeTRMFP) + "\r" 
note wIMFP, " "
note wIMFP, "# Inelastic mean free path"
note wIMFP, snote
note wTRMFP, " "
note wTRMFP, "# Transport mean free path"
note wTRMFP, snote
note wMED, " "
note wMED, "# Mean escape depth (film)"
note wMED, snote
note wEALb, " "
note wEALb, "# Practical effective attenuation length (bulk)"
note wEALb, snote
note wEALf, " "
note wEALf, "# Practical effective attenuation length (film)"
note wEALf, snote
note wEALm, " " 
note wEALm, "# Practical effective attenuation length (marker)"
note wEALm, snote
note wSEP, " " 
note wSEP, "# Surface excitation parameter"
note wSEP, snote

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Plot of practical effective attenuation lengths
Function Plot_Material2EALvsThick(material, KE, Alpha, Psi, Theta, aBeta, typeIMFP, typeTRMFP,typeChandra, IDlim)

string material
variable KE, Alpha, Psi, Theta, aBeta, typeIMFP,typeTRMFP,typeChandra,IDlim

struct StructIMFP MATIMFP
variable checkMATIMFP

//--extract information from database
checkMATIMFP = mfp_database#Get_IMFP_Database(material,MATIMFP)
//--if material exists, calculate and plot IMFP 
if(checkMATIMFP!=1)
	return 0
endif

//--calculate IMFP
variable IMFP =  Get_IMFP(typeIMFP,KE,MATIMFP)
//--calculate TRMFPs	
variable TRMFP = Get_TRMFP(typeTRMFP,KE,IMFP,MATIMFP)
//-- calculate ALBEDO
variable ALBEDO = IMFP/(IMFP+ TRMFP)	

//--setup 
string nEALf_vst = "EALf_Thick_"+material
variable np = 800
variable ThickStep = -IMFP*cos(Alpha/180*pi)*ln(1.-IDlim/100)/50
make /d/o/n=(np)  $nEALf_vst
wave /d wEALf_vst  = $nEALf_vst 
wEALf_vst = nan
setscale/P x 0.5, ThickStep, "�", wEALf_vst
setscale d 0, 10000, "�", wEALf_vst	

//-- calculate EALf	up to the information depth at IDlim%
// A. Jablonski, C.J. Powell / Journal of Electron Spectroscopy and Related Phenomena 199 (2015) 27�37
variable kk, PHIb, PHIf, IDRatio
for(kk=0;kk<np;kk+=1)
	wEALf_vst[kk] =  ip4_eal#Get_EALf(typeChandra,IMFP,ALBEDO,Alpha,Psi,Theta,aBeta,pnt2x(wEALf_vst,kk),PHIb_out=PHIb,PHIf_out=PHIf)
	IDRatio 		= (PHIb-PHIf)/PHIb
	if(IDRatio>IDlim/100)
		break
	endif	
endfor
//-- redimension the waves
deletepoints kk, np-kk, wEALf_vst

//-- calculate MED
string nMED_vst = "MED_Thick_"+material
duplicate /o  wEALf_vst	 $nMED_vst
wave /d wMED_vst 	= $nMED_vst 
variable MED_val = ip4_eal#Get_MED(typeChandra,IMFP,ALBEDO,Alpha,Psi,Theta,aBeta)
wMED_vst = MED_val

//-- calculate EALb
string nEALb_vst = "EALb_Thick_"+material
duplicate /o  wEALf_vst	 $nEALb_vst
wave /d wEALb_vst 	= $nEALb_vst 
variable EALb_val = ip4_eal#Get_EALb(typeChandra,IMFP,ALBEDO,Alpha,Psi,Theta,aBeta)
wEALb_vst = EALb_val

//-- calculate EALm
string nEALm_vst = "EALm_Thick_"+material
duplicate /o  wEALf_vst	 $nEALm_vst
wave /d wEALm_vst 	= $nEALm_vst 
wEALm_vst = ip4_eal#Get_EALm(typeChandra,IMFP,ALBEDO,Alpha,Psi,Theta,aBeta,pnt2x(wEALf_vst,p))
	
//--the others
string nIMFP_vst = "IMFP_Thick_"+material		
string nTRMFP_vst = "TRMFP_Thick_"+material	
duplicate /o wEALf_vst $nIMFP_vst, $nTRMFP_vst
wave /d wIMFP_vst 	= $nIMFP_vst 
wave /d wTRMFP_vst = $nTRMFP_vst 
wIMFP_vst	= IMFP
wTRMFP_vst 	= TRMFP

//-- calculate information depth and average values of EAL
variable ID = pnt2x(wEALf_vst,numpnts(wEALf_vst)-1)
wavestats /q wEALf_vst
variable EAL_av   = V_avg
variable dEAL_av = V_sdev

//--display graph
display wMED_vst, wEALb_vst, wEALf_vst, wEALm_vst, wIMFP_vst, wTRMFP_vst
modifygraph standoff=0
modifygraph tick=2
modifygraph mirror=2
modifygraph minor=1
modifygraph fSize=16
label left "\\Z20Pratical effective attenuation length (�)"
label bottom "\\Z20Thickness / Depth (�)"
modifygraph mode($nMED_vst)=0, marker($nMED_vst)=7, rgb($nMED_vst)=(0,65280,0)
modifygraph mode($nEALb_vst)=0, marker($nEALb_vst)=7, rgb($nEALb_vst)=(65280,43520,0)
modifygraph mode($nEALf_vst)=3, marker($nEALf_vst)=19, rgb($nEALf_vst)=(0,0,0)
modifygraph mode($nEALm_vst)=3, marker($nEALm_vst)=8, rgb($nEALm_vst)=(0,0,0)	
modifygraph mode($nIMFP_vst)=0, marker($nIMFP_vst)=5, rgb($nIMFP_vst)=(65280,0,0)
modifygraph mode($nTRMFP_vst)=0, marker($nTRMFP_vst)=6, rgb($nTRMFP_vst)=(0,0,65280)
modifygraph lsize=2
setaxis bottom 0,*
setaxis left 0,*
modifygraph width={Aspect,1}
string slegend 
slegend   =  "\\Z16 Material :  " + material + "\r"
slegend +=  "\\s("+nIMFP_vst+") IMFP\r"
slegend +=  "\\s("+nTRMFP_vst+") TRMFP\r"
slegend +=  "\\s("+nMED_vst+") MED\r"
slegend +=  "\\s("+nEALb_vst+") EAL(bulk)\r"	
slegend +=  "\\s("+nEALf_vst+") EAL(film)\r"		
slegend +=  "\\s("+nEALm_vst+") EAL(marker)"		
legend/C/N=text0/J slegend
string svalues
sprintf svalues,"%s%6.3f%s%4.2f%s%s%4.2f%s%4.2f%s", "\\Z18ID(", IDlim, "%) = ", ID, " �\r", "EAL(ave) = ", EAL_av, " � ", dEAL_av, " �"
textbox/C/N=text1/F=0/A=MC svalues
dowindow/T kwTopWin,"Pratical effective attenuation lengths of " + material

//-- the notes
string snote 
snote   = "# Material : " 					+ material + "\r"
snote += "# Bulk density (g/cm^3) : "		+ num2str(MATIMFP.rho) + "\r"
snote += "# Number of valence electrons per atom or molecule : "  + num2str(MATIMFP.Nv)	 + "\r"
snote += "# Molar mas (g/mol) : " 			+ num2str(MATIMFP.M) + "\r"
snote += "# Band gap (eV) : "			+ num2str(MATIMFP.Eg) + "\r"
snote += "# Atomic number / Fraction : "	+ MATIMFP.Z	 + "\r"
snote += "# k1-Gries parameter (�-1)  : " 	+ num2str(MATIMFP.k1) + "\r"
snote += "# k2-Gries parameter  : " 		+ num2str(MATIMFP.k2) + "\r"
snote += "# Kinetic energy (eV)  : " 		+ num2str(KE) + "\r" 	
snote += "# Normal/Analyzer angle (deg) : "+ num2str(Alpha) + "\r"
snote += "# Analyzer/X-ray angle (deg) : "	+ num2str(Psi) + "\r"
snote += "# Normal/X-ray angle (deg) : "	+ num2str(Theta) + "\r"	
snote += "# Asymmetry factor : " 			+ num2str(aBeta) + "\r"	
snote += "# IMFP source : "				+ num2str(typeIMFP) + "\r"
snote += "# IMFP (�) : "					+ num2str(IMFP) + "\r"
snote += "# TRMFP source : " 			+ num2str(typeTRMFP)	 + "\r"
snote += "# TRMFP (�) : "				+ num2str(TRMFP) + "\r"
snote += "# ALBEDO : "				+ num2str(ALBEDO) + "\r"
snote += "# EAL(bulk) (�) : "				+ num2str(EALb_val) + "\r"
snote += "# MED (�) : "					+ num2str(MED_val) + "\r"
snote += "# Information depth limit (%) : "	+ num2str(IDlim) + "\r"	
snote += "# Information depth (�) : "		+ num2str(ID) + "\r"	
snote += "# Average film EAL (�) : "		+ num2str(EAL_av) +  " � " + num2str(dEAL_av) + "\r"
//-- copy to all
note wIMFP_vst, " "
note wIMFP_vst, "# Inelastic mean free path"
note wIMFP_vst, snote
note wTRMFP_vst, " "
note wTRMFP_vst, "# Transport mean free path"
note wTRMFP_vst, snote
note wMED_vst, " "
note wMED_vst, "# Mean escape depth (film)"
note wMED_vst, snote
note wEALb_vst, " "
note wEALb_vst, "# Practical effective attenuation length (bulk)"
note wEALb_vst, snote
note wEALf_vst, " "
note wEALf_vst, "# Practical effective attenuation length (film)"
note wEALf_vst, snote
note wEALm_vst, " " 
note wEALm_vst, "# Practical effective attenuation length (marker)"
note wEALm_vst, snote

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Plot of emission depth distribution function
Function Plot_Material2EDDFvsDepth(material, Depth1, Depth2, dDepth, KE, Alpha, Psi, Theta, aBeta, typeIMFP, typeTRMFP,typeChandra)

string material
variable Depth1, Depth2, dDepth, KE, Alpha, Psi, Theta, aBeta, typeIMFP, typeTRMFP,typeChandra

struct StructIMFP MATIMFP
variable checkMATIMFP

//--extract information from database
checkMATIMFP = mfp_database#Get_IMFP_Database(material,MATIMFP)
//--if material exists, calculate and plot IMFP 
if(checkMATIMFP!=1)
	return 0
endif

//-setup
string nEDDF = "EDDF_"+material
variable np = Abs(Depth2 - Depth1 ) / dDepth + 1
make /d/o/n=(np)  $nEDDF
wave /d wEDDF = $nEDDF
setscale/P x Depth1, dDepth, "�", wEDDF
setscale d 0, 10000, "", wEDDF	

//--calculate IMFPs
variable IMFP =  Get_IMFP(typeIMFP,KE,MATIMFP)
//--calculate TRMFPs	
variable TRMFP = Get_TRMFP(typeTRMFP,KE,IMFP,MATIMFP)
//-- calculate ALBEDO
variable ALBEDO = IMFP/(IMFP+ TRMFP)
//-- calculate EDDF	
wEDDF = Get_EDDF(typeChandra,IMFP,ALBEDO,Alpha,Psi,Theta,aBeta,x)
wEDDF /= Get_EDDF(typeChandra,IMFP,ALBEDO,Alpha,Psi,Theta,aBeta,0)

//--display graph
display wEDDF
modifygraph standoff=0
modifygraph tick=2,minor=1,mirror=2
modifygraph log(left)=1
label left "\\Z20Normalized emission depth distribution function"
label bottom "\\Z20Depth (�)"
modifygraph fSize=16
modifygraph mode=3
modifygraph rgb($nEDDF)=(0,0,0), marker($nEDDF)=8
modifygraph msize=4
setaxis bottom 0,*
setaxis left *,1
modifygraph width={Aspect,1}
string slegend 
slegend   =  "\\Z16 Material :  " + material + "\r"
slegend +=    "\\Z16\\s("+nEDDF+") EDDF"
legend/C/N=text0/J slegend
dowindow/T kwTopWin,"Emission depth distribution function of  " + material
    
 //-- the informations
string snote 
snote    = "# Material : "  + material + "\r"
snote  += "# Bulk density (g/cm^3) : "		+ num2str(MATIMFP.rho) + "\r"
snote  += "# Number of valence electrons per atom or molecule : "  + num2str(MATIMFP.Nv) + "\r"	
snote  += "# Molar mas (g/mol) : " 		+ num2str(MATIMFP.M) + "\r"
snote  += "# Band gap (eV) : " 			+ num2str(MATIMFP.Eg) + "\r"
snote  += "# Atomic number / Fraction : " 	+ MATIMFP.Z	 + "\r"
snote  += "# k1-Gries parameter (�-1)  : " 	+ num2str(MATIMFP.k1) + "\r"
snote  += "# k2-Gries parameter  : " 		+ num2str(MATIMFP.k2) + "\r"	
snote  += "# Kinetic energy (eV)  : " 		+ num2str(KE) + "\r"	
snote  += "# Normal/Analyzer angle (deg) : "  + num2str(Alpha) + "\r"
snote  += "# Analyzer/X-ray angle (deg) : "  	+ num2str(Psi) + "\r"
snote  += "# Normal/X-ray angle (deg) : "  	+ num2str(Theta) + "\r"	
snote  += "# Asymmetry factor : "			+ num2str(aBeta) + "\r"
snote  += "# IMFP source : " 				+ num2str(typeIMFP) + "\r"
snote  += "# IMFP (�) : " 				+ num2str(IMFP) + "\r"
snote  += "# TRMFP source : "			+ num2str(typeTRMFP) + "\r"
snote  += "# TRMFP (�) : " 				+ num2str(TRMFP) + "\r"	
snote  += "# ALBEDO : " 				+ num2str(ALBEDO) + "\r"	
note wEDDF,  " " 
note wEDDF,  "# Emission depth distribution function"
note wEDDF,  snote

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Plot of CF function
Function Plot_Material2CFvsDepth(material, Depth1, Depth2, dDepth, KE, Alpha, Psi, Theta, aBeta, typeIMFP, typeTRMFP,typeChandra)

string material
variable Depth1, Depth2, dDepth, KE, Alpha, Psi, Theta, aBeta, typeIMFP, typeTRMFP,typeChandra

struct StructIMFP MATIMFP
variable checkMATIMFP

//--extract information from database
checkMATIMFP = mfp_database#Get_IMFP_Database(material,MATIMFP)
//--if material exists, calculate and plot IMFP 
if(checkMATIMFP!=1)
	return 0
endif

//-setup
string nCF = "CF_"+material
variable np = Abs(Depth2 - Depth1 ) / dDepth + 1
make /d/o/n=(np)  $nCF
wave /d wCF = $nCF
setscale/P x Depth1, dDepth, "�", wCF
setscale d 0, 10000, "", wCF	

//--calculate IMFPs
variable IMFP =  Get_IMFP(typeIMFP,KE,MATIMFP)
//--calculate TRMFPs	
variable TRMFP = Get_TRMFP(typeTRMFP,KE,IMFP,MATIMFP)
//-- calculate ALBEDO
variable ALBEDO = IMFP/(IMFP+ TRMFP)
//-- calculate CF	
wCF = Get_CF(typeChandra,IMFP,ALBEDO,Alpha,Psi,Theta,aBeta,x)

//--display graph
display wCF
modifygraph standoff=0
modifygraph tick=2,minor=1,mirror=2
label left "\\Z20CF function = EDDF with/without elastic scattering"
label bottom "\\Z20Depth (�)"
modifygraph fSize=16
modifygraph mode=3
modifygraph rgb($nCF)=(0,0,0), marker($nCF)=8
modifygraph msize=4
setaxis bottom 0,*
setaxis left *,*
modifygraph width={Aspect,1}
string slegend 
slegend   =  "\\Z16 Material :  " + material + "\r"
slegend +=    "\\Z16\\s("+nCF+") CF"
legend/C/N=text0/J slegend
dowindow/T kwTopWin,"CF function : ratio of emission depth distribution function with to without elastic scattering" + material
    
 //-- the informations
string snote 
snote    = "# Material : "  + material + "\r"
snote  += "# Bulk density (g/cm^3) : "		+ num2str(MATIMFP.rho) + "\r"
snote  += "# Number of valence electrons per atom or molecule : "  + num2str(MATIMFP.Nv) + "\r"	
snote  += "# Molar mas (g/mol) : " 		+ num2str(MATIMFP.M) + "\r"
snote  += "# Band gap (eV) : " 			+ num2str(MATIMFP.Eg) + "\r"
snote  += "# Atomic number / Fraction : " 	+ MATIMFP.Z	 + "\r"
snote  += "# k1-Gries parameter (�-1)  : " 	+ num2str(MATIMFP.k1) + "\r"
snote  += "# k2-Gries parameter  : " 		+ num2str(MATIMFP.k2) + "\r"	
snote  += "# Kinetic energy (eV)  : " 		+ num2str(KE) + "\r"	
snote  += "# Normal/Analyzer angle (deg) : "  + num2str(Alpha) + "\r"
snote  += "# Analyzer/X-ray angle (deg) : "  	+ num2str(Psi) + "\r"
snote  += "# Normal/X-ray angle (deg) : "  	+ num2str(Theta) + "\r"	
snote  += "# Asymmetry factor : "			+ num2str(aBeta) + "\r"
snote  += "# IMFP source : " 				+ num2str(typeIMFP) + "\r"
snote  += "# IMFP (�) : " 				+ num2str(IMFP) + "\r"
snote  += "# TRMFP source : "			+ num2str(typeTRMFP) + "\r"
snote  += "# TRMFP (�) : " 				+ num2str(TRMFP) + "\r"	
snote  += "# ALBEDO : " 				+ num2str(ALBEDO) + "\r"	
note wCF,  " " 
note wCF,  "# CF function"
note wCF,  snote

End

