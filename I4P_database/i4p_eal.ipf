// Module : i4p_eal
// Author : Rémi Lazzari, remi.lazzari@insp.jussieu.fr
// Date : 2019-2020
// Usage : calculate the effective attenuation  from  geometry and theory of transport


#pragma rtGlobals= 3		
#pragma ModuleName = ip4_eal


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mean escape depth
// Jablonski and Powell, Journal of Vacuum Science & Technology A 21, 274 (2003), Phys. Rev. B 54 (1996) 10927
Function Get_MED(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta)

variable typeChandra, IMFP,Omega,Alpha,Psi,Theta,aBeta

//--global variables and waves
variable /G tCh   = typeChandra
variable /G ome = Omega
variable /G mua = min(cos(Alpha/180*pi),0.9999)
variable  mut      = cos(Theta/180*pi)
variable  mup     = cos(Psi/180*pi)
variable  bet       = aBeta

//--get the coefficient of Chandrasekhar function
if(tCh==4) 
	Get_Kawabata_Coeff()
endif

//--MED calculation
variable C1, C2, Chi
C1 = -ome*bet/16*(3.*mut^2-1.)* Integrate1D(Int_fHm,0,1.,2,0)
C2 =  ome*bet/16*(3.*mut^2-1.)* Integrate1D(Int_fHb,0,1.,2,0)
Chi = (ome/2.)/sqrt(1.-ome)* Integrate1D(Int_fHa,0,1.,2,0)
variable S1, S2
S1 = Chi/sqrt(1.-ome) + C1
S2 = 1./sqrt(1.-ome) - bet/4.*(3.*mup^2-1.)/fH(mua,ome) + C2
variable MED
MED = IMFP*(1.-ome)* (mua + S1/S2)

//--clean
killvariables /z tCh, ome, mua
killwaves /z  AACh, BBCh

return MED

End



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Effective attenuation length of a bulk
/// Jablonski and Powell, Surf. Sci. Rep. 47 (2002)33-91 P50
// A. Jablonski, C.J. Powell / Journal of Electron Spectroscopy and Related Phenomena 218 (2017) 112
Function Get_EALb(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta)

variable typeChandra, IMFP,Omega,Alpha,Psi,Theta,aBeta

//--global variables and waves
variable /G tCh	= typeChandra
variable /G ome	= Omega
variable /G mua     = min(cos(Alpha/180*pi),0.9999)
variable /G mut      = cos(Theta/180*pi)
variable /G mup     = cos(Psi/180*pi)
variable /G bet       = aBeta
variable /G lbi		= IMFP

//--get the coefficient of Chandrasekhar function
if(tCh==4) 
	Get_Kawabata_Coeff()
endif

//--integral of EMDDF Phi(z)
variable PHI, PHISLA
PHI   =  Get_PHIb()
PHISLA = lbi*mua/(4.*pi)/(1. - bet/4.*(3.*mup^2-1))
//--get the effective attenuation length
variable EALb
EALb =lbi*PHI/PHISLA

//--clean
killvariables /z tCh, ome, mua, mut, mup, mub, bet, lbi
killwaves /z  AACh, BBCh

return EALb

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Integral of EMDDF Phi(z) to infinity
/// Jablonski and Powell, Surf. Sci. Rep. 47 (2002)33-91 P50
// A. Jablonski, C.J. Powell / Journal of Electron Spectroscopy and Related Phenomena 218 (2017) 112
Static Function Get_PHIb()

NVAR ome	= ome
NVAR mua	= mua
NVAR mut      = mut
NVAR mup     = mup
NVAR bet      	= bet
NVAR lbi		= lbi

variable D1, D2, Qx, beteff
variable PHIb

D1 = fH(mua,ome)/sqrt(1.-ome)
D2 = bet/16*ome*(3*mut^2-1.)*fH(mua,ome) * Integrate1D(Int_fHb,0,1.,2,0)
Qx = (1.-ome)*(D1+D2)
beteff = bet*(1.-ome)/Qx
PHIb = lbi*mua*Qx/(4.*pi)* (1. - beteff/4.*(3.*mup^2-1.))

return PHIb

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Practical effective attenuation length of a film
/// Jablonski and Powell, Surf. Sci. Rep. 47 (2002)33-91 P50
Function Get_EALf(typeChandra, IMFP,Omega,Alpha,Psi,Theta,aBeta,Thick,[PHIb_out,PHIf_out])

variable typeChandra, IMFP, Omega, Alpha, Psi, Theta, aBeta, Thick
variable &PHIb_out, &PHIf_out

//--global variables and waves
variable /G tCh	= typeChandra	
variable /G ome	= Omega
variable /G mua	= min(cos(Alpha/180*pi),0.9999)
variable /G mut	= cos(Theta/180*pi)
variable /G mup	= cos(Psi/180*pi)
variable /G bet	= aBeta
variable /G nu0	= Get_Nu0(ome)
variable /G tau	= Thick / ((1.-ome)*IMFP)
variable /G lbi		= IMFP

//--get the coefficient of Chandrasekhar function
if(tCh==4) 
	Get_Kawabata_Coeff()
endif

//--integral of EMDDF Phi(z)
variable PHIb, PHIf
PHIb =  Get_PHIb()
PHIf  =  Get_PHIf()
if(paramisdefault(PHIb_out) == 0)
	PHIb_out = PHIb
endif 
if(paramisdefault(PHIf_out) == 0)
	PHIf_out = PHIf
endif 

// A. Jablonski, C.J. Powell / Journal of Electron Spectroscopy and Related Phenomena 199 (2015) 2737
variable EALf
EALf = Thick/(mua*ln(PHIb/PHIf))

//--clean
killvariables /z tCh, ome, mua, mut, mup, bet, nu0, tau, lbi
killwaves /z AACh, BBCh

return EALf

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Integral of EMDDF Phi(z) for a thin-layer
/// Jablonski and Powell, Surf. Sci. Rep. 47 (2002)33-91 P50
// A. Jablonski / Journal of Electron Spectroscopy and Related Phenomena 185 (2012) 498508
Static Function Get_PHIf()

NVAR ome	= ome
NVAR mua	= mua
NVAR mut	= mut
NVAR mup	= mup
NVAR bet	= bet
NVAR nu0	= nu0
NVAR tau	= tau
NVAR lbi		= lbi

variable Gamma_i, Gamma_a

Gamma_i   = fH(mua,ome)*nu0*exp(-tau/nu0)
Gamma_i += fgg(mua,ome) * ( 1.-mua*ome/2.*ln( mua*(1.+mua)/((1.-mua)^2) ) ) * (mua*exp(-tau/mua) - nu0*exp(-tau/nu0))
Gamma_i += mua*ome/2.*fH(mua,ome)*Integrate1D(Int_fR,0,1.,2,0)
Gamma_i /= 4*pi

Gamma_a  = fgg(mua,ome)*(3.*mua^2*(1.-ome)-1.) * (1. - mua*ome/2.*ln( mua*(1.+mua)/((1.-mua)^2) ) ) - (3.*mua^2 -1.)
Gamma_a *= mua*exp(-tau/mua) - nu0*exp(-tau/nu0)
Gamma_a += mua*ome/2.*fH(mua,ome)*Integrate1D(Int_fS,0,1.,2,0)
Gamma_a *= (3.*mut^2-1.)/2.
Gamma_a += mua*ome/2.*(3.*mut^2-1)*fH(mua,ome)*nu0*exp(-tau/nu0)*Integrate1D(Int_fHl,0,1.,2,0)/2.
Gamma_a +=  (3.*mup^2-1.)*mua*exp(-tau/mua)
Gamma_a /= 4*pi

variable PHIf 
PHIf =  Gamma_i - bet/4.*Gamma_a
PHIf *=  (1.-ome)*lbi

return PHIf

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Practical effective attenuation length of a marker
/// Jablonski and Powell, Surf. Sci. Rep. 47 (2002)33-91 P50
// A. Jablonski, C.J. Powell / Journal of Electron Spectroscopy and Related Phenomena 218 (2017) 112
Function Get_EALm(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta,Depth)

variable typeChandra,IMFP, Omega, Alpha, Psi, Theta, aBeta, Depth

//--global variables and waves
variable /G tCh	= typeChandra	
variable /G ome	= Omega
variable /G mua	= min(cos(Alpha/180*pi),0.9999)
variable /G mut	= cos(Theta/180*pi)
variable /G mup	= cos(Psi/180*pi)
variable /G bet	= aBeta
variable /G nu0	= Get_Nu0(ome)
variable /G tau
variable /G lbi		= IMFP

//--get the coefficient of Chandrasekhar function
if(tCh==4) 
	Get_Kawabata_Coeff()
endif

//--integral of EMDDF Phi(z)
variable PHI0, PHIz
PHI0 =  Get_PHIz(0)
PHIz  =  Get_PHIz(Depth)

// A. Jablonski, C.J. Powell / Journal of Electron Spectroscopy and Related Phenomena 199 (2015) 2737
variable EALm
EALm= Depth/(mua*ln(PHI0/PHIz))

//--clean
killvariables /z tCh, ome, mua, mut, mup, bet, nu0, tau, lbi
killwaves /z AACh, BBCh

return EALm

End



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EMDDF Phi(z) 
/// Jablonski and Powell, Surf. Sci. Rep. 47 (2002)33-91 P50
// A. Jablonski, C.J. Powell / Journal of Electron Spectroscopy and Related Phenomena 218 (2017) 112
Static Function Get_PHIz(z)

variable z
NVAR ome	= ome
NVAR mua	= mua
NVAR mut	= mut
NVAR mup	= mup
NVAR bet	= bet
NVAR nu0	= nu0
NVAR tau	= tau
NVAR lbi		= lbi

variable gamma_i, gamma_a
tau = z / ((1.-ome)*lbi)

gamma_i   = fH(mua,ome)*exp(-tau/nu0)
gamma_i += fgg(mua,ome) * ( 1.-mua*ome/2.*ln( mua*(1.+mua)/((1.-mua)^2) ) ) * (exp(-tau/mua) - exp(-tau/nu0))
gamma_i += mua*ome/2.*fH(mua,ome)*Integrate1D(Int_fF,0,1.,2,0)
gamma_i /= 4*pi

gamma_a  = fgg(mua,ome)*(3.*mua^2*(1.-ome)-1.) * (1. - mua*ome/2.*ln( mua*(1.+mua)/((1.-mua)^2) ) ) - (3.*mua^2 -1.)
gamma_a *= exp(-tau/mua) - exp(-tau/nu0)
gamma_a += mua*ome/2.*fH(mua,ome)*Integrate1D(Int_fG,0,1.,2,0)
gamma_a *= (3.*mut^2-1.)/2.
gamma_a += mua*ome/2.*(3.*mut^2-1)*fH(mua,ome)*exp(-tau/nu0)*Integrate1D(Int_fHl,0,1.,2,0)/2.
gamma_a +=  (3.*mup^2-1.)*exp(-tau/mua)
gamma_a /= 4*pi

variable PHIz 
PHIz =  gamma_i - bet/4.*gamma_a

return PHIz

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Emission Depth Distribution Function = EDDF
/// Jablonski and Powell, Surf. Sci. Rep. 47 (2002)33-91 P50
// A. Jablonski, C.J. Powell / Journal of Electron Spectroscopy and Related Phenomena 218 (2017) 112
Function Get_EDDF(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta,Depth)

variable typeChandra, IMFP, Omega, Alpha, Psi, Theta, aBeta, Depth

//--global variables and waves
variable /G tCh	= typeChandra	
variable /G ome	= Omega
variable /G mua	= min(cos(Alpha/180*pi),0.9999)
variable /G mut	= cos(Theta/180*pi)
variable /G mup	= cos(Psi/180*pi)
variable /G bet	= aBeta
variable /G nu0	= Get_Nu0(ome)
variable /G tau
variable /G lbi		= IMFP

//--get the coefficient of Chandrasekhar function
if(tCh==4) 
	Get_Kawabata_Coeff()
endif

//-- EMDDF Phi(z)
variable PHIz
PHIz  =  Get_PHIz(Depth)

//--clean
killvariables /z tCh, ome, mua, mut, mup, bet, nu0, tau, lbi
killwaves /z AACh, BBCh

return PHIz

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CF Function = ration between EDDF with and without elastic scattering
// A. Jablonski, S. Tougaard / Surface Science 432 (1999) 211227
Function Get_CF(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta,Depth)

variable typeChandra, IMFP, Omega, Alpha, Psi, Theta, aBeta, Depth

//--global variables and waves
variable /G tCh	= typeChandra	
variable /G ome	= Omega
variable /G mua	= min(cos(Alpha/180*pi),0.9999)
variable /G mut	= cos(Theta/180*pi)
variable /G mup	= cos(Psi/180*pi)
variable /G bet	= aBeta
variable /G nu0	= Get_Nu0(ome)
variable /G tau
variable /G lbi		= IMFP

//--get the coefficient of Chandrasekhar function
if(tCh==4) 
	Get_Kawabata_Coeff()
endif

//-- EDDF Phi(z) with elastic scattering
variable PHIz
PHIz  =  Get_PHIz(Depth)
//--EDDF Phi0(z) without elastic scattering
variable PHI0z
PHI0z = (1.-bet/4*(3*mup^2-1.))/(4*pi) *  exp(-Depth/(lbi*mua))

//--clean
killvariables /z tCh, ome, mua, mut, mup, bet, nu0, tau, lbi
killwaves /z AACh, BBCh

return PHIz/PHI0z

End





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// INTERMEDIATRE FUNCTIONS IN THE TRANSPORT THEORY
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Get_Nu0(ome)

variable ome
variable nu1 = 1.0001, nu2 = 1e4
variable f1, f2, fm
variable nu, num, dnu, dnumin = 1e-8

do
	nu1 = 1. + (nu1-1)/10
	nu2 = 10*nu2
	f1 = Fnu(nu1,ome) 
	f2 = Fnu(nu2,ome) 
while(f1*f2>0)

do 
	dnu = nu2-nu1
	num = (nu1+nu2)/2.0
	fm = Fnu(num,ome)
	if(f1*fm<0)
		nu2 = num
		f2 = fm
	else
		nu1 = num
		f1 = fm
	endif
while(dnu>dnumin)

return num

End

Function Fnu(nu,ome)
	variable nu, ome
	return nu*ln((nu+1.)/(nu-1.)) - 2./ome
End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Int_fHb(xx)

variable xx
NVAR mua 	= mua  
NVAR ome	= ome
variable IntfHb

IntfHb = fH(xx,ome)*xx*(3*xx^2-1.)/(xx+mua)
return IntfHb

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Int_fHl(xx)

variable xx
NVAR mua 	= mua  
NVAR ome	= ome
variable IntfHl

IntfHl = fH(xx,ome)*(3.*xx^2-1.)/(xx+mua)
return IntfHl

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Int_fHm(xx)

variable xx
NVAR mua 	= mua  
NVAR ome	= ome
variable IntfHm

IntfHm = fH(xx,ome)*(xx^2+xx*mua)*(3.*xx^2-1.)/(xx+mua)
return IntfHm

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Int_fHa(xx)

variable xx
NVAR ome	= ome
variable IntfHa

IntfHa = fH(xx,ome)*xx
return IntfHa

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Int_fF(xx)

variable xx
NVAR ome	= ome
NVAR mua 	= mua
NVAR tau	= tau
NVAR nu0	= nu0  
variable IntfF

IntfF = (fF(xx,ome,tau,nu0)-fF(mua,ome,tau,nu0))/(xx-mua)
return IntfF

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Int_fG(xx)

variable xx
NVAR ome	= ome
NVAR mua 	= mua
NVAR tau	= tau
NVAR nu0	= nu0  
variable IntfG

IntfG = (fG(xx,ome,tau,nu0)-fG(mua,ome,tau,nu0))/(xx-mua)
return IntfG

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Int_fR(xx)

variable xx
NVAR ome	= ome
NVAR mua 	= mua
NVAR tau	= tau
NVAR nu0	= nu0  
variable IntfR

IntfR = (fR(xx,ome,tau,nu0)-fR(mua,ome,tau,nu0))/(xx-mua)
return IntfR

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Int_fS(xx)

variable xx
NVAR ome	= ome
NVAR mua 	= mua
NVAR tau	= tau
NVAR nu0	= nu0  
variable IntfS

IntfS = (fS(xx,ome,tau,nu0)-fS(mua,ome,tau,nu0))/(xx-mua)
return IntfS

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function fgg(xx,ome)

variable xx, ome
variable fggo

fggo  =(ome*pi*xx/2.)^2 + (1.-ome*xx/2.*ln((1.+xx)/(1.-xx)))^2
fggo = 1./fggo
return fggo

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function fF(xx,ome,tau,nu0)

variable xx, ome,tau, nu0
variable fFo

fFo = fgg(xx,ome)/fH(xx,ome) * (exp(-tau/xx) - exp(-tau/nu0))
return fFo

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function fG(xx,ome,tau,nu0)

variable xx, ome,tau, nu0
variable fGo

fGo = fF(xx,ome,tau,nu0) * (3.*xx^2*(1.-ome)-1.)
return fGo

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function fR(xx,ome,tau,nu0)

variable xx, ome,tau, nu0
variable fRo

fRo = fgg(xx,ome)/fH(xx,ome) * (xx*exp(-tau/xx) - nu0*exp(-tau/nu0))
return fRo

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function fS(xx,ome,tau,nu0)

variable xx, ome,tau, nu0
variable fSo

fSo = fR(xx,ome,tau,nu0)*(3.*xx^2*(1.-ome)-1.)
return fSo

End






////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// THE CHANDRASEKHAR FUNCTION 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function fH(xx,ome)
variable xx, ome
NVAR tCh = tCh

switch(tCh)
case 0: // Recommended integral form
	return fH0(xx,ome)  
	break
case 1: // Analytical approximate value > 1%
	return fH1(xx,ome) 
	break
case 2: // Analytical approximate value at 1% (Davidovic)
	return fH2(xx,ome) 
	break
case 3: // Analytical approximate value at 0.1% (Davidovic)
	return fH3(xx,ome) 
	break
case 4: // Polynom approximate value at 10-6 (Kawabata)
	return fH4(xx,ome) 
	break
endswitch

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Recommended integral form
/// Jablonski and Powell, Surf. Sci. Rep. 47 (2002)33-91
// A. Jablonski / Computer Physics Communications 196 (2015) 416428
Function fH0(xx,ome)

variable xx, ome
variable /G omeH = ome
variable /G xxH = xx
variable fHo

fHo = Integrate1D(Int_fHCh,1e-3,1e3,2,0)
fHo = exp(-xx*fHo/pi)
killvariables /Z omeH, xxH
return fHo

End

Function Int_fHCh(tt)
variable tt
NVAR omeH = omeH
NVAR xxH = xxH
return  ln(1. - omeH/tt*atan(tt)) / (1. + (xxH*tt)^2)
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Analytical approximate value at >2.5%
// Hapke, B., 1981. Bidirectional reflectance spectroscopy. 1. Theory. J. Geophys. Res. 86, 30393054.  
// with the value h11 of A. Jablonski / Computer Physics Communications 196 (2015) 416428

Function fH1(xx,ome)

variable xx, ome
variable fHo

fHo =(1.+xx*1.90781) / (1. + xx*1.90781*sqrt(1.-ome))
return fHo

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Analytical approximate value at better than 2.5% from Davidovic Icarus 194 (2008) 389397
Function fH2(xx,ome)

variable xx, ome
variable xi, sq
variable fHo

sq = sqrt(1.-ome)
xi = 0.431 / (1. + 0.316*sq) + 0.105*xx
fHo = (xi + xx) / (xi + xx*sq)

return fHo

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Analytical approximate value at better than 0.07% from Davidovic Icarus 194 (2008) 389397
Function fH3(xx,ome)

variable xx, ome
variable a, b, sq
variable fHo

sq = sqrt(1.-ome)
a = 0.431 / (1. + 0.316*sq) 
b = 0.105
fHo = a*(1. - sq)/(b + sq) * ln( 1. + (b+sq)/a)
fHo += (a - (b+1)*xx)*ln(1.+1./xx)
fHo *= 0.5*ome*xx/(a - xx*(b +sq) )
fHo = 1.- fHo
fHo = 1./fHo

return fHo

End


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Polynom approximate value at 10-6
// Kiyoshi Kawabata · Sanjay S. Limaye  Astrophys Space Sci (2011) 332: 365371 and  Astrophys Space Sci (2013) 348:601
Function fH4(xx,ome)

variable xx, ome
variable xi = xx^0.25
wave /D Ak = AACh
wave /D Bkn = BBCh
variable kk
variable fHo

make /d/o/n=(9) Bn, Ck
for(kk=0;kk<9;kk+=1)
	Bn[] = Bkn[p][kk]
	Ck[kk] = poly(Bn,sqrt(1.-ome))
endfor
fHo =  poly(Ak,xi) /(1.+poly(Ck,xi))
killwaves /z Bn, Ck

return fHo

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Get_Kawabata_Coeff()

make /o/d/n=(9) AACh
AACh  = {9.999982706853756e-1,3.465443224211651e-4,-1.411107006687451e-2,3.269177042230116e-1,4.133809356648527,-7.188546622876579,7.772939980710241,-3.883055730606847,7.595128286312914e-1}

make /o/d/n=(9,9) BBCh
BBCh[][0] = {-1.368687418901498e-6,6.744526217097578e-5,-8.816747094601710e-4,4.731152489223286e-3,-1.352739541743824e-2,2.236433018731980e-2,-2.147081702708310e-2,1.112257595951489e-2,-2.406003988429531e-3}
BBCh[][1] = {8.737822937355147e-5,-5.250514244222347e-3,7.644952859355422e-2,-4.664908220536214e-1,1.482688198325839,-2.663033364728811,2.727252555244034,-1.485444888951274,3.340921510758153e-1}
BBCh[][2] = {-1.427222952750036e-3,9.300028322140796e-2,-1.413069914567426,8.880428860986575,-2.866825946137678e+1,5.178036196746675e+1,-5.307180734532348e+1,2.885782084328829e+1,-6.471219440031649}
BBCh[][3] = {9.066801756884433e-3,-6.354984995808299e-1,1.021262226727643e+1,-6.444360574298017e+1,2.105330190640368e+2,-3.824039368443171e+2,3.930240665640704e+2,-2.139686267143788e+2,4.800025272319539e+1}
BBCh[][4] = {-2.855922558150419e-2,3.880224653851042,-3.174231079700075e+1,2.303877926374539e+2,-7.626655021168267e+2,1.394034249890738e+3,-1.438476211044276e+3,7.852393856327993e+2,-1.764969590005163e+2}
BBCh[][5] = {4.941209676842531e-2,-3.976393849244121,6.000178277203062e+1,-4.542543148444882e+2,1.512146625692455e+3,-2.779737284749243e+3,2.880598698878311e+3,-1.577451021926768e+3,3.554375808436865e+2}
BBCh[][6] = {-4.798519468590785e-2,4.112841572654386,-6.655808348671680e+1,5.000349699512032e+2,-1.672172432180451e+3,3.091851778649070e+3,-3.218110914157008e+3,1.768094273655673e+3,-3.994358424590589e+2}
BBCh[][7] = {2.461700902387896e-2,-2.233648393380449,3.900465646584139e+1,-2.880699974056035e+2,9.688954523412610e+2,-1.802235503900686e+3,1.883990440310628e+3,-1.038462482861755e+3,2.352061082130820e+2}
BBCh[][8] = {-5.211353622987505e-3,4.967427514273564e-1,-9.292147966163522,6.773895398390997e+1,-2.294206635762768e+2,4.292903843888321e+2,-4.506396634901928e+2,2.491623632369491e+2,-5.657192709351447e+1}

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Check_Chandrasekhar_Function(mu,ome)

variable mu, ome
variable H0, H1, H2, H3, H4
variable timerRefNum, microSeconds 

Get_Kawabata_Coeff()

timerRefNum = startMSTimer
H0 = fH0(mu,ome)
microSeconds = stopMSTimer(timerRefNum)
printf  "%s%13.10f\r", "H(Accurate) = ", H0
printf  "%s%13.2f\r", "Time = ", microSeconds
print " "

timerRefNum = startMSTimer
H1 = fH1(mu,ome)
microSeconds = stopMSTimer(timerRefNum)
printf  "%s%13.10f\r", "H1 = ", H1
printf  "%s%13.10f\r", "H1(rel) X10^2 = ", abs((H1-H0)/H0)*100
printf  "%s%13.2f\r", "Time = ", microSeconds
print " "

timerRefNum = startMSTimer
H2 = fH2(mu,ome)
microSeconds = stopMSTimer(timerRefNum)
printf  "%s%13.10f\r", "H2 = ", H2
printf  "%s%13.10f\r", "H2(rel) X10^3 = ", abs((H2-H0)/H0)*1000
printf  "%s%13.2f\r", "Time = ", microSeconds
print " "

timerRefNum = startMSTimer
H3 = fH3(mu,ome)
microSeconds = stopMSTimer(timerRefNum)
printf  "%s%13.10f\r", "H3 = ", H3
printf  "%s%13.10f\r", "H3(rel) X10^4 = ", abs((H3-H0)/H0)*10000
printf  "%s%13.2f\r", "Time = ", microSeconds
print " "

 timerRefNum = startMSTimer
H4 = fH4(mu,ome)
microSeconds = stopMSTimer(timerRefNum)
printf  "%s%13.10f\r", "H4 = ", H4
printf  "%s%13.10f\r", "H4(rel)x10^6 = ", abs((H4-H0)/H0)*1e6
printf  "%s%13.2f\r", "Time = ", microSeconds
print " "

killwaves /z AACh, BBCh

End




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Angles between normal to sample, analyzer and X-ray source in the apparatuses of 
// Institut des NanoSciences de Paris
Function AngleXPS_INSP(analyzer,Alpha)

variable analyzer // Type of configuration
variable Alpha // Rotation angle of manipulator
variable Psi, Theta
 
 
switch(analyzer)
case 0: // EA125 with DARX40
	Psi = 54.44
	Theta = cos(35.56/180*pi)*sin(125/180*pi)*sin(Alpha/180*pi) + sin(35.56/180*pi)*cos(Alpha/180*pi)
	Theta = acos(Theta)/pi*180
	print "EA125 analyzer with DARX40 source  (unmonochromated)"
break

case 1: // PHOIBOS 110 with monochromated source
	Psi = 70
	Theta = Psi - Alpha
	print "PHOIBOS 110 analyzer with XM1000 source (monochromated)"
break

case 2: // PHOIBOS 110 with unmonochromated source
	Psi = 54.8536
	Theta = sin(45/180*pi)*cos((35.5+Alpha)/180*pi)
	Theta = acos(Theta)/pi*180
	print "PHOIBOS 110 analyzer with XR50 source (unmonochromated)"
break

default: // Unknwon
	print "Unknown configuration !"
	Alpha	= nan
	Psi		= nan
	Theta	= nan
break

endswitch

// results
if(Alpha<0 || Alpha>90)
	print "Warning : Alpha outside practical range !"
endif
printf "%s%6.2f%s\r", "Analyzer/normal angle  Alpha = ", Alpha, " (deg)"
printf "%s%6.2f%s\r", "Analyzer/X-ray angle     Psi    = ", Psi, " (deg)"
printf "%s%6.2f%s\r", "Normal/X-ray angle       Theta = ", Theta, " (deg)"

End
