// Procedure :  i4p_datatabse
// Author : R�mi Lazzari, lazzari@insp.jussieu.fr
// Date : 2019
// Usage :  identify spectral peaks from database of core level and Auger transitions 
// (binding energies, kinetic energies, photoionization cross section, asymmetry parameter, spin-orbit splitting)

#pragma rtGlobals = 3		
#pragma ModuleName = i4pdatabase

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Killwindows and delete the folder
Static Function Clean_I4PDatabase()

string listofwindows = winlist("*", ";","")

if(whichlistitem("PeriodicTablePanel",listofwindows)!=-1)
	killwindow PeriodicTablePanel
endif
if(whichlistitem("QuantificationPanel",listofwindows)!=-1)
	killwindow QuantificationPanel
endif
if(whichlistitem("MFPPanel",listofwindows)!=-1)
	killwindow MFPPanel
endif
if(whichlistitem("QuantificationPanel_SA",listofwindows)!=-1)
	killwindow QuantificationPanel_SA
endif
if(whichlistitem("QuantificationPanel_SS",listofwindows)!=-1)
	killwindow QuantificationPanel_SS
endif

killdatafolder /z  root:I4P:I4Pperiodictable:
killdatafolder /z  root:I4P:I4Pquantification:
killdatafolder /z  root:I4P:I4Pquantification_sa:
killdatafolder /z  root:I4P:I4Pquantification_ss:

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Plot core-levle and Auger transitions from the periodic table
Static Function PlotElementFromI4PDatabase()

//-------------do not update
silent -1; pauseupdate

//-------------save current data folder
DFREF saveDFR = GetDataFolderDFR()	

//-------------get the top graph and clean it for all element tags
string listofgrahs = winlist("*", ";","WIN:1")
if(itemsinlist(listofgrahs)==0)
	beep
	doalert /T="WARNING" 0, "ERROR \r\rNo graph !"
	return 0
endif
string topgraph = stringfromlist(0,listofgrahs,";")
// print "==> Top graph : " + topgraph
string listoftraces = tracenamelist("",";",1)
string listoftraces_element = listmatch(listoftraces,"X=*",";")  + listmatch(listoftraces,"'X=*'",";") 
variable kk 
for(kk=itemsinlist(listoftraces_element,";")-1;kk>=0;kk-=1)
	wave /d trace2wave = TraceNameToWaveRef("", stringfromlist(kk,listoftraces_element,";"))
	removefromgraph /z  $stringfromlist(kk,listoftraces_element,";")
	killwaves /z trace2wave
endfor

//-------------get the maximum of intensity
variable scalefactor = 0
listoftraces = tracenamelist("",";", 1)
for(kk=0;kk<itemsinlist(listoftraces,";");kk+=1)
	string trace = stringfromlist(kk,listoftraces, ";")
	wave /d wavetrace = tracenametowaveref("",trace)
	wavestats /q wavetrace
	scalefactor = max(scalefactor, V_max)
endfor
scalefactor /= 20.
if(scalefactor==0)
	scalefactor = 1
endif


//-------------go to the peridoic table folder
setdatafolder root:I4P:I4Pperiodictable

//-------------clean the folder from useless waves
string listofwaves_element 
listofwaves_element = wavelist("X=*",";","")
for(kk=0;kk<itemsinlist(listofwaves_element,";");kk+=1)
	killwaves /z  $stringfromlist(kk,listofwaves_element,";")
endfor

//-------------get the photon energy and the satellite position and relative intensities
NVAR Psi = pangle
SVAR Xray_source = pxrs
variable hv, nsat, nghost
strswitch(Xray_source)	
case "Al":	
	hv = ghv_Al
	//----------------------------------------- Klauber1993
	// From "Refinement of Mg and Al Kalpha X-ray source functions", by C. Klauber, Surf. Interf. Anal. 20 (1993) 703-715
	// Satellites characteristics : relative position and amplitude relative to Kalpha1
	// Ka', Ka3, Ka3', Ka4, Ka5, Ka6
	// nsat = 7
	//make /o/n=(nsat) sat_pos, sat_int
	//sat_pos = {-0.2075,5.452,9.526,9.958,11.701,20.072,23.576}
	//sat_int   = {1.5,0.01928,0.07774,0.02373,0.06139,0.00634,0.00276}	
	// Renormalize to the Ka1+Ka2
	//sat_pos = sat_pos + 0.415*0.5	
	//sat_int = sat_int/1.5
	//----------------------------------------- Krause1975
	// From "X ray emission spectra from Mg and Al", by M.O Krause and J.G. Ferreira, J. Phys B : Atom Mol. Phys. 8 (1975) 2007
	// a",a',a",a3,a4, a8,a5,a7,a6,a9,a11,b',a10,a13,a14,b,b1/2
	//nsat = 16
	//make /o/n=(nsat) sat_pos, sat_int
	//sat_pos = {0,4.7,5.8,9.7,11.7,18.5,20.0,21.8,23.6,28.0,30.5,33.6,38.8,42.9,70.6,72.7}
	//sat_int = {1,0.003,0.007,0.073,0.031,0.0010,0.0041,0.0010,0.0028,0.0003,0.0002,0.0005,0.0008,0.0005,0.0076,0.024}
	//----------------------------------------- WagnerHandbook	
	// From "Handbook of X-ray photoelectron spectroscopy", by C.D. Wagner et al, 1979
	// a3, a4, a5, a6, b
	nsat = 6
	make /o/n=(nsat) sat_pos, sat_int
	sat_pos = {0,9.8,11.8,20.1,23.4,69.7}
	sat_int = {1,0.064,0.032,0.004,0.003,0.0055}	
	//----------------------------------------- Ghost lines from WagnerHandbook		
	nghost = 3
	make /o/n=(nghost) ghost_pos, ghost_int
	ghost_pos = {961.7,556.9,233}
	ghost_int = {0.05,0.001,0.03}		
break
case "Mg":	
	hv = ghv_Mg
	//----------------------------------------- Klauber1993
	// From "Refinement of Mg and Al Kalpha X-ray source functions", by C. Klauber, Surf. Interf. Anal. 20 (1993) 703-715
	// Satellites characteristics : relative position and amplitude relative to Kalpha1
	// Ka', Ka3, Ka3', Ka4, Ka5, Ka6
	// nsat = 7
	//make /o/n=(nsat) sat_pos, sat_int
	//sat_pos = {-0.1325,4.740,8.210,8.487,10.095,17.404,20.430}
	//sat_int   = {1.5,0.02099,0.07868,0.04712,0.09071,0.01129,0.00538}
	// Renormalize to the Ka1+Ka2
	//sat_pos = sat_pos + 0.265*0.5
	//sat_int = sat_int/1.5
	//----------------------------------------- Krause1975
	// From "X ray emission spectra from Mg and Al", by M.O Krause and J.G. Ferreira, J. Phys B : Atom Mol. Phys. 8 (1975) 2007
	// a",a',a",a3,a4, a8,a5,a7,a6,a9,a11,b',a10,a13,a14,b,b1/2
	//nsat = 17
	//make /o/n=(nsat) sat_pos, sat_in
	//sat_pos = {0,3.6,4.6,8.5,10.1,15.7,17.4,19.2,20.6,24.0,27.1,28.8,30.0,33.8,37.7,48.6,49.9}
	//sat_int = {1,0.003,0.01,0.091,0.051,0.0012,0.0076,0.0029,0.0048,0.0002,0.0003,0.0002,0.0006,0.0001,0.0001,0.0055,0.017}
	//----------------------------------------- WagnerHandbook	
	// From "Handbook of X-ray photoelectron spectroscopy", by C.D. Wagner et al, 1979
	// a3, a4, a5, a6, b
	nsat = 6
	make /o/n=(nsat) sat_pos, sat_int
	sat_pos = {0,8.4,10.2,17.5,20.0,48.5}
	sat_int = {1,0.08,0.041,0.0055,0.0045,0.005}
	//----------------------------------------- Ghost line from WagnerHandbook		
	nghost = 3
	make /o/n=(nghost) ghost_pos, ghost_int
	ghost_pos = {728.7,323.9,-233}
	ghost_int = {0.05,0.001,0.03}	
break
case "He I":	
	hv = ghv_HeI
	nsat = 6
	make /o/n=(nsat) sat_pos, sat_int
	sat_pos = {0,1.87,2.52,19.59,27.15,29.8}
	sat_int = {1,0.015,0.005,0.01,0.001,0}
	nghost = 0
break			
endswitch

//-------------define the wave used by the periodic table
wave /t ElementSym = ElementSym
wave ElemSelect = ElemSelect

//-------------loop over selected elemtents and plot them
STRUCT I4PElementPhi Element
variable nn, ncl
string TagElement
string StrXElementCL, StrYElementCL, StrYElementCLTag
string StrXElementAuger, StrYElementAuger, StrYElementAugerTag
variable ncorelevel, nauger
variable checkElem
NVAR PrintInfo = pinfo
NVAR Plotsatellite = psatellite
NVAR Plotghost = pghost
NVAR PlotAuger= pauger
NVAR nTF = ptransfunc
variable TFC1s = (hv-284.5)^nTF
for(nn=1;nn<numpnts(ElemSelect);nn+=1)
	if(ElemSelect[nn]==1)
		//---selected element
		TagElement = ElementSym[nn]
		//--information from database
		checkElem = I4PDatabasePhi(TagElement,Element)
	 	if(checkElem==0)
	 		print "==> Element " + TagElement  + " not yet tabulated !"
	 		continue
	 	endif
	 	//---number of core levels and Auger transitions
	 	ncorelevel = itemsinlist(Element.cl,";")
	 	nauger 	  = itemsinlist(Element.auger,";")
		//---define the wave to plot : core levels
		listofwaves_element	= wavelist("X=*_CLv*",";","")
		if(itemsinlist(listofwaves_element,";")==0)
			ncl = 1
		else
			ncl = 0
			do
				ncl += 1
				StrYElementCL =  "X="+TagElement+"_CLv" + num2str(ncl)
			while(FindListItem(StrYElementCL,listofwaves_element)!=-1)
		endif
		StrXElementCL		= "X="+TagElement+"_CLbe"+ num2str(ncl)
		StrYElementCL		= "X="+TagElement+"_CLv"+ num2str(ncl)
		StrYElementCLTag	= "X="+TagElement+"_CLtag"+ num2str(ncl)
		make /d/o/n=(ncorelevel,nsat+nghost)	$StrXElementCL
		make /d/o/n=(ncorelevel,nsat+nghost)	$StrYElementCL
		make /t/o/n=(ncorelevel)	$StrYElementCLTag
		wave /d XElementCL		= $StrXElementCL
		wave /d YElementCL		= $StrYElementCL
		wave /t YElementCLTag	=  $StrYElementCLTag
		//---define the wave to plot : Auger transitions
		StrXElementAuger	= "X="+TagElement+"_Augerbe"+ num2str(ncl)
		StrYElementAuger	= "X="+TagElement+"_Auger"+ num2str(ncl)
		StrYElementAugerTag	= "X="+TagElement+"_Augertag"+ num2str(ncl)
		make /d/o/n=(nauger) $StrXElementAuger
		make /d/o/n=(nauger) $StrYElementAuger
		make /t/o/n=(nauger)  $StrYElementAugerTag
		wave /d XElementAuger	 =  $StrXElementAuger
		wave /d YElementAuger 	 =  $StrYElementAuger
		wave /t YElementAugerTag =  $StrYElementAugerTag
		//---fill the waves to plot: core-levels and Auger transitions
	 	for(kk=0;kk<ncorelevel;kk+=1)
	 		XElementCL[kk][0] = str2num(stringfromlist(kk, Element.be, ";"))
	 		//--photoionization cross section normalized to C1s
	 		YElementCL[kk][0]  = Element2PICS(Element.sy,stringfromlist(kk, Element.cl, ";"),Xray_source,Psi)
	 		if(XElementCL[kk][0]>hv)
	 			XElementCL[kk][0] = nan
	 			YElementCL[kk][0] = nan
	 		endif
	 		YElementCLTag[kk] = Element.sy + " " + stringfromlist(kk, Element.cl, ";")
	 	endfor
		//---fill the waves to plot: Auger transitions
	 	for(kk=0;kk<nauger;kk+=1)
	 		XElementAuger[kk] = hv-str2num(stringfromlist(kk, Element.ke, ";"))
	 		YElementAuger[kk] = 1.
	 		if(XElementAuger[kk]<0.)
	 			XElementAuger[kk] = nan
	 			YElementAuger[kk] = nan
	 		endif
	 		YElementAugerTag[kk] =   Element.sy  + " " + stringfromlist(kk, Element.auger, ";")
	 	endfor
	 	//--correct by the transmission function
	 	YElementCL[][0]      /= (hv - XElementCL[p][0])^nTF / TFC1s   	
	 	YElementAuger /= (hv - XElementAuger)^nTF / TFC1s
	 	//---print the information
	 	if(PrintInfo==1)
	 		print " "
	 		print "==> Element  symbol: " + TagElement
	 		print "==> Element name : " + Element.na 		
	 		print "==> Atomic number : " + Element.nu 		
	 		print "==> Core level / Binding energy (eV) / RSF (norm. to C1s-AlKa)"
	 		for(kk=0;kk<ncorelevel;kk+=1)
	 			if(numtype(XElementCL[kk][0])==0)
	 				print YElementCLTag[kk], XElementCL[kk][0] , YElementCL[kk][0]
	 			endif 
	 		endfor
	 		print "==> Auger transition / Binding energy (eV) / Kinetic energy (eV)"
	 		for(kk=0;kk<nauger;kk+=1)
	 			if(numtype(XElementAuger[kk])==0)
	 				print YElementAugerTag[kk], XElementAuger[kk], hv-XElementAuger[kk]
	 			endif
	 		endfor
	 	endif
	 	//---normalize to the graph maximum
	 	YElementCL[][0]	*= scalefactor
	 	YElementAuger	*= scalefactor
	 	//--fill the satellite/ghost waves
	 	variable ss
	 	for(ss=1;ss<nsat;ss+=1)
	 		XElementCL[][ss] = XElementCL[p][0] - sat_pos[ss]
	 		YElementCL[][ss] = YElementCL[p][0] * sat_int[ss]
	 		for(kk=0;kk<ncorelevel;kk+=1)
	 			if(XElementCL[kk][ss]>hv ||  XElementCL[kk][ss]<0)
	 				XElementCL[kk][ss] = nan
	 				YElementCL[kk][ss] = nan
	 			endif
	 		endfor
	 	endfor
	 	for(ss=nsat;ss<nsat+nghost;ss+=1)
	 		XElementCL[][ss] = XElementCL[p][0]  + ghost_pos[ss-nsat]
	 		YElementCL[][ss] = YElementCL[p][0] * ghost_int[ss-nsat]
	 		for(kk=0;kk<ncorelevel;kk+=1)
	 			if(XElementCL[kk][ss]>hv ||  XElementCL[kk][ss]<0)
	 				XElementCL[kk][ss] = nan
	 				YElementCL[kk][ss] = nan
	 			endif
	 		endfor
	 	endfor	 	
	 	//---set the information 
	 	note YElementCL, " "
	 	note YElementCL, "# Relative sensitivity factor : "
	 	note YElementCL, "# Excitation energy (eV) : "		+ num2str(hv)	
	 	note YElementCL, "# Trans. func. expo : "			+ num2str(nTF)	
		note YElementCL, "# Chemical element : "			+ Element.sy
		note YElementCL, "# Atomic number : "				+ Element.nu
		note YElementCL, "# Core-levels : "					+ Element.cl		
		note YElementCL, "# Core-levels BE (eV): "			+ Element.be	
	 	note YElementAuger, " "
	 	note YElementAuger, "# Relative sensitivity factor : "
	 	note YElementAuger, "# Excitation energy (eV) : "			+ num2str(hv)
	 	note YElementAuger, "# Trans. func. expo : "				+ num2str(nTF)		
		note YElementAuger, "# Chemical element : "				+ Element.sy
		note YElementAuger, "# Atomic number : "					+ Element.nu		
		note YElementAuger, "# Auger transistions : "				+ Element.auger		
		note YElementAuger, "# Auger transistions KE (eV) : "		+ Element.ke	
		note YElementAuger, "# Auger transistions BE at Al-Ka (eV) : "	+ Element.bea			
		note YElementAuger, "# Auger transistions BE at Mg-Ka (eV) : "	+ Element.bem	
		//---append to graph the core-levels
	 	appendtograph YElementCL[][0] vs XElementCL[][0]
	 	modifygraph mode($StrYElementCL)=8,marker($StrYElementCL)=19,lsize($StrYElementCL)=2,msize($StrYElementCL)=3
	 	//---append to graph the auger lines if required
	 	if(PlotAuger==1)
	 		appendtograph YElementAuger vs XElementAuger
	 		modifygraph mode($StrYElementAuger)=8,marker($StrYElementAuger)=16,lsize($StrYElementAuger)=2,msize($StrYElementAuger)=3
	 	endif
	 	//--- append the satellites if required
	 	string StrYElementSAT
	 	if(Plotsatellite==1)
			for(ss=1;ss<nsat;ss+=1)
				StrYElementSAT = StrYElementCL + "_SA#" + num2str(ss)
				appendtograph  YElementCL[][ss] /TN=$StrYElementSAT vs XElementCL[][ss]
	 		endfor 
	 		listoftraces = tracenamelist("",";",1)
			listoftraces_element = listmatch(listoftraces,"'*_SA*'",";") 
			for(kk=0;kk<itemsinlist(listoftraces_element,";");kk+=1)
				StrYElementSAT = stringfromlist(kk,listoftraces_element,";")
				modifygraph mode($StrYElementSAT)=8,marker($StrYElementSAT)=8,lsize($StrYElementSAT)=2,msize($StrYElementSAT)=3
			endfor		
	 	endif
	 	//--- append the ghost lines if required
	 	string StrYElementGHOST
	 	if(Plotghost==1)
			for(ss=nsat;ss<nsat+nghost;ss+=1)
				StrYElementGHOST = StrYElementCL + "_GH#" + num2str(ss-nsat+1)
				appendtograph YElementCL[][ss] /TN=$StrYElementGHOST vs XElementCL[][ss]
	 		endfor 	
	 		listoftraces = tracenamelist("",";",1)
			listoftraces_element = listmatch(listoftraces,"'*_GH*'",";") 
			for(kk=0;kk<itemsinlist(listoftraces_element,";");kk+=1)
				StrYElementGHOST = stringfromlist(kk,listoftraces_element,";")
				modifygraph mode($StrYElementGHOST)=8,marker($StrYElementGHOST)=6,lsize($StrYElementGHOST)=2,msize($StrYElementGHOST)=3
			endfor	
	 	endif	 	 	
	 	//--tag the data
	 	for(kk=0;kk<ncorelevel;kk+=1)
	 		if(numtype(XElementCL[kk][0])==0)
	 			tag /b=2/f=0/i=1/l=2/m=0/o=0/x=0/y=10/tl={fat=0.4,frame=1}   $StrYElementCL, kk, "\\Z12"+ YElementCLTag[kk]
	 		endif
	 	endfor
	 	if(PlotAuger==1)
	 		for(kk=0;kk<nauger;kk+=1)
	 			if(numtype(XElementAuger[kk])==0)
	 				tag /b=2/f=0/i=1/l=2/m=0/o=0/x=0/y=10/tl={fat=0.4,frame=1}   $StrYElementAuger, kk, "\\Z12"+ YElementAugerTag[kk]
	 			endif
	 		endfor
	 	endif
	 	//- legend only for existing traces
	 	Legend/K/N=legengI4P
	 	Legend/C/N=legengI4P/F=0
	endif
endfor

//-------------color rainbow
listoftraces = tracenamelist("",";", 1)
listoftraces_element = listmatch(listoftraces,"X=*",";")  +  listmatch(listoftraces,"'X=*'",";")
colortab2wave Rainbow256
wave /i/u M_colors
variable nM_colors = dimsize(M_colors,0)
variable ncolors = 15
variable ink = (nM_colors-1)/(ncolors-1)
string trace_element
variable kkcolors
for(kk=0;kk<itemsinlist(listoftraces_element,";");kk+=1)
	trace_element = stringfromlist(kk,listoftraces_element)
	kkcolors = mod(str2num(WaveNote2Value(TraceNameToWaveRef("", trace_element ),"Atomic number")),ncolors)
	modifygraph rgb($trace_element)=(M_colors[nM_colors-1-kkcolors*ink][0],M_colors[nM_colors-1-kkcolors*ink][1],M_colors[nM_colors-1-kkcolors*ink][2])
endfor

//-------------go back to current datafolder
setdatafolder saveDFR


End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get the photo-ionization cross section of a given core level
Static Function Element2PICS(TagElement,TagCL,Xray_source,Psi)

string TagElement		// chemical element
string TagCL			// given core-level 
string Xray_source	// X-ray source
variable Psi		// angle between X-ray source and analyzer

//--decompose the core-level
string cl, ll, jj
cl	= TagCL[0,1]		// core-level
ll  	= TagCL[1]		// orbital momemtum L
jj	= TagCL[2,4]		// J=L+S

//--multiplier in case of spin-orbit split core-level
variable Xpics = 1.
strswitch(ll)
case "s":
	Xpics = 1.
break
case "p":
	strswitch(jj)
	case "1/2":
		Xpics = 1./3.
	break
	case "3/2":
		Xpics = 2./3.
	break	
	endswitch
break
case "d":
	strswitch(jj)
	case "3/2":
		Xpics = 2./5.
	break
	case "5/2":
		Xpics = 3./5.
	break	
	endswitch
break
case "f":
	strswitch(jj)
	case "5/2":
		Xpics = 3./7.
	break
	case "7/2":
		Xpics = 4./7.
	break	
	endswitch
break
endswitch

//--extract the corresponding PICS
variable PICS
PICS = CoreLevel2PICS(TagElement,cl,Xray_source,Angle=Psi)

//--return the value
return PICS*Xpics

End


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Find the closest core-level or Auger transition from a given binding energy
Function BE2Peaks([Xray_source,BE,dBE])

variable BE, dBE
string Xray_source

//--Bring the command windows to front
dowindow /h/f

//---if BE not present, look for cursor position
if(paramisdefault(BE)==1)
	string listofgrahs = winlist("*", ";","WIN:1")
	if(itemsinlist(listofgrahs)==0)
		beep
		doalert /T="WARNING" 0, "ERROR \r\rNo graph !"
		return 0
	endif
	string csinfoA = csrinfo(A) 
	if(strlen(csinfoA) == 0)
		beep
		doalert /T="WARNING" 0, "ERROR \r\rCursor A must be on the graph to define binding energy !"
		return 0
	endif
	variable Aisfree = numberbykey("ISFREE",csinfoA)
	if(Aisfree==1)
		beep
		doalert /T="WARNING" 0, "ERROR \r\rCursor A is free and not on a trace !"
		return 0
	endif
	string Atname = stringbykey("TNAME",csinfoA)
	string infoAtrace = traceinfo("",Atname,0)
  	string soffset
  	variable xoffset, xmultoffset
  	soffset = stringbykey("offset(x)",infoAtrace,"=",";")
	sscanf soffset, "{%g}", xoffset
	soffset = stringbykey("muloffset(x)",infoAtrace,"=",";")	
	sscanf soffset, "{%g}", xmultoffset 
	if(xmultoffset==0)
		xmultoffset = 1
	endif 	
	BE =  xcsr(A)*xmultoffset + xoffset
endif

//---if not dBE present, define the energy windows
if(paramisdefault(dBE)==1)
	dBE = 1.5
endif

//---photon energy
variable hv
if(paramisdefault(Xray_source)==0)
	strswitch(Xray_source)	
	case "Al":	
		hv = ghv_Al
	break
	case "Mg":	
		hv = ghv_Mg
	break
	case "He I":	
		hv = ghv_HeI
	break			
	default:							
		beep
		doalert /T="WARNING" 0, "ERROR \r\rUnknown photon source : " + Xray_source + " !"
		return 0
	break
	endswitch
else
	wave /d wA = csrwaveref(A)
	hv = str2num(WaveNote2Value(wA,"Excitation energy (eV)"))
endif

//--limits in energy
variable bemin, bemax, KE, kemin, kemax 
KE = hv - BE
bemin	= BE - Abs(dBE)
bemax	= BE + Abs(dBE)
kemin	= hv - bemax
kemax	= hv - bemin

//----list of chemical elements
variable maxelem = 104
make /t/o/n=(maxelem) ElementSym
ElementSym[0]= {"","H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y"}
ElementSym[40]= {"Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir"}
ElementSym[78]= {"Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr"}

//-------------loop over selected all elements and look for the closest one 
STRUCT I4PElementPhi Element
variable nn, kk
string TagElement
variable checkElem
string listoftcl		= ""
string listoftauger = ""
string listoftcl2plot = "\Z12"
string listoftauger2plot = "\Z12"
string closestcl = ""
string closestauger = ""
variable bee, kee, dbee, dkee
variable dbeec = inf, dkeec = inf
string sdbee, sdkee
for(nn=1;nn<maxelem;nn+=1)
		//---selected element
		TagElement = ElementSym[nn]
		//--information from database
		checkElem = I4PDatabasePhi(TagElement,Element)
	 	if(checkElem==0)
	 		// print "==> Element " + TagElement  + " not yet tabulated !"
	 		continue
	 	endif
	 	//---look for the closest core-levels
	 	for(kk=0;kk<itemsinlist(Element.cl,";");kk+=1)
	 		bee = str2num( stringfromlist(kk, Element.be, ";") )
		 	if(bee>=bemin && bee<=bemax)
		 		sprintf sdbee, "%1.1f", bee-BE
		 	 	listoftcl	+= Element.sy + " " + stringfromlist(kk,Element.cl,";") + " (BE=" + stringfromlist(kk,Element.be,";")  +"; dBE=" + sdbee + ")\r "
		 	 	listoftcl2plot  += Element.sy + " " + stringfromlist(kk,Element.cl,";") + " (E\BB\M\Z12=" + stringfromlist(kk,Element.be,";")  +"; \F'Symbol'D\F'Arial'E\BB\M\Z12=" + sdbee + ")\r "	 	
		 	endif
		 	dbee = abs(bee-BE)
		 	 if(dbee<dbeec)
		 	 	dbeec = dbee
		 	 	closestcl = Element.sy + " " + stringfromlist(kk,Element.cl,";") + " (BE" + stringfromlist(kk,Element.be,";") +"; dBE=" + num2str(dbee) + ")\r " 	
		 	 endif	
	 	endfor
	 	//---look for the closest Auger transitions
	 	for(kk=0;kk<itemsinlist(Element.auger,";");kk+=1)
	 		kee = str2num( stringfromlist(kk, Element.ke, ";") )
		 	if(kee>=kemin && kee<=kemax)
		 		sprintf sdkee, "%1.1f", kee-KE
		 	 	listoftauger	+= Element.sy + " " + stringfromlist(kk,Element.auger,";") + " (KE=" + stringfromlist(kk,Element.ke,";") +"; dKE=" + sdkee + ")\r "
		 	 	listoftauger2plot	+= Element.sy + " " + stringfromlist(kk,Element.auger,";") + " (E\BK\M\Z12=" + stringfromlist(kk,Element.ke,";") +"; \F'Symbol'D\F'Arial'E\BK\M\Z12=" + sdkee + ")\r "	 	
		 	endif
		 	dkee = abs(kee-KE) 
		 	if(dkee<dkeec)
		 	 	dkeec = dkee
		 	 	closestauger = Element.sy + " " + stringfromlist(kk,Element.auger,";") + " (KE=" + stringfromlist(kk,Element.ke,";") +"; dKE=" + num2str(dkee) + ")\r"
		 	endif
	 	endfor
endfor

//-------------print the results
print " "
print "==> Binding energy (eV) : " + num2str(BE) + " " + num2char(-79) + " "  + num2str(dBE) 
print "==> Kinetic energy (eV)  : " + num2str(KE) + " " + num2char(-79) + " "  + num2str(dBE) 
if(itemsinlist(listoftcl,";")!=0)
	print "==> Core-levels [BE (eV)] in energy windows"
	print listoftcl
else
	print "==> No core-levels in energy windows"
	print "       Closest core-level [BE (eV)] : " + closestcl
endif
if(itemsinlist(listoftauger,";")!=0)
	print "==> Closest Auger transitions [KE (eV)] at hv = " + num2str(hv) + " eV in the energy windows"
	print listoftauger
else
	print "==> No Auger transitions in energy windows"
	print "       Closest Auger transitions [KE (eV)] : " + closestauger
endif


//-------------tag the cursor position in the top graph
if(paramisdefault(BE)==1)
	if(strlen(listoftcl)!=0 || strlen(listoftauger)!=0)
		tag /b=2/f=0/i=1/l=2/m=0/o=0/x=0/y=10/tl={fat=0.4,frame=1}   $csrwave(A), xcsr(A), "\Z12E\BB\M\Z12 =  " + num2str(BE) + " eV\r"+ listoftcl2plot + listoftauger2plot
	else
		tag /b=2/f=0/i=1/l=2/m=0/o=0/x=0/y=10/tl={fat=0.4,frame=1}   $csrwave(A), xcsr(A), "\Z12E\BB\M\Z12 =  "  + num2str(BE) + " eV\r" + "None"
	endif
endif

//-------------clean
killwaves /z ElementSym

End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Photoionization cross section taking into account the asymmetry factor
// From J.J. Yeh and I. Lindau, Atomic subshell photoionization cross sections and asymmetry parameters: 1<Z<300,
// At. Data Nucl. Data Tables,32 1985 1-155
Function CoreLevel2PICS(TagElement,Atomic_level,Xray_source,[PrintInfo,Angle])

string TagElement, Atomic_level, Xray_source
variable PrintInfo, Angle
variable Psi  
struct I4PElementYL Element


//--extract the element from the database
variable checkElem
checkElem = I4PDatabaseYL(TagElement,Element)
if(checkElem==0)
	beep
	doalert /T="WARNING" 0, "ERROR \r\r Element " + TagElement  + " not yet tabulated !"
	return 0
endif

//--extract the atomic level 
variable ncl
ncl = whichlistitem(Atomic_level,Element.cl)
//print Atomic_level
//print Element.cl
//print ncl
//print "========================================"
if(ncl==-1)
	beep
	doalert /T="WARNING" 0, "ERROR \r\rAtomic level " + Atomic_level +   " of " + TagElement +  " not yet tabulated !"
	return 0
endif

//---angle between x-ray source and analyzer
if(paramisdefault(Angle)!=0)
	// MAGIC ANGLE = Acos(1/sqrt(3))
	Psi = gPsi_Magic
else
	Psi = Angle
endif	

//--extract data from structure
variable Sigma, Assym
strswitch(Xray_source)	
case "Mg":
	Sigma   =    str2num(stringfromlist(ncl,Element.picsMg))
	Assym  =    str2num(stringfromlist(ncl,Element.asymMg))
break
case "Al":		
	Sigma   =    str2num(stringfromlist(ncl,Element.picsAl))
	Assym  =    str2num(stringfromlist(ncl,Element.asymAl))
break
case "He I":		
	Sigma   =    str2num(stringfromlist(ncl,Element.picsHeI))
	Assym  =    str2num(stringfromlist(ncl,Element.asymHeI))
	print Atomic_level, Sigma, Assym
break
default:
	beep
	doalert /T="WARNING" 0, "ERROR \r\rUnknown photon source : " + Xray_source + " !"
	return 0	
endswitch

// Actual photoionization cross section for unpolarized light * 4pi (unit = Mbarns)
variable CosPsi, PICS
CosPsi = cos(Psi/180*pi)
PICS  = Sigma*(1. - Assym/4.*(3.*CosPsi^2-1.))

//-- the normalization value to C1s at the magic angle for Al-Ka
variable PICS_C1s = 0.13e-1

//--print the information if required
if(paramisdefault(PrintInfo) == 0)
	if(PrintInfo==1)
		print "==> Total photoionization cross section  : ", PICS
		print "==> Total photoionization cross section (normalized to C1s)  : ", PICS/PICS_C1s
		print "==> Photoionization cross section : ", Sigma
		print "==> Asymmetry factor : ", Assym
		print "==> Element information " + TagElement 
		print Element
	endif
endif


// Return
return PICS/PICS_C1s

End 




