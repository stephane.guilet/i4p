// Module : peak_characteristics
// Author : Remi Lazzari; lazzari@insp.jussieu.fr
// Date : December 25th, 2018
// Usage : give characteristics of a peak pointed by cursors

#pragma rtGlobals = 3		
#pragma ModuleName = peak_characteristics

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Core_Level_Characteristics()
	
// Make sure both cursors are on the same wave.
If(StringMatch(CsrInfo(a),"") || StringMatch(CsrInfo(b),""))
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rCursors must be on the top graph !"
	Return 0
EndIf
Wave /d wA = CsrWaveRef(A)
Wave /d wB = CsrWaveRef(B)
String dfA = GetWavesDataFolder(wA, 2)
String dfB = GetWavesDataFolder(wB, 2)
If (CmpStr(dfA, dfB) != 0)
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rBoth cursors must be on the same trace !"
	Return 0
Endif

// Find the wave that the cursors are on and make spectrum wave
Wave /d spectrum = CsrWaveRef(A)

// Cursor position
Variable xa, xb
xa = xcsr(a)
xb = xcsr(b)

// Peak characteristics
PeakValues(spectrum,xa,xb)

// Peak characteristics of the substracted waves
String spectrum_BGS = dfA+"_BGS"
If(WaveExists($spectrum_BGS)==1)
	PeakValues($spectrum_BGS,xa,xb)
Endif
	
End

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function  PeakValues(spectrum,xa,xb)

Wave /d spectrum
Variable xa,xb
Variable AreaPeak, FWHM,x1,x2

//--Bring the command windows to front
dowindow /h/f

// Print some infos
printf "==> Folder                       : %s\r", GetWavesDataFolder(spectrum, 1)
printf "==> Spectrum                  : %s\r", NameofWave(spectrum)
printf "==> Cursor positions (eV) : %10.2f%10.2f\r", xa, xb

// Statistics of the subwave
WaveStats /Q /R=(xa,xb) spectrum

// Peak area
AreaPeak =  abs(area(spectrum,xa,xb))

// FWHM FindValue
FindLevel /Q /R=(xa,V_maxloc) spectrum, V_max/2.
x1 = V_LevelX
FindLevel /Q /R=(V_maxloc,xb) spectrum, V_max/2.
x2= V_LevelX	
FWHM = Abs(x2-x1)

// Print the data
printf "==> Energy step (eV) : %10.2f\r", DimDelta(spectrum,0)	
printf "==> Maximum  (cps)  :  %10.2f\r", V_max
printf "==> Position (eV)       :  %10.2f\r", V_maxloc 
printf "==> Area (cps.eV)      :  %10.2f\r", AreaPeak
printf "==> FWHM (eV)         :  %10.2f\r", FWHM
printf "==> Skewness           :  %10.2f\r", V_skew
printf "==> Kurtosis              :  %10.2f\r", V_kurt
		
End 

