// Module :  tools
// Author : Remi Lazzari; lazzari@insp.jussieu.fr
// Date : December 28th, 2018
// Usage : tool box


#pragma rtGlobals =3 		// Use modern global access method.
#pragma ModuleName = toolsi4p


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Various global constants
// Photon energies in eV
constant ghv_Al	= 1486.6		// Al-Ka
constant ghv_Mg	= 1253.6		// Mg-Ka
constant ghv_HeI	= 21.22		// He I
constant ghv_HeII	= 40.81		// He II
// MAGIC ANGLE = Acos(1/sqrt(3))
constant gPsi_Magic =  54.735610317245345684622999669981
// Gaussian Sigma to FWHM 
constant gGsig2fwhm =2.3548200450309493820231386529194
// Analyzer modes
strconstant gFAT = "FixedAnalyzerTransmission"
strconstant gFRR = "FixedRetardingRatio"
strconstant gFE   = "FixedEnergies"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Convert mode to standard tag
Function /S GetAnalyzerModeName(EAmode)

	String EAmode
	String EAmode_out
	
	EAmode_out = ReplaceString(" ",EAmode,"")
	Strswitch(EAmode)
		// Omicron EA 125	
		Case "CAE":		
			EAmode_out = gFAT
		Break
		Case "CRR":		
			EAmode_out = gFRR
		Break
		// Vamas
		Case "FAT":		
			EAmode_out = gFAT
		Break
		Case "FRR":		
			EAmode_out = gFRR
		Break
		//  SPECS Phoibos
		Case "FixedAnalyzerTransmission":		
			EAmode_out = gFAT
		Break	
		Case "FixedRetardingRatio":		
			EAmode_out = gFRR
		Break	
		Case "FixedEnergies":		
			EAmode_out = gFE
		Break			
	Endswitch					
	
	Return EAmode_out
	
End



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Define the default graph style for I4P
Proc I4P_GraphStyle() : GraphStyle
	PauseUpdate; Silent 1
	String graphlist = Winlist("*", ";", "WIN:1")
	If(ItemsInList(graphlist)==0)
		Beep
		DoAlert /T="WARNING" 0, "ERROR \r\rNo graph !"
		Return 0
	Endif
	ModifyGraph/Z wbRGB=(65535,65535,65535),gbRGB=(65535,65535,65535)
	ModifyGraph/Z mode=0
	ModifyGraph/Z lstyle=0
	ModifyGraph/Z lSize=1.5
	ModifyGraph/Z rgb=(65280,0,0)
	ModifyGraph/Z msize=2
	ModifyGraph/Z mirror=2
	ModifyGraph/Z font="Arial"
	ModifyGraph/Z minor=1
	ModifyGraph/Z fSize=16
	ModifyGraph/Z lblMargin(left)=5,lblMargin(bottom)=3
	ModifyGraph/Z standoff=0
	ModifyGraph/Z axOffset(left)=-0.0833333,axOffset(bottom)=0.333333
	ModifyGraph/Z notation(left)=1
	ModifyGraph/Z lblPos(left)=53,lblPos(bottom)=50
	ModifyGraph/Z lblLatPos(left)=5,lblLatPos(bottom)=-1
	Label/Z left "\\Z20\\F'Arial'Intensity (\\U)"
	Label/Z bottom "\\Z20\\F'Arial'Binding energy (\\U)"
	SetAxis/Z/A
	SetAxis/Z/A/R bottom
	Legend/C/N=legengI4P/F=0/A=MC
	ModifyGraph nticks=5
	ModifyGraph noLabel=0
	ModifyGraph offset={0,0},muloffset={0,0}
	ModifyGraph log=0
	ModifyGraph width=0
	//Cursor/P/S=0/H=1/C=(0,0,0)/M A 
	Setcolor()
EndMacro

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Define the default graph style for I4P
Proc ISS_GraphStyle() : GraphStyle
	PauseUpdate; Silent 1
	String graphlist = Winlist("*", ";", "WIN:1")
	If(ItemsInList(graphlist)==0)
		Beep
		DoAlert /T="WARNING" 0, "ERROR \r\rNo graph !"
		Return 0
	Endif
	I4P_GraphStyle() 
	SetAxis/Z/A bottom
	Label/Z bottom "\\Z20\\F'Arial'Mass (\\U)"
	
EndMacro
