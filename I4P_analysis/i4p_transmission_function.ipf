// Module :  transmission function
// Author : Remi Lazzari, lazzari@insp.jussieu.fr
// Date : October 11th 2019
// Usage : Remove transmission function from data

#pragma rtGlobals = 3		
#pragma ModuleName = transmission_function


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function CorrectFromTF()

// List of waves in the current datafolder
String saveDF = GetDataFolder(1)
String ListofWaves
ListofWaves = "From top graph;"+ "*;" + WaveList("*", ";", "" )

// The datafolder and variables
NewDataFolder /O/S root:I4P
NewDataFolder /O/S :Variables

// Ask for the wave to be corrected.
String Tracename
Prompt Tracename, "Select the spectrum to be corrected", popup ListofWaves
Variable Exponent = NumVarOrDefault("gExponent",1.0)
Prompt Exponent, "Exponent of the FAT/CAE transmission function"
String Generate_TF = StrVarOrDefault("gGenerate_TF","no")
Prompt Generate_TF, "Generate transmission function", popup "no;yes"
String HelpString
HelpString   = "1- Select the spectrum to be corrected\r"
HelpString += "2- Enter the exponent of the transmission function Tf ~(hv/2*Ek)^x\r"
HelpString += "3- Generate transmission function ?\r"
HelpString += "4- Run\r"
DoPrompt /HELP=HelpString "I4P : Correct from transmission function", Tracename, Exponent, Generate_TF
 // User canceled
If( V_Flag )
	SetDataFolder saveDF
	Print "==> Error in the input parameters"
	Return 0
Endif

// Save the data
Variable /G	gExponent 	  = Exponent
String /G	gGenerate_TF = Generate_TF

// Go back to the initial datafolder
SetDataFolder saveDF

String spectrum_name, spectrum_name_tfc, spectrum_name_tf
// In case of correction applied to the top graph
If(Cmpstr(Tracename,"From top graph")==0)
	// Make sure a cursor is on wave
	If(StringMatch(CsrInfo(A),""))
		Beep
		DoAlert /T="WARNING" 0, "ERROR \r\rCursor A must be on the graph !"
		Return 0
	EndIf
	// Find the wave on which cursor points
	Wave /D spectrum = CsrWaveRef(A)
	// Set the data folder  to that of the pointed wave
	SetDataFolder GetWavesDataFolder(spectrum, 1)
	// The spectra names	
	spectrum_name 		= NameofWave(spectrum)
	spectrum_name_tfc 	= StringName_Ext(spectrum_name,"_TFC")
	spectrum_name_tf   	= StringName_Ext(spectrum_name,"_TF")	
	// The waves
	Duplicate/O spectrum $spectrum_name_tfc
	Wave /D  spectrum_tfc = $spectrum_name_tfc
	Duplicate/O spectrum $spectrum_name_tf
	Wave /D  spectrum_tf  = $spectrum_name_tf
	// Correct the wave from the transmission function
	DivideByTF(spectrum_tfc,spectrum_tf,exponent)
	// Clean in case of
	If(Cmpstr(Generate_TF,"no")==0)
		KillWaves /Z spectrum_tf
	Endif
	//Clean the graph if macro has already been run and the new graph
	RemoveFromGraph /Z $spectrum_name_tfc
	AppendtoGraph /C=(10000,65535,10000) $spectrum_name_tfc //Green
	// Go back to current datafolder
	SetDataFolder saveDF
	// Warn the user	
	Print "==> Correct from transmission function :  ", spectrum_name

// In case of correction applied to the waves in the folder
Else 

	// The list of waves to be corrected 
	ListofWaves = WaveList(Tracename, ";", "" )
	// Correct all of them from transmission function
	Variable kk
	For(kk=0;kk<ItemsInList(ListofWaves,";");kk+=1)
	 	// The spectra names
		spectrum_name		= Stringfromlist(kk,ListofWaves,";")
		spectrum_name_tfc	= StringName_Ext(spectrum_name,"_TFC")
		spectrum_name_tf	= StringName_Ext(spectrum_name,"_TF")	
		// The waves
		Wave /D spectrum = $spectrum_name
		Duplicate/O spectrum $spectrum_name_tfc
		Wave /D  spectrum_tfc = $spectrum_name_tfc
		Duplicate/O spectrum $spectrum_name_tf
		Wave /D  spectrum_tf  = $spectrum_name_tf
		// Correct the wave from the transmission function
		DivideByTF(spectrum_tfc,spectrum_tf,exponent)
		// Clean in case of
		If(Cmpstr(Generate_TF,"no")==0)
			KillWaves /Z spectrum_tf
		Endif
		// Warn the user	
		Print "==> Correct from transmission function :  ", spectrum_name
	 EndFor

Endif

// Go back to the initial datafolder
SetDataFolder saveDF

End

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function DivideByTF(spectrum,TF,expo)

Wave /D &spectrum, &TF
Variable expo
Variable hv, dE, Es
String mode

// Excitation energy to define kinetic energy
hv = str2num(WaveNote2Value(spectrum,"Excitation energy (eV)"))
// Starting binding energy
Es =  DimOffset(spectrum, 0) 
// Binding energy step
dE =  DimDelta(spectrum,0)

// Working mode of the analyzer
mode = WaveNote2Value(spectrum,"Analyzer mode")

// Divide by the transmission function
TF  = 1.0
Strswitch(mode)
Case gFAT:	
	TF =  ((hv/2.)/(hv -Es -p*dE))^expo
Break
Case gFRR:
	TF = (hv -Es -p*dE)/(hv/2.) 
Break
Endswitch
spectrum /= TF

// Add some infos to the waves
Note spectrum, " "
Note spectrum, "# Region : corrected by transmission function" 
Note spectrum, "# Exponent of transmission function : " + num2str(expo) 
Note /K  TF
Note TF, " "
Note TF, "# Exponent of transmission function : " + num2str(expo) 
Note TF, "# Filename : "                                  + WaveNote2Value(spectrum,"Filename")
Note TF, "# Transmission function of region : "  + WaveNote2Value(spectrum,"Region")
Note TF, "# Excitation energy (eV) : "  		    + WaveNote2Value(spectrum,"Excitation energy (eV)")
Note TF, "# Analyzer mode : "  			    + WaveNote2Value(spectrum,"Analyzer mode")

End


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function CorrectFromAnalyzerTF(spectrum_name,[exponent,generateTF])

String spectrum_name
Variable exponent, generateTF

// If the parameters are not present, get default values
If(paramisdefault(exponent)==1)
	exponent = 1.0
Endif
If(paramisdefault(generateTF)==1)
	generateTF = 0
Endif

// Check
String ListofWaves = Wavelist(spectrum_name, ";", "" )
If(Itemsinlist(ListofWaves,";")==0)
	Print "==> The spectrum does not exist !"
	Return 0
Endif
Wave /D spectrum= $spectrum_name

// Generate the output waves
String spectrum_name_tfc, spectrum_name_tf
spectrum_name_tfc	= StringName_Ext(spectrum_name,"_TFC")
spectrum_name_tf	= StringName_Ext(spectrum_name,"_TF")	
Duplicate/O spectrum $spectrum_name_tfc
Wave /D  spectrum_tfc = $spectrum_name_tfc
Duplicate/O spectrum $spectrum_name_tf
Wave /D  spectrum_tf  = $spectrum_name_tf

// Correct the wave from the transmission function
DivideByTF(spectrum_tfc,spectrum_tf,exponent)

// Clean in case of
If(generateTF==0)
	KillWaves /Z spectrum_tf
Endif

End


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function GetTFfromScan()

// List of waves in the current datafolder
String saveDF = GetDataFolder(1)

// The datafolder and variables
NewDataFolder /O/S root:I4P
NewDataFolder /O/S :Variables

// Parameters
String TougaardType = StrVarOrDefault("gTougaardType","Universal Tougaard (Two-parameters)")
String BackList
BackList  = "Universal Tougaard (Two-parameters);"
BackList  += "Li (Z=3);Be (Z=4);B (Z=5);C (Z=6);Mg (Z=12);Al (Z=13);Si (Z=14);Ca (Z=20);Sc (Z=21);Ti (Z=22);V (Z=23);Cr (Z=24);Mn (Z=25);Fe (Z=26);"  
BackList  += "Co (Z=27);Ni (Z=28);Cu (Z=29);Zn (Z=30);Ga (Z=31);Ge (Z=32);Se (Z=34);Sr (Z=38);Y (Z=39);Zr (Z=40);Nb (Z=41);Mo (Z=42);Ru (Z=44);Rh (Z=45);" 
BackList  += "Pd (Z=46);Ag (Z=47);Cd (Z=48);In (Z=49);Sn (Z=50);Sb (Z=51);Te (Z=52);Ba (Z=56);La (Z=57);Ce (Z=58);Pr (Z=59);Nd (Z=60);Sm (Z=62);Eu (Z=63);"
BackList  += "Gd (Z=64);Tb (Z=65);Dy (Z=66);Ho (Z=67);Er (Z=68);Tm (Z=69);Yb (Z=70);Lu (Z=71);Hf (Z=72);Ta (Z=73);W (Z=74);Re (Z=75);Ir (Z=77);Pt (Z=78);"
BackList  += "Au (Z=79);Pb (Z=82);Bi (Z=83);"
BackList += "Three-parameters : Fe,Pd,Ti,Cu,Ag,Au;Three-parameters : Polymers;Three-parameters : Silicon dioxide;Three-parameters : Silicon;Three-parameters : Germanium;Three-parameters : Aluminium;"
BackList += "Tougaard (user given)"
Prompt TougaardType, "Tougaard background", popup BackList
Variable Gap = NumVarOrDefault("gGap",0.0)
Prompt Gap, "Gap (eV)"
String BCDG = StrVarOrDefault("gBCDG","3006; 1643; 0; 0")
Prompt BCDG, "User given :  B; C; D; Gap"
String HelpString
HelpString   = "1- Select the wide scan to be analyzed in the top graph"
HelpString += "2- Select the Tougaard background options\r"
HelpString += "3- Run\r"
DoPrompt /HELP=HelpString "I4P : Get transmission function from background of wide scan",  TougaardType, Gap, BCDG
 // User canceled
If( V_Flag )
	SetDataFolder saveDF
	Print "==> Error in the input parameters"
	Return 0
Endif

// Save the data
String /G 	gTougaardType 	= TougaardType
Variable /G 	gGap			= Gap
String /G	gBCDG			= BCDG

// Go back to the initial datafolder
SetDataFolder saveDF

// Get the Tougaard parameters
Variable B=0, C=0, D=0
If(Cmpstr(TougaardType,"Tougaard (user given)")==0)
		B = Str2Num(StringFromList(0,BCDG))
		C = Str2Num(StringFromList(1,BCDG))
		D = Str2Num(StringFromList(2,BCDG))	
		Gap = Str2Num(StringFromList(3,BCDG))	
Else
	background#Tougaard_Parameters(TougaardType,B,C,D)
Endif

// Make sure both cursors are on the same wave.
If(StringMatch(CsrInfo(A),"")==1 || StringMatch(CsrInfo(B),"")==1)
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rBoth cursors must be on the top graph !"
	Return 0
EndIf
Wave /d wA = CsrWaveRef(A)
Wave /d wB = CsrWaveRef(B)
String dfA = GetWavesDataFolder(wA, 2)
String dfB = GetWavesDataFolder(wB, 2)
If (CmpStr(dfA, dfB) != 0)
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rBoth cursors must be on the same trace !"
	Return 0
Endif

// Check that the measurement is in FAT
If(Cmpstr(WaveNote2Value(wA,"Analyzer mode"),gFAT)!=0)
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rSpectrum should be in Fixed Analyer Transmission mode !"
	Return 0
Endif

// Cursor positions
Variable start = xcsr(A)
Variable stop = xcsr(B)
//Swap cursors if they are backwards and the y values
If(start>stop)
	Swap(start,stop)
EndIf

// Go to the datafolder
SetDataFolder GetWavesDataFolder(wA, 1)

// Create the working wave
String spectrum_name = StringName_Ext(NameofWave(wA),"_CUT")
Duplicate /O/R=(start,stop) wA $spectrum_name
Wave /D spectrum= $spectrum_name

// Search for the minimum of area for Tf~1/Ek^n
Variable nn = 50 		// Total number of points in exponent
Variable dnn = 0.05	// Step in exponent
Variable nn0 = -0.1 	// Start value of exponent
Variable nq = 3		// Number of points around the minimum for quadratic regression
Make /O/D/N=(nn)  expo, areaabsTFCTBGS
expo = p*dnn + nn0
Variable kk
For(kk=0;kk<numpnts(expo);kk+=1)
	areaabsTFCTBGS[kk] =  GetArea_TFC_TBGS(expo[kk],B,C,D,Gap,spectrum)
Endfor
// Statistics of results
WaveStats /M=1/Q areaabsTFCTBGS
// Get the exponent from function minium
Variable expoTF, dexpoTF 
Printf "==> Get the transmission exponent function between %10.2f%s%10.2f%s\r", start, " and ", stop, " eV"
Printf "==> Wave : " + NameofWave(wA) + "\r"
If(V_minloc-nq>=0 && V_minloc+nq<nn)
	// Quadratic regression
	Make /D/O/N=(2*nq+1) win_expo, win_area, win_fit 
	win_expo  = expo[V_minloc-nq+p]
	win_area  = areaabsTFCTBGS[V_minloc-nq+p]
	CurveFit /K={expo[V_minloc]} /M=0 /N=1 /NTHR=0  /ODR=1 /Q=1 /W=2  Poly_XOffset 3,  win_area  /A=0   /X=win_expo /D=win_fit
	// Results
	Wave /D  W_coef = W_coef, W_fitConstants = W_fitConstants
	expoTF =  W_fitConstants[0] - 2*W_coef[1]/W_coef[2]
	Variable areaend, spm1, spm2, spp
	//dexpoTF = W_coef[2]*(expoTF-W_fitConstants[0])^2 + W_coef[1]*(expoTF-W_fitConstants[0]) + W_coef[0]
	areaend = GetArea_TFC_TBGS(expoTF,B,C,D,Gap,spectrum,ym1=spm1, ym2=spm2, yp=spp)
	Variable dareaend = Sqrt(Abs(spp))*(stop-start)
	dexpoTF = Sqrt(Abs(dareaend/W_coef[2]))
	// Warn the user
	Printf "==> Exponent : " + num2str(expoTF)  + " � " + num2str(dexpoTF) + "\r"
	Printf "==> Area (abs) : " + num2str(areaend)  + " � " + num2str(dareaend) + "\r"
	Printf "==> <I> (abs) : " + num2str(spm1) + "\r"
	Printf "==> sqrt[<(I-<I>)^2>] (abs) : " + num2str(spm2)  + "\r"
	Printf "==> Most probable I (abs) : " + num2str(spp) + "\r"	
	// Clean
	Killwaves /Z win_expo, win_area, win_fit
	Killwaves /Z W_coef, M_Jacobian, W_sigma, W_fitConstants
Else
	expoTF = 0
	dexpoTF = 0
	Printf "==> WARNING : minimum ill-defined !\r"
Endif
// Clean
KillWaves /Z expo, areaabsTFCTBGS

// The spectra names	
String spectrum_name_tfc 	= StringName_Ext(spectrum_name,"_TFC")
String spectrum_name_tf   	= StringName_Ext(spectrum_name,"_TF")	
String spectrum_name_bg  	= StringName_Ext(spectrum_name_tfc,"_BG")	
String spectrum_name_bgs  	= StringName_Ext(spectrum_name_tfc,"_BGS")	
// Transmission corrected / Transmission function
Duplicate/O spectrum $spectrum_name_tfc
Wave /D  spectrum_tfc = $spectrum_name_tfc
Duplicate/O spectrum $spectrum_name_tf
Wave /D  spectrum_tf  = $spectrum_name_tf
Setscale d, DimOffset(spectrum,0), DimDelta(spectrum,0), "",  spectrum_tf
// Transmission function
TFC(expoTF,spectrum_tfc,tf=spectrum_tf)
// Add some infos 
Note spectrum_tfc, " "
Note spectrum_tfc, "# Region : corrected by transmission function" 
Note spectrum_tfc, "# Exponent of transmission function : " + num2str(expoTF) 
Note /K  spectrum_tf
Note spectrum_tf, " "
Note spectrum_tf, "# Filename : "                                  + WaveNote2Value(spectrum,"Filename")
Note spectrum_tf, "# Transmission function of region : "  + WaveNote2Value(spectrum,"Region")
Note spectrum_tf, "# Excitation energy (eV) : "  		    + WaveNote2Value(spectrum,"Excitation energy (eV)")
Note spectrum_tf, "# Analyzer mode : "  			    + WaveNote2Value(spectrum,"Analyzer mode")
Note spectrum_tf, "# Exponent of transmission function : " + num2str(expoTF) + "�" + num2str(dexpoTF)
// Background corrected / Background
Duplicate/O spectrum_tfc $spectrum_name_bg
Wave /D  spectrum_bg = $spectrum_name_bg
Duplicate/O spectrum_tfc $spectrum_name_bgs
Wave /D  spectrum_bgs = $spectrum_name_bgs
// Background and background substracted spectrum
TBG(B,C,D,Gap,spectrum_tfc,spectrum_bg)
spectrum_bgs = spectrum_tfc - spectrum_bg 
// Add some infos 
Note spectrum_bgs, " "
Note spectrum_bgs, "# Substracted background : " + TougaardType
Note spectrum_bgs, "# Binding energy range (eV) : [" + num2str(start)+";"+num2str(stop)+"]"
Note spectrum_bgs, "# B-factor : " + num2str(B)
Note spectrum_bgs, "# C-factor : " + num2str(C)
Note spectrum_bgs, "# D-factor : " + num2str(D)
Note spectrum_bgs, "# Gap : "      + num2str(Gap)
Note spectrum_bg, " "
Note spectrum_bg,  "# Substracted background : " + TougaardType
Note spectrum_bg,   "# Binding energy range (eV) : [" + num2str(start)+";"+num2str(stop)+"]"
Note spectrum_bg,   "# B-factor : " + num2str(B)
Note spectrum_bg,   "# C-factor : " + num2str(C)
Note spectrum_bg,   "# D-factor : " + num2str(D)
Note spectrum_bg,   "# Gap : " 	+ num2str(Gap)

//Clean the graph if macro has already been run and the new graph
// Transmission corrected spectrum = Black
RemoveFromGraph /Z $spectrum_name_tfc
AppendtoGraph /C=(0,0,0) $spectrum_name_tfc 
// Background spectrum = Blue
RemoveFromGraph /Z $spectrum_name_bg
AppendtoGraph/C=(0,0,65535)  $spectrum_name_bg 
// Substracted spectrum = Green
RemoveFromGraph /Z $spectrum_name_bgs
AppendtoGraph/C=(0000,65535,10000)  $spectrum_name_bgs 
// Transmission function = Black dotted 
RemoveFromGraph /Z  $spectrum_name_tf
AppendToGraph /R/C=(0,0,0)  $spectrum_name_tf
ModifyGraph lstyle($spectrum_name_tf)=3
ModifyGraph tick=2,minor=1,standoff=0
Label right "\\Z20\\F'Arial' Transmission function (arb. units) "
Legend/C/N=text0/F=0/A=MC
// Result in the grah
String ExpoString 
If(dexpoTF==0)
	Sprintf ExpoString "%s",  "\\Z24\\F'Arial' T\\Bf\\M\\Z24 ill-defined"
Else
	Sprintf ExpoString "%s%4.2f%s%4.2f",  "\\Z24\\F'Arial' T\\Bf\\M\\Z24~1/E\\BK\\M\\Z24\S", expoTF,  "�", dexpoTF
Endif
TextBox/C/N=text1/F=0/A=MC ExpoString
// Zero line
ModifyGraph zero(left)=4

// Go back to the initial datafolder
SetDataFolder saveDF


End

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Area of the absolute of the transmission/background corrected wave
Static Function GetArea_TFC_TBGS(nn,B,C,D,G,yw,[ym1, ym2, yp])

Variable nn,B,C,D,G
Wave /D yw
Variable &ym1, &ym2, &yp
Variable areatfctbgs

// Working wave
Duplicate /O yw ywi, ywb
// Correct from transmission Tf ~ 1/KE^nn
TFC(nn,ywi)
// Get the Tougaard background
TBG(B,C,D,G,ywi,ywb)
// The outputs
ywi = Abs(ywi-ywb)
areatfctbgs = Area(ywi)
If(ParamIsDefault(ym1)==0 && ParamIsDefault(ym2)==0 &&  ParamIsDefault(yp)==0)
	Make/N=250/O ywiHIST
	Histogram/P/C/B=1 ywi,ywiHIST
	Duplicate /O ywiHIST yXywiHIST, y2XywiHIST
	yXywiHIST = ywiHIST*x
	y2XywiHIST = ywiHIST*x^2
	ym1 = area(yXywiHIST)
	ym2 = area(y2XywiHIST) - ym1^2
	ym2 = sqrt(abs(ym2))
	WaveStats /Q/M=1 ywiHIST
	yp =V_maxloc
	KillWaves /Z ywiHIST, yXywiHIST, y2XywiHIST
Endif

// Clean
KillWaves /Z ywi, ywb

Return areatfctbgs

End


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Divide by the transmission function
Static Function TFC(nn,yw,[tf])

Variable nn
Wave /D  yw
Wave /D  tf

Variable hv, Es, dE

// Excitation energy to define kinetic energy
hv = str2num(WaveNote2Value(yw,"Excitation energy (eV)"))
// Starting binding energy
Es =  DimOffset(yw, 0) 
// Binding energy step
dE =  DimDelta(yw,0)
// Get the transmission function
Duplicate /O  yw ywtf
ywtf  = 1.0
ywtf =  ((hv/2.)/(hv -Es -p*dE))^nn
// Divide by the transmission function
yw /= ywtf
// Output of the transmission function
If(ParamIsDefault(tf)==0)
	tf = ywtf
Endif 
// Clean
KillWaves /Z ywtf

End

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Tougaard background
Static Function TBG(B,C,D,G,yw,ywb)

Variable B,C,D,G
Wave /D yw, ywb
Variable Es, dE

// Starting binding energy
Es =  DimOffset(yw, 0) 
// Binding energy step
dE =  DimDelta(yw,0)
// Binding energy waves
Make /O/D/N=(numpnts(yw))  xw
xw = Es + p*dE
// Step of data and number of points
Variable Tpnts = 10*(sqrt(abs(C/3))+G)/dE   // 10 times the position of the maximum of the loss function
// Definition of the convolution function
Make /O/D/N=(2*Tpnts+1) xT, yT
xT = -Tpnts*dE + p*dE
xT = xT-G
yT = xT  / ((C+xT^2)^2 + D*xT^2) * (xT>=-G) * (xT>=0)
// Offset of background
Duplicate /O yw yw_conv
yw_conv = yw - yw[0] 
// Convolution without edge correction
Convolve /A yT yw_conv
ywb = dE * B * yw_conv + yw[0]
//--------clean waves
KillWaves /z   xw  xT yT yw_conv

End