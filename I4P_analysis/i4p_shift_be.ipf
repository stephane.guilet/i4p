// Module  :  shift_be
// Author : James Mudd;  james.mudd@gmail.com, modifed by Remi Lazzari, lazzari@insp.jussieu.fr
// Date : January, 4th 2019
// Usage : shifting the binding energy scale of scaled XPS spectra and of "_BE" waves

#pragma rtGlobals = 3		
#pragma ModuleName = shift_be

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Shift_BindingEnergy()

// The list of waves in the current data folder
String saveDF = GetDataFolder(1)
String ListofWave
ListofWave = "*;" + WaveList("*", ";", "" )

// The datafolder of variables
NewDataFolder /O/S root:I4P
NewDataFolder /O/S :Variables
	
//Ask for the wave to be corrected.
String Tracename
Prompt Tracename, "Select the spectrum to be corrected", popup ListofWave
Variable BEShift = NumVarOrDefault("gBEShift",0.0)
Prompt BEShift, "Enter the shift of the Binding energy (eV)"
String HelpString
HelpString   = "1- Select the spectrum to be corrected in the current data folder\r"
HelpString += "2- Enter the value of the binding energy shift (applied to spectrum scale and spectrum_BE)\r"
HelpString += "3- Run\r"
DoPrompt /HELP=HelpString "I4P :  Shift binding energy scale", Tracename, BEShift
 // User canceled
If( V_Flag )
	SetDataFolder saveDF
	Print "==> Error in the input parameters !"
	Return 0
Endif

// Save the data
Variable /G	gBEShift 	= BEShift

// Go back to the initial datafolder
SetDataFolder saveDF

// The list of waves to be corrected 
ListofWave = WaveList(tracename, ";", "" )

// Warn the user
Print "==> Shifting binding energy scale by ", BEShift, " eV"
		
// Get the name of the wave in the selected data folder and shift them depending if theya are scaled or BE
Variable i
Variable offset
String spectrum_name
String sBEShift_old, sBEShift_new
String spectrum_note

For(i=0;i<ItemsInList(ListofWave,";");i+=1)

	spectrum_name =StringFromList(i,ListofWave,";")
	Wave /d spectrum = $spectrum_name
	
	If( Cmpstr(spectrum_name[Strlen(spectrum_name)-3,Strlen(spectrum_name)],"_BE")==0 && Cmpstr(WaveUnits(spectrum,-1),"cps")!=0 ) 
		spectrum = spectrum + BEShift
	Else
		offset = DimOffset(spectrum, 0) + BEShift
		Setscale/P x, offset, DimDelta(spectrum,0), spectrum
	Endif
	Print "==> Wave :  ", spectrum_name
	
	// Set the shift value as wave notes
	sBEShift_old = WaveNote2Value(spectrum,"Binding energy shift (eV)")
	If(StrLen(sBEShift_old)==0)
		Note spectrum, " "
	 	Note spectrum, "# Binding energy shift (eV) : " + Num2Str(BEShift)
	Else
		sBEShift_new =  " " + Num2Str( Str2Num(sBEShift_old) + BEShift)
		spectrum_note = Note(spectrum)
		spectrum_note = ReplaceStringByKey("# Binding energy shift (eV) ",spectrum_note,sBEShift_new,":","\r")
		Note /K spectrum
		Note spectrum, spectrum_note
	Endif	

 EndFor

End	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function I4P_Shift_BE_Spectrum(spectrum_name,BEShift)

String spectrum_name
Variable BEShift
Wave /d spectrum = $spectrum_name
Variable offset

If(WaveExists(spectrum)==1) 
	If(Cmpstr(WaveUnits(spectrum,-1),"cps")!=0 )
		Print "==> Not a valid spectrum !"
	Else
		offset = DimOffset(spectrum, 0) + BEShift
		Setscale/P x, offset, DimDelta(spectrum,0), spectrum
		Print "==> Shifting wave :  ",  spectrum_name, " by ", BEShift, " eV"
	Endif
Else
	Print "==> The spectrum does not exist !"
Endif

If(WaveExists($spectrum_name+"_BE")==1)
	Wave /d spectrum_BE = $spectrum_name+"_BE"
	spectrum_BE += BEShift
	Print "==> Shifting wave :  ",  spectrum_name+"_BE", " by ", BEShift, " eV"
Endif 

End 

