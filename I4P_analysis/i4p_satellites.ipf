// Module :  satellites
// Author : Remi Lazzari, lazzari@insp.jussieu.fr
// Date : December 27th 2018
// Usage : Remove satellites from a non-monochromatic XPS/UPS spectrum

#pragma rtGlobals = 3		
#pragma ModuleName = satellites


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Substract_Satellites()

// Make sure one cursor is on the top graph.
If(StringMatch(CsrInfo(A),""))
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\A cursor must be on graph to point at trace !"
	Return 0
EndIf

// The current datafolder
String saveDF = GetDataFolder(1)

// The datafolder and variables
NewDataFolder /O/S root:i4P
NewDataFolder /O/S :Variables

// Selected parameters
String Xray_source = StrVarOrDefault("gXray_source","Al Kalpha")
Prompt Xray_source, "Select excitation source", popup "Al Kalpha;Mg Kalpha;He I with He II;He II with He I"
String Xray_reference = StrVarOrDefault("gXray_reference","Klauber1993")
Prompt Xray_reference, "Select reference (XPS)", popup "Klauber1993;WagnerHandBook;Krause1975"
Variable He_ratio = NumVarOrDefault("gHe_ratio",0.0)
Prompt He_ratio, "Enter the He I/II or II/I ratio (%)"
String HelpString
HelpString   = "1- Put a cursor on spectrum \r"
HelpString += "2- Select the excitation source \r"
HelpString += "3- Select reference for satellites \r"	
HelpString += "4- Enter the He II/I ratio for UPS \r"
HelpString += "5- Run\r"	
HelpString += "Warning : substraction not valid for Auger lines/ no variations of photoionization cross section \r "
DoPrompt /HELP = HelpString "I4P : Source satellite substraction", Xray_source, Xray_reference, He_ratio
He_ratio /= 100
If (V_Flag)
	SetDataFolder saveDF
	Print "==> Error in the input parameters !"
	Return 0
Endif

// Save the data
String /G	gXray_source	= Xray_source
String /G 	gXray_reference	= Xray_reference
Variable /G	gHe_ratio 		= He_ratio

// go back to current datafolder
SetDataFolder saveDF

// Warning
Print "==> Warning : satellite removal is not valid for Auger lines ! "

// Find the Wave on which the cursor is on and point to the spectrum Wave
Wave /D spectrum = CsrWaveRef(A)

// Set the data folder 
SetDataFolder(GetWavesDataFolder(spectrum, 1))

// Clean Waves in case of 
KillWaves /Z satellites_position, satellites_amplitude, satellites_intensity, satellites_factor 
KillWaves /Z sp, spo, spectrum_extra, spectrum_extra_sub


// Characteristics of each spectrum
Variable nsat
Strswitch(Xray_reference)
Case "Klauber1993":
	nsat = 6
Break
Case "Krause1975":
		Strswitch(Xray_source)
		Case "Mg Kalpha":
			nsat = 16
		Break
		Case "Al Kalpha":
			nsat = 15
		Break
		Endswitch
Break
Case "WagnerHandBook":
	nsat = 5
Endswitch

// Case of UPS
Strswitch(Xray_source)
Case "He I with He II":
	nsat = 5
Break
Case "He II with He I":
	nsat = 2
Break
Endswitch
	
// Positions and factors for all satellites
Variable Xray_energy
Make /D/N=(nsat) satellites_position, satellites_amplitude, satellites_intensity,  satellites_factor
Strswitch(Xray_source)
	Case "Mg Kalpha": 
		Xray_energy = ghv_Mg
		// Warn the user
		Print "==> Substract satellites from Mg Kalpha X-ray source (", Xray_reference,")"
		// Select the reference
		Strswitch(Xray_reference)
		Case "Klauber1993":
		// From "Refinement of Mg and Al Kalpha X-ray source functions", by C. Klauber, Surf. Interf. Anal. 20 (1993) 703-715
		// Satellites characteristics : relative position and amplitude relative to Kalpha1
		// Ka', Ka3, Ka3', Ka4, Ka5, Ka6
		satellites_position = {4.740,8.210,8.487,10.095,17.404,20.430}
		satellites_amplitude = {0.01027,0.06797,0.03469,0.04905,0.004268,0.003362}
		satellites_intensity = {0.02099,0.07868,0.04712,0.09071,0.01129,0.00538}
		// Take the centroid between Kalpha1-Kalpha2 lines (E_Kalpha2-E_Kalpha1=-0.2655; I_Kalpha2/I_Kalpha1=0.5)
		satellites_position = satellites_position + 0.265*0.5
		// Renormalize to Kalpha1+Kalpha2 intensity
		satellites_amplitude = satellites_amplitude/1.5
		satellites_intensity = satellites_intensity/1.5
		// The used values
		satellites_factor = satellites_intensity
		Break
		Case "Krause1975":
		// From "X ray emission spectra from Mg and Al", by M.O Krause and J.G. Ferreira, J. Phys B : Atom Mol. Phys. 8 (1975) 2007
		// a",a',a",a3,a4, a8,a5,a7,a6,a9,a11,b',a10,a13,a14,b,b1/2
		satellites_position = {3.6,4.6,8.5,10.1,15.7,17.4,19.2,20.6,24.0,27.1,28.8,30.0,33.8,37.7,48.6,49.9}
		satellites_intensity = {0.3,1.0,9.1,5.1,0.12,0.76,0.29,0.48,0.02,0.03,0.02,0.06,0.01,0.01,0.55,1.7}
		satellites_intensity = satellites_intensity/100
		// The used values
		satellites_factor = satellites_intensity
		Break
		Case "WagnerHandBook":
		// From "Handbook of X-ray photoelectron spectroscopy", by C.D. Wagner et al, 1979
		// a3, a4, a5, a6, b
		satellites_position = {8.4,10.2,17.5,20.0,48.5}
		satellites_intensity = {8.0,4.1,0.55,0.45,0.5}
		satellites_intensity = satellites_intensity/100
		// The used values
		satellites_factor = satellites_intensity
		Break
		Endswitch	
	Break
	Case "Al Kalpha":
		Xray_energy = ghv_Al  
		// Warn the user
		Print "==> Substract satellites from Al Kalpha X-ray source (", Xray_reference,")"
		// Select the reference
		Strswitch(Xray_reference)
		Case "Klauber1993":
		// From "Refinement of Mg and Al Kalpha X-ray source functions", by C. Klauber, Surf. Interf. Anal. 20 (1993) 703-715
		// Satellites characteristics : relative position and amplitude relative to Kalpha1
		// Ka', Ka3, Ka3', Ka4, Ka5, Ka6
		satellites_position = {5.452,9.526,9.958,11.701,20.072,23.576}
		satellites_amplitude = {0.008369,0.06514,0.02078,0.03081,0.002459,0.001828}
		satellites_intensity = {0.01928,0.07774,0.02373,0.06139,0.00634,0.00276}		
		// Take the centroid between Kalpha1-Kalpha2 lines (E_Kalpha2-E_Kalpha1=-0.4155; I_Kalpha2/I_Kalpha1=0.5)
		satellites_position = satellites_position + 0.415*0.5
		// Renormalize to Kalpha1+Kalpha2 intensity
		satellites_amplitude = satellites_amplitude/1.5
		satellites_intensity = satellites_intensity/1.5
		// The used values
		satellites_factor = satellites_intensity	
		Break
		Case "Krause1975":
		// From "X ray emission spectra from Mg and Al", by M.O Krause and J.G. Ferreira, J. Phys B : Atom Mol. Phys. 8 (1975) 2007
		// a",a',a",a3,a4, a8,a5,a7,a6,a9,a11,b',a10,a13,a14,b,b1/2
		satellites_position = {4.7,5.8,9.7,11.7,18.5,20.0,21.8,23.6,28.0,30.5,33.6,38.8,42.9,70.6,72.7}
		satellites_intensity = {0.3,0.7,7.3,3.1,0.10,0.41,0.10,0.28,0.03,0.02,0.05,0.08,0.05,0.76,2.4}
		satellites_intensity = satellites_intensity/100
		// The used values
		satellites_factor = satellites_intensity
		Break
		Case "WagnerHandBook":
		// From "Handbook of X-ray photoelectron spectroscopy", by C.D. Wagner et al, 1979
		// a3, a4, a5, a6, b
		satellites_position = {9.8,11.8,20.1,23.4,69.7}
		satellites_intensity = {6.4,3.2,0.4,0.3,0.55}
		satellites_intensity = satellites_intensity/100
		// The used values
		satellites_factor = satellites_intensity
		Break
		Endswitch	
	Break
	Case "He I with He II":
		Xray_energy = ghv_HeI
		// Warn the user
		Print "==> Substract satellites from He I (approximative values / depend on gas pressure)"
		// He Ib, Ig, IIa, IIb, IIg
		satellites_position = {1.87,2.52,19.59,27.15,29.8}
		satellites_intensity = {0.015,0.005,He_ratio,He_ratio*0.1,He_ratio*0}
		// The used values
		satellites_factor = satellites_intensity
	Break
	Case "He II with He I":
		Xray_energy = ghv_HeII
		// Warn the user
		Print "==> Substract satellites from He II (approximative values / depend on gas pressure)"
		// He IIb, IIg
		satellites_position = {7.56,10.2}
		satellites_intensity = {0.1,0}
		// The used values
		satellites_factor = satellites_intensity
	Break	
Endswitch

// Extrapolate the highest binding energy point up to a  BE distance equal to K-beta line
Variable npe = numpnts(spectrum)
Variable deltaBE, startBE, endBE
deltaBE = Deltax(spectrum) 
startBE = DimOffset(spectrum,0)
endBE = startBE + (npe-1)*deltaBE
Variable BEoffset = Min(satellites_position[nsat-1],Xray_energy-endBE)
Variable npee = npe + Round(BEoffset/deltaBE)
Make /O/D/N=(npee) spectrum_extra, spectrum_extra_sub
spectrum_extra[0,npe-1] = spectrum[p]
spectrum_extra[npe,npee-1] = spectrum[npe-1]	
SetScale /P x, startBE, deltaBE,"", spectrum_extra
SetScale /P x, startBE, deltaBE,"", spectrum_extra_sub
		
// Loop over energies and substract the satellites
endBE = startBE + (npee-1)*deltaBE
Variable be, i, k
For(be=endBE;be>=startBE;be-=deltaBE)
	i = x2pnt(spectrum_extra_sub,be)
	spectrum_extra_sub[i] = spectrum_extra[i] 
	For(k=0;k<nsat;k+=1)
		If(be<=endBE-satellites_position[k])
			spectrum_extra_sub[i] = spectrum_extra_sub[i]  - satellites_factor[k] *spectrum_extra_sub(be+satellites_position[k])
		Endif
	Endfor
Endfor
	
// Treat the special case of He II with He I lines
// Remove ghost lines by increasing BE
If(cmpstr(Xray_source,"He II with He I")==0)
	Make /O/D/N=3 position_He, factor_He
	// He Ia, Ib, Ig 
	position_He = {17.07,17.72,19.59}
	factor_He = {0.005,0.015,1}
	factor_He *= He_ratio
	For(be=startBE;be<endBE+deltaBE;be+=deltaBE)
		i = x2pnt(spectrum_extra_sub,be)
		For(k=0;k<numpnts(position_He);k+=1)
			If(be>= startBE+position_He[k])
				spectrum_extra_sub[i] = spectrum_extra_sub[i] - factor_He[k]*spectrum_extra_sub(be-position_He[k])
			Endif
		Endfor
	Endfor
	KillWaves /Z position_He, factor_He
Endif
		
// Create the satellite substracted Waves
String spectrum_name = nameofWave(spectrum)
String spectrum_name_sa = StringName_Ext(spectrum_name,"_SA")
Duplicate/O spectrum $spectrum_name_sa
String spectrum_name_sas = StringName_Ext(spectrum_name,"_SAS")
Duplicate/O spectrum $spectrum_name_sas
	
Wave /D satellites = $spectrum_name_sa
Wave /D satellites_sub = $spectrum_name_sas

// Cut out the results	
satellites_sub[0,npe-1] = spectrum_extra_sub[p]
satellites = spectrum - satellites_sub 

	 	
//Clean the graph if macro has already been run
RemoveFromGraph/Z  $spectrum_name_sas
RemoveFromGraph/Z  $spectrum_name_sa

// Add some infos 
Note satellites_sub, " "
Note satellites_sub, "# Substracted satellites of : " + Xray_source + " (" + Xray_reference +  ")"
Note satellites, " "
Note satellites,        "# Substracted satellites of : " + Xray_source + " (" + Xray_reference +  ")"

//Add Waves to graph
AppendtoGraph/C=(10000,65535,10000) satellites_sub //Green
AppendtoGraph/C=(0,0,65535) satellites //Blue

// Clear Waves
KillWaves /Z satellites_position, satellites_amplitude, satellites_intensity, satellites_factor 
KillWaves /Z sp, spo, spectrum_extra, spectrum_extra_sub

// go back to current datafolder
SetDataFolder saveDF

End



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Deconvolute_Satellites()

// Make sure one cursor is on the top graph.
If(StringMatch(CsrInfo(A),""))
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\A cursor must be on graph to point at trace !"
	Return 0
EndIf

// The datafolder and variables
String saveDF = GetDataFolder(1)
NewDataFolder /O/S root:i4P
NewDataFolder /O/S :Variables

// Selected parameters
String Xray_source = StrVarOrDefault("gXray_source","Al Kalpha")
Prompt Xray_source, "Select excitation source", popup "Al Kalpha;Mg Kalpha;He I with He II"
String Xray_reference = StrVarOrDefault("gXray_reference","Klauber1993")
Prompt Xray_reference, "Select reference (XPS)", popup "Klauber1993;WagnerHandBook;Krause1975"
Variable He_ratio = NumVarOrDefault("gHe_ratio",0.0)
Prompt He_ratio, "Enter the He II/I ratio (%)"
String Ka1Ka2 = StrVarOrDefault("gKa1Ka2","Ka1+Ka2")
Prompt Ka1Ka2, "Keep X-ray lines",  popup "Ka1+Ka2;Ka1 only;none"
Variable FWHMG  = NumVarOrDefault("gFWHMG",0.0)
Prompt FWHMG, "Apparatus Gaussian FWHM (eV)"
String Wiener_Filtering = StrVarOrDefault("gWiener_Filtering","no")
Prompt Wiener_Filtering, "Wiener filtering",  popup "no;yes"
Variable npf = NumVarOrDefault("gnpf",7)
Prompt npf, "Number of points of the moving windows"
Variable deg = NumVarOrDefault("gdeg",2)
Prompt deg, "Interpolation polynom order"
String HelpString
HelpString   = "1- Put a cursor on spectrum\r"
HelpString += "2- Select the excitation source\r"
HelpString += "3- Select reference for satellites\r"	
HelpString += "4- Enter the He II/I for UPS`r"
HelpString += "5- Enter the apparatus FWHM resolution\r"
HelpString += "6- X-ray lines to keep\r"	
HelpString += "7- Activate Wiener filtering and enter its parameters\r"	
HelpString += "8- Run\r"	
HelpString += "Warning : deconvolution not valid for Auger lines \r "
DoPrompt /HELP = HelpString "I4P : Source satellites deconvolution", Xray_source, Xray_reference ,He_ratio, Ka1Ka2, FWHMG, Wiener_Filtering, npf, deg
He_ratio /= 100
If (V_Flag)
	SetDataFolder saveDF
	Print "==> Error in the input parameters !"
	Return 0
Endif

// Save the data
String /G	gXray_source	= Xray_source
String /G 	gXray_reference	= Xray_reference
Variable /G	gHe_ratio 		= He_ratio
String /G	gKa1Ka2		= Ka1Ka2
Variable /G	gFWHMG 		= FWHMG
String /G	gWiener_Filtering	= Wiener_Filtering
Variable /G	gnpf				= npf
Variable /G	gdeg			= deg

// go back to current datafolder
SetDataFolder saveDF

// Warning
Print "==> Warning : satellite deconvolution is not valid for Auger lines ! "

// Find the wave on which the cursor is on and point to the spectrum wave
Wave /D spectrum_wave = CsrWaveRef(A)
String spectrum_name = NameofWave(spectrum_wave)

// Set the data folder of the spectrum wave
SetDataFolder(GetWavesDataFolder(spectrum_wave, 1))

// Fill the satellite wave
Variable nsat
//==> X-ray source
Strswitch(Xray_reference)
Case "WagnerHandBook":
// From "Handbook of X-ray photoelectron spectroscopy", by C.D. Wagner et al, 1979
// a3, a4, a5, a6, b
	Strswitch(Xray_source)
	Case "Mg Kalpha":
		// Mg Ka
		nsat = 6
		Make /O/D/N=(nsat,3)/o satellites
		satellites[][0] = {0.,8.4,10.2,17.5,20.0,48.5}
		satellites[][1] = {1.,0.08,0.041,0.0055,0.0045,0.005}
		satellites[][2] = 0.68
	Break
	Case "Al Kalpha":	
		// Al Ka
		nsat = 6
		Make /O/D/N=(nsat,3)/o satellites
		satellites[][0] = {0.,9.8,11.8,20.1,23.4,69.7}
		satellites[][1] = {1.,0.064,0.032,0.004,0.003,0.0055}
		satellites[][2] = 0.83
	Break
	EndSwitch
Break
Case "Klauber1993":
// From "Refinement of Mg and Al Kasymm X-ray source functions", by C. Klauber, Surf. Interf. Anal. 20 (1993) 703-715
// Satellites characteristics : relative position and amplitude relative to Ka1
// Ka', Ka3, Ka3', Ka4, Ka5, Ka6
	Strswitch(Xray_source)
	Case "Mg Kalpha":
		// Mg Ka
		nsat = 8
		Make /O/D/N=(nsat,3)/o satellites
		satellites[][0] = {0.,-0.265,4.740,8.210,8.487,10.095,17.404,20.430}
		satellites[][1] = {1.,0.5,0.02099,0.07868,0.04712,0.09071,0.01129,0.00538}
		satellites[][2] = {0.541,0.541,1.1056,0.6264,0.7349,1.0007,1.4311,0.8656}
	Break
	Case "Al Kalpha":
		// Al Ka
		nsat = 8
		Make /O/D/N=(nsat,3)/o satellites
		satellites[][0] = {0.,-0.415,5.452,9.526,9.958,11.701,20.072,23.576}
		satellites[][1] = {1.,0.5,0.01928,0.07774,0.02373,0.06139,0.00634,0.00276}
		satellites[][2] = {0.58,0.58,1.3366,0.6922,0.6623,1.1557,1.495,0.877}
	Break
	EndSwitch
Break
Case "Krause1975":
// From "X ray emission spectra from Mg and Al", by M.O Krause and J.G. Ferreira, J. Phys B : Atom Mol. Phys. 8 (1975) 2007
// a",a',a",a3,a4, a8,a5,a7,a6,a9,a11,b',a10,a13,a14,b,b1/2
	Strswitch(Xray_source)
	Case "Mg Kalpha":
		// Mg Ka
		nsat = 17
		Make /O/D/N=(nsat,3)/o satellites
		satellites[][0]= {0.,3.6, 4.6 ,8.5, 10.1, 15.7, 17.4, 19.2, 20.6, 24.0, 27.1, 28.8, 30.0, 33.8, 37.7, 48.6, 49.9}
		satellites[][1] = {1.,0.003,0.01,0.091,0.051,0.0012,0.0076,0.0029,0.0048,0.0002,0.0003,0.0002,0.0006,0.0001,0.0001,0.0055,0.017}
		satellites[][2] = 0.68 
	Break
	Case "Al Kalpha":
		// Al Ka
		nsat = 16
		Make /O/D/N=(nsat,3)/o satellites
		satellites[][0] = {0.,4.7, 5.8, 9.7, 11.7, 18.5, 20.0, 21.8, 23.6, 28.0, 30.5, 33.6, 38.8, 42.9, 70.6, 72.7}
		satellites[][1] = {1.,0.003,0.007,0.073,0.031,0.0010,0.0041,0.0010,0.0028,0.0003,0.0002,0.0005,0.0008,0.0005,0.0076,0.024}
		satellites[][2] =  0.83
	Break
	EndSwitch
Break
EndSwitch
//==> UV source
If(Cmpstr(Xray_source,"He I with He II")==0)
	// He I
	nsat = 6
	Make  /O/D/N=(nsat,3)/o satellites
	satellites[][0] = {0.,1.87,2.52,19.59,27.15,29.8}
	satellites[][1] = {1.,0.015,0.005,He_ratio,He_ratio*0.1,He_ratio*0}
	satellites[][2] = 0.01
Endif

// Number of kept X-ray lines after deconvolution
Variable nsatkept
StrSwitch(Ka1Ka2)
Case "Ka1+Ka2":  
	nsatkept = 2
Break
Case "Ka1 only":
	nsatkept = 1
Break
Case "none":
	nsatkept = 0
Break
EndSwitch
If(Cmpstr(Xray_source,"He I with He II")==0 || Cmpstr(Xray_reference,"WagnerHandBook")==0 ||  Cmpstr(Xray_reference,"Krause1975")==0)
	nsatkept = 1
Endif

// Make the extended spectrum wave ot avoid edge effect in the Fourier transform
Variable nps = numpnts(spectrum_wave)
Make /D/O/N=(3*nps) spectrum
SetScale/P x, DimOffset(spectrum_wave,0)-nps*DimDelta(spectrum_wave,0), DimDelta(spectrum_wave,0), "eV",  spectrum
spectrum[0,nps-1] 		  = spectrum_wave[0]  // + (p-nps+1)*spectrum_wave[0]/(nps-1) 
spectrum[nps,2*nps-1] 	  = spectrum_wave[p-nps]
spectrum[2*nps,3*nps-1]	  = spectrum_wave[nps-1] // - (p-2*nps+1)*spectrum_wave[nps-1]/(nps-1) 

// For FFT, ensure that the number of points in the spectrum is even by deleting the last one
If(Mod(numpnts(spectrum),2)==1)
	DeletePoints numpnts(spectrum)-1,1, spectrum
Endif

// FFT of the spectrum wave without windowing
FFT	/OUT=1/DEST=spectrum_FFT spectrum 

// Fourier transform of the satelitte resolution function as a sum of the continuous Fourier transform of Lorentzian peaks
Duplicate /O/C spectrum_FFT RFS_FFT // with all satellites
Duplicate /O/C spectrum_FFT RF_FFT // with only Ka1/Ka2
Make /D/O/N=(numpnts(RFS_FFT)) qq // reciprocal vector
qq = p*DimDelta(RFS_FFT,0)
Variable ns
RFS_FFT = 0.
For(ns=0;ns<nsat;ns+=1)
	RFS_FFT += FFTLorentz(qq,satellites[ns][1],satellites[ns][0],satellites[ns][2])
Endfor 
If(nsatkept!=0)
	RF_FFT = 0.
	For(ns=0;ns<nsatkept;ns+=1)
		RF_FFT += FFTLorentz(qq,satellites[ns][1],satellites[ns][0],satellites[ns][2])
	Endfor
Else
	RF_FFT = 1.
Endif

// Mutiply by Fourier transform of the Gaussian apparatus resolution function
 RFS_FFT *= FFTGaussian(qq,FWHMG)

// Deconvolution through division by the FT of the resolution function with or without Wiener filtering
StrSwitch(Wiener_Filtering)
Case "no":
	// Simple division
	spectrum_FFT /= RFS_FFT
Break
Case "yes":
	// Define the noise through fit over a moving windows (kind of filtering)
	Duplicate /O spectrum be
	Duplicate /O spectrum noise
	Duplicate /O spectrum signal
	be = DimOffset(spectrum,0) +  p*DimDelta(spectrum,0)
	Variable npe = numpnts(spectrum)
	Variable nw = floor(npf/2)
	Make  /O/D/N=(npf) win, win_be, win_res
	Variable ii
	For(ii=nps+nw;ii<2*nps-nw;ii+=1)		
		win_be  = be[ii-nw+p]
		win 	= spectrum[ii-nw+p]
		CurveFit /K={be[ii]} /M=0 /N=1 /NTHR=0  /ODR=1 /Q=1 /W=2  Poly_XOffset deg+1,  win  /A=0  /R=win_res /X=win_be 
		noise[ii] 	= win_res[nw]
	Endfor
	For(ii=0;ii<nps+nw;ii+=1)
		noise[ii] 	= noise[nps+nw]
	Endfor	
	For(ii=2*nps-nw;ii<npe;ii+=1)
		noise[ii] 	= noise[2*nps-nw-1]
	Endfor
	// Clean		
	KillWaves /Z be, win, win_res, win_be
	KillWaves /Z W_coef, M_Jacobian, W_sigma, W_fitConstants
	// Signal
	signal = spectrum - noise
	// FFT of the noise and signal
	FFT	/OUT=1/DEST=noise_FFT noise 
	FFT	/OUT=1/DEST=signal_FFT signal 
	// Wiener filtering
	spectrum_FFT *=  Conj(RFS_FFT) /  ( RFS_FFT*Conj(RFS_FFT)  + noise_FFT*Conj(noise_FFT)/ (signal_FFT*Conj(signal_FFT)) )
	// Clean
	KillWaves /Z noise, noise_FFT, signal, signal_FFT
Break
EndSwitch

// Multiply by the FT of the resolution kept function (Ka1+Ka2, Ka1, none)
spectrum_FFT *= RF_FFT

// Back Fourier transform
IFFT /DEST=spectrum_IFFT spectrum_FFT

// Create the satellite substracted waves
String spectrum_name_sa = StringName_Ext(spectrum_name,"_SA")
Duplicate /O spectrum_wave  $spectrum_name_sa
String spectrum_name_sad = StringName_Ext(spectrum_name,"_SAD")
Duplicate /O spectrum_wave $spectrum_name_sad
	
Wave /D spectrum_satellites   	= $spectrum_name_sa
Wave /D spectrum_deconv	 	= $spectrum_name_sad

// Cut out the results	
spectrum_deconv 	= spectrum_IFFT[p+nps]
spectrum_satellites	= spectrum[p+nps] - spectrum_IFFT[p+nps]
	 	
//Clean the graph if macro has already been run
RemoveFromGraph/Z  $spectrum_name_sad
RemoveFromGraph/Z  $spectrum_name_sa

// Add some infos 
Note spectrum_deconv, 	" "
Note spectrum_deconv,	"# Substracted satellites of : " + Xray_source + " (" + Xray_reference +  ")"
Note spectrum_deconv,	"# Keep satellites : " + Ka1Ka2
Note spectrum_satellites,	" "
Note spectrum_satellites,	"# Substracted satellites of : " + Xray_source + " (" + Xray_reference +  ")"
Note spectrum_satellites,	"# Keep satellites : " + Ka1Ka2

//Add Waves to graph
AppendtoGraph/C=(10000,65535,10000) spectrum_deconv 	//Green
AppendtoGraph/C=(0,0,65535) spectrum_satellites			 //Blue

// Clean waves
KillWaves /Z satellites, qq, spectrum, spectrum_FFT, RFS_FFT, RF_FFT, spectrum_IFFT

// go back to current datafolder
SetDataFolder saveDF

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function /C  FFTLorentz(qq,A,x0,FWHM)

Variable qq, A, x0, FWHM
Variable /C FFTLor
Variable absqq

absqq  = abs(qq)
// Compared to expected defintion of the FFT the minus is introduced here to take into account the fact that the satellites are not in BE but hv
FFTLor = A*exp(-2*pi*cmplx(0.,1.)*qq*x0-FWHM*pi*absqq)

Return FFTLor

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function /C  FFTGaussian(qq,FWHM)

Variable qq, FWHM
Variable /C FFTGauss
Variable sig2

sig2 = FWHM^2/(8.*ln(2.))
FFTGauss = exp(-qq^2*sig2)

Return FFTGauss

End
