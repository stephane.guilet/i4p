// Module :  iss
// Author :  Remi Lazzari, lazzari@insp.jussieu.fr
// Date : 2019
// Usage :  convert kinetic energy scale to mass scale in Ion Scattering Spectroscopy/ Low Energy Ion Spectroscopy

#pragma rtGlobals = 3		
#pragma ModuleName = iss

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function ISS_KineticEnergy2Mass()

// The list of waves in the current data folder
string saveDF = getdatafolder(1)
string listofwaves
listofwaves = wavelist("*", ";", "" )

// The datafolder of variables
newdatafolder /o/s root:i4P
newdatafolder /o/s :Variables

// The inputs
string spectrumname1
prompt spectrumname1, "Select spectrum for mass scale calculation" , popup listofwaves
string spectrumname2 =  strvarordefault("gspectrumname2","none")  
prompt spectrumname2, "Or enter spectra tag (none or * or *Ag* or Ag; Ti)" 
string makewave_MS = strvarordefault("gmakewave_MS","no")  
prompt makewave_MS, "Generate mass scale wave", popup "no;yes"
variable massoversampling = numvarordefault("gmassoversampling",2)
prompt massoversampling, "Mass scale oversampling"
variable m1 = numvarordefault("gm1",4.0026)  
prompt  m1, "Mass of the primary ion (amu)"
string E0choice = strvarordefault("gE0choice","from data")  
prompt  E0choice, "Kinetic energy of primary ion (eV)", popup "from data; from below"
variable E0 = numvarordefault("gE0",1000)
prompt  E0, "Kinetic energy of primary ion (eV)"
variable theta =  numvarordefault("gtheta",225)
prompt  theta, "Scattering angle (deg)"
string helpstring 
helpstring  =  "1- Select spectra to be analyzed\r"
helpstring += "2- Mass m1 (amu; default He)\r"
helpstring += "3- Choice for impact energy E0\r"
helpstring += "4- E0 value (eV)\r"
helpstring += "5- Scattering angle Theta (deg)"
helpstring += "6- Run" 
doprompt /HELP=helpstring "I4P : Ion Scattering Spectroscopy / kinetic energy to mass scale", spectrumname1, spectrumname2, makewave_MS, massoversampling, m1, E0choice, E0, theta
if( V_Flag )
	setdatafolder saveDF
	print "==> Error in the input parameters !"
	return 0
endif

// Save the data
string /g		gspectrumname2 = spectrumname2
string /g		gmakewave_MS= makewave_MS
variable /g	gmassoversampling = massoversampling
variable /g	gm1			= m1
string /g		gE0choice  	= E0choice
variable /g	gE0			= E0
variable /g 	gtheta		= theta

// Go back to the initial datafolder
setdatafolder saveDF

// get the list of waves  in the current data folder
string spectrumname
if(cmpstr(spectrumname2, "none")!=0)
	spectrumname = spectrumname2
else
	spectrumname = spectrumname1
endif
listofwaves = wavelist(spectrumname, ";", "" )
if(itemsinlist(listofwaves,";")==0)
	print "==> Data folder : ", saveDF 
	print "==> No spectra with name containing : ", spectrumname
	return 0
endif

// loop over waves in the current datafolder
print "==> Data folder : ", saveDF
variable ii
string massname, ISSname
variable nmass, dmass
variable costheta = cos(theta/180*pi)
variable kk, uu
for(ii=0;ii<itemsinlist(listofwaves,";");ii+=1)
	// define data and mass waves
	spectrumname = stringfromlist(ii,listofwaves,";")
	wave /d spectrum = $spectrumname
	massname = StringName_Ext(spectrumname,"_MS")
	duplicate /o spectrum $massname
	wave /d mass =$massname
	// incident ion energy from wave note
	if(cmpstr(E0choice,"from data")==0)
		E0 = str2num(WaveNote2Value(spectrum,"Excitation energy (eV)"))
	endif
	// kinetic energy over incident energy
	duplicate /o mass Er
	Er = dimoffset(spectrum, 0) + p*dimdelta(spectrum,0)
	Er = E0 - Er
	Er = Er/E0
	// loop over points
	for(kk=0;kk<numpnts(spectrum);kk+=1)
		uu = Er[kk]
		mass[kk] = uu + 1 -2*sqrt(uu)*costheta
		mass[kk] *= m1/(1-uu)
	endfor
	killwaves /z Er
	// annotations of the mass scale
	note mass, " "
	note mass, "# Mass scale of : "    + spectrumname	
	note mass, "# Mass of primary ion : "    + num2str(m1)		
	note mass, "# Kinetic energy of primary ion (eV) : "    + num2str(E0)	
	note mass, "# Scattering angle (deg) : "    + num2str(theta)
	// create the corrected wave
	nmass = floor(massoversampling)*numpnts(spectrum)
	ISSname = StringName_Ext(spectrumname,"_ISS")
	make /d/o/n=(nmass) $ISSname
	wave /d ISS = $ISSname
	// Interpolated the wave over mass scale
	interpolate2 /T=2 /E=2 /I=0 /N=(nmass) /Y=ISS mass, spectrum
	// set the new scale for the spectrum
	// reverted scale since spectra are in bindin energy and not kinetci energyr
	dmass = (mass[0] - mass[numpnts(mass)-1])/(nmass-1)
	setscale /p x, mass[numpnts(mass)-1] , dmass, "amu", ISS
	setscale d 0,0,"cps", ISS
	// annotation of the corrected spectrum
	note ISS, note(spectrum)
	note ISS, " "
	note ISS, "# Mass scale corrected spectrum :  "  
	note ISS, "# Mass of primary ion : "    + num2str(m1)		
	note ISS, "# Kinetic energy of primary ion (eV) : "    + num2str(E0)	
	note ISS, "# Scattering angle (deg) : "    + num2str(theta)	
	// Clean the wave in case of
	if(cmpstr(makewave_MS,"no")==0)
		killwaves /z mass
	endif
	// Warn the user
	print "==> Convert kinetic energy to mass scale of  : " + spectrumname
endfor


End