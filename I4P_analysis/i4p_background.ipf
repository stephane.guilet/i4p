// Module : background
// Author : James Mudd;  james.mudd@gmail.com
// Date : 2018
// Usage : substracting XPS backgrounds Tougaard and Shirley or Linear

#pragma rtGlobals = 3		
#pragma ModuleName = background

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Substract_Background()

// Make sure both cursors are on the same wave.
If(StringMatch(CsrInfo(A),"")==1 || StringMatch(CsrInfo(A),"")==1)
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rCursors must be on the top graph !"
	Return 0
EndIf
Wave /d wA = CsrWaveRef(A)
Wave /d wB = CsrWaveRef(B)
String dfA = GetWavesDataFolder(wA, 2)
String dfB = GetWavesDataFolder(wB, 2)
If (CmpStr(dfA, dfB) != 0)
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rBoth cursors must be on the same trace !"
	Return 0
Endif


// The current datafolder
String saveDF = GetDataFolder(1)

// The datafolder and variables
NewDataFolder /O/S root:I4P
NewDataFolder /O/S :Variables
	
// Prompt the user for the selected background
String BackgroundType = StrVarOrDefault("gBackgroundType","Shirley (pinned on cursors)")
Prompt BackgroundType, "Select background type", popup "Shirley (pinned on cursors);Tougaard (pinned on cursors);Tougaard (B-fixed);Shirley+Tougaard (user given);Linear (pinned on cursors); None"
String TougaardType = StrVarOrDefault("gTougaardType","Universal Tougaard (Two-parameters)")
String BackList
BackList  = "Universal Tougaard (Two-parameters);"
BackList  += "Li (Z=3);Be (Z=4);B (Z=5);C (Z=6);Mg (Z=12);Al (Z=13);Si (Z=14);Ca (Z=20);Sc (Z=21);Ti (Z=22);V (Z=23);Cr (Z=24);Mn (Z=25);Fe (Z=26);"  
BackList  += "Co (Z=27);Ni (Z=28);Cu (Z=29);Zn (Z=30);Ga (Z=31);Ge (Z=32);Se (Z=34);Sr (Z=38);Y (Z=39);Zr (Z=40);Nb (Z=41);Mo (Z=42);Ru (Z=44);Rh (Z=45);" 
BackList  += "Pd (Z=46);Ag (Z=47);Cd (Z=48);In (Z=49);Sn (Z=50);Sb (Z=51);Te (Z=52);Ba (Z=56);La (Z=57);Ce (Z=58);Pr (Z=59);Nd (Z=60);Sm (Z=62);Eu (Z=63);"
BackList  += "Gd (Z=64);Tb (Z=65);Dy (Z=66);Ho (Z=67);Er (Z=68);Tm (Z=69);Yb (Z=70);Lu (Z=71);Hf (Z=72);Ta (Z=73);W (Z=74);Re (Z=75);Ir (Z=77);Pt (Z=78);"
BackList  += "Au (Z=79);Pb (Z=82);Bi (Z=83):"
BackList  += "Three-parameters : Fe,Pd,Ti,Cu,Ag,Au;Three-parameters : Polymers;Three-parameters : Silicon dioxide;Three-parameters : Silicon;Three-parameters : Germanium;Three-parameters : Aluminium"
Prompt TougaardType, "Type of Tougaard background", popup BackList
Variable Gap = NumVarOrDefault("gGap",0.0)
Prompt Gap, "Gap (eV)"
String SBCD = StrVarOrDefault("gSBCD","0.03; 3006; 1643; 0")
Prompt SBCD, "User given : Shirley factor S; B; C; D; Gap"
String makeWave_BE = StrVarOrDefault("gmakeWave_BE","no")
Prompt makeWave_BE, "Make binding energy Wave", popup "no;yes"
String HelpString
HelpString   = "1- Put two cursors on spectrum at which positions background is zero (below and above peak)\r"
HelpString += "2- Select the type of background\r"	
HelpString += "3- In case of Tougard option, select default B,C,D, gap values/TougaardType specific ones (Seah) or enter all values for blend\r"
HelpString += "4- Make or not binding energy wave"		
HelpString += "5- Run"	
DoPrompt /HELP = HelpString "I4P : Background substraction", BackgroundType, TougaardType, Gap, SBCD, makeWave_BE
If (V_Flag)
	SetDataFolder saveDF
	Print "==> Error in the input parameters !"
	Return 0
Endif

// Save the data
String /G 	gBackgroundType = BackgroundType
String /G 	gTougaardType		= TougaardType
Variable /G 	gGap			= Gap
String/G		gSBCD			= SBCD
String /G	gmakeWave_BE	= makeWave_BE

// go back to current datafolder
SetDataFolder saveDF

// Find the Wave that the cursors are on and make spectrum Wave
Wave /D spectrum = CsrWaveRef(A)

// Set the data folder 
SetDataFolder GetWavesDataFolder(spectrum, 1)

// Cursor positions
Variable start = xcsr(A)
Variable stop = xcsr(B)
//Swap cursors if they are backwards and the y values
If(start>stop)
	Swap(start,stop)
EndIf
	
// Name of the Waves	
String spectrum_name
spectrum_name = nameofWave(spectrum)

String spectrum_name_cp = StringName_Ext(spectrum_name,"_cp")
Duplicate/O/R=(start,stop) spectrum $spectrum_name_cp
String spectrum_name_bg = StringName_Ext(spectrum_name,"_BG")		
Duplicate/O/R=(start,stop) spectrum $spectrum_name_bg
String spectrum_name_bgs = StringName_Ext(spectrum_name,"_BGS")	
Duplicate/O/R=(start,stop) spectrum $spectrum_name_bgs
		
Wave /D spectrum_cp = $spectrum_name_cp
Wave /D background = $spectrum_name_bg
Wave /D spectrum_bgs = $spectrum_name_bgs

//Clean the graph if macro has already been run
RemoveFromGraph/Z $spectrum_name_bg
RemoveFromGraph/Z $spectrum_name_bgs
			
// Calculate the background
Variable S=0, B=0, C=0, D=0, slope=0
Variable verbose=1
Variable peak_area
Strswitch(BackgroundType)
	Case "Shirley (pinned on cursors)":
		Printf "==> Substract Shirley background  between %10.2f%s%10.2f%s\r", start, " and ", stop, " eV"
		peak_area = Shirley_Pinned_BG(S,start,stop,spectrum_cp,background,spectrum_bgs,verbose)
	Break
	Case "Tougaard (pinned on cursors)":
		Tougaard_Parameters(TougaardType,B,C,D)
		Printf "==> Substract Tougaard background  between %10.2f%s%10.2f%s\r", start, " and ", stop, " eV"
		peak_area = Tougaard_Pinned_BG(B,C,D,Gap,start,stop,spectrum_cp,background,spectrum_bgs,verbose)
	Break
	Case "Tougaard (B-fixed)":
		Tougaard_Parameters(TougaardType,B,C,D)
		Printf "==> Substract B-C fixed Tougaard background  between %10.2f%s%10.2f%s\r", start, " and ", stop, " eV"	
		peak_area = Tougaard_Bfixed_BG(B,C,D,Gap,start,stop,spectrum_cp,background,spectrum_bgs,verbose)		
	Break
	Case "Shirley+Tougaard (user given)":
		Printf "==> Substract the blend of Shirley and Tougaard backgrounds with user given values between %10.2f%s%10.2f%s\r", start, " and ", stop, " eV"
		S = Str2Num(StringFromList(0,SBCD))
		B = Str2Num(StringFromList(1,SBCD))
		C = Str2Num(StringFromList(2,SBCD))
		D = Str2Num(StringFromList(3,SBCD))	
		peak_area = ShirleyTougaard_User_BG(S,B,C,D,Gap,start,stop,spectrum_cp,background,spectrum_bgs)
	Break
	Case "Linear (pinned on cursors)":
		Printf "==> Substract Linear background  between %10.2f%s%10.2f%s\r", start, " and ", stop, " eV"
		peak_area = Linear_Pinned_BG(slope,spectrum_cp,background,spectrum_bgs)
	Break
	Case "None":
		Printf "==> Cut out-spectrum between %10.2f%s%10.2f%s\r", start, " and ", stop, " eV"
		background = 0.
		spectrum_bgs = spectrum_cp
		peak_area = Area(spectrum_bgs)
	Break
Endswitch

//Add Waves to graph
AppendtoGraph/C=(10000,65535,10000) $spectrum_name_bgs //Green
AppendtoGraph/C=(0,0,65535)  $spectrum_name_bg //Blue
//ModifyGraph offset($spectrum_name_bg)={0,spectrum_offset} //Shifts to allow comparison to original data

// Add some infos 
Note spectrum_bgs, " "
Note spectrum_bgs, "# Substracted background : " + BackgroundType
Note spectrum_bgs, "# Binding energy range (eV) : [" + num2str(start)+";"+num2str(stop)+"]"
Note spectrum_bgs, "# Spectrum area: " + num2str(peak_area)
Note background, " "
Note background,  "# Substracted background : " + BackgroundType
Note background,	  "# Binding energy range (eV) : [" + num2str(start)+";"+num2str(stop)+"]"
Note background,  "# Spectrum area: " + num2str(peak_area)
Strswitch(BackgroundType)
	Case "Shirley (pinned on cursors)":
	Case "Shirley+Tougaard (user given)":
		Note spectrum_bgs,    "# S-factor : " + num2str(S)
		Note background,        "# S-factor : " + num2str(S)
	Case "Tougaard (pinned on cursors)":
	Case "Tougaard (B-fixed)":
	Case "Shirley+Tougaard (user given)":
		Note spectrum_bgs,    "# B-factor : " + num2str(B)
		Note background,        "# B-factor : " + num2str(B)
		Note spectrum_bgs,    "# C-factor : " + num2str(C)
		Note background,        "# C-factor : " + num2str(C)
		Note spectrum_bgs,    "# D-factor : " + num2str(D)
		Note background,        "# D-factor : " + num2str(D)
		Note spectrum_bgs,	 "# Gap  : " + num2str(Gap)
		Note background,        "# Gap  : " + num2str(Gap)
	Break
	Case "Linear (pinned on cursors)":	
		Note spectrum_bgs,    "# Slope-factor : " + num2str(slope)
		Note background,        "# Slope-factor : " + num2str(slope)
	Break
	Case "None":
	Break
	Default:
Endswitch
	
// Clean Wave
KillWaves /Z spectrum_cp

// Area under background subtracted curve
Printf "==> Area under background subtracted spectrum : %10.2f\r", peak_area

// Create a binding energy scale
If(Cmpstr(makeWave_BE,"yes")==0)
	String spectrum_name_bgs_be
	spectrum_name_bgs_be = StringName_Ext(spectrum_name,"_BGS_BE")
	Make /D/O/N=(numpnts(background))  $spectrum_name_bgs_be
	Wave /D spectrum_bgs_be =  $spectrum_name_bgs_be
	spectrum_bgs_be = DimOffSet(background,0) + p*DimDelta(background,0)
	SetScale/P x, 0, 1, "None",  spectrum_bgs_be 
Endif

// In case of simply cut-out spectrum Wave
If(Cmpstr(BackgroundType,"None")==0)
	RemoveFromGraph $spectrum_name_bg
	KillWaves /Z  background
	String spectrum_name_cut	
	spectrum_name_cut = StringName_Ext(spectrum_name,"_CUT")
	Duplicate /O  spectrum_bgs  $spectrum_name_cut
	RemoveFromGraph $spectrum_name_bgs
	KillWaves /Z spectrum_bgs
	If(Cmpstr(makeWave_BE,"yes")==0)
		String spectrum_name_cut_be
		spectrum_name_cut_be = StringName_Ext(spectrum_name,"_CUT_BE")
		Duplicate /O spectrum_bgs_be $spectrum_name_cut_be; KillWaves /Z spectrum_bgs_be
	Endif
Endif

// go back to current datafolder
SetDataFolder saveDF

End


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Shirley_Pinned_BG(S,start,stop,spectrum,background,spectrum_bgs,verbose)

Variable &S, start, stop, verbose
Wave /D spectrum,background,spectrum_bgs

Variable threshold = 0.005 // Threshold for convergence
Variable nloops = 200 // Maximum number of loops
Variable Sconvergence = 0.75 // Convergence factor

Variable i,j,integralS	
Variable spectrum_offset, background_offset,loops, background_offset_old, delta,spectrum_area

// Initialize
background = 0
spectrum_bgs = spectrum

// Temporary Wave
Duplicate /O spectrum bg_temp

//Zero pin the spectrum copy Wave
//spectrum_offset = Wavemin(spectrum)
spectrum_offset = min(spectrum(start),spectrum(stop))
spectrum -= spectrum_offset

// Set inital guess for background just constant
background = 0 

// Set inital k value
S=0.0001 

// Step
delta = deltax(spectrum)

//Detect the direction of the data
If(spectrum(start)>spectrum(stop))
	// Iteration for k
	Do 
		bg_temp = background
		//Loop through the energies i is in energy
		For(i=stop;i>=(start - delta/2);i-=delta) 
			integralS  = area(spectrum,i,stop) - area(bg_temp,i,stop) 
			background[x2pnt(background,i)] = S*integralS
		Endfor
		
		//Adjust k to converge fitting
		background_offset = background(start) - spectrum(start)
		S = S - (background_offset/background(start))*S*Sconvergence //Sconvergence is to ensure convergence
		background_offset_old = background_offset
		loops+=1
		
	While(abs(background_offset)>(threshold*spectrum(start)) && loops<=nloops)
	
Else
	// Iteration for k	
	Do 
		bg_temp = background
		 //Loop through the energies i is in energy
		For(i=start;i<(stop + delta/2);i+=delta)
			integralS  = area(spectrum,start,i) - area(bg_temp,start,i) 
			background[x2pnt(background,i)] = S*integralS
		Endfor
		
		//Adjust k to make background fitting good
		background_offset = background(stop) - spectrum(stop)
		//print k, background_offset, background(start), background(stop)
		S = S - (background_offset/background(stop))*S*Sconvergence //Sconvergence is to ensure convergence
		loops+=1
	While(abs(background_offset)>(threshold*spectrum(stop)) && loops<=nloops)

EndIf

// Clean Waves
KillWaves /Z bg_temp

//Make background subtracted Wave
spectrum_bgs = spectrum - background
// Offset the background
background += spectrum_offset
spectrum += spectrum_offset

// Calculate the area
spectrum_area =  abs(area(spectrum_bgs,start,stop))

// Warn the user
If(loops >= nloops)
	Print  "==> Determination of prefactor did not converge properly"
EndIf	
If(verbose==1)
	Print "==> Number of iterations : ", loops
	Print "==> S-value : " , S 
Endif

// Return the area
Return spectrum_area

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Tougaard_Pinned_BG(B,C,D,Gap,start,stop,spectrum,background,spectrum_bgs,verbose)

Variable &B,C,D,Gap,start, stop,verbose
Wave /D spectrum,background,spectrum_bgs

Variable threshold = 0.005 // Threshold for convergence
Variable nloops = 200 // Maximum number of loops
Variable Sconvergence = 0.75 // Convergence factor

Variable i,j,t,integral,delta,tmg
Variable spectrum_offset, background_offset,loops,spectrum_area

// Initialize
background = 0
spectrum_bgs = spectrum	
	
//Zero pin the spectrum copy wave
//spectrum_offset = Wavemin(spectrum)
spectrum_offset = min(spectrum(start),spectrum(stop))
spectrum -= spectrum_offset

// Step
delta = deltax(spectrum)

//Check the direction needed for the intergration
If(spectrum(start)>spectrum(stop))
	 // Itteration for B
	Do
		For(i=stop;i>(start-delta/2);i-=delta) //Loop through the energies i is in energy
		
			integral=0
			For(j=i;j<=stop;j+=delta)
				t = Abs(j-i)
				If(t>=Gap)
					tmg = t-gap
				 	integral +=  tmg / ((C+tmg^2)^2+D*tmg^2) *spectrum(j)//Do the Tougaard Intergration
				Endif
			Endfor
			background[x2pnt(background,i)] = B*integral*delta 
				
		Endfor
		
		//Adjust B to make background fitting good
		background_offset = background(start) - spectrum(start) //+ve means B need to be larger
		B = B - (background_offset / background(start))*B*Sconvergence //Sconvergence to ensure convergence
		loops+=1
	While(abs(background_offset)>(threshold*spectrum(start)) && loops<=nloops)
	
Else //If background is incresing towards higher energy values
	// Itteration for B
	Do 
		For(i=start;i<(stop+delta/2);i+=delta) //Loop through the energies i is in energy
		
			integral=0
			For(j=i;j>=start;j-=delta)
				t = Abs(j-i)
				If(t>=Gap)
					tmg = t-gap
					integral += tmg / ((C+tmg^2)^2 + D*tmg^2) *spectrum(j) //Do the Tougaard Intergration
				Endif
			Endfor		
			background[x2pnt(background,i)] = B*integral*delta
		Endfor
				
		//Adjust B to make background fitting good
		background_offset = background(stop) - spectrum(stop) //+ve means B need to be larger
		B = B - (background_offset / background(stop))*B*Sconvergence //Sconvergence to ensure convergence
		loops+=1
	While(abs(background_offset)>(threshold*spectrum(stop)) && loops<=nloops)	
	
EndIf

//Make background subtracted Wave
spectrum_bgs = spectrum - background
// Offset the background
background +=spectrum_offset
spectrum += spectrum_offset

// Calculate the area
spectrum_area =  abs(area(spectrum_bgs,start,stop))

// Warn the user
If(loops >= nloops)
	Print "==> Fitting did not converge properly"
EndIf	
If(verbose==1)
	Print "==> Number of iterations : ", loops
	Print "==> B-value : ", Abs(B)
	Print "==> C-value : ", C
	Print "==> D-value : ", D
Endif

// Return the offset
Return spectrum_area

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Tougaard_Bfixed_BG(B,C,D,Gap,start,stop,spectrum,background,spectrum_bgs,verbose)

Variable B,C,D,Gap,start, stop,verbose
Wave /D spectrum,background,spectrum_bgs

Variable i,j,t,integral,delta,tmg
Variable spectrum_offset,spectrum_area

// Initialize
background = 0
spectrum_bgs = spectrum

//Zero pin the spectrum copy Wave
spectrum_offset = min(spectrum(start),spectrum(stop))
spectrum -= spectrum_offset

// Step
delta = deltax(spectrum)

//Check the direction needed for the intergration
If(spectrum(start)>spectrum(stop))

	For(i=stop;i>(start-delta/2);i-=delta) //Loop through the energies i is in energy
		integral=0
		For(j=i;j<=stop;j+=delta)
			t = Abs(j-i)
			If(t>=Gap)
				tmg = t-gap
				integral += tmg / ((C+tmg^2)^2 + D*tmg^2) *spectrum(j)//Do the Tougaard Integration
			Endif
		Endfor
		background[x2pnt(background,i)] = B*integral*delta 
	Endfor
			
Else //If background is incresing towards higher energy values
	
	For(i=start;i<(stop+delta/2);i+=delta) //Loop through the energies i is in energy
		integral=0
		For(j=i;j>=start;j-=delta)
			t = Abs(j-i)
			If(t>=Gap)
				tmg = t-gap
				integral += tmg / ((C+tmg^2)^2 + D*tmg^2) *spectrum(j) //Do the Tougaard Integration
			Endif
		Endfor		
		background[x2pnt(background,i)] = B*integral*delta
	Endfor			

EndIf

//Make background subtracted Wave
spectrum_bgs = spectrum - background
// Offset the background
background += spectrum_offset
spectrum += spectrum_offset

// Calculate the area
spectrum_area =  abs(area(spectrum_bgs,start,stop))

// Warn the user
If(verbose==1)
	Print "==> B-value : ", Abs(B)
	Print "==> C-value : ", C
	Print "==> D-value : ", D
Endif

// Return the area
Return spectrum_area

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function ShirleyTougaard_User_BG(S,B,C,D,Gap,start,stop,spectrum,background,spectrum_bgs)

Variable S, B, C, D, Gap, start, stop
Wave /D spectrum,background,spectrum_bgs

Variable i,j,t,tmg
Variable integralS, integralT 	
Variable spectrum_offset, delta,spectrum_area


// Initialize
background = 0
spectrum_bgs = spectrum

//Zero pin the spectrum copy 
spectrum_offset = min(spectrum(start),spectrum(stop))
spectrum -= spectrum_offset


// Step
delta = deltax(spectrum)

//Detect the direction of the data
If(spectrum(start)>spectrum(stop))
	
		//Loop through the energies i is in energy
		For(i=stop;i>=(start - delta/2);i-=delta)
			//Do the Shirley Integration 
			integralS  = area(spectrum,i,stop) 
			//Do the Tougaard Integration
			integralT=0.
			For(j=i;j<=stop;j+=delta)
				t = Abs(j-i)
				If(t>=Gap)
					tmg = t-gap
					integralT += tmg / ((C+tmg^2)^2 + D*tmg^2) *spectrum(j)
				Endif
			Endfor
			background[x2pnt(background,i)] = S*integralS +  B*integralT*delta 
		Endfor
		
Else

		 //Loop through the energies i is in energy
		For(i=start;i<(stop + delta/2);i+=delta)
			//Do the Shirley Integration 
			integralS  = area(spectrum,start,i) 
			//Do the Tougaard Integration
			integralT=0.
			For(j=i;j>=start;j-=delta)
				t = Abs(j-i)
				If(t>=Gap)
					tmg = t-gap
					integralT += tmg / ((C+tmg^2)^2 + D*tmg^2) *spectrum(j)  
				Endif
			Endfor	
			background[x2pnt(background,i)] = S*integralS +  B*integralT*delta			
		Endfor

EndIf

//Make background subtracted Wave
spectrum_bgs = spectrum - background
// Offset the background
background += spectrum_offset
spectrum += spectrum_offset

// Calculate the area
spectrum_area =  abs(area(spectrum_bgs,start,stop))

// Return the area
Return spectrum_area

End


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Linear_Pinned_BG(slope,spectrum,background,spectrum_bgs)

Variable &slope
Wave /D spectrum,background,spectrum_bgs

Variable i
Variable delta, spectrum_area
Variable np = numpnts(spectrum)

// Initialize
background = 0
spectrum_bgs = spectrum

// Calculate gradient
slope = (spectrum[np-1] - spectrum[0])/(pnt2x(spectrum,np-1) - pnt2x(spectrum,0))

// Step
delta = deltax(spectrum)

//Make background Wave
For(i=0; i<np; i+=1)
	background[i] = spectrum[0] + slope*i*delta
Endfor

//Make background subtracted Wave
spectrum_bgs = spectrum - background

// Calculate the area
spectrum_area =  abs(area(spectrum_bgs))

// Return the offset
Return spectrum_area
	
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Area_BGS(BackgroundType,start,stop,spectrum_name,keepbackground,[S,B,C,D,Gap,BE,FWHM])

String BackgroundType
Variable start, stop, keepbackground
String spectrum_name
Variable S, B, C, D, Gap
Variable &BE, &FWHM

String TougaardType
Variable S_in, B_in, C_in, D_in, Gap_in, slope_in
Variable peak_area, verbose=0

// Check
String ListofWaves = Wavelist(spectrum_name, ";", "" )
If(Itemsinlist(ListofWaves,";")==0)
	Print "==> The spectrum does not exist !"
	Return nan
Endif
Wave /D spectrum = $spectrum_name
	
//Swap values in Case of 
If(start>stop)
	Swap(start,stop)
EndIf
start = Max( pnt2x(spectrum,x2pnt(spectrum,start)), pnt2x(spectrum,0) )
stop = Min( pnt2x(spectrum,x2pnt(spectrum,stop)), pnt2x(spectrum,numpnts(spectrum)-1) )

// Make temporary Waves
Duplicate/O/R=(start,stop) spectrum spectrum_cp
Duplicate/O/R=(start,stop) spectrum background	
Duplicate/O/R=(start,stop) spectrum spectrum_bgs
Duplicate/O/R=(start,stop) spectrum bg_temp			

// Calculate the background according to selected type
	Gap_in = 0
	Strswitch(BackgroundType)
	Case "Shirley":
		peak_area = Shirley_Pinned_BG(S_in,start,stop,spectrum_cp,background,spectrum_bgs,verbose)
	Break
	Case "Tougaard":
		TougaardType = "Universal Tougaard (Two-parameters)"
		Tougaard_Parameters(TougaardType,B_in,C_in,D_in)
		peak_area = Tougaard_Pinned_BG(B_in,C_in,D_in,Gap_in,start,stop,spectrum_cp,background,spectrum_bgs,verbose)
	Break
	Case "Tougaard_fixed":
		TougaardType = "Universal Tougaard (Two-parameters)"
		Tougaard_Parameters(TougaardType,B_in,C_in,D_in)
		peak_area = Tougaard_Bfixed_BG(B_in,C_in,D_in,Gap_in,start,stop,spectrum_cp,background,spectrum_bgs,verbose)		
	Break
	Case "ShirleyTougaard_user":
		If(ParamIsDefault(S)==0 && ParamIsDefault(B)==0 && ParamIsDefault(C)==0 && ParamIsDefault(D)==0 && ParamIsDefault(Gap)==0)  
			S_in = S
			B_in = B
			C_in = C
			Gap_in = Gap
		Else
			Print "==> Input missing in Area_BGS : Shirley factor (S) or Tougaard parameters (B, C, D, Gap)" 
			Return Nan
		Endif
		peak_area = ShirleyTougaard_User_BG(S_in,B_in,C_in,D_in,Gap_in,start,stop,spectrum_cp,background,spectrum_bgs) 		
	Break	
	Case "Linear":
		peak_area = Linear_Pinned_BG(slope_in,spectrum_cp,background,spectrum_bgs)
	Break
	Case "None":
		background = 0.
		spectrum_bgs = spectrum_cp
		peak_area = Area(spectrum_bgs)
	Break
Endswitch

// In case of, calculate the position and FWHM of the peak
If(ParamIsDefault(BE)==0 && ParamIsDefault(FWHM)==0)
 Variable x1, x2
 WaveStats /Q /R=(start,stop) spectrum_bgs
 BE = V_maxloc
 FindLevel /Q /R=(start,V_maxloc) spectrum, V_max/2.
 x1 = V_LevelX
 FindLevel /Q /R=(V_maxloc,stop) spectrum, V_max/2.
 x2= V_LevelX	 
 FWHM = Abs(x2-x1)
Endif

// Clean the Waves or not
If(keepbackground==1) 
	// Add some infos 
	Note spectrum_bgs, " "
	Note spectrum_bgs, "# Substracted background : " + BackgroundType
	Note spectrum_bgs, "# Binding energy range (eV) : [" + num2str(start)+","+num2str(stop)+"]"
	Note spectrum_bgs, "# Spectrum area: " + num2str(peak_area)
	Note background, " "
	Note background,  "# Substracted background : " + BackgroundType
	Note background,	  "# Binding energy range (eV) : [" + num2str(start)+","+num2str(stop)+"]"
	Note background,  "# Spectrum area: " + num2str(peak_area)
	Strswitch(BackgroundType)
		Case "Shirley (pinned on cursors)":
		Case "Shirley+Tougaard (user given)":
			Note spectrum_bgs,    "# S-factor : " + num2str(S_in)
			Note background,        "# S-factor : " + num2str(S_in)
		Case "Tougaard (pinned on cursors)":
		Case "Tougaard (B-fixed)":
		Case "Shirley+Tougaard (user given)":
			Note spectrum_bgs,    "# B-factor : " + num2str(B_in)
			Note background,        "# B-factor : " + num2str(B_in)
			Note spectrum_bgs,    "# C-factor : " + num2str(C_in)
			Note background,        "# C-factor : " + num2str(C_in)
			Note spectrum_bgs,    "# D-factor : " + num2str(D_in)
			Note background,        "# D-factor : " + num2str(D_in)
			Note spectrum_bgs,    "# Gap : " 	  + num2str(Gap_in)
			Note background,        "# Gap : " 	  + num2str(Gap_in)				
		Break
		Case "Linear (pinned on cursors)":	
			Note spectrum_bgs,    "# Slope-factor : " + num2str(slope_in)
			Note background,        "# Slope-factor : " + num2str(slope_in)
		Break
		Case "None":
		Break
		Default:
	Endswitch
	// Copy the waves
	Duplicate /O  background $spectrum_name+"_BG"
	Duplicate /O  spectrum_bgs $spectrum_name+"_BGS"
Endif
KillWaves /Z spectrum_cp, background, spectrum_bgs, bg_temp

// Peak area
Return peak_area

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function Graph2Peaks(BackgroundType,start,stop,keepbackground)

String BackgroundType
Variable start, stop, keepbackground

// Save the current datfolder
String saveDF = GetDataFolder(1)

// Name of the top graph
String listofgraphs = WinList("*", ";","WIN:1")
String topgraphname =  StringFromList(0, listofgraphs, ";")

// List of traces in the top graph
String listoftraces = TraceNamelist("",";", 1) 
Variable ntraces = ItemsInList(listoftraces)

// Make the outputwaves
Make /T/O/N=(ntraces) Peaks_Name
Make /D/O/N=(ntraces) Peaks_BE, Peaks_Area, Peaks_FWHM
Note Peaks_Name, " "
Note Peaks_Name, "# Names of spectra from graph : " +  topgraphname
Note Peaks_BE, " "
Note Peaks_BE, "# Maximum of background subtracted spectra from graph : " +  topgraphname
Note Peaks_BE, "# Background type : "  + BackgroundType
Note Peaks_BE, "# Binding energy (eV) windows : [" + num2str(start) + "," + num2str(stop) + "]"
Note Peaks_Area, " "
Note Peaks_Area, "# Background subtracted areas from graph : " +  topgraphname
Note Peaks_Area, "# Background type : "  + BackgroundType
Note Peaks_Area, "# Binding energy (eV) windows : [" + num2str(start) + "," + num2str(stop) + "]"
Note Peaks_FWHM, " "
Note Peaks_FWHM, "# FWHM of background subtracted spectra from graph : " +  topgraphname
Note Peaks_FWHM, "# Background type : "  + BackgroundType
Note Peaks_FWHM, "# Binding energy (eV) windows : [" + num2str(start) + "," + num2str(stop) + "]"

// Loop over traces
String trace_name, spectrum_name, spectrum_datafolder
Variable kk
Variable BEPeak=0, FWHMpeak=0
For (kk=0; kk<ntraces; kk+=1)
  // Extract the wave infos
  trace_name = StringFromList(kk, listoftraces, ";")
  Wave /D spectrum = TraceNameToWaveRef("",trace_name)
  spectrum_name = NameOfWave(spectrum)
  spectrum_datafolder = GetWavesDataFolder(spectrum, 1 )
  SetDataFolder spectrum_datafolder
  // The outputs
  Peaks_Name[kk] = GetWavesDataFolder(spectrum, 2)
  Peaks_Area[kk] =  Area_BGS(BackgroundType,start,stop,spectrum_name,keepbackground,BE=BEpeak,FWHM=FWHMpeak)
  Peaks_FWHM[kk] = FWHMpeak
  Peaks_BE[kk] = BEpeak
Endfor

// Go back to the initial datafolder
SetDataFolder saveDF

// Warn the user
Print "Results are stored in waves (Peaks_Name, Peaks_BE,  Peaks_FWHM, Peaks_Area) of folder : " + saveDF

End



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Tougaard_Parameters(TougaardType, B, C, D)

String TougaardType
Variable  &B, &C, &D
Variable  Ebar=0, E1 = 714.9 

Strswitch(TougaardType)
Case "Universal Tougaard (Two-parameters)":
	Ebar = 63.670558406122   // C = 1643
	Break 
Case "Li (Z=3)":  
	Ebar = 11.57  
	Break
Case "Be (Z=4)":  
	Ebar = 12.75  
	Break
Case "B (Z=5)":  
	Ebar = 20.64  
	Break
Case "C (Z=6)":  
	Ebar = 18.28  
	Break
Case "Mg (Z=12)":  
	Ebar = 18.8  
	Break
Case "Al (Z=13)":  
	Ebar = 22.48  
	Break
Case "Si (Z=14)":  
	Ebar = 20.71  
	Break
Case "Ca (Z=20)": 
	Ebar = 25.21  
	Break
Case "Sc (Z=21)":  
	Ebar = 23.37  
	Break
Case "Ti (Z=22)":  
	Ebar = 28.08  
	Break
Case "V (Z=23)":  
	Ebar = 32.14  
	Break
Case "Cr (Z=24)":  
	Ebar = 44.74  
	Break
Case "Mn (Z=25)": 
	Ebar = 44.74  
	Break
Case "Fe (Z=26)":  
	Ebar = 43.27  
	Break
Case "Co (Z=27)":  
	Ebar = 43.93  
	Break
Case "Ni (Z=28)":  
	Ebar = 47.1  
	Break
Case "Cu (Z=29)": 
	Ebar = 48  
	Break
Case "Zn (Z=30)":  
	Ebar = 45.26 
	Break
Case "Ga (Z=31)": 
	Ebar = 34.64  
	Break
Case "Ge (Z=32)":  
	Ebar = 40.69  
	Break
Case "Se (Z=34)":  
	Ebar = 26.09  
	Break
Case "Sr (Z=38)": 
	Ebar = 31.84  
	Break
Case "Y (Z=39)":  
	Ebar = 31.84  
	Break
Case "Zr (Z=40)":  
	Ebar = 34.28  
	Break
Case "Nb (Z=41)":  
	Ebar = 40.84  
	Break
Case "Mo (Z=42)": 
	Ebar = 34.5  
	Break
Case "Ru (Z=44)":  
	Ebar = 40.1  
	Break
Case "Rh (Z=45)":  
	Ebar = 49.46 
	Break
Case "Pd (Z=46)":  
	Ebar = 50.27  
	Break
Case "Ag (Z=47)":  
	Ebar = 50.12  
	Break
Case "Cd (Z=48)":  
	Ebar = 45.18  
	Break
Case "In (Z=49)":  
	Ebar = 45.26  
	Break
Case "Sn (Z=50)":  
	Ebar = 40.98  
	Break
Case "Sb (Z=51)":  
	Ebar = 39.66  
	Break
Case "Te (Z=52)":  
	Ebar = 35.01  
	Break
Case "Ba (Z=56)":  
	Ebar = 28.45  
	Break
Case "La (Z=57)":  
	Ebar = 29.85  
	Break
Case "Ce (Z=58)":  
	Ebar = 31.07  
	Break
Case "Pr (Z=59)":  
	Ebar = 29.41  
	Break
Case "Nd (Z=60)":  
	Ebar = 29.71  
	Break
Case "Sm (Z=62)":  
	Ebar = 29.26  
	Break
Case "Eu (Z=63)":  
	Ebar = 23.29  
	Break
Case "Gd (Z=64)":  
	Ebar = 29.56  
	Break
Case "Tb (Z=65)":  
	Ebar = 27.05  
	Break
Case "Dy (Z=66)":  
	Ebar = 25.58  
	Break
Case "Ho (Z=67)":  
	Ebar = 23.88  
	Break
Case "Er (Z=68)":  
	Ebar = 25.72  
	Break
Case "Tm (Z=69)":  
	Ebar = 24.18  
	Break
Case "Yb (Z=70)":  
	Ebar = 22.19  
	Break
Case "Lu (Z=71)":  
	Ebar = 25.21  
	Break
Case "Hf (Z=72)": 
	Ebar = 22.48  
	Break
Case "Ta (Z=73)":  
	Ebar = 25.95  
	Break
Case "W (Z=74)":  
	Ebar = 29.85  
	Break
Case "Re (Z=75)":  
	Ebar = 32.58  
	Break
Case "Ir (Z=77)":  
	Ebar = 39.07  
	Break
Case "Pt (Z=78)":  
	Ebar = 35.53  
	Break
Case "Au (Z=79)":  
	Ebar = 39.14  
	Break
Case "Pb (Z=82)":  
	Ebar = 32.21  
	Break
Case "Bi (Z=83)": 
	Ebar = 29.26  
	Break
Endswitch

C = (2*Ebar/pi)^2
B = 2*C*Exp(-Ebar/E1)
D = 0

Strswitch(TougaardType)
Case "Three-parameters : Fe,Pd,Ti,Cu,Ag,Au":
	B = 4210
	C = -1000
	D = 13300
	Break
Case "Three-parameters : Polymers":
	B = 434
	C = -551
	D = 436
	Break
Case "Three-parameters : Silicon dioxide":
	B = 325
	C = -542
	D = 275
	Break
Case "Three-parameters : Silicon":
	B = 132
	C = -325
	D = 96
	Break
Case "Three-parameters : Germanium":
	B = 73
	C = -260
	D = 62
	Break
Case "Three-parameters : Aluminium":
	B = 16.5
	C = -230
	D = 4.5
	Break
Endswitch

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : plot the inelastic electron scattering cross section 
Function Plot_IESCS([Type, B, C, D, G, Estep])

Variable Type, B, C, D, G, Estep

// Default values
If(ParamIsDefault(Type)==1)
	Type = 1
Endif
If(ParamIsDefault(B)==1)
	B = 3006
Endif
If(ParamIsDefault(C)==1)
	C = 1643
Endif
If(ParamIsDefault(D)==1)
	D = 0
Endif
If(ParamIsDefault(G)==1)
	G = 0
Endif
If(ParamIsDefault(Estep)==1)
	Estep = 0.5
Endif

// Positive values, except c to comply with definitions
B = Abs(B)
D = Abs(D)
G = Abs(G)
Estep =  Abs(Estep)

// The waves
Variable ELmax = 8*(sqrt(abs(C/3)) + G)
Variable np = ELmax / Estep + 1
Make /D/O/N=(np) EL
Variable kk = 0
String IESCSName
Do
	IESCSName = "IESCS_"+num2str(kk)
	kk += 1
While(WaveExists($IESCSName)==1)
Make /D/O/N=(np) $IESCSName
Wave /D IESCS =  $IESCSName
Setscale/P x 0, Estep, IESCS
Setscale d 0, 10000, IESCS

// Kinectic energy and Inelastic cross section
EL = p*Estep - G
Switch(Type)
Case 1:
	IESCS = B * EL  / ((C+EL^2)^2 + D*EL^2)  * (EL>=0)
Break
Case 2:
	IESCS=  B * (1.-exp(-D*EL)) / (C + EL^2)  * (EL>=0)
Break
Default:
	Print "==> Inelastic electron scatetring cross-section not supported !"
	Return 0
Break
Endswitch

// Display graph
Display IESCS
Modifygraph standoff=0
Modifygraph tick=2,mirror=2,minor=1
Label left "\\Z20Inelastic cross section (eV\\S-1\\M\\Z20)";DelayUpdate
Label bottom "\\Z20Energy loss (eV)"
Modifygraph fSize=16
Modifygraph mode=3,marker=8
Modifygraph msize=2
Setaxis bottom 0,*
Setaxis left 0,*
String LabelString
LabelString  =  "Inelastic Cross Section \r"
Switch(Type)
Case 1:
	LabelString +=  "Type : Tougaard\r"
	LabelString +=  "B = " + num2str(B) + " eV\\S2\\M\r"
	LabelString +=  "C = " + num2str(C) + " eV\\S2\\M\r"
	LabelString +=  "D = " + num2str(D) + " eV\\S2\\M\r"
	LabelString +=  "Gap = " + num2str(G) + " eV"
Break
Case 2:
	LabelString +=  "Type : Distorted-Shirley\r"
	LabelString +=  "B = " + num2str(B) + " eV\\M\r"
	LabelString +=  "C = " + num2str(C) + " eV\\S2\\M\r"
	LabelString +=  "D = " + num2str(D) + " eV\\S-1\\M\r"
	LabelString +=  "Gap = " + num2str(G) + " eV"
Break
Endswitch
TextBox/C/N=text1/F=2/A=MC LabelString
Dowindow/T kwTopWin,"inelastic Electron Scattering Cross Section"

// Note
Note IESCS, " "
Note IESCS, "# Inelastic Electron Scattering Cross Section"
Switch(Type)
Case 1:
	Note IESCS,  "# Type : Tougaard"
Break
Case 2:
	Note IESCS,  "# Type : Distorted-Shirley"
Break
Endswitch
Note IESCS,  "# B-factor = " + num2str(B) 
Note IESCS,  "# C-factor = " + num2str(C) 
Note IESCS,  "# D-factor = " + num2str(D) 
Note IESCS,  "# Gap-factor = " + num2str(G)

// Clean
KillWaves /Z EL

End