// Module :  ghostlines
// Author : Remi Lazzari, lazzari@insp.jussieu.fr
// Date : December 27th 2018
// Usage : Remove ghost lines by substraction from a non-monochromatic XPS spectrum

#pragma rtGlobals = 3		
#pragma ModuleName = ghostlines

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Substract_GhostLines()

// Make sure one cursor is on the same wave.
If(StringMatch(CsrInfo(a),""))
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rA cursor must be on top graph to point at trace !"
	Return 0
EndIf

// The current datafolder
String saveDF = GetDataFolder(1)

// The datafolder of variables
NewDataFolder /O/S root:I4P
NewDataFolder /O/S :Variables

// Selected X-ray source
String Xray_source = StrVarOrDefault("gXray_source","Al Kalpha")
Variable ghostX = NumVarOrDefault("gghostX",1.0)
Variable ghostO = NumVarOrDefault("gghostO",0.0)
Variable ghostCu = NumVarOrDefault("gghostCu",0.0)
Prompt Xray_source, "Select X-ray source", popup "Al Kalpha;Mg Kalpha"
Prompt ghostX, "Relative Mg/Al-Ka ghost intensity (%) :"
Prompt ghostCu, "Relative Cu-La ghost intensity (%) :"
Prompt ghostO, "Relative O-Ka ghost intensity (%) :"
String HelpString
HelpString   = "1- Put a cursor on spectrum\r"
HelpString += "2- Select the excitation source\r"
HelpString += "3- Enter the relative ratio of Mg/Al Ka, Cu La, O Ka\r"	
HelpString += "5- Run\r"	
HelpString += "Warning : substraction not valid for Auger lines/ no variations of photoionization cross section \r "
DoPrompt /HELP = HelpString "I4P :  Ghostlines substraction", Xray_source, ghostX, ghostO, ghostCu
If (V_Flag)
	SetDataFolder saveDF
	Print "==> Error in the input parameters !"
	Return 0
Endif
ghostX = ghostX/100
ghostO = ghostO/100
ghostCu = ghostCu/100

// Save the data
Variable /G	gghostX 	  = ghostX
Variable /G	gghostO   = ghostO
Variable /G	gghostCu  = ghostCu
String /G 	gXray_source = Xray_source

// Go back to the initial datafolder
SetDataFolder saveDF

// Find the wave on which the cursor is on and point to the spectrum wave
Wave /D spectrum = CsrWaveRef(A)

// Set the data folder 
SetDataFolder(GetWavesDataFolder(spectrum, 1))

// Characteristics of the ghost lines
Variable nsat = 3
Make /O/D/N=(nsat) ghosts_position, ghosts_factor
Variable Xray_energy
Strswitch(Xray_source)
Case "Al Kalpha": 
		Xray_energy = ghv_Al
		Print "==> Substract ghost lines from Al Kalpha X-ray source "
		ghosts_position = {233,556.9,961.7}
		ghosts_factor = {ghostX, ghostCu, ghostO}
Break
Case "Mg Kalpha": 
		Xray_energy = ghv_Mg
		Print "==> Substract ghost lines from Mg Kalpha X-ray source "
		ghosts_position = {0,323.9,728.7}
		ghosts_factor = {0, ghostCu, ghostO}
Break
EndSwitch	

// Warning
Print "==> Warning : ghost lines substraction is not valid for Auger lines ! "
Print "==> Warning : ghost lines substraction does not account from variations of photoionization cross sections ! "
					
// Make substracted wave
String spectrum_name = nameofwave(spectrum)
String spectrum_name_gl = StringName_Ext(spectrum_name,"_GL")
Duplicate/O spectrum $spectrum_name_gl
String spectrum_name_gls = StringName_Ext(spectrum_name,"_GLS")
Duplicate/O spectrum $spectrum_name_gls
Wave /D ghosts = $spectrum_name_gl
Wave /D ghosts_sub = $spectrum_name_gls

// Some varaiables
Variable np = numpnts(spectrum)
Variable deltaBE, startBE, endBE
deltaBE = Deltax(spectrum) 
startBE = DimOffset(spectrum,0)
endBE = startBE + (np-1)*deltaBE
Variable be, i, k	

// Remove ghost lines by increasing BE
ghosts_sub = spectrum
For(be=startBE;be<=endBE;be+=deltaBE)
	i = x2pnt(ghosts_sub,be)
	For(k=0;k<nsat;k+=1)
		If(be>= startBE+ghosts_position[k])
			ghosts_sub[i] = ghosts_sub[i] - ghosts_factor[k]*ghosts_sub(be-ghosts_position[k])
		Endif
	Endfor
Endfor
		
// Treat the special case of Al-Ka ghost line in Mg-Ka source
// Remove by decreasing BE
If(cmpstr(Xray_source,"Mg Kalpha")==0)
	For(be=endBE;be>=startBE;be-=deltaBE)
		i =  x2pnt(ghosts_sub,be)
		If(be<=endBE-233)
			ghosts_sub[i] = ghosts_sub[i] - ghostX*ghosts_sub(be+233)
		Endif
	Endfor
Endif
	
// Ghost lines
ghosts = spectrum - ghosts_sub	

//Clean the graph if macro has already been run
RemoveFromGraph/Z $spectrum_name_gls
RemoveFromGraph/Z $spectrum_name_gl

// Add some infos 
Note ghosts_sub, " "
Note ghosts_sub,  "# Substracted ghost lines of : "                   + Xray_source 
Note ghosts_sub,  "# Relative Mg/Al-Ka ghost intensity (%) : "  + num2str(ghostX)
Note ghosts_sub,  "# Relative Cu-La ghost intensity (%) : "       + num2str(ghostCu)
Note ghosts_sub,  "# Relative O-Ka ghost intensity (%) : "        + num2str(ghostO)
Note ghosts, " "
Note ghosts,  "# Substracted ghost lines of : "                   + Xray_source 
Note ghosts,  "# Relative Mg/Al-Ka ghost intensity (%) : "  + num2str(ghostX)
Note ghosts,  "# Relative Cu-La ghost intensity (%) : "       + num2str(ghostCu)
Note ghosts,  "# Relative O-Ka ghost intensity (%) : "        + num2str(ghostO)

//Add waves to graph
AppendtoGraph/C=(10000,65535,10000) ghosts_sub //Green
AppendtoGraph/C=(0,0,65535) ghosts //Blue

// Clear waves
KillWaves /Z ghosts_position, ghosts_factor
KillWaves /Z sp, sp

// Go back to the initial datafolder
SetDataFolder saveDF

End




