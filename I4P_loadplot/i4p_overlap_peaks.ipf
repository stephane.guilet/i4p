// Module : overlap_peaks
// Author : Remi Lazzari; lazzari@insp.jussieu.fr
// Date : December 25th, 2018
// Usage : overlap peak by offset 

#pragma rtGlobals = 3		
#pragma ModuleName = overlap_peaks

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Overlap_Photoemission()


// Make sure both cursors are on the same wave.
If(StringMatch(CsrInfo(a),"") || StringMatch(CsrInfo(b),""))
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rCursors must be on the top graph !"
	Return 0
EndIf
Wave /d wA = CsrWaveRef(A)
Wave /d wB = CsrWaveRef(B)
String dfA = GetWavesDataFolder(wA, 2)
String dfB = GetWavesDataFolder(wB, 2)
If (CmpStr(dfA, dfB) != 0)
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rBoth cursors must be on the same trace !"
	Return 0
Endif

// Position of the cursors
Variable xca, xcb, tmp
xca = xcsr(a)
xcb = xcsr(b)
If(xca==xcb)
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rBoth cursors at the same position !"
	Return 0
Endif
If(xca>xcb)
 tmp = xca
 xca = xcb
 xcb = tmp
Endif
	

// The datafolder of variables
String saveDF = GetDataFolder(1)
NewDataFolder /O/S root:I4P
NewDataFolder /O/S :Variables

// Choice of method of overlap
String Overlap_method = StrVarOrDefault("gOverlap_method", "Background and peak")
Prompt Overlap_method, "Overlap on", popup "Background and peak;Background multiplication;Background subtraction"
Variable Shift_Y_peak = NumVarOrDefault("gShift_Y_peak", 0.)
Prompt Shift_Y_peak, "Shift peaks vertically by "
String Shift_BE_peak = StrVarOrDefault("gShift_BE_peak", "no")
Prompt Shift_BE_peak, "Shift peak binding energy", popup "no;yes"
Variable BE_target = NumVarOrDefault("gBE_target", nan)
Prompt BE_target, "Target binding energy (eV)"
Variable BEwin_back = NumVarOrDefault("gBEwin_back", 0.4)
Prompt BEwin_back, "Background binding energy windows (eV)"
Variable BEwin_peak = NumVarOrDefault("gBEwin_peak", 0.3)
Prompt BEwin_peak, "Peak binding energy windows (eV)"
String Substract_peak  = StrVarOrDefault("gSubstract_peak", "no")
Prompt Substract_peak, "Generate substracted peaks", popup "no;yes"
String Append_peak =  StrVarOrDefault("gAppend_peak", "no")
Prompt Append_peak, "Append to graph substracted peaks", popup "no;yes"
String HelpString
HelpString   = "1- Put two cursors on master spectrum \r" 
HelpString += "    @low BE  = background\r" 
HelpString += "    @high BE = above peak\r" 
HelpString += "2- Select the overlap method and shift options\r"	
HelpString += "3- Select the windows energies at background and peak\r"	
HelpString += "4- Calculate and append to graph (or not) substracted peaks\r"	
DoPrompt /HELP=HelpString "I4P : Overlap peaks", Overlap_method, Shift_Y_peak, Shift_BE_peak,  BE_target, BEwin_back, BEwin_peak, Substract_peak, Append_peak
If (V_Flag)
	SetDataFolder saveDF
	Print "==> Error in the input parameters !"
	Return 0
Endif

// Save the data
String /G 	gOverlap_method = Overlap_method
Variable /G	gShift_Y_peak 	= Shift_Y_peak
String /G 	gShift_BE_peak	= Shift_BE_peak
Variable /G	gBE_target		= BE_target
Variable /G	gBEwin_back	= BEwin_back
Variable /G	gBEwin_peak	= BEwin_peak
String /G	gSubstract_peak  = Substract_peak
String /G	gAppend_peak	= Append_peak

// Go back to the initial datafolder
SetDataFolder saveDF

// Warn the user
Print "==> Overlap on : ", Overlap_method

// Find the wave on which the cursors are on and make spectrum wave
Wave /d masterwave = CsrWaveRef(A)

Variable xpeak, vpeak, xback, vback
Variable dxpeakmin, dxpeakmax
// Position and maximum of the peak from a parabola fit
Wave2PeakParam(masterwave,xca,xcb,xpeak,vpeak,BEwin_peak)	
// Position and average value of the background on low binding energy side
xback = xca
Wave2BackParam(masterwave,xca,vback,BEwin_back)
// Distance to the peak
dxpeakmin  = xpeak - xca
dxpeakmax = xcb - xpeak

// Actual peak target binding energy
Variable xT
If(cmpstr(Shift_BE_peak,"yes")==0)
	If(numtype(BE_target)==2)
		xT = xpeak
	Else
		xT = BE_target
	Endif
Else
	xT = xpeak
Endif

// List of traces on the active graph without master wave and "*_CUT" waves
String list = TraceNamelist("",";", 1) 
list = ReplaceString("'", list, "") // remove quote ' from list
string masterwave_name = NameOfWave(masterwave)
list = RemoveFromList(masterwave_name, list, ";")
Variable kk
String wave2overlap_name, list_tmp=""
For (kk=0; kk<itemsinlist(list); kk+=1)
  wave2overlap_name = Stringfromlist(kk, list, ";")
  If(StringMatch(wave2overlap_name,"*_SUB")==0)
  	list_tmp += wave2overlap_name + ";"
  Endif
Endfor
list = list_tmp

// Remove offsets on the master wave
ModifyGraph offset($masterwave_name)={0,0}
ModifyGraph muloffset($masterwave_name)={0,0}
// Offset the master trace
ModifyGraph offset($masterwave_name)={xT-xpeak,0}

// Warn the user
String wName
wName = WinName(0,1)
Print "==> Windows title : ", ConstructTitle(wName)
Print "==> Offsets traces : ", list, " from graph : ", wName
Print  "==> Master trace : ", NameofWave(masterwave)
Printf "==> Peak value : %10.2f%s%10.2f%s\r", vpeak,  " at binding energy : ", xpeak, " eV"
Printf "==> Background  value :  %10.2f%s%10.2f%s\r", vback, " at at binding energy : ", xback, " eV"


// Loop over the traces on the graph
Variable xa, xb, xp, vp, vb, xmin, xmax, ymult, yshift
String wave2overlap_name_sub, wave2overlap_path_sub
Variable cR, cG, cB
For (kk=0; kk<itemsinlist(list); kk+=1)
   	wave2overlap_name = stringfromlist(kk, list, ";")
   	ModifyGraph offset($wave2overlap_name)={0,0}
   	ModifyGraph muloffset($wave2overlap_name)={0,0}
    	Wave /d wave2overlap = TraceNametoWaveRef("", wave2overlap_name)
 	Wavestats /Q/R=(xca,xcb) wave2overlap
 	xa = V_maxloc - dxpeakmin
  	xb = V_maxloc + dxpeakmax 	
   	If(pnt2x(wave2overlap,numpnts(wave2overlap)-1) < xa || pnt2x(wave2overlap,0) > xb)
   		Print "==> WARNING : no overlap in binding energy between " +masterwave_name + " and " + stringfromlist(kk, list, ";")
   		xmin  = pnt2x(wave2overlap,0)
   		xmax = pnt2x(wave2overlap,numpnts(wave2overlap)-1)
   	Else
		xmin  = Max(xa,pnt2x(wave2overlap,0))
		xmax = Min(xb,pnt2x(wave2overlap,numpnts(wave2overlap)-1))
	Endif		
	Wave2PeakParam(wave2overlap,xmin,xmax,xp,vp,BEwin_peak)
 	Wave2BackParam(wave2overlap,xmin,vb,BEwin_back)
 	
   	Strswitch(Overlap_method)
   	Case "Background and peak":
   		ymult = (vpeak-vback)/(vp-vb)
   		yshift = (kk+1)*(vpeak-vback)*Shift_Y_peak
 		ModifyGraph muloffset($wave2overlap_name)={0,ymult}
 		If(cmpstr(Shift_BE_peak,"yes")==0)
  			ModifyGraph offset($wave2overlap_name)={xT-xp,vback-vb*ymult+yshift}
 		Else
  			ModifyGraph offset($wave2overlap_name)={0,vback-vb*ymult+yshift}
 		Endif
 		If(cmpstr(Substract_peak,"yes")==0)
 			wave2overlap_name_sub =  "'" + StringName_Ext(wave2overlap_name,"_SUB") +  "'"
 			wave2overlap_path_sub = GetWavesDataFolder(wave2overlap, 1) + wave2overlap_name_sub
 			If(cmpstr(Shift_BE_peak,"yes")==0) 			
 				SubstractOverlapedWaves(xpeak-xp,vback-vb*ymult,ymult,xT-xpeak,masterwave,wave2overlap,wave2overlap_path_sub)
 			Else
  				SubstractOverlapedWaves(0.,vback-vb*ymult,ymult,xT-xpeak,masterwave,wave2overlap,wave2overlap_path_sub)
 			Endif
  			If(cmpstr(Append_peak,"yes")==0) 
  			  	RemoveFromGraph /Z  $wave2overlap_name_sub
  				AppendToGraph $wave2overlap_path_sub
  				GetTraceColor("",wave2overlap_name,cR,cG,cB)
  				ModifyGraph rgb($wave2overlap_name_sub)=(cR,cG,cB)
  				ModifyGraph lstyle($wave2overlap_name_sub)=3
  				ModifyGraph offset($wave2overlap_name_sub)={0,-yshift}
  			Endif				
 		Endif
   	Break
   	Case "Background multiplication":
    		ymult = vback/vb
    		yshift = (kk+1)*(vpeak-vback)*Shift_Y_peak
 		ModifyGraph muloffset($wave2overlap_name)={0,ymult}
 		If(cmpstr(Shift_BE_peak,"yes")==0)
 			ModifyGraph offset($wave2overlap_name)={xT-xp,yshift}
 		Else
 			ModifyGraph offset($wave2overlap_name)={0,yshift}
 		Endif
 		If(cmpstr(Substract_peak,"yes")==0)
 			wave2overlap_name_sub =  "'" + StringName_Ext(wave2overlap_name,"_SUB") +  "'"
 			wave2overlap_path_sub = GetWavesDataFolder(wave2overlap, 1) + wave2overlap_name_sub 
 			If(cmpstr(Shift_BE_peak,"yes")==0) 			
 				SubstractOverlapedWaves(xpeak-xp,0,ymult,xT-xpeak,masterwave,wave2overlap,wave2overlap_path_sub)
 			Else
  				SubstractOverlapedWaves(0.,0.,ymult,xT-xpeak,masterwave,wave2overlap,wave2overlap_path_sub)
 			Endif
  			If(cmpstr(Append_peak,"yes")==0) 
  			  	RemoveFromGraph /Z  $wave2overlap_name_sub
  				AppendToGraph $wave2overlap_path_sub
  				GetTraceColor("",wave2overlap_name,cR,cG,cB)
  				ModifyGraph rgb($wave2overlap_name_sub)=(cR,cG,cB)
  				ModifyGraph lstyle($wave2overlap_name_sub)=3
  				ModifyGraph offset($wave2overlap_name_sub)={0,-yshift}
  			Endif				
 		Endif
 	 Break
    	Case "Background subtraction":
    		ymult = 1.
    		yshift = (kk+1)*(vpeak-vback)*Shift_Y_peak
    		 ModifyGraph muloffset($wave2overlap_name)={0,ymult}
 		If(cmpstr(Shift_BE_peak,"yes")==0)
 			ModifyGraph offset($wave2overlap_name)={xT-xp,vback-vb+yshift}
 		Else
 			ModifyGraph offset($wave2overlap_name)={0,vback-vb+yshift}
 		Endif
 		If(cmpstr(Substract_peak,"yes")==0)
 			wave2overlap_name_sub =  "'" + StringName_Ext(wave2overlap_name,"_SUB") +  "'"
 			wave2overlap_path_sub = GetWavesDataFolder(wave2overlap, 1) + wave2overlap_name_sub 
 			If(cmpstr(Shift_BE_peak,"yes")==0) 			
 				SubstractOverlapedWaves(xpeak-xp,vback-vb,ymult,xT-xpeak,masterwave,wave2overlap,wave2overlap_path_sub)
 			Else
  				SubstractOverlapedWaves(0.,vback-vb,ymult,xT-xpeak,masterwave,wave2overlap,wave2overlap_path_sub)
 			Endif
  			If(cmpstr(Append_peak,"yes")==0) 
  			  	RemoveFromGraph /Z  $wave2overlap_name_sub
  				AppendToGraph $wave2overlap_path_sub
  				GetTraceColor("",wave2overlap_name,cR,cG,cB)
  				ModifyGraph rgb($wave2overlap_name_sub)=(cR,cG,cB)
  				ModifyGraph lstyle($wave2overlap_name_sub)=3
  				ModifyGraph offset($wave2overlap_name_sub)={0,-yshift}
  			Endif				
 		Endif
 	 Break		
    	EndSwitch	
Endfor

	
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Wave2PeakParam(wavein,xa,xb,xpeak,vpeak,xwin)

Wave /d wavein
Variable xa,xb, &xpeak,&vpeak,xwin
Variable x0

// Position of the maximum
WaveStats /Q /R=(xa,xb) wavein
x0 = V_maxloc
// Average position and maximum of the peak through a parabola fit
// The cut wave
Duplicate /O /R=(x0-xwin,x0+xwin) wavein wavein_cut
// Enough points for a fit ?
If(numpnts(wavein_cut)<=4 || xwin==0)
	xpeak  = x0
	vpeak  = wavein(x0)
	Return 0
Endif
// The fit weights
Duplicate /O wavein_cut wavein_cut_weight
wavein_cut_weight = sqrt(abs(wavein_cut))
// The polynom coefficients
Make /O/D/N=3 poly_coef
// The fit
CurveFit /K={x0} /M=0 /N=1 /NTHR=0  /ODR=1 /Q=1 /W=2  poly_XOffset 3,  kwCWave=poly_coef, wavein_cut  /A=0 /I=1 /W=wavein_cut_weight
// The parabola values
xpeak  = x0-poly_coef[1]/2/poly_coef[2]
vpeak  = poly_coef[0] + poly_coef[1]*(xpeak-x0) + poly_coef[2]*(xpeak-x0)^2
// Clean useless waves
KillWaves /Z  wavein_cut, wavein_cut_weight, poly_coef, M_Jacobian, W_sigma, W_fitConstants

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Wave2BackParam(wavein,xa,vback,xwin)

Wave /d wavein
Variable xa,&vback,xwin

// Average value of the background over the windows
// The cut wave
Duplicate /O /R=(xa-xwin,xa+xwin) wavein wavein_cut
// Enough points for a fit ?
If(numpnts(wavein_cut)<=3 || xwin==0)
	vback  = wavein(xa)
	Return 0
Endif
// The fit weights
Duplicate /O wavein_cut wavein_cut_weight
wavein_cut_weight = sqrt(abs(wavein_cut))
// The polynom coefficients
Make /O/D/N=2 line_coef
// The fit
CurveFit /M=0 /N=1 /NTHR=0  /ODR=1 /Q=1 /W=2  line,  kwCWave=line_coef , wavein_cut  /A=0 /I=1 /W=wavein_cut_weight
// The linear background value values
vback  = line_coef[0] + line_coef[1]*xa
// Clean useless waves
KillWaves /Z  wavein_cut, wavein_cut_weight, line_coef, M_Jacobian, W_sigma, W_fitConstants
	
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function SubstractOverlapedWaves(dx,dy,Xy,dxO,wA,wB,wavenameC)

Variable dx // Offset in x
Variable dy // Offset in y
Variable Xy // Multiplication factor in Y
Variable dxO // Global offset of all waves
Wave /d  wA // Master wave
Wave /d wB // Working wave
String wavenameC // Full path to the final result

Variable xA0, dxA, xB0, dxB, nA, nB, x1, x2

//-- internal copies of the waves
Duplicate /O wA wAi
Duplicate /O wB wBi

//-- wave scaling of waves wAi, wBi
nA    = numpnts(wAi)
xA0  = DimOffset(wAi, 0)
dxA  = DimDelta(wAi,0)
nB    = numpnts(wBi)
xB0  = DimOffset(wBi, 0)
dxB  = DimDelta(wBi,0)

//-- offset the wave wBi
Setscale/P x, xB0+dx, dxB, wBi
wBi = wBi[p]*Xy + dy

//-- common x-limits of master and offseted working waves
x1 = Max(xA0, xB0+dx)
x2 = Min(xA0 + (nA-1)*dxA, xB0 + dx + (nB-1)*dxB)

//--- cut out wAi over the common range 
Duplicate /O/R=(x1,x2) wAi wAic
nA    = numpnts(wAic)
xA0  =  DimOffset(wAic, 0)
Make /O/N=(nA) xA, wBic
xA = xA0 + p*dxA

//-- interpolate wBi over this range
Interpolate2 /T=2 /E=2 /I=3 /Y=wBic /X=xA wBi 
Note /K wBic, note(wBi) 
Setscale /P x, xA0 + dxO, dxA, wBic

//-- substract waves
wBic = wBic[p] - wAic[p]

//-- information
Note wBic, " "
Note wBic,  "# Substracted wave : "	+ GetWavesDataFolder(wA,2)
Note wBic,  "# Offset (eV) : "		+  num2str(dxO)
Note wBic,  "# Number of points : "	+  num2str(nA)
Note wBic,  "# Start value (eV) : "	+  num2str(pnt2x(wBic,0)) 
Note wBic,  "# End value (eV) : " 	+  num2str(pnt2x(wBic,numpnts(wBic)-1))   
Note wBic,  "# Step (eV) : "		+  num2str(dxA)

//-- make the output copy
Duplicate /O wBic $wavenameC

//-- clean
KillWaves /Z wAi, wBi, wAic, wBic, xA

End