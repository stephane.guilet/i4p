// Module :  load_vamas
// Author : Adapted from internet by Remi Lazzari; lazzari@insp.jussieu.fr
// Date : December 30th, 2018
// Usage : reading Vamas format file exported by Omicron EIS and SpecsLabProdigy



#pragma rtGlobals = 3			// use modern global access method and strict wave access.
#pragma ModuleName = load_vamas

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Load_Vamas()		
	
//Opens file window and opens file read only
Variable refNum = 0
String filefilters
filefilters = "Vamas Files (*.vms):.vms;"  +  "All Files (*.*):.*;"
Open /R /F=filefilters refNum
FStatus refNum 
If(stringmatch(S_filename, "")==1)
	Print "==> Error: File was not opened sucessfully !"
	Return 0
EndIf

// Put file name in history
Print "==> Load Vamas data file: ", S_fileName

//Sets S_fileName to just the file name for inclusing in the wave notes
String filename
sscanf S_fileName,"%[^.]", filename
	
//Check that the first line matches that of Vamas text file
String currentline
FReadLine refNum, currentline
If(!stringmatch(currentline, "VAMAS Surface Chemical Analysis Standard Data Transfer Format 1988 May 4\r"))
	Close refNum
	Beep
	Doalert /T="WARNING" 0, "ERROR \r\rNot a Vamas text file"
	Return 0
Endif

// The datafolder of variables
String saveDF = GetDataFolder(1)
NewDataFolder /O/S root:I4P
NewDataFolder /O/S :Variables
 
//Get folder name
String foldername = filename
Prompt foldername, "Enter a folder name or keep the filename as folder name"
String makewave_BE  = StrVarOrDefault("gmakewave_BE","no") 
Prompt makewave_BE, "Make binding energy wave", popup "no;yes"
String makewave_TF  = StrVarOrDefault("gmakewave_TF","no")
Prompt makewave_TF, "Load transmission function wave", popup "no;yes"
String helpstring
helpstring = "1- Select folder name\r 2- Make or not binding energy wave \r 3- Make or not transmission function wave and correct data"
DoPrompt /HELP=helpstring "I4P : Load VAMAS *.vms", foldername, makewave_BE, makewave_TF
if( V_Flag )
	SetDataFolder saveDF
	Close refNum
	Print "==> Error in the input parameters !"
	Return 0
endif

// Save the data
String /G  	gmakewave_BE = makewave_BE
String /G	gmakewave_TF = makewave_TF

// go back to current datafolder
SetDataFolder saveDF

//Check if DF already exists to prevent overwites.
String ExistingDataFolders
foldername = StringName(foldername,"")
ExistingDataFolders = DataFolderDIR(1)
If(-1 != strsearch(ExistingDataFolders, foldername, 0))
	Close refNum
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rData folder already exists !"
	Return 0
EndIf

//Make a new folder to hold the file
NewDataFolder/S $foldername	

// Extracted data
String usercomment = "", scanmode = "", datetext = ""
String technique="", source = "", EAmode = ""
String Tspecies = "", Tlevel = ""
String Xlabel = "", Xunit = ""
Variable hv = 0, Epass = 0, workfkt = 0, dwell = 0, scans = 0
Variable Xstart = 0, Xstep = 1, points = 1
String regionname="", region_list=""
String instrument=""
String Ylabel = "", Ylabel2 =""
String experimentcomment=""
Variable NYaxis

Variable gencnt = 0, gennum = 0, datablocks = 0, sectioncounter = 0
//----------------------------------------------------------------------------------------------------------------------
// beginning of file header
//----------------------------------------------------------------------------------------------------------------------
FReadLine refNum, currentline		// institution
FReadLine refNum, currentline		// instrument model
instrument =  RemoveEnding(currentline,"\r")
if(strlen(instrument)==0)
 	instrument = "EA 125"
endif
FReadLine refNum, currentline		// operator
FReadLine refNum, currentline		// experiment descritption
FReadLine refNum, currentline		// number of comments
gennum = str2num(currentline)		// extract the number
for (gencnt = 0; gencnt < gennum; gencnt += 1)
	FReadLine refNum, currentline	// get comments
	experimentcomment +=  "# ---> " + RemoveEnding(currentline,"\r")	+ "\r" // save the comments
endfor
if(cmpstr(instrument,"EA 125")==0 && cmpstr(experimentcomment,"Experiment Type: ISS")==0)  // Get from user the impact energy in ISS or EELS
	Variable hv_loss = 1000
	Prompt hv_loss, "Excitation energy (eV) for ISS/EELS"
	helpstring = "Give the excitation energy to calculate energy loss"
	DoPrompt /HELP=helpstring  "Excitation energy (eV) for ISS/EELS ?", hv_loss
endif	
FReadLine refNum, currentline		// experiement mode (MAP, MAPDP, NORM, or SDP)
FReadLine refNum, currentline		// scan mode (should be REGULAR)
scanmode = RemoveEnding(currentline,"\r")
FReadLine refNum, currentline		// number of spectra
datablocks = str2num(currentline)	// extract the number
// number of analysis positions, discrete x, and y coords only specified for experiment modes MAP and MAPDP
FReadLine refNum, currentline		// experimental variables (=0)
gennum = str2num(currentline)		// extract the number
for (gencnt = 0; gencnt < gennum; gencnt += 1)
	FReadLine refNum, currentline	// variable description
	FReadLine refNum, currentline	// variable unit
endfor
FReadLine refNum, currentline		// relic line (=0)
gennum = str2num(currentline)		// extract the number
if (gennum)
	Close refNum
	beep
	doalert /T="WARNING" 0, "ERROR \r\rUnsupported old file version !"
	return 0
endif
FReadLine refNum, currentline		// # of manually entered items (=0)
gennum = str2num(currentline)		// extract the number
for (gencnt = 0; gencnt < gennum; gencnt += 1)
	FReadLine refNum, currentline	// read item
endfor
FReadLine refNum, currentline		// # of future experiment upgrade entries (=0)
gennum = str2num(currentline)		// extract the number
FReadLine refNum, currentline		// \r# of future upgrade block entries (=0)
for (gencnt = 0; gencnt < gennum; gencnt += 1)
	FReadLine refNum, currentline	// read item
endfor
FReadLine refNum, currentline		// number of data blocks (= # of spectra)
gennum = str2num(currentline)		// extract the number
if (gennum != datablocks)
	Close refNum
	beep
	doalert /T="WARNING" 0, "ERROR \r\rRead error on the number of spectra !"
	return 0
endif
//----------------------------------------------------------------------------------------------------------------------
// end of file header
//----------------------------------------------------------------------------------------------------------------------



for (sectioncounter = 0; sectioncounter < datablocks; sectioncounter +=1)
	//----------------------------------------------------------------------------------------------------------------------
	// beginning of block header
	//----------------------------------------------------------------------------------------------------------------------
	FReadLine refNum, currentline		// block identifier
	FReadLine refNum, currentline		// sample identifier
	
	datetext = ""
	FReadLine refNum, currentline		// year
	sprintf datetext, datetext+"%.4d-", str2num(currentline)	// write year
	FReadLine refNum, currentline		// month
	sprintf datetext, datetext+"%.2d-", str2num(currentline)	// write month
	FReadLine refNum, currentline		// day
	sprintf datetext, datetext+"%.2d ", str2num(currentline)	// write day
	
	Variable RawTime = 0
	FReadLine refNum, currentline			// hour
	RawTime += str2num(currentline)*60*60	// write hour
	FReadLine refNum, currentline			// minute
	RawTime += str2num(currentline)*60	// write minute
	FReadLine refNum, currentline			// seconds
	RawTime += str2num(currentline)		// write seconds
	FReadLine refNum, currentline			// GMT offset
	datetext += Secs2Time(RawTime,3)	// convert to string
	
	FReadLine refNum, currentline		// comments again
	gennum = str2num(currentline)		// extract the number
	regionname = ""
	usercomment = ""
	for (gencnt = 0; gencnt < gennum; gencnt += 1)
		FReadLine refNum, currentline	// get comments
		if(gennum==1&& cmpstr(instrument,"EA 125") ==0) // Case of Vamas exportation from OMICRON EIS 
		 	regionname =  RemoveEnding(currentline,"\r")
		else
			usercomment += "# ---> " + RemoveEnding(currentline,"\r") + "\r"	 
		endif
	endfor
	FReadLine refNum, currentline		// technique (XPS)
	technique = RemoveEnding(currentline,"\r")
	FReadLine refNum, currentline		// light source
	source	= RemoveEnding(currentline,"\r")
	FReadLine refNum, currentline		// light source energy
	hv		= str2num(currentline)
	if(cmpstr(instrument,"EA 125")==0 && hv>10000)  // Get from user the impact energy in ISS or EELS
		hv =  hv_loss
	endif	
	FReadLine refNum, currentline		// light source strength
	FReadLine refNum, currentline		// light source x width
	FReadLine refNum, currentline		// light source y width
	FReadLine refNum, currentline		// light source incident angle (polar)
	FReadLine refNum, currentline		// light source incident angle (azimuth)
	
	FReadLine refNum, currentline		// analyzer mode (FAT or FRR)
	EAmode	= RemoveEnding(currentline,"\r")
	EAmode = GetAnalyzerModeName(EAmode)

	FReadLine refNum, currentline		// analyzer pass energy or retardation
	Epass	= str2num(currentline)
	FReadLine refNum, currentline		// analyzer magnification
	FReadLine refNum, currentline		// analyzer work function
	workfkt	= str2num(currentline)
	FReadLine refNum, currentline		// targetBias
	FReadLine refNum, currentline		// analysis x width
	FReadLine refNum, currentline		// analysis y width
	FReadLine refNum, currentline		// analyzer axis take off (polar)
	FReadLine refNum, currentline		// analyzer axis take off (azimuth)
	
	FReadLine refNum, currentline		// sample species
	Tspecies	= RemoveEnding(currentline,"\r")	
	if(cmpstr(instrument,"Phoibos")==0) // Case of Vamas exportation from SpecsLab Prodigy
		 regionname =  Tspecies
	endif
	FReadLine refNum, currentline		// species level
	Tlevel	= RemoveEnding(currentline,"\r")
	
	FReadLine refNum, currentline		// charge of detected particle (-1 for electron)
	if (stringmatch(scanmode, "REGULAR"))
		FReadLine refNum, currentline	// x axis label
		Xlabel	= RemoveEnding(currentline,"\r")
		FReadLine refNum, currentline	// x axis unit
		Xunit	= RemoveEnding(currentline,"\r")
		FReadLine refNum, currentline	// x axis start
		Xstart	= str2num(currentline)
		FReadLine refNum, currentline	// x axis increment
		Xstep	= str2num(currentline)
	endif
	FReadLine refNum, currentline		// corresponding y axes (should be 1)
	NYaxis = str2num(currentline)
	FReadLine refNum, currentline		// y axis label
	Ylabel	= RemoveEnding(currentline,"\r")
	FReadLine refNum, currentline		// y axis unit
	if(cmpstr(instrument,"Phoibos")==0 && NYaxis==2)  // Case of Vamas exportation from SpecsLab Prodigy 
		FReadLine refNum, currentline	
		Ylabel2 = RemoveEnding(currentline,"\r")	
		FReadLine refNum, currentline		
	endif
	
	FReadLine refNum, currentline		// signal mode
	FReadLine refNum, currentline		// dwell
	dwell	= str2num(currentline)
	FReadLine refNum, currentline		// # of scans
	scans	= str2num(currentline)
	
	FReadLine refNum, currentline		// signal time correction
	FReadLine refNum, currentline		// sample angle (tilt, polar)
	FReadLine refNum, currentline		// sample angle (tilt, azimuth)
	FReadLine refNum, currentline		// sample angle (rotation)
	FReadLine refNum, currentline		// # of additional parameters (= 0)

	FReadLine refNum, currentline		// # of values
	points = str2num(currentline)/NYaxis // Divide by the number of axis
	FReadLine refNum, currentline		// min y
	FReadLine refNum, currentline		// max y
	if(cmpstr(instrument,"Phoibos")==0)  // Case of Vamas exportation from SpecsLab Prodigy 
	 FReadLine refNum, currentline
	 FReadLine refNum, currentline
	endif
	//----------------------------------------------------------------------------------------------------------------------
	// end of block header
	//----------------------------------------------------------------------------------------------------------------------
	
	Make/O/D/N=(points) RegionLoaded	// create wave for data loading
	Make/O/D/N=(points) RegionLoaded2    // create wave for transmission function loading
	RegionLoaded2 = 1
	for (gencnt = 0; gencnt < points; gencnt += 1)		// extract data
		FReadLine refNum, currentline
		RegionLoaded[gencnt] = str2num(currentline) 
		if(cmpstr(instrument,"Phoibos")==0 && NYaxis!=1)  // Case of Vamas exportation from SpecsLab Prodigy 
		  FReadLine refNum, currentline
		  RegionLoaded2[gencnt] = str2num(currentline) 
		endif
	endfor

	// Normalize the data
	RegionLoaded = RegionLoaded/scans
	if(cmpstr(Ylabel,"counts")==0 || cmpstr(Ylabel,"count rate")==0)   // "count rate" opion is for Omicron EIS since the exportation in vms seems wrong
		RegionLoaded = RegionLoaded/dwell
		Print "==> Warning : internal conversion to counts per second"
	endif

	//Reverse RegionLoaded
	If(cmpstr(Xlabel,"kinetic energy")==0) // Revert to binding energy scale
		Xstart = hv - Xstart - (points-1)*Xstep
		Reverse RegionLoaded
		Reverse RegionLoaded2
	Endif
	SetScale/P x Xstart, Xstep,Xunit, RegionLoaded
	SetScale d 0,0,"cps", RegionLoaded
	
	// append experiment note
	Note RegionLoaded, " "
	Note RegionLoaded, "# Instrument : "                +  instrument
	//Note RegionLoaded, "# Folder name : "           + foldername
	Note RegionLoaded, "# Filename : "                  + S_fileName
	Note RegionLoaded, "# Region : "                     + regionname
	Note RegionLoaded, "# Acquisition date : "	    + datetext
	If(cmpstr(Xlabel,"Index")!=0)
		Note RegionLoaded, "# X-scale : binding energy or energy loss (eV) "
	Else
		Note RegionLoaded, "# X-scale : time (s) "
	Endif
	Note RegionLoaded, "# Y-scale : count rate (cps) "
	Note RegionLoaded, "# Dwell time (sec) : "       + num2str(dwell)
	Note RegionLoaded, "# Number of scans : "      + num2str(scans)
	Note RegionLoaded, "# Number of points : "      + num2str(points)
	Note RegionLoaded, "# Start value (eV) : "        + num2str(Xstart)
	Note RegionLoaded, "# End value (eV) : "	    + num2str(Xstart+Xstep*(points-1)) 
	Note RegionLoaded, "# Step (eV) : "		    + num2str(Xstep)
	Note RegionLoaded, "# Technique : "	  	    + technique
	Note RegionLoaded, "# Excitation source : "	    + source
	Note RegionLoaded, "# Excitation energy (eV) : "  + num2str(hv)
	Note RegionLoaded, "# Analyzer mode : "	    + EAmode
	Note RegionLoaded, "# Pass energy (eV) or Retardation ratio : " + num2str(Epass)
	Note RegionLoaded, "# Eff. workfunction (eV) : " + num2str(workfkt)
	if (strlen(usercomment) > 0)
		Note RegionLoaded, "# Comments : \r"	     + usercomment
	endif
	if (strlen(experimentcomment) > 0)
		Note RegionLoaded, "# General comments : \r"	     + experimentcomment
	endif		
	//Note RegionLoaded, "# Species : "		    + Tspecies
	//Note RegionLoaded, "# Energy level : "	          + Tlevel

	//Check if region is unamed if it is add region name
	If(stringmatch(regionname,""))
		regionname = "Region_" + num2str(sectioncounter)
	EndIf

	//Check if wavename will excede maxium lentgh and corrects it.
	RegionName = StringName(regionname,"")

	//Check if region name is the same as another name and changes it.
	If(strsearch(region_list, regionname, 0) != -1)
		RegionName = StringName(RegionName,"_" + num2str(sectioncounter))
	EndIf

	//Add name to list to keep trrack of previous names
	region_list = region_list + regionname + "; "
	
	// Print the final region name
	Print  "==> Region : ", regionname

	// Make the store wave
	Duplicate/O RegionLoaded, $regionname		


	// Make a binding energy wave with the same information as the wave itself
	If(cmpstr(makewave_BE,"yes")==0)
		String regionname_BE = StringName(regionname,"_BE")
		Make/O/D/N=(points)  $regionname_BE
		Wave /D region_BE =  $regionname_BE
		region_BE =  Xstart + p* Xstep
		SetScale/P x, 0, 1, "",  region_BE
		SetScale d 0,0, "", region_BE
		Note region_BE, " "
		Note region_BE, "# Filename : "                                          + S_fileName
		Note region_BE, "# Binding energy scale (eV) of region : "    + regionname
	Endif  

	// Make a transmission function wave
	If(cmpstr(makewave_TF,"yes")==0)
		String regionname_TF = StringName(regionname,"_TF")
		Make/O/D/N=(points)  $regionname_TF
		Wave /D region_TF = $regionname_TF 		
		Strswitch(instrument)	
		Case "Phoibos":	
			 wavestats /q RegionLoaded2
			 If(V_max==V_min && V_avg==1)
				Strswitch(EAmode)
				Case gFAT:
					region_TF =  62.905/((hv - Xstart - p* Xstep)^0.5077)
				Break
				Case gFRR:
					region_TF = (hv - Xstart - p* Xstep)/(hv/2.)
				Break
				Endswitch
			 Else
			 	region_TF = RegionLoaded2
			 Endif
		Break					
		Case "EA 125":
			Strswitch(EAmode)
			Case gFAT:
				region_TF = (hv/2.)/(hv - Xstart - p* Xstep)
			Break
			Case gFRR:
				region_TF = (hv - Xstart - p* Xstep)/(hv/2.)
			Break
			Endswitch
		Break
		Default:	
			 region_TF = 1.	
		Endswitch
		SetScale/P x, Xstart, Xstep, "eV",  region_TF
		SetScale d 0,0,"Arb. units", region_TF
		Note region_TF, " "
		Note region_TF, "# Filename : "                                    + S_fileName
		Note region_TF, "# Transmission function of region : "    + regionname
		Note region_TF, "# Excitation energy (eV) : "  + num2str(hv)
	Endif   

	// Make a transmission function corrected wave
	If(cmpstr(makewave_TF,"yes")==0)
		String regionname_TFC = StringName(regionname,"_TFC")
		Duplicate/O RegionLoaded, $regionname_TFC
		Wave /D region_TFC =$regionname_TFC 	
		region_TFC = RegionLoaded/region_TF
		Note region_TFC, " "
		Note region_TFC, "# Region : corrected by transmission function"
	Endif

	// Clean waves
	KillWaves RegionLoaded, RegionLoaded2

endfor

//Add info to history
Print "==> Number of regions imported : ", datablocks

// Close the file
Close refNum

End