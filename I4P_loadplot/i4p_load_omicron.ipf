// Module :  load_omicron
// Author : James Mudd;  james.mudd@gmail.com adapated by Remi Lazzari, lazzari@insp.jussieu.fr
// Date : 2011
// Usage :  reading Omicron XPS/UPS data from text files exported by EIS software into Igor.

#pragma rtGlobals = 3		
#pragma ModuleName = load_omicron

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Load_EIS()
	
// Some variables
Variable refNum, RegionCounter=0
String Region_list=""
 
 //Opens file window and opens file read only
 String filefilters
 filefilters = "Omicron EIS Files (*.txt):.txt;" +  "All Files (*.*):.*;"
Open/R /F=filefilters  refNum 
FStatus refNum
If(stringmatch(S_filename, "")==1)
	Print "==> Error: File was not opened sucessfully !"
	Return 0
EndIf

// Put file name in history
Print "==> Load Omicron Data File: ", S_fileName 

//Sets S_fileName to just the file name for inclusing in the wave notes
String filename
sscanf S_fileName,"%[^.]", filename

//Check if the first line matches that of Omicron text file
String line
FReadLine refNum, line 
If(0==Stringmatch(line, "Region\tEnabled\tDataFlag\tStart\tEnd\tStep\tSweeps\tDwell\tMode\tCAE/CRR\tMag\tDivide by Ten\tNotes\tFunction\tLine-Style\tPoint-Style\tSweeps-Acquired\r"))
	Close refNum
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rNot a Omicron EIS/Sphera text file !"
	Return 0
EndIf

// The datafolder of variables
String saveDF = GetDataFolder(1)
NewDataFolder /O/S root:I4P
NewDataFolder /O/S :Variables

//Get folder name and photon energy
String foldername = filename
Prompt foldername, "Enter a folder name or keep the filename as folder name"
String Photon_source = StrVarOrDefault("gPhoton_source","Al Kalpha")
Prompt Photon_source, "Select excitation source", popup "Al Kalpha;Mg Kalpha;He I;He II;Ion Scattering Spectroscopy;Electron Energy Loss Spectroscopy"
Variable  Photon_Energy = NumVarOrDefault("gPhoton_Energy",1000)
Prompt Photon_Energy, "Excitation energy (eV) for ISS/EELS"
String makewave_BE = StrVarOrDefault("gmakewave_BE","no")
Prompt makewave_BE, "Make binding energy wave", popup "no;yes"
String makewave_TF = StrVarOrDefault("gmakewave_TF","no")
Prompt makewave_TF, "Make transmission function wave", popup "no;yes"
String helpstring
helpstring   =  "1- Select folder name\r"
helpstring +=  "2- Choose excitation source (not written in the data file !) \r"
helpstring +=  "3- Make or not binding energy wave \r"
helpstring += " 4- Make or not transmission function wave and correct data"
DoPrompt /HELP=helpstring  "I4P : Load OMICRON EIS *.dat", foldername, Photon_source, Photon_Energy, makewave_BE, makewave_TF
if( V_Flag )
	SetDataFolder saveDF
	Close refNum
	Print "==> Error in the input parameters !"
	Return 0
endif

// Save the data
String /G 	gPhoton_source = Photon_source
Variable /G 	gPhoton_Energy= Photon_Energy
String /G  	gmakewave_BE = makewave_BE
String /G	gmakewave_TF = makewave_TF

// go back to current datafolder
SetDataFolder saveDF

// The photon energy
Strswitch(Photon_source)
	Case "Mg Kalpha": 
		Photon_Energy = ghv_Mg
	Break
	Case "Al Kalpha":
		Photon_Energy = ghv_Al
	Break
	Case "He I":
		Photon_Energy = ghv_HeI
	Break
	Case "He II":
		Photon_Energy = ghv_HeII
	Break
Endswitch
		
//Check if DF already exists to prevent overwites.
String ExistingDataFolders
ExistingDataFolders = DataFolderDIR(1)
foldername = StringName(foldername,"")
If(-1 != strsearch(ExistingDataFolders, foldername, 0))
	Close refNum
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rData folder already exists !"
	Return 0
EndIf

//Make a new folder to hold the file
NewDataFolder/S $foldername 	
	
//Region Variables
Variable startEn, endEn, stepEn, DwellT, PassEn, Sweeps, Channels, EnDummy, Counts, Points, i, dummy2, Region_Num=0
String Mode, RegionName, DivideTen, Mag, dummy

//Start Reading Regions
Do

	//Increment Region Number incase of more unamed Regions
	Region_Num +=1 

	// Comment line
	FReadLine refNum, line
	
	//Load lots of Region infomation into variables
	sscanf line, "%s\t%s\t%s\t%e\t%e\t%e\t%e\t%e\t%s\t%e\t%s\t%s\t%[^\t]\t%e", dummy, dummy, dummy, startEn, endEn, stepEn, Sweeps, DwellT, Mode, PassEn, Mag, DivideTen, RegionName, Channels
	
	// Use standard name for analyzer mode
	Mode = GetAnalyzerModeName(Mode) 
	
	//Catch problem with unamed Regions
	If(stringmatch(RegionName,"7 Channel Acquisition"))
		RegionName = ""
		Channels = 7
	EndIf
	
	If(stringmatch(RegionName,"5 Channel Acquisition"))
		RegionName = ""
		Channels = 5
	EndIf
	
	If(stringmatch(RegionName,"3 Channel Acquisition"))
		RegionName = ""
		Channels = 3
	EndIf
	
	If(stringmatch(RegionName,"1 Channel Acquisition"))
		RegionName = ""
		Channels = 1
	EndIf
	
	//Calculate number of points in Region added rounding to prevent make errors
	Points = Round(Abs(Abs(endEn - startEn) / stepEn) +1)
	
	//Check if Region is unamed if it is add Region name
	If(stringmatch(RegionName,""))
		RegionName = "Region " + num2str(Region_Num)
	EndIf
		
	//Check if wavename will excede maxium lentgh and corrects it.
	RegionName = StringName(RegionName,"")
	
	//Check if Region name is the same as another name and changes it.
	If(strsearch(Region_list, RegionName, 0) != -1)
		RegionName = StringName(RegionName,"_" + num2str(Region_Num))
	EndIf
	
	//Add name to list to keep trrack of previous names
	Region_list = Region_list + RegionName + "; "
	
	// Print the final Region name
	Print  "==> Region : ", RegionName
	
	//Make wave to hold Region
	Make /D/O/N=(Points) $RegionName
	//Set wave pointer
	Wave /D Region = $RegionName 
	 //Sets x scaling and units
	SetScale/P x, startEn, stepEn, "eV",  Region
	SetScale d 0,0,"cps", Region
	
	//Skip extra header lines
	FReadLine refNum, line
	FReadLine refNum, line
	FReadLine refNum, line
	
	//Read data into Region wave
	For(i=0;i<points;i+=1)
		FReadLine refNum, line
		// Read spectrum data energy value is not needed as wave is already scaled
		sscanf line, "%e%e", EnDummy, Counts
		//Put data into wave
		Region[i] = Counts
	EndFor
	
	//Check that last point had the correct energy value
	If(EnDummy != endEn)
		Print "==> Error in Region : ", RegionName, " (Final BE not the expected one !)"
	EndIf
	
	//Convert data into counts per sec
	Print "==> Warning : internal conversion to counts per second"
	//Divide counts by sweeps
	Region/=Sweeps
	//Divide counts by dwell to counts per sec
	Region/=DwellT

	//Reverses backward data 
	If(stepEn<0)
		Reverse Region
		startEn = EndEn
		stepEn = - stepEn
		endEn = startEn + (points-1)*stepEn
		SetScale/P x, startEn, stepEn, "eV",  Region
	Endif

	//From KE to energy loss in case of EELS or ISS
	If(cmpstr(Photon_source,"Ion Scattering Spectroscopy")==0 || cmpstr(Photon_source,"Electron Energy Loss Spectroscopy")==0)
		Reverse Region
		startEn = Photon_Energy - endEn
		endEn  = startEn + (points-1)*stepEn
		SetScale/P x, startEn, stepEn, "eV",  Region			
	Endif
	
	//Add Extra info to wave notes
	Note Region, " "
	Note Region, "# Instrument : EA 125" 
	//Note Region, "# Folder name : "      + foldername
	Note Region, "# Filename : "            + S_fileName
	Note Region, "# Region : "               + RegionName
	Note Region, "# X-scale : binding energy or energy loss (eV) "
	Note Region, "# Y-scale : count rate (cps) "
	Note Region, "# Dwell time (sec) : "  + num2str(DwellT)
	Note Region, "# Number of scans : " + num2str(Sweeps)
	Note Region, "# Number of points : " + num2str(Points)
	Note Region, "# Start value (eV) : "   + num2str(startEn)
	Note Region, "# End value (eV) : "     + num2str(endEn)
	Note Region, "# Step (eV) : "            + num2str(stepEn)	
	Note Region, "# Excitation source : " +  Photon_source
	Note Region, "# Excitation energy (eV) : " + num2str(Photon_Energy)
	Note Region, "# Analyzer mode : "     + Mode
	Note Region, "# Pass energy (eV) or Retardation ratio : " + num2str(PassEn)
	Note Region, "# Channels : "             + num2str(Channels)
	Note Region, "# Magnification : "       + Mag
	Note Region, "# Divide by ten : "       + DivideTen
					
	// Count up nunmber of Regions imported
	RegionCounter+=1 
	
	//Read the next line of the file if its another Region start loop again
	FReadLine refNum, line
	
	//Check for mysterious half Regions sometimes added by EIS
	//Catch the omicron file error and skip it
	If(1==stringmatch(line, "Layer\tEnabled\tDataFlag\tNotes\tFormula\tLine-Style\tPoint-Style\r"))
	 //Set flag for error message 
	 	Print "==> Warning : mysterious half regions added by EIS"
		Do
			For(i=0;i<points+3;i+=1)
				FReadLine refNum, line
				// Read spectrum data energy value is not needed as wave is already scaled
				sscanf line, "%e%e", EnDummy, Counts
			EndFor
		While(1==stringmatch(line, "Layer\tEnabled\tDataFlag\tNotes\tFormula\tLine-Style\tPoint-Style\r"))
	EndIf
	
	// Make a binding energy wave with the same information as the wave itself
	If(cmpstr(makewave_BE,"yes")==0)
		String RegionName_BE = StringName(RegionName,"_BE")
		Make/O/D/N=(Points) $RegionName_BE
		Wave/D Region_BE = $RegionName_BE
		Region_BE =  startEn + p* stepEn
		SetScale/P x, 0, 1, "",  Region_BE
		SetScale d 0,0,"", Region_BE
		Note Region_BE, " "
		Note Region_BE, "# Filename : "                                           + S_fileName
		Note Region_BE, "# Binding energy scale (eV) of  region : "    + RegionName
	Endif 
	
	// Make a transmission function wave		
	If(cmpstr(makewave_TF,"yes")==0)
		String RegionName_TF = StringName(RegionName,"_TF")
		Make/O/D/N=(Points)  $RegionName_TF
		Wave /D Region_TF = $RegionName_TF
		Strswitch(Mode)
		Case gFAT: 		
			Region_TF = (Photon_Energy/2.)/(Photon_Energy -startEn -p*stepEn)
		Break
		Case gFRR:
			Region_TF = (Photon_Energy -startEn -p*stepEn)/(Photon_Energy/2.)
		Break
		Default:
			Region_TF = 1.
		Endswitch
		SetScale/P x, startEn, stepEn, "eV",  Region_TF
		SetScale d 0,0,"Arb. units", Region_TF
		Note Region_TF, " "
		Note Region_TF, "# Filename : "                                    + S_fileName
		Note Region_TF, "# Transmission function of region : "    + RegionName
		Note Region_TF, "# Excitation energy (eV) : " + num2str(Photon_Energy)
	Endif

	// Make a transmission function corrected wave
	If(cmpstr(makewave_TF,"yes")==0)
		String RegionName_TFC = StringName(RegionName,"_TFC")
		Duplicate/O Region, $RegionName_TFC
		Wave /D Region_TFC =$RegionName_TFC 	
		Region_TFC = Region/Region_TF
		Note Region_TFC, " "
		Note Region_TFC, "# Region : corrected by transmission function"
	Endif

				
//Check if file contains any more Regions
While(0!=stringmatch(line, "Region\tEnabled\tDataFlag\tStart\tEnd\tStep\tSweeps\tDwell\tMode\tCAE/CRR\tMag\tDivide by Ten\tNotes\tFunction\tLine-Style\tPoint-Style\tSweeps-Acquired\r"))

//Add info to history
Print "==> Number of Regions imported : ", RegionCounter

// Close the file
Close refNum

End