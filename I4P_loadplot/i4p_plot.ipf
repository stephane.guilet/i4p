// Module : plotxps
// Author : Remi Lazzari, lazzari@insp.jussieu.fr
// Date : December 26th 2018
// Usage : Plot all the spectra of a directory with a given name

#pragma rtGlobals = 3		
#pragma ModuleName = ploti4p

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Plot selected XPS data
Static Function Plot_CoreLevel()
	
	//Get wavename
	String ListofWave
	String Directory
	Variable Index = 0
	String SampleList =""
	String name
	Variable NumberofSamples = 0 
	Variable i
	
	Directory = GetDataFolder(1)
	NumberofSamples = 	CountObjectsDFR(GetDataFolderDFR(),4)
	for (i = 0 ; i < NumberofSamples ; i +=1)
		name= GetIndexedObjNameDFR (GetDataFolderDFR(),4, i)
		SampleList = SampleList + name + ";"
	endfor
	
	Printf "SampleList of %s = %s \r",Directory,SampleList
	
	String line1
	Prompt line1, "Current data folder is "+Directory
	String line2
	Prompt line2, "You can select another Data Folder (Sample)" , popup SampleList
	DoPrompt "I4P : Plot Spectra, Choose Sample", line1, line2
	If( V_Flag == 1 )
		Print "==> Operation Canceled by user !"
		// Come back to the initial Folder before Plot operation.
		SetDataFolder Directory
		Return 0
	Endif
	SetDataFolder line2 

	ListofWave = WaveList("*", ";", "" )
	
	String tracename1
	Prompt tracename1, "Select core level to plot" , popup ListofWave
	String tracename2 = "----------"
	Prompt tracename2, "Enter core level name to plot (e.g. * or *Ag or *Ag* or Ag 3d;Ti 2p)" 
	String HelpString
	HelpString = "1- Select or enter the names of the spectra to plot in the current folder (with or without wildcards separated by ';') \r"
	HelpString += "2- Run"	
	DoPrompt /HELP = HelpString "I4P : Plot spectra", tracename1, tracename2
	If( V_Flag )
		Print "==> Error in the input parameters !"
		// Come back to the initial Folder before Plot operation.
		SetDataFolder Directory
		Return 0
	Endif
	
	// Get the list of waves  in the current data folder
	String tracename
	If(cmpstr(tracename2, "----------")!=0)
		tracename = tracename2
	Else
		tracename = tracename1
	Endif
	ListofWave = WaveList(tracename, ";", "" )
	If(ItemsInList(ListofWave,";")==0)
		Print "==> Data folder : ", GetDataFolder(1) 
		Print "==> No wave to trace with name containing : ", tracename
		Beep
		DoAlert /T="WARNING" 0, "ERROR \r\rNo wave to trace !"
		Return 0
	Endif

	// Get the list of graphs
	String ListofGraph
	ListofGraph = WinList("I4P_Plot_Window",";","WIN:1")
	
	// Make a new graph with name "I4P_Plot_Window" if it is not present
	If(ItemsInList(ListofGraph,";")==0)
		Display /N=I4P_Plot_Window
	Endif
	
	// Append trace to the graph "I4P_Plot_Window" from waves in the list
	String iWaveName, XUnitofWave, YUnitofWave
	Print "==> Data folder : ", GetDataFolder(1) 
	For(i=0;i<ItemsInList(ListofWave,";");i+=1)
		iWaveName = StringFromList(i,ListofWave,";")
		Wave /d TempWave=$iWaveName
		XUnitofWave = WaveUnits(TempWave,0)
		YUnitofWave = WaveUnits(TempWave,-1)
		// Append to graph only scaled waves with "eV" units
		If(cmpstr(XUnitofWave,"eV")==0 && cmpstr(YUnitofWave,"cps")==0)
	  		Print "==> Append to graph wave : ", iWaveName
	  		AppendtoGraph /W=I4P_Plot_Window  TempWave
	 	Endif
	EndFor
	
	// Customize the graph
	Execute "I4P_GraphStyle()"
	SetColor()
	
	// Come back to the initial Folder before Plot operation.
	SetDataFolder Directory
	
End
	

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : From binding energy to kinetic energy in the top  given graph through trace offset
Static Function BE2KE_Plot()

	// List of traces on the active graph
	string graphlist = winlist("*", ";", "WIN:1")
	if(itemsinlist(graphlist)==0)
		beep
		doalert /T="WARNING" 0, "ERROR \r\rNo graphs available !"
		return 0
	endif
	string tracelist = tracenamelist("",";", 1)  
	
	// From BE to KE by offsetting all traces in the graph
	variable ii
	for (ii=0; ii<itemsinlist(tracelist); ii+=1)
		string trace2offset = stringfromlist(ii,tracelist, ";")
		wave /d wave2offset = tracenametowaveref("",trace2offset)
		variable hv
		hv = str2num(WaveNote2Value(wave2offset,"Excitation energy (eV)"))
		modifygraph muloffset($trace2offset)={-1,*}	
		modifygraph offset($trace2offset)={hv,*}
		label bottom "\\Z20\\F'Arial'Kinetic energy (\\U)"
		setAxis/A bottom
	endfor


End

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : From kinetic energy to binding energy in the top  graph through trace offset
Static Function KE2BE_Plot()

	// List of traces on the active graph
	string graphlist = winlist("*", ";", "WIN:1")
	if(itemsinlist(graphlist)==0)
		beep
		doalert /T="WARNING" 0, "ERROR \r\rNo graphs available !"
		return 0
	endif
	string tracelist = tracenamelist("",";", 1)  
	
	// From KE to BE by removing all trace offsets
	variable ii
	for (ii=0; ii<itemsinlist(tracelist); ii+=1)
		string trace2offset = stringfromlist(ii,tracelist, ";")
		wave /d wave2offset = tracenametowaveref("",trace2offset)
		modifygraph muloffset($trace2offset)={0,*}	
		modifygraph offset($trace2offset)={0,*}
		label bottom "\\Z20\\F'Arial'Binding energy (\\U)"
		setAxis/A/R bottom
	endfor

End

