// Module : convert_spectrum
// Author :  Remi Lazzari, lazzari@insp.jussieu.fr
// Date : 2019
// Usage :  convert (x,y) waves to the format adapted to  package


#pragma rtGlobals = 3		
#pragma ModuleName = convert_spectrum

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Convert2Spectrum()
		
// The list of waves in the current data folder
string saveDF = getdatafolder(1)
string listofwaves
listofwaves = wavelist("*", ";", "" )

// the datafolder of variables
newdatafolder /o/s root:I4P
newdatafolder /o/s :Variables

// the inputs
string spectrumnameX
prompt spectrumnameX, "Select binding energy / energy loss wave" , popup listofwaves	
string spectrumnameY
prompt spectrumnameY, "Select counting rate" , popup listofwaves
variable stepBE = numvarordefault("gstepBE",0.1)
prompt stepBE, "Energy step (eV)"	
string Photon_source = strvarordefault("gPhoton_source","Al Kalpha")
prompt Photon_source, "Select excitation source", popup "Al Kalpha;Mg Kalpha;He I;He II;Ion Scattering Spectroscopy;Electron Energy Loss Spectroscopy"
variable  Photon_Energy = numvarordefault("gPhoton_Energy",1000)
prompt Photon_Energy, "Excitation energy (eV) for ISS/EELS"
string analyzermode = strvarordefault("ganalyzermode","FAT")
prompt analyzermode, "Select the analyzer mode", popup "FAT;FRR"
string helpstring
helpstring   =  "1- Select X and Y waves to be combined\r"
helpstring   =  "2- Give energy step (eV)\r"
helpstring   =  "3- Select the excitation source or give the exciation energy for EELS/ISS\r"
helpstring   =  "4- Select the analyzer mode\r"
helpstring   =  "5- Run\r"
doprompt /HELP=helpstring  "I4P : Convert waves to spectrum", spectrumnameX, spectrumnameY,stepBE, Photon_source, Photon_Energy, analyzermode
if( V_Flag )
	setdatafolder saveDF
	print "==> Error in the input parameters !"
	return 0
endif

// save the data
variable /G 	gstepBE = stepBE
string /G 	gPhoton_source = Photon_source
variable /G 	gPhoton_Energy= Photon_Energy
string /G 	ganalyzermode = analyzermode

// go back to current datafolder
setdatafolder saveDF

// some checks
wave /d spectrumX0 = $spectrumnameX
wave /d spectrumY0 = $spectrumnameY
variable npx = numpnts(spectrumX0)
variable npy = numpnts(spectrumY0)
if(npx!=npy)
	beep
	doalert /T="WARNING" 0, "ERROR \r\rWaves have different numbers of points !"
	return 0
endif

// the photon energy
strswitch(Photon_source)
	case "Mg Kalpha": 
		Photon_Energy = ghv_Mg
	break
	case "Al Kalpha":
		Photon_Energy = ghv_Al
	break
	case "He I":
		Photon_Energy = ghv_HeI
	break
	case "He II":
		Photon_Energy = ghv_HeII
	break
endswitch

// sort the input waves by increasing binding energy
duplicate /o spectrumX0 spectrumX
duplicate /o spectrumY0 spectrumY
sort spectrumX, spectrumX, spectrumY

// make the waves
wavestats /q spectrumX
variable np = floor((V_max-V_min)/max(stepBE,0.001))
string spectrumname = spectrumnameY
spectrumname = StringName_Ext(spectrumname,"_CON")
make /d/o/n=(np) $spectrumname
wave /d spectrum = $spectrumname

// interpolate the data
interpolate2 /T=2 /E=2 /I=0 /N=(np) /Y=spectrum spectrumX, spectrumY

// wave scaling and order
variable startBE  = spectrumX[0]
setscale/P x, startBE, stepBE, "eV",  spectrum	
setscale d 0,0,"cps", spectrum

// annotation of the data
note spectrum, " "
Note spectrum, "# Filename : " 			+ spectrumname
Note spectrum, "# Region : " 			    	+ spectrumname
Note spectrum, "# Combined X-wave : "        	+ spectrumnameX
Note spectrum, "# Combined Y-wave : "        + spectrumnameY
note spectrum, "# X-scale : binding energy or energy loss (eV) "
note spectrum, "# Y-scale : count rate (cps) "
note spectrum, "# Number of points : " 		+ num2str(np)
note spectrum, "# Start value (eV) : "   		+ num2str(startBE)
note spectrum, "# End value (eV) : "     		+ num2str(stepBE + (np-1)*stepBE)
note spectrum, "# Step (eV) : "            		+ num2str(stepBE)	
note spectrum, "# Excitation source : " 		+  Photon_source
note spectrum, "# Excitation energy (eV) : " 	+ num2str(Photon_Energy)
note spectrum, "# Analyzer mode : " 		+ analyzermode

// warn the user
print "==> Convert to spectrum : " + spectrumname
print "==> X-wave :  " + spectrumnameX
print "==> Y-wave :  " + spectrumnameY

// clean
killwaves /z spectrumX, spectrumY
			
End