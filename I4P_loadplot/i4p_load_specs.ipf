// Module :  load_specs
// Author : Remi Lazzari; lazzari@insp.jussieu.fr
// Date : December 25th, 2018
// Usage : reading SpecsLab Prodigy data  as *.xy file into Igor with comments into scaled waves


#pragma rtGlobals = 3		
#pragma ModuleName = load_specs

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Load_SpecsLabProdigy()

 //Opens file window and opens file read only
Variable refNum
 String filefilters
 filefilters = "SpecsLabProdigy Files (*.xy):.xy;"  +  "All Files (*.*):.*;"
Open/R /F=filefilters refNum 
FStatus refNum
If(stringmatch(S_filename, "")==1)
	Print "==> Error: File was not opened sucessfully !"
	Return 0
EndIf

// Put file name in history
Print "==> Load SpecsLab Prodigy data file : ", S_fileName 

//Sets S_fileName to just the file name for inclusing in the wave notes
String filename
sscanf S_fileName,"%[^.]", filename

//Scan first line
String line
FReadLine refNum, line 

//Check line matches that of SpecsLab Prodigy text file
If(0!=strsearch(line, "# Created by:        SpecsLab Prodigy",0,1))
	Close refNum
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rNot a SpecsLab Prodigy text file !"
	Return 0
EndIf

// The datafolder of variables
String saveDF = GetDataFolder(1)
NewDataFolder /O/S root:I4P
NewDataFolder /O/S :Variables

//Get folder name
String foldername = filename
String makewave_BE = StrVarOrDefault("gmakewave_BE","no") 
Prompt makewave_BE, "Make binding energy wave", popup "no;yes"
String makewave_TF = StrVarOrDefault("gmakewave_TF","no")
Prompt makewave_TF, "Load transmission function wave", popup "no;yes"
Prompt foldername, "Enter a folder name or keep the filename as folder name"
String helpstring
helpstring = "1- Select folder name\r 2- Make or not binding energy wave \r 3- Make or not transmission function wave and correct data"
DoPrompt /HELP=helpstring "I4P :  Load SPECS *.xy", foldername, makewave_BE, makewave_TF
if( V_Flag )
	SetDataFolder saveDF
	Close refNum
	Print "==> Error in the input parameters !"
	Return 0
endif

// Save the data
String /G  	gmakewave_BE = makewave_BE
String /G	gmakewave_TF = makewave_TF

// go back to current datafolder
SetDataFolder saveDF

//Check if DF already exists to prevent overwites.
String ExistingDataFolders
foldername = StringName(foldername,"")
ExistingDataFolders = DataFolderDIR(1)
If(-1 != strsearch(ExistingDataFolders, foldername, 0))
	Close refNum
	Beep
	DoAlert /T="WARNING" 0, "ERROR \r\rData folder already exists !"
	Return 0
EndIf

//Make a new folder to hold the file and save it
NewDataFolder/S $foldername
String SavedDataFolder= GetDataFolder(1) 
	
//Group and region Variables
Variable GroupCounter = 0, RegionCounter =0
String region_list=""
String CountperSec, KEaxis, SpecGroup, SpecGroupString, TransFunc
Variable NumScans, Points, DwellT, ExcEner, KineEner, RetardRatio = -1, PassEn = -1, WorkFunc 
String RegionName, RegionNameString, DateData, Lens, Slits, Comment, PostComment, Mode
Variable Region_Num, i, BE, Counts, TF, StepEn, StartEn

// Read the header
FReadLine refNum, line
FReadLine refNum, line
FReadLine refNum, line	
FReadLine refNum, line
FReadLine refNum, line	
CountperSec = CleanString(line, "#   Counts Per Second:")
FReadLine refNum, line	
KEaxis = CleanString(line, "#   Kinetic Energy Axis:")
FReadLine refNum, line
FReadLine refNum, line	
FReadLine refNum, line
FReadLine refNum, line				
TransFunc = CleanString(line, "#   Transmission Function:")
FReadLine refNum, line	
FReadLine refNum, line
FReadLine refNum, line
FReadLine refNum, line	
// Warn the user
Print "==> X-axis scale KE : ", KEaxis

	
// Loop over the group
Do

	// Go back to root
	SetDataFolder SavedDataFolder
	
	// Initialize the counters and region list
	Region_Num = 0
	RegionCounter = 0
	region_list=""
	
	// Read group header	
	SpecGroupString = CleanString(line, "# Group:")
	SpecGroup = StringName(SpecGroupString,"")
	If(DataFolderExists(SpecGroup)!=0)
		i = 1
		Do
			SpecGroup = StringName(SpecGroup,"_"+num2str(i))
			i += 1
		While(DataFolderExists(SpecGroup)!=0)
	Endif
	//Make a new folder  for a given group
	NewDataFolder/S $SpecGroup
	// Print the final region name
	Print  "-----------------------------------------------------------------------------"
	Print  "==> Spectrum group : ", SpecGroup
	
	// Read lines
	FReadLine refNum, line	
	FReadLine refNum, line
											
	//  Loop over the region a given group
	Do

		//Increment Region Number in case of more unamed regions
		Region_Num +=1 
	
		//Load lots of region infomation into variables
		FReadLine refNum, line
		RegionNameString = CleanString(line, "# Region:")
		RegionName = RegionNameString
		//sscanf line, "%s", RegionName
		FReadLine refNum, line
		DateData = CleanString(line, "# Acquisition Date:")
		FReadLine refNum, line
		FReadLine refNum, line
		FReadLine refNum, line
		Lens = CleanString(line, "# Analyzer Lens:")	
		FReadLine refNum, line
		Slits = CleanString(line, "# Analyzer Slit:")		
		FReadLine refNum, line
		Mode = CleanString(line, "# Scan Mode:")
		Mode = GetAnalyzerModeName(Mode)
		FReadLine refNum, line
		sscanf line, "# Number of Scans: %f", NumScans
		FReadLine refNum, line
		FReadLine refNum, line
		sscanf line, "# Values/Curve: %f", Points
		FReadLine refNum, line
		sscanf line, "# Dwell Time: %f", DwellT
		FReadLine refNum, line
		sscanf line, "# Excitation Energy: %f", ExcEner
		FReadLine refNum, line
		sscanf line, "# Kinetic Energy: %f", KineEner
		FReadLine refNum, line
		sscanf line, "# Pass Energy: %f", PassEn
		if(cmpstr(mode,gFRR)==0)
		FReadLine refNum, line
		sscanf line, "# Retardation Ratio: %f", RetardRatio
		endif
		FReadLine refNum, line
		FReadLine refNum, line
		FReadLine refNum, line
		sscanf line, "# Eff. Workfunction: %f", WorkFunc
		FReadLine refNum, line
		FReadLine refNum, line
		Comment = CleanString(line, "# Comment:")	
		FReadLine refNum, line
		PostComment = CleanString(line, "#")
		If(strlen(PostComment)!=0)
		 Do
		 	Comment = Comment + " / " + PostComment
			FReadLine refNum, line
			PostComment = CleanString(line, "#")		 	
		 While(strlen(PostComment)!=0)
		Endif
		
		FReadLine refNum, line
		FReadLine refNum, line
		FReadLine refNum, line
		FReadLine refNum, line
		FReadLine refNum, line
		FReadLine refNum, line
	
		//Check if region is unamed if it is add region name
		If(stringmatch(RegionName,""))
			RegionName = "Region_" + num2str(Region_Num)
		EndIf
			
		//Check if wavename will excede maxium lentgh and corrects it.
		RegionName = StringName(RegionName,"")

		//Check if region name is the same as another name and changes it.
		If(strsearch(region_list, RegionName, 0) != -1)
			RegionName = StringName(RegionName,"_" + num2str(Region_Num))
		EndIf
		
		//Add name to list to keep trrack of previous names
		region_list = region_list + RegionName + "; "
		
		// Print the final region name
		Print  "==> Region : ", RegionName

		//Make wave to hold region		
		Make /D/O/N=(Points) $RegionName
		Wave /D region = $RegionName 
		// Make a binding energy wave 
		String RegionName_BE = StringName(RegionName,"_BE")
		Make /D/O/N=(Points) $RegionName_BE
		Wave /D region_BE = $RegionName_BE
		// Make a transmission function wave
		String RegionName_TF = StringName(RegionName,"_TF")
		Make /D/O/N=(Points) $RegionName_TF
		Wave /D region_TF = $RegionName_TF		
		 

		//Read data into region wave
		For(i=0;i<points;i+=1)
			FReadLine refNum, line
			// Read energy and counts
			sscanf line, "%e%e%e", BE, Counts, TF
			// In case the TF value is absent
			// Default transmission function supplied by SPECS
			If(V_flag==2)
			  TF = 62.905/((ExcEner-BE)^0.5077)
			Endif
			//Put data into wave
			region_BE[i] = BE
			region[i]       = Counts
			region_TF[i]  = TF
		EndFor

		//Convert data into counts per sec
		If(1!=stringmatch(CountperSec, "yes"))
			Print "==> Warning : internal conversion to counts per second"
			//Divide counts by number of scans
			region/=NumScans
			//Divide counts by dwell to counts per sec
			region/=DwellT
		Endif
		
		// Convert from KE to BE
		If(stringmatch(KEaxis, "yes")==1 && stringmatch(Mode, gFE) !=1)
			Print "==> Warning : internal conversion from kinetic to binding energy"
			region_BE = ExcEner - region_BE
		Endif		
				
		//Reverses backward data 
		StepEn = (region_BE[points-1]-region_BE[0])/(points-1)
		If(StepEn<0)
			Reverse region
			Reverse region_BE
			Reverse region_TF
			StepEn = -StepEn
		Endif
		StartEn = region_BE[0]
		
		// Set wave scaling and add infos
		If(stringmatch(Mode, gFE) !=1 )
			SetScale/P x, StartEn, StepEn, "eV",  region
		Else
			SetScale/P x, StartEn, StepEn, "s",  region
		Endif
		SetScale d 0,0,"cps", region
		SetScale/P x, 0, 1, "",  region_BE  
		SetScale d 0,0,"", region_BE 
		Note region_BE, " "	 	 	
		Note region_BE, "# Filename : "                                          + S_fileName
		If(stringmatch(Mode, gFE) !=1 )
			Note region_BE, "# Binding energy scale (eV) of region : "    + RegionName 
		Else
			Note region_BE, "# Time scale (s) of region : "    + RegionName 
		Endif
		
		//Add Extra info to wave notes
		Note region, " "
		Note region, "# Instrument :  Phoibos" 	
		Note region, "# Filename : "             + S_fileName
		Note region, "# Group : "                 + SpecGroupString
		Note region, "# Region : "                + RegionNameString
		Note region, "# Acquisition date : "  + DateData
		If(stringmatch(Mode, gFE) !=1 )
			Note region, "# X-scale : binding energy  or energy loss (eV) "
		Else
			Note region, "# X-scale : time (s) "
		Endif
		Note region, "# Y-scale : count rate (cps) "
		Note region, "# Dwell time (sec) : " + num2str(DwellT) 
		Note region, "# Number of scans : " + num2str(NumScans)	
		Note region, "# Number of points : " + num2str(points)
		Note region, "# Start value (eV) : "   + num2str(region_BE[0])
		Note region, "# End value (eV) : "    + num2str(region_BE[points-1])
		Note region, "# Step (eV) : "            + num2str(StepEn)	
		Note region, "# Excitation energy (eV) : "+ num2str(ExcEner)
		Note region, "# Analyzer mode : "+ Mode
		strswitch(Mode)
			case gFAT:
			Note region, "# Pass energy (eV) or Retardation ratio : "  + num2str(PassEn)
			break			
			case gFE:
			Note region, "# Kinetic Energy : "  + num2str(KineEner)
			Note region, "# Pass energy (eV) or Retardation ratio : "  + num2str(PassEn)
			break		
			case gFRR:
			Note region, "# Pass energy (eV) or Retardation ratio : "  + num2str(RetardRatio)
			break
		endswitch
		Note region, "# Analyzer lens : "     + Lens
		Note region, "# Analyzer slit :  "       + Slits		
		Note region, "# Eff. Workfunction (eV) : "  + num2str(WorkFunc)
		Note region, "# Comment : "             +Comment
				
		//Read the last lines of the region
		FReadLine refNum, line
		FReadLine refNum, line
		
		// Remove the unwanted binding energy wave
		If(cmpstr(makewave_BE,"no")==0)
			KillWaves /Z region_BE
		Endif
		
		// Remove the unwanted transmission function wave
		If(cmpstr(makewave_TF,"no")==0)
			KillWaves /Z region_TF
		Else
			// Notes on transmission function
			SetScale/P x, StartEn, StepEn, "eV",  region_TF
			SetScale d 0,0,"Arb. units", region_TF
			Note region_TF, " "
			Note region_TF, "# Filename : "                                    + S_fileName
			Note region_TF, "# Transmission function of region : "    + regionname
			Note region_TF, "# Excitation energy (eV) : "+ num2str(ExcEner)
			//  Create the corrected data 
			String RegionName_TFC = StringName(RegionName,"_TFC")
			Make /D/O/N=(Points) $RegionName_TFC
			Wave /D region_TFC = $RegionName_TFC
			Duplicate /O region region_TFC
			region_TFC = region/region_TF
			Note region_TFC, " "
			Note region_TFC, "# Region : corrected by transmission function"
		Endif			

		 // Count up number of regions imported
		RegionCounter+=1
				
	//Check if group contains any more regions
	While(1==stringmatch(line[0,10], "# values in"))

	//Add info to history
	Print "==> Number of imported regions : ", RegionCounter	
	
	// Count number of groups imported
	GroupCounter+=1

//Check if file contains more groups
While(1==stringmatch(line[0,6], "# Group"))

//Add info to history
Print " "
Print "==> Number of imported groups :  ", GroupCounter


// Close the file
Close refNum

// Go back to initial datafolder
//SetDataFolder saveDF

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function/S CleanString(line, str)

String line, str
line = ReplaceString(str,line,"")
line = ReplaceString("\r",line,"")
line = ReplaceString("\"",line," ")
line = ReplaceString("'",line," ")
line = RemoveLeadingWhitespace(line)
line = RemoveEndingWhitespace(line)
//line = line[0,strlen(line)-1]
return line

End 

