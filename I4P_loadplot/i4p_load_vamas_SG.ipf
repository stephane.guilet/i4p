// Module :  load_vamas
// Author : Adapted from internet by Remi Lazzari; Dec. 2018; lazzari@insp.jussieu.fr
// Author : modified for CasaXPS VMS files by Stephane Guilet; guilet@insp.jussieu.fr
// Date : November, 2020
// Usage : reading Vamas format file exported by Omicron EIS and SpecsLabProdigy and CasaXPS(?)



#pragma rtGlobals = 3			// use modern global access method and strict wave access.
#pragma ModuleName = load_vamas_sg

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function Load_VAMAS_SG()		
	
	//Opens file window and opens file read only
	Variable refNum = 0
	Variable lineNumber = 0
	String filefilters
	filefilters = "Vamas Files (*.vms):.vms;"  +  "All Files (*.*):.*;"
	Open /R /F=filefilters refNum
	FStatus refNum 
	If(stringmatch(S_filename, ""))
		Print "==> Error: File was not opened sucessfully !"
		Return 0
	EndIf

	// Put file name in history
	Print "==> Load Vamas data file: ", S_fileName
	
	//Sets S_fileName to just the file name for inclusing in the wave notes
	String filename
	sscanf S_fileName,"%[^.]", filename
		
	//Check that the first line matches that of Vamas text file
	String currentline
	currentline = NextLine (refNum, lineNumber)
	If(!stringmatch(currentline, "VAMAS Surface Chemical Analysis Standard Data Transfer Format 1988 May 4"))
		Close refNum
		Beep 
		Doalert /T="WARNING" 0, "ERROR \r\rNot a Vamas text file"
		Return 0
	Endif
	
	// The datafolder of variables
	String saveDF = GetDataFolder(1)
	NewDataFolder /O/S root:I4P
	NewDataFolder /O/S :Variables
	 
	//Get folder name
	String foldername = filename
	Prompt foldername, "Enter a folder name or keep the filename as folder name"
	String makewave_BE  = StrVarOrDefault("gmakewave_BE","no") 
	Prompt makewave_BE, "Make binding energy wave", popup "no;yes"
	String makewave_TF  = StrVarOrDefault("gmakewave_TF","no")
	Prompt makewave_TF, "Load transmission function wave", popup "no;yes"
	String helpstring
	helpstring = "1- Select folder name\r 2- Make or not binding energy wave \r 3- Make or not transmission function wave and correct data"
	DoPrompt /HELP=helpstring "I4P : Load VAMAS *.vms", foldername, makewave_BE, makewave_TF
	if( V_Flag )
		SetDataFolder saveDF
		Close refNum
		Print "==> Error in the input parameters !"
		Return 0
	endif
	
	// Save the data
	String /G  	gmakewave_BE = makewave_BE
	String /G	gmakewave_TF = makewave_TF
	
	// go back to current datafolder
	SetDataFolder saveDF
	
	//Check if DF already exists to prevent overwites.
	String ExistingDataFolders
	foldername = StringName(foldername,"")
	ExistingDataFolders = DataFolderDIR(1)
	If(strsearch(ExistingDataFolders, foldername, 0) != -1)
		Close refNum
		Beep
		DoAlert /T="WARNING" 0, "ERROR \r\rData folder already exists !"
		Return 0
	EndIf
	
	//Make a new folder to hold the file
	NewDataFolder/S root:$foldername	
	
	// Extracted data
	String usercomment = "", scanmode = "", datetext = ""
	String technique="", source = "", EAmode = ""
	String Tspecies = "", Tlevel = ""
	String Xlabel = "", Xunit = ""
	Variable hv = 0, Epass = 0, workfkt = 0, dwell = 0, scans = 0
	Variable Xstart = 0, Xstep = 1, points = 1
	String regionname="", region_list=""
	String instrument=""
	String Ylabel = "", Ylabel2 =""
	String experimentcomment=""
	Variable NYaxis
	Variable Casa = 0					// to check if it is a CasaXPS VMS File genrated
	
	Variable gencnt = 0, gennum = 0, datablocks = 0, sectioncounter = 0, value = 0
	
	
//	//----------------------------------------------------------------------------------------------------------------------
//	// beginning of file header
//	//----------------------------------------------------------------------------------------------------------------------
	currentline = NextLine (refNum, lineNumber)					// institution
	
	instrument = NextLine (refNum, lineNumber)					// instrument model
	
	if(strlen(instrument)==0)
	 	instrument = "EA 125"
	endif
	currentline = NextLine (refNum, lineNumber)					// operator
	currentline = NextLine (refNum, lineNumber)					// experiment descritption
	currentline = NextLine (refNum, lineNumber)					// number of comments
	gennum = str2num(currentline)					// extract the number
	for (gencnt = 0; gencnt < gennum; gencnt += 1)
		currentline = NextLine (refNum, lineNumber)				// get comments
		experimentcomment +=  "# ---> " + currentline	+ "\r" // save the comments
	endfor
	if(cmpstr(instrument,"EA 125")==0 && cmpstr(experimentcomment,"Experiment Type: ISS")==0)  // Get from user the impact energy in ISS or EELS
		Variable hv_loss = 1000
		Prompt hv_loss, "Excitation energy (eV) for ISS/EELS"
		helpstring = "Give the excitation energy to calculate energy loss"
		DoPrompt /HELP=helpstring  "Excitation energy (eV) for ISS/EELS ?", hv_loss
	endif	
	currentline = NextLine (refNum, lineNumber)					// experiement mode (MAP, MAPDP, NORM, or SDP)
	scanmode = NextLine (refNum, lineNumber)					// scan mode (should be REGULAR) 
	currentline = NextLine (refNum, lineNumber)					// number of spectra
	datablocks = str2num(currentline)				// extract the number
	// number of analysis positions, discrete x, and y coords only specified for experiment modes MAP and MAPDP
	currentline = NextLine (refNum, lineNumber)					// experimental variables (=0)
	gennum = str2num(currentline)					// extract the number
	for (gencnt = 0; gencnt < gennum; gencnt += 1)
		currentline = NextLine (refNum, lineNumber)				// variable description
		currentline = NextLine (refNum, lineNumber)				// variable unit
	endfor
	currentline = NextLine (refNum, lineNumber)					// relic line (=0)
	gennum = str2num(currentline)					// extract the number
	if (gennum)
		Close refNum							// Quit the procedure with an error message
		beep
		doalert /T="WARNING" 0, "ERROR \r\rUnsupported old file version !"
		return 0
	endif
	currentline = NextLine (refNum, lineNumber)		// # of manually entered items (=0)
	gennum = str2num(currentline)		// extract the number
	for (gencnt = 0; gencnt < gennum; gencnt += 1)
		currentline = NextLine (refNum, lineNumber)	// read item
	endfor
	currentline = NextLine (refNum, lineNumber)		// # of future experiment upgrade entries (=0)
	gennum = str2num(currentline)		// extract the number
	currentline = NextLine (refNum, lineNumber)		// \r# of future upgrade block entries (=0)
	for (gencnt = 0; gencnt < gennum; gencnt += 1)
		currentline = NextLine (refNum, lineNumber)	// read item
	endfor
	currentline = NextLine (refNum, lineNumber)		// number of data blocks (= # of spectra)
	gennum = str2num(currentline)		// extract the number
	if (gennum != datablocks)
		//
		beep
		Print "Number of Samples = ", datablocks
		Print "Number of Blocks = ", gennum
		
		Print "More BLocks than Spectra = We will extract all the first blocks "
		if (gennum < datablocks)
			Close refNum
			DoAlert /T="ALERT" 0, "ERROR \r\rLess DataBLocks than Number of spectra ! \r\r STOPPING Procedure"
			return -1
		endif
		
		Variable samples = datablocks // Keep in memory number of samples
		datablocks = gennum  // Extraction of all the blocks (all the spectra) and try to tag them with the name of the sample
		
	endif
	//----------------------------------------------------------------------------------------------------------------------
	// end of file header
	//----------------------------------------------------------------------------------------------------------------------
	
	String Blockname =""
	String SampleName = ""
	Variable SampleNum = 0

	for (sectioncounter = 0; sectioncounter < datablocks; sectioncounter +=1)
		//----------------------------------------------------------------------------------------------------------------------
		// beginning of block header
		//----------------------------------------------------------------------------------------------------------------------
		currentline = NextLine (refNum, lineNumber, ReplaceChar=1)				// block identifier and option to exclude special char in the string
		BlockName	=	currentline
		currentline = NextLine (refNum, lineNumber, ReplaceChar=1)				// sample identifier and option to exclude special char in the string
		if (cmpstr (currentline, SampleName) != 0)
			SampleName = StringName(currentLine,"")
			SampleNum += 1
			
			Printf "Block N�%g= %s \rSample N�%g Name = %s \r",sectioncounter,BlockName, SampleNum, SampleName
			
			SetDataFolder root:$foldername														
			NewDataFolder/S $SampleName															//Create a subfolder for each sample
		endif
			
		
		datetext = ""
		currentline = NextLine (refNum, lineNumber)						// year
		sprintf datetext, datetext+"%.4d-", str2num(currentline)	// write year
		currentline = NextLine (refNum, lineNumber)						// month
		sprintf datetext, datetext+"%.2d-", str2num(currentline)	// write month
		currentline = NextLine (refNum, lineNumber)						// day
		sprintf datetext, datetext+"%.2d ", str2num(currentline)	// write day
		
		Variable RawTime = 0
		currentline = NextLine (refNum, lineNumber)						// hour
		RawTime += str2num(currentline)*3600									// write hour
		currentline = NextLine (refNum, lineNumber)						// minute
		RawTime += str2num(currentline)*60									// write minute
		currentline = NextLine (refNum, lineNumber)						// seconds
		RawTime += str2num(currentline)											// write seconds
		currentline = NextLine (refNum, lineNumber)						// GMT offset
		datetext += Secs2Time(RawTime,3)										// convert to string
		
		currentline = NextLine (refNum, lineNumber)						// comments again
		gennum = str2num(currentline)											// extract the number of comment line
		regionname = ""
		usercomment = ""
		for (gencnt = 0; gencnt < gennum; gencnt += 1) 
			currentline = NextLine (refNum, lineNumber)					// get comments line
			//Print "Comments N�"+ num2str(gencnt) +" : ",currentline  ///////////////: DEBUG
			if (StringMatch(currentline, "Casa*")==1)						//check if CasaXPS Block
				Casa = 1																	// Switch Casa to 1 if CasaXPS Block
				Print " This is a CasaXPS Block ! : ",Casa				// debug
			endif
			if(gennum==1 && cmpstr(instrument,"EA 125")==0)		 	// Case of Vamas exportation from OMICRON EIS 
			 	regionname = currentline
			else
				usercomment += "# ---> " + currentline + "\r"	 
			endif
		endfor
		technique = NextLine (refNum, lineNumber)					// technique should be (XPS)
		
		if (Casa == 1)
			currentline = NextLine (refNum, lineNumber)				// For CasaXPS VMS files there is a dummy line for  x-axis, y-axis and experimental  variable
		endif
		
		source = NextLine (refNum, lineNumber)					// light source
		currentline = NextLine (refNum, lineNumber)					// light source energy
		hv = str2num(currentline)
		if(cmpstr(instrument,"EA 125")==0 && hv>10000) 	 // Get from user the impact energy in ISS or EELS
			hv =  hv_loss
		endif	
		currentline = NextLine (refNum, lineNumber)					// light source strength
		currentline = NextLine (refNum, lineNumber)					// light source x width
		currentline = NextLine (refNum, lineNumber)					// light source y width
		currentline = NextLine (refNum, lineNumber)					// light source incident angle (polar)
		currentline = NextLine (refNum, lineNumber)					// light source incident angle (azimuth)
		
		EAmode = NextLine (refNum, lineNumber)							// analyzer mode (FAT or FRR)
		EAmode = GetAnalyzerModeName(EAmode)
	
		currentline = NextLine (refNum, lineNumber)					// analyzer pass energy or retardation
		Epass	= str2num(currentline)
		currentline = NextLine (refNum, lineNumber)					// analyzer magnification
		currentline = NextLine (refNum, lineNumber)					// analyzer work function
		workfkt	= str2num(currentline)
		currentline = NextLine (refNum, lineNumber)					// targetBias
		currentline = NextLine (refNum, lineNumber)					// analysis x width
		currentline = NextLine (refNum, lineNumber)					// analysis y width
		currentline = NextLine (refNum, lineNumber)					// analyzer axis take off (polar)
		currentline = NextLine (refNum, lineNumber)					// analyzer axis take off (azimuth)
		
		Tspecies = NextLine (refNum, lineNumber)						// sample species
		if(cmpstr(instrument,"Phoibos")==0 || Casa==1)				// Case of Vamas exportation from SpecsLab Prodigy
			 regionname =  Tspecies
		endif
		
		Tlevel = NextLine (refNum, lineNumber)							// species level
		
		currentline = NextLine (refNum, lineNumber)					// charge of detected particle (-1 for electron)
		if (stringmatch(scanmode, "REGULAR"))	
			Xlabel = NextLine (refNum, lineNumber)						// x axis label
		
			Xunit = NextLine (refNum, lineNumber)						// x axis unit
			
			currentline = NextLine (refNum, lineNumber)				// x axis start
			Xstart	= str2num(currentline)
			
			currentline = NextLine (refNum, lineNumber)				// x axis increment
			Xstep	= str2num(currentline)
		endif
		currentline = NextLine (refNum, lineNumber)					// corresponding y axes (should be 1 or 2)
		NYaxis = str2num(currentline)	
		Ylabel = NextLine (refNum, lineNumber)							// y axis label
		currentline = NextLine (refNum, lineNumber)					// y axis unit
		
		if((cmpstr(instrument,"Phoibos")==0 || Casa==1) && NYaxis==2)  // special for Vamas exportation from SpecsLab Prodigy 
			Ylabel2 = NextLine (refNum, lineNumber)	
			currentline = NextLine (refNum, lineNumber)		
		endif
		
		currentline = NextLine (refNum, lineNumber)					// signal mode
		currentline = NextLine (refNum, lineNumber)					// dwell
		dwell	= str2num(currentline)
		currentline = NextLine (refNum, lineNumber)					// # of scans
		Print "# of scans : ", currentline
		scans	= str2num(currentline)
		
		currentline = NextLine (refNum, lineNumber)					// signal time correction
		currentline = NextLine (refNum, lineNumber)					// sample angle (tilt, polar)
		currentline = NextLine (refNum, lineNumber)					// sample angle (tilt, azimuth)
		currentline = NextLine (refNum, lineNumber)					// sample angle (rotation)

		currentline = NextLine (refNum, lineNumber)					// Number of additional parameters (can be != 0)
		gennum = 3*str2num(currentline)										// Extract the number of parameters
		for (gencnt = 0; gencnt < gennum; gencnt += 1) 
			currentline = NextLine (refNum, lineNumber)				// get parameters line and three lines per parameter
		endfor
	
		currentline = NextLine (refNum, lineNumber)					// Number of points in the spetra (# of values)
		points = str2num(currentline)/NYaxis 							// Divide by the number of axis
		currentline = NextLine (refNum, lineNumber)					// min y
		currentline = NextLine (refNum, lineNumber)					// max y
		if(cmpstr(instrument,"Phoibos")==0 || Casa==1) 			// Case of Vamas exportation from SpecsLab Prodigy and for CASAXPS VMS files
			currentline = NextLine (refNum, lineNumber)
			currentline = NextLine (refNum, lineNumber)
		endif
		
		//----------------------------------------------------------------------------------------------------------------------
		// end of block header
		//----------------------------------------------------------------------------------------------------------------------
	
		Make/O/D/N=(points) RegionLoaded				// create wave for data loading
		Make/O/D/N=(points) RegionLoaded2				// create wave for transmission function loading
		RegionLoaded2 = 1
		RegionLoaded = 1
		
		// DEBUG (can be removed)
		Print "Specs : ", scans, dwell, instrument, technique, hv, points, Epass, Tspecies, regionname, Ylabel2, Ylabel, Xlabel	
		FStatus refNum
		
		//print "debug @"+ num2str(V_filePos) + "= ", currentline   //to print the position in the file curently read
		
		//end DEBUG
		
		for (gencnt = 0; gencnt < points; gencnt += 1)		// extract data
			currentline = NextLine (refNum, lineNumber)
			value = str2num(currentline)
			RegionLoaded[gencnt] = value 
			//Printf "Point N�%g : %g\r",gencnt, value
			if((cmpstr(instrument,"Phoibos")==0 || Casa==1) && NYaxis!=1)  // Case of Vamas exportation from SpecsLab Prodigy and CasaXPS VMS
			  currentline = NextLine (refNum, lineNumber)
			  RegionLoaded2[gencnt] = str2num(currentline) 
			endif
		endfor
	
		// Normalize the data, For CasaXPS VMS files, the label is "Intensity"
		RegionLoaded = RegionLoaded/scans
		if(cmpstr(Ylabel,"counts")==0 || cmpstr(Ylabel,"count rate")==0 || cmpstr(Ylabel, "Intensity")==0)   // "count rate" option is for Omicron EIS since the exportation in vms seems wrong
			
			RegionLoaded = RegionLoaded/dwell
			Print "==> Warning : internal conversion to counts per second"
			
		endif

		//Reverse RegionLoaded
		If(cmpstr(Xlabel,"Kinetic Energy")==0)			// Revert to binding energy scale
			Xstart = hv - Xstart - (points-1)*Xstep
			Reverse RegionLoaded
			Reverse RegionLoaded2
		Endif
		
		SetScale/P x Xstart, Xstep,Xunit, RegionLoaded
		SetScale d 0,0,"cps", RegionLoaded
		
		// append experiment note
		Note RegionLoaded, " "
		Note RegionLoaded, "Sample Name : " + SampleName
		Note RegionLoaded, "Spectra Label : " + BlockName
		Note RegionLoaded, "# Instrument : "                +  instrument
		//Note RegionLoaded, "# Folder name : "           + foldername
		Note RegionLoaded, "# Filename : "                  + S_fileName
		Note RegionLoaded, "# Region : "                     + regionname
		Note RegionLoaded, "# Acquisition date : "	    + datetext
		If(cmpstr(Xlabel,"Index")!=0)
			Note RegionLoaded, "# X-scale : binding energy or energy loss (eV) "
		Else
			Note RegionLoaded, "# X-scale : time (s) "
		Endif
		Note RegionLoaded, "# Y-scale : count rate (cps) "
		Note RegionLoaded, "# Dwell time (sec) : "       + num2str(dwell)
		Note RegionLoaded, "# Number of scans : "      + num2str(scans)
		Note RegionLoaded, "# Number of points : "      + num2str(points)
		Note RegionLoaded, "# Start value (eV) : "        + num2str(Xstart)
		Note RegionLoaded, "# End value (eV) : "	    + num2str(Xstart+Xstep*(points-1)) 
		Note RegionLoaded, "# Step (eV) : "		    + num2str(Xstep)
		Note RegionLoaded, "# Technique : "	  	    + technique
		Note RegionLoaded, "# Excitation source : "	    + source
		Note RegionLoaded, "# Excitation energy (eV) : "  + num2str(hv)
		Note RegionLoaded, "# Analyzer mode : "	    + EAmode
		Note RegionLoaded, "# Pass energy (eV) or Retardation ratio : " + num2str(Epass)
		Note RegionLoaded, "# Eff. workfunction (eV) : " + num2str(workfkt)
		if (strlen(usercomment) > 0)
			Note RegionLoaded, "# Comments : \r"	     + usercomment
		endif
		if (strlen(experimentcomment) > 0)
			Note RegionLoaded, "# General comments : \r"	     + experimentcomment
		endif		
		//Note RegionLoaded, "# Species : "		    + Tspecies
		//Note RegionLoaded, "# Energy level : "	          + Tlevel
	
		//Check if region is unamed if it is add region name
		If(StringMatch(regionname,""))
			regionname = "Region_" + num2str(sectioncounter)
		EndIf
	
		//Check if wavename will excede maximum lentgh and corrects it.
		RegionName = StringName(regionname,"")
	
		//Check if region name is the same as another name and changes it.
		If(strsearch(region_list, regionname, 0) != -1)
			RegionName = StringName(RegionName,"_" + num2str(sectioncounter))
		EndIf
	
		//Add name to list to keep track of previous names
		region_list = region_list + regionname + "; "
		
		//Add the first 10 characters of the sample name to region name
		if (strlen(samplename) > 10) 
			regionname = samplename[0,9] + regionname
		else
			regionname = samplename[0,9] + regionname
		endif
			
		regionname = StringName (regionname,"")		
		
		// Print the final region name
		Print  "==> Region : ", regionname
		
		// Make the store wave
		Duplicate/O RegionLoaded, $regionname		
		

	
		// Make a binding energy wave with the same information as the wave itself
		If(cmpstr(makewave_BE,"yes")==0)
			String regionname_BE = StringName(regionname,"_BE")
			Make/O/D/N=(points)  $regionname_BE
			Wave /D region_BE =  $regionname_BE
			region_BE =  Xstart + p* Xstep
			SetScale/P x, 0, 1, "",  region_BE
			SetScale d 0,0, "", region_BE
			Note region_BE, " "
			Note region_BE, "# Filename : " + S_fileName
			Note region_BE, "# Binding energy scale (eV) of region : " + regionname
		Endif  
	
		// Make a transmission function wave
		If(cmpstr(makewave_TF,"yes")==0)
			String regionname_TF = StringName(regionname,"_TF")
			Make/O/D/N=(points)  $regionname_TF
			Wave /D region_TF = $regionname_TF 		
			Strswitch(instrument)	
			Case "Phoibos":	
				 wavestats /q RegionLoaded2
				 If(V_max==V_min && V_avg==1)
					Strswitch(EAmode)
					Case gFAT:
						region_TF =  62.905/((hv - Xstart - p* Xstep)^0.5077)
					Break
					Case gFRR:
						region_TF = (hv - Xstart - p* Xstep)/(hv/2.)
					Break
					Endswitch
				 Else
				 	region_TF = RegionLoaded2
				 Endif
			Break					
			Case "EA 125":
				Strswitch(EAmode)
				Case gFAT:
					region_TF = (hv/2.)/(hv - Xstart - p* Xstep)
				Break
				Case gFRR:
					region_TF = (hv - Xstart - p* Xstep)/(hv/2.)
				Break
				Endswitch
			Break
			Default:	
				 region_TF = 1.	
			Endswitch
			SetScale/P x, Xstart, Xstep, "eV",  region_TF
			SetScale d 0,0,"Arb. units", region_TF
			Note region_TF, " "
			Note region_TF, "# Filename : "                                    + S_fileName
			Note region_TF, "# Transmission function of region : "    + regionname
			Note region_TF, "# Excitation energy (eV) : "  + num2str(hv)
		Endif   
	
		// Make a transmission function corrected wave
		If(cmpstr(makewave_TF,"yes")==0)
			String regionname_TFC = StringName(regionname,"_TFC")
			Duplicate/O RegionLoaded, $regionname_TFC
			Wave /D region_TFC =$regionname_TFC 	
			region_TFC = RegionLoaded/region_TF
			Note region_TFC, " "
			Note region_TFC, "# Region : corrected by transmission function"
		Endif
		
		// Clean waves (Do it for each block/sample to avoid it in the directory)
		KillWaves RegionLoaded, RegionLoaded2
		
		print "******************End of Block  ***********************"
			
	endfor
	
	
	
	
	//Add info to history
	Print "==> Number of regions imported : ", datablocks
	
	// Close the file
	Close refNum	
	
	// Return to the 1st level (name of the file)
	SetDataFolder root:$foldername

End