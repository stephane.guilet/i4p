# I4P

I4P is an XPS data manipulation and calculation procedures stack for IGOR procedures

https://www.wavemetrics.com/products/igorpro

Igor Pro is an interactive software environment for experimentation with scientific and engineering data and for the production of publication-quality graphs and page layouts.

I4P tries to concatenate all the actions you need to get the accurate physical interpretation of your experimental measurments. It is dedicated to Photoemission spectroscopy.