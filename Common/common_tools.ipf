// Module :commontools
// Author : Remi Lazzari; lazzari@insp.jussieu.fr
// Date : December 28th, 2018

#pragma rtGlobals = 3		
#pragma ModuleName = commontools

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : get the windows title of a plotted wave
Function/S ConstructTitle(wName)
	
	string wName		// The name of the window.
	string wTitle
		
	GetWindow $(wName) title
	if (strlen(S_value) <= 0)
		wTitle = wName + ":"
	else
		wTitle = S_value
	endif
	
	return wTitle

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : set rainbow color for all graphs in a plot
Function Setcolor()

	String trl=tracenamelist("",";",1), item
	Variable items=itemsinlist(trl), i
	If(items==1)
		Return -1
	Endif
	Colortab2wave Rainbow256
	Wave/i/u M_colors
	Variable Ncolor= DimSize(M_colors,0)
	Variable ink=(Ncolor-1)/(items-1)
	For(i=0;i<items;i+=1)
		item=stringfromlist(i,trl)
		ModifyGraph rgb($item)=(M_colors[i*ink][0],M_colors[i*ink][1],M_colors[i*ink][2])
	Endfor
	Killwaves/z M_colors

Return 0

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : string suitable in length for a folder or wave name 
Function /S StringName(stringinput,stringend)

	String stringinput, stringend
	Variable maxstrlength = 30
	String stringtmp
	
	stringtmp = stringinput
	If(strlen(stringtmp) + strlen(stringend) > maxstrlength)
		stringtmp = stringtmp[0,maxstrlength - strlen(stringend) -3]
		stringtmp = stringtmp + "..." + stringend
	Else
		stringtmp = stringtmp + stringend
	EndIf
	
	Return stringtmp

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : string suitable in length for a wave name keeping the added extension while adding a new one
Function /S StringName_Ext(stringinput,stringend)

	String stringinput, stringend
	Variable maxstrlength = 30
	String stringtmp = ""
	String ext = "_ABS;_BG;_BGS;_BGS_BE;_BL;_BLS;_CON;_cp;_CUT;_CUT_BE;_GL;_GLS;_ISS;_MS;_SA;_SAD;_SAS;_SUB;_TF;_TFC"
	String exttmp = ""
	Variable i, k
	
	stringtmp = stringinput
	If(strlen(stringinput) + strlen(stringend) < maxstrlength)
			stringtmp = stringtmp +  stringend
	Else
		For(i=strlen(stringinput)-1;i>0;i-=1)
			For(k=0;k<ItemsInList(ext);k+=1)
				If(StringMatch(stringtmp[i,strlen(stringtmp)-1], StringFromList(k,ext))==1)
					exttmp = StringFromList(k,ext) + exttmp
					stringtmp = stringtmp[0,strlen(stringtmp)-1-strlen(StringFromList(k,ext))]
				Endif
			Endfor
		Endfor
		stringtmp = stringtmp[0,maxstrlength-strlen(exttmp)-strlen(stringend)-3] + "..." + exttmp + stringend	
	EndIf 
	
	Return stringtmp

End 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : swap two values
Function  Swap(x1, x2)

	Variable &x1, &x2
	Variable tmp
	
	tmp = x1
	x1	= x2
	x2	= tmp
	Return 0
	
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : get the color of a trace on a graph
Function GetTraceColor(graph,trace,red,green,blue)
    
String graph, trace
Variable &red,&green,&blue

String info = TraceInfo(graph,trace,0)
String color=StringByKey("rgb(x)",info,"=")
sscanf color,"(%d,%d,%d)",red,green,blue
Return 0
    
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : print ASCII character
Function PrintASCIITabel()

	Variable kk
	Variable np = 1000
	
	For(kk=0;kk<np;kk+=1)
		print "k = ", kk, "ASCII = ", num2char(kk)
	Endfor
	Return 0

End 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Find a given tag value in the wave comment
Function /S WaveNote2Value(wavein,notetag)

	wave /d wavein
	string notetag
	string wavenote
	string notevalue
	
	wavenote = note(wavein)
	if(strsearch(wavenote,notetag,0)!=-1)
		notevalue = StringByKey("# "+notetag+" ",wavenote,":","\r")
		notevalue = ReplaceString(" ",notevalue,"")
	else
		notevalue = ""
	endif

return notevalue

End

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : label all the curves in the grah with a given value from wave info
Function TagTraceonGraphFromNote(notetag)

	string notetag
	string listoftrace= TraceNamelist("",";", 1) 
	
	variable kk
	string tracename, tracenotestring
	string legendstring = ""
	for (kk=0; kk<itemsinlist(listoftrace); kk+=1)
		tracename = stringfromlist(kk, listoftrace, ";")
		wave /d trace2wave = tracenametowaveref("",tracename)
		tracenotestring = WaveNote2Value(trace2wave,notetag)
		legendstring += "\\s(" + tracename + ") " + tracenotestring +"\r"
	endfor
	
	Legend/C/N=text0/J legendstring

	Return 0
End 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Get the characteristics of a peak
Function peakparameters(xw,ywi,areaP,maxP,x0,fwhm)

	wave /d xw, ywi
	variable &areaP, &maxP, &x0, &fwhm
	variable imax, vmax, npt, ip, im

	///-------------for >0 and <0 peak
	duplicate /O ywi yw
	yw = abs(ywi)
	///-------------area
	areaP = areaxy(xw,ywi)
	///-------------in case of
	if(numtype(areaP)!=0)
		areaP = nan
		maxP = nan
		x0 = nan
		fwhm = nan
		killwaves /z yw
		return 0
	endif
	//-------------statistics
	wavestats /q/m=1  yw
	imax = V_maxRowLoc
	vmax= V_max
	npt	 = V_npnts
	///-------------maximum
	maxP = ywi[imax]
	///-------------position
	x0 	= xw[imax]
	///-------------fwhm
	if(imax==0 || imax==npt-1)
		fwhm = NaN
		return 0
	endif
	
	ip= imax
	do
		ip +=1 
	while(yw[ip]>=vmax/2 &&  ip<npt-1)
	
	if(ip!=npt-1)
	 ip = ip -1
	endif
	
	im= imax
	do
		im -=1
	while(yw[im]>=vmax/2 && im>0)
	
	if(im!=0)
		im = im + 1
	endif
	
	fwhm = abs(xw[ip] - xw[im])
	///-------------clean
	killwaves /z yw
	
return 0

End

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Get a peak position, maximum and FWHM from a parabola fit at maximum
Function PeakParametersFromParabolaFit(xw,ywi,dx,x0,maxP,fwhm)

	wave /d xw, ywi
	variable dx
	variable &x0, &maxP, &fwhm
	variable imax, npt, xmax, dxw
	variable dn, nmin, nmax
	variable ip, im, xp, xm
	
	///-------------for >0 and <0 peak
	duplicate /o ywi yw
	yw = abs(ywi)
	//-------------statistics
	wavestats /q/m=1  yw
	imax   = V_maxRowLoc
	npt	   = V_npnts
	xmax  = xw[imax]
	//------------ define the waves around the maximum and fit with a parabola
	dxw = abs(xw[1] - xw[0])
	dn     = floor(dx/dxw) 
	nmin  = max(0,imax-dn)
	nmax = min(npt,imax+dn) 
	make /d/o/n=(3) W_coef
	duplicate /o /r=[nmin,nmax] xw xwcut
	duplicate /o /r=[nmin,nmax] yw ywcut
	curveFit /K={xmax} /M=0 /N=1 /NTHR=0  /ODR=1 /Q=1 /W=2  Poly_XOffset 3,  kwCWave=W_coef,  ywcut   /A=0  /X=xwcut  
	//------------ peak position and maximum
	x0 	   = xmax - W_coef[1]/(2*W_coef[2])
	maxP = W_coef[0] + W_coef[1]*(x0-xmax) + W_coef[2]*(x0-xmax)^2
	///-------------fwhm
	if(imax==0 || imax==npt-1)
		fwhm = NaN
		return 0
	endif
	ip= imax
	do
		ip +=1
	while(yw[ip]>=maxP/2 &&  ip<npt-1)
	if(ip!=npt-1)
		ip = ip -1
		xp =  xw[ip]  + (maxP/2-yw[ip])/(yw[ip+1]-yw[ip])*dxw
	else
		xp =  xw[ip] 
	endif

	im= imax
	do
		im -=1
	while(yw[im]>=maxP/2 && im>0)

	if(im!=0)
		im = im + 1
		xm = xw[im-1] + (maxP/2-yw[im-1])/(yw[im]-yw[im-1])*dxw 	
	else
		xm = xw[im] 
	endif
	
	fwhm = abs(xp-xm)
	///-------------clean
	killwaves /z yw, xwcut, ywcut, W_coef, M_Jacobian, W_sigma, W_fitConstants
	///-------------return the peak position

	return 0

End

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : Remove leading/ ending white space from a string
Function/S RemoveLeadingWhitespace(str)
string str
variable check = 0
if (strlen(str) == 0)
	return ""
endif
do
	check =  cmpstr(str[0]," ")
	if (check==0)
		str= str[1,inf]
	endif  
while (check==0 && strlen(str) >= 0)
return str
End

Function/S RemoveEndingWhitespace(str)
string str
variable check = 0
if (strlen(str) == 0)
	return ""
endif
do
	check = cmpstr(str[strlen(str) - 1]," ")
	if (check==0)
		str = str[0, strlen(str) - 2]
	endif
while (check==0 && strlen(str) >= 0)
return str
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : shift all traces in a graph by a given XOffset
Function ShiftTraces(XShift)

Variable XShift

// List of traces in the top graph
String listoftraces = TraceNamelist("",";", 1) 

// Loop over traces and add an extra Xoffset
Variable ntraces = ItemsInList(listoftraces)
Variable kk
String trace_name, info, offsets
Variable instance = 0
Variable xOffset,yOffset
For (kk=0; kk<ntraces; kk+=1)
  trace_name = StringFromList(kk, listoftraces, ";")
  info= TraceInfo("",trace_name, instance)
  offsets = StringByKey("offset(x)",info,"=",";")
  sscanf offsets, "{%g,%g}",xOffset,yOffset
  xOffset += XShift
  ModifyGraph offset($trace_name)={xOffset,yOffset}
Endfor

End
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage : remove a trace from a graph pointed by cursors A dn B
Function RemoveTraceFromGraph()

// Make sure both cursors are on the same trace
	If(StringMatch(CsrInfo(a),"") || StringMatch(CsrInfo(b),""))
		Beep
		DoAlert /T="WARNING" 0, "ERROR \r\rCursors must be on the top graph !"
		Return 0
	EndIf
	Wave /d wA = CsrWaveRef(A)
	Wave /d wB = CsrWaveRef(B)
	String dfA = GetWavesDataFolder(wA, 2)
	String dfB = GetWavesDataFolder(wB, 2)
	If (CmpStr(dfA, dfB) != 0)
		Beep
		DoAlert /T="WARNING" 0, "ERROR \r\rBoth cursors must be on the same trace !"
		Return 0
	Endif

	// Remove the trace
	RemoveFromGraph $NameofWave(wA)

End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function GetTraceOffset(graphName, traceName, instance, which)
    String graphName    // "" for top graph
    String traceName
    Variable instance   // Usually 0
    Variable which      // 0 for X, 1 for Y
   
    String info= TraceInfo(graphName,traceName, instance)
   
    String offsets = StringByKey("offset(x)",info,"=",";") // "{xOffset,yOffset}"
   
    Variable xOffset,yOffset
    sscanf offsets, "{%g,%g}",xOffset,yOffset
   
    if (which)
        return yOffset
    endif
    return xOffset
End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////:
// function to read a line in the input file and increment +1 in a counter
// This is the next line in a file.
// if ReplaceChar is set to 1 then try to get a string compatible with wavename
//
// author : St�phane Guilet
// Date : November 2020
//
// input : FileBuffer, counter, ReplaceChar (optionnal)
// output : return string, counter in pass as reference (&counter)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function /S NextLine (FileBuffer, counter, [ReplaceChar])
	Variable FileBuffer
	Variable &counter
	Variable ReplaceChar
	String LineString
	
	
	FReadline FileBuffer, LineString
	counter += 1
	//printf "line #%g = %s",counter, LineString   // For debuging printing line read in file
	
	LineString = RemoveEnding(LineString,"\r")  // Remove the "return" @ the end of the line.
	
//	Replace Special character ",:./()'" by "_" for wave name or column or any objects in IGOR
	if (ReplaceChar == 1)
		LineString = ReplaceString(",", LineString, "")
		LineString = ReplaceString(":", LineString, "_")
		LineString = ReplaceString("'", LineString, "_")
		LineString = ReplaceString(".", LineString, "")
		LineString = ReplaceString("/", LineString, "_")
		LineString = ReplaceString("(", LineString, "-")
		LineString = ReplaceString(")", LineString, "-")
		LineString = ReplaceString(" ", LineString, "")
		LineString = ReplaceString("�", LineString, "e")
		LineString = ReplaceString("�", LineString, "a")
		LineString = ReplaceString("�", LineString, "e")
	endif		
	
	return LineString
	
End