// Macro :  I4PFit()
// Author : R�mi LAZZARI (lazzari@insp.jussieu.fr) based on initial work of Francesco BRUNO (bruno@tasc.infm.it) 
// Date : 2019
// Usage : Fit of XPS (see manual)


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Table of contents
// BACKGROUND
// LINESHAPES
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#pragma rtGlobals=3		// Use modern global access method and strict wave access.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////   BACKGROUND
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//===========================================
// Background  
//===========================================
function integral_background(xw,yw,ywb,backgroundtype,S,B,C,D,gap)

variable backgroundtype, S, B, C, D, gap
wave /d xw,yw,ywb
variable q

ywb = 0
 // define here only C as positive to comply with a common defintion of Tougaard formula with 2 and 3 parameters
C = abs(C)  

switch(backgroundtype)
case 0: // None
	return 0
break
case 1:  // Shirley
	if(S==0)
		return 0
	endif
	ywb =  yw
	integrate  /METH=1 ywb /X=xw
	ywb*=abs(S)
break
case 2: // Tougaard : two-parameter cross section
case 6:
	Tougaard_background(0,B,+C,0,gap,xw,yw,ywb)
break
case 3: // Tougaard : cross section normalized to one
case 7:
	B = 2*C
	Tougaard_background(0,B,+C,0,gap,xw,yw,ywb)
break
case 4: // Tougaard : three-parameter cross section
case 8:
	Tougaard_background(0,B,-C,D,gap,xw,yw,ywb)
break
case 5: // Tougaard : three-parameter cross section normalized to one
case 9:
	q = D*(4*C-D)
	if(abs(q)<1e-5)
		q = sign(q)*1e-5
	endif
	if(q>0)
		B  = sqrt(q)
		B /= pi/2- atan((D-2*C)/sqrt(q))
	else
		B  = -2*sqrt(-q)
		B /= ln( (D-2*C-sqrt(-q))/(D-2*C+sqrt(-q)) )
	endif
	// Tougaard_background_inte(B,-C,D,gap,xw,yw,ywb)
	Tougaard_background(0,B,-C,D,gap,xw,yw,ywb)
break
case 10: // ShirleyCS : modified Shirley cross section  
	Tougaard_background(1,B,+C,D,gap,xw,yw,ywb)
break
case 11: // ShirleyCS : modified Shirley cross section normalized to one
	B = pi/(2*sqrt(C)) - 1./(D*C)
	B = 1./B
	Tougaard_background(1,B,+C,D,gap,xw,yw,ywb)
break
endswitch

switch(backgroundtype)
// Combination of a Shirley and a Tougaard background
case 6:
case 7:
case 8:
case 9:
	if(S==0)
		return 0
	endif
	duplicate /o yw ywbs
	integrate  /METH=1 ywbs /X=xw
	ywbs*=abs(S)
	ywb  = ywb + ywbs
	killwaves /z ywbs
break
endswitch

end

//===========================================
//  Tougaard background
//===========================================
function Tougaard_background(kerneltype,B,C,D,gap,xw,yw,ywb)

variable kerneltype, B,C,D,gap
wave /d xw, yw, ywb

//---no background
if(B==0)
	ywb = 0
	return 0
endif

//---loop over background up to a given accuracy
variable threshold = 0.01
variable error = 10*threshold
variable kk=0, kkmax = 20
duplicate /o yw ywi, ywb1, ywb2, ywbd
ywi 	  = yw
ywb1 = 0
ywb2 = 0
ywbd = 0
do
	ywi = yw + ywb1
	Tougaard_background_convol(kerneltype,B,C,D,gap,xw,ywi,ywb2)
	ywbd = abs(ywb2-ywb1)/abs(ywb2)
	wavestats /q/m=1 ywbd
	error  = V_max
	ywb1 = ywb2
	kk+= 1
while(error>threshold && kk<=kkmax)
ywb = ywb2

//---clean
killwaves /z  ywi, ywb1, ywb2, ywbd

end 


//===========================================
//  Tougaard background through convolution 
//===========================================
function Tougaard_background_convol(kerneltype,B,C,D,gap,xw,yw,ywb)

variable kerneltype, B,C,D,gap
wave /d xw, yw, ywb

//---step of data and number of points
variable step = xw[1]-xw[0]
variable Tpnts = 10*(sqrt(abs(C/3)) + Gap)/step  // 10 times the position of the maximum of the loss function
////---definition of the convolution function
make /d/o/n=(2*Tpnts+1) xT, yT
xT = -Tpnts*step + p*step
xT = xT - gap
switch(kerneltype)
case 0: // Tougaard
	yT =  xT / ((C+xT^2)^2 + D*xT^2) * (xT>=-gap) * (xT>=0)
break
case 1: // Modified Shirely
	yT =  (1.-exp(-D*abs(xT))) / (C + xT^2) * (xT>=-gap) * (xT>=0)
break
endswitch

//--------convolution without edge correction
duplicate /o yw yw_conv
convolve /a yT yw_conv
ywb = step * B * yw_conv

//--------clean waves
killwaves /z   yw_conv xT yT 

end


//===========================================
//  Tougaard background through trapezoidal integration
//===========================================
function Tougaard_background_inte(B,C,D,gap,xw,yw,ywb)

variable B,C,D,gap
wave /d xw, yw,ywb
variable np= numpnts(xw)
variable  i, j, int_toug, int1, int2, tx

//---no background
if(B==0)
	ywb = 0
	return 0
endif

// the steps of integration
variable dx = xw[1] - xw[0]

ywb[0] = 0.
for(i=1;i<np;i+=1)
	 // trapezoidal integration
	 int_toug = 0
	 for(j=1;j<=i;j+=1)
	 	 tx = xw[i]-xw[j-1] - gap
	 	 if(tx>=0)
		 	int1 = tx / ( (C + tx^2)^2 + D*tx^2 ) * yw[j-1]
		 else
		 	int1 = 0
		 endif
		 tx = xw[i]  -xw[j] -gap		 
	 	 if(tx>=0)		
		 	int2 = tx /  ( (C + tx^2)^2 + D*tx^2) * yw[j]
		 else
		 	int2 = 0		 
		 endif
		  int_toug += (int1+int2)/2
	 endfor	 
	 ywb[i] = abs(int_toug)
endfor
ywb *= B*dx

end


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////   TRANSMISSION FUNCTION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function transmisssion_function(xw,yw,mode,hnu,nn)

wave /d xw, yw
variable mode, hnu, nn

switch(mode)
case 0:  // NONE
break 
case 1: // CAE or FAT : Tf normalized to half of hnu
	duplicate /o xw tf
	tf = (hnu/2)^nn / ((hnu-xw)^nn)
	yw *= tf
	killwaves /z tf
break
case 2: // CRR or FRR : Tf normalized to half of hnu
	duplicate /o xw tf
	tf =(hnu-xw) / (hnu/2)
	yw *= tf
	killwaves /z tf
break
case 3: // CAE or FAT  : Tf normalized to average
	duplicate /o xw tf
	tf = 1/((hnu-xw)^nn)
	wavestats /q tf
	yw *= tf/V_avg
	killwaves /z tf
break
case 4: // CRR or KRR  : Tf normalized to average
	duplicate /o xw tf
	tf = hnu-xw
	wavestats /q tf
	yw *= tf/V_avg
	killwaves /z tf
break
endswitch 

end


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////   LINESHAPES
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//===========================================
// Lorentzian 
//===========================================
function Func_Lorentz(A,x0,fwhm,x)

variable A, x0, fwhm, x
variable lorentz

lorentz =  A*fwhm/(2*pi)/((x-x0)^2+(fwhm/2)^2)

return lorentz

end

//===========================================
// Gaussian  
//===========================================
function Func_Gaussian(A,x0,fwhm,slope,x)

variable A,x0,fwhm,slope,x
variable sig2
variable gaussian

sig2        = ((fwhm+slope*(x-x0))/gGsig2fwhm)^2
sig2      += 1e-8	// to avoid zero value if I put the fwhm slope
gaussian = A*exp(-(x-x0)^2/(2*sig2))/(sqrt(2*pi*sig2))	

return gaussian

end

//===========================================
// Pseudo-Voigt  
//===========================================
function Func_PseudoVoigt(A,x0,fwhm,mix,x)

variable A,x0,fwhm,mix,x
variable fwhmx, sigx2
variable gaussian, lorentz, pseudovoigt

gaussian	=  Func_Gaussian(A,x0,fwhm,0.,x)
lorentz	=  Func_Lorentz(A,x0,fwhm,x)
pseudovoigt = mix*gaussian + (1.-mix)*lorentz

return pseudovoigt

end

//===========================================
// Lorentz X Gaussian
//===========================================
function Func_LorentzXGaussian(A,x0,fwhmL,fwhmG,x)

variable A,x0,fwhmL,fwhmG,x
variable sig, gam, LXG

if(fwhmL<1e-5)
	fwhmL = 1e-5
endif
if(fwhmG<1e-5)
	fwhmG = 1e-5
endif

sig    = fwhmG/gGsig2fwhm
gam  = fwhmL/2.
LXG = exp(-(x-x0)^2/(2*sig^2)) / ((x-x0)^2 + gam^2)
LXG /= pi*exp(gam^2/(2*sig^2)) * ERFC(gam/(sqrt(2.)*sig)) / gam 
LXG *= A

return LXG

end

//===========================================
// Oscillator
//===========================================
function Func_Oscillator(A,x0,fwhm,x)

variable A,x0,fwhm,x
variable sig, gam, OSC

if(fwhm<1e-5)
	fwhm = 1e-5
endif

OSC   = 1./ ( (x^2-x0^2)^2 + x^2*fwhm^2)
OSC *=  A*2*x0^2*fwhm/pi

return OSC

end

//===========================================
// Doniach-Sunjic
//===========================================
function Func_Doniach_Sunjic(A,x0,fwhm,asymm,x)

variable A,x0,fwhm,asymm,x
variable doniach_sunjic
variable gam,g2, pow, aaa

gam= fwhm/2.
g2	= gam^2
pow	= (1.-asymm)/2.
aaa	= pi*asymm/2.
doniach_sunjic	= 1./(((x-x0)^2+g2)^pow) * cos(aaa-(1.-asymm)*atan((x-x0)/gam))
//-----normalization
doniach_sunjic   *= A*gamma(1.-asymm)/pi

return doniach_sunjic

end

//===========================================
// Generalized Doniach-Sunjic
//===========================================
function Func_Gen_Doniach_Sunjic(A,x0,fwhm,gam,mu,x)

variable A,x0,fwhm,gam,mu,x
variable dx
variable doniach_sunjic

//---- reference peak
dx = 2.*(x-x0)/fwhm
doniach_sunjic	=  (pi/2.+pi/mu+atan(dx)) / ((1.+dx^2)^gam) 
//-----normalization
doniach_sunjic *= A/(fwhm/2.*pi^2*(mu+2.)/(2.*mu))

return doniach_sunjic

end

//===========================================
// Mahan
//===========================================
function Func_Mahan(A,x0,alpha,ksi,x)

variable A,x0,alpha,ksi,x
variable Mahan

Mahan 	= 0
if(x>x0)
	Mahan = exp(-(x-x0)/ksi) * (x-x0)^(alpha-1)
endif
// ----the normalization
Mahan /= ksi^alpha*Gamma(alpha)
Mahan *= A

return Mahan

end


//===========================================
// Post-collision interaction
//===========================================
function Func_PCI(A,x0,fwhm,asymm,x)

variable A,x0,fwhm,asymm,x
variable PCI, dx0

dx0 = asymm*fwhm/2
PCI	  =1./((x+dx0-x0)^2+(fwhm/2)^2) * exp(2*asymm*atan(2*(x+dx0-x0)/fwhm))
//---- normalization
PCI *= A*fwhm/(2*pi)

return PCI

end

//===========================================
// Asymmetric Lorentzian 
//===========================================
function Func_asymLorentz(A,x0,fwhm1,fwhm2,x)

variable A, x0, fwhm1, fwhm2, x
variable asymlorentz

asymlorentz =  (1.+sign(x-x0))*fwhm1^2/((x-x0)^2+(fwhm1/2)^2)  + (1.-sign(x-x0))*fwhm2^2/((x-x0)^2+(fwhm2/2)^2)
asymlorentz *= A/(2*pi*(fwhm1+fwhm2))

return asymlorentz

end

//===========================================
// Asymmetric pseudo-Voigt lineshape 
//===========================================
function Func_asymPseudoVoigt(A,x0,fwhm,mix,asymm,sigmshift,x)

variable A,x0,fwhm,mix,asymm,sigmshift,x
variable fwhmx, sigx2
variable gaussian, lorentz, pseudovoigt

fwhmx  = 2*fwhm/( 1. + exp(-asymm*(x-x0-sigmshift)) )
sigx2    = (fwhmx/gGsig2fwhm)^2
sigx2  += 1e-8	// to avoid zero value
gaussian     = A*exp(-(x-x0)^2/(2*sigx2))/(sqrt(2*pi*sigx2))	
lorentz        =  A*fwhmx/(2*pi)/((x-x0)^2+(fwhmx/2)^2)
pseudovoigt = mix*gaussian + (1.-mix)*lorentz

return pseudovoigt

end

//===========================================
// Fano
//===========================================
function Func_Fano(A,x0,fwhm,asymm,x)

variable A,x0,fwhm,asymm,x
variable qq, fano

if(abs(asymm)<1e-5)
	asymm = sign(asymm)*1e-5
endif
if(abs(asymm)>0.99999)
	asymm = sign(asymm)*0.99999
endif
qq = (1.-asymm)/asymm

fano   = (x-x0 + qq*fwhm/2)^2/((x-x0)^2+(fwhm/2)^2)
fano  /= 1. + (qq*fwhm/2)^2
fano  *=  A*fwhm/(2*pi)

return fano

end


//===========================================
// Plasmon 
//===========================================
function Func_Plasmon(A,x0,fwhm,plas,fwhm_plas,damp_a,damp_b,x)

variable A, x0, fwhm, plas, fwhm_plas, damp_a, damp_b, x
variable plasmon
variable kk
variable kkmax = 15 // Number of plasmon losses
variable Anorm

plasmon = 0
Anorm = 0
for(kk=1;kk<=kkmax;kk+=1)
	plasmon +=  Func_Lorentz(1.,x0+kk*plas,fwhm+kk*fwhm_plas,x)* (damp_a^kk + damp_b^kk/Factorial(kk))
	Anorm    +=  (damp_a^kk + damp_b^kk/Factorial(kk))
endfor
plasmon *=  A/Anorm

return plasmon

end	


//===========================================
// Band bending lorentz 
//===========================================
function Func_Band_Bending_Lorentz(A,x0,fwhm,Vbb,Ww,Lambda,x)

variable A, x0, fwhm, Vbb, Ww, Lambda, x
variable /G gx0 =x0
variable /G gfwhm = fwhm
variable /G gVbb= Vbb
variable /G gLambdaoWw = Lambda/Ww
variable /G gx = x
variable bandbending

bandbending = Integrate1D(Func_Band_Bending_Lorentz_int,0.0,1.0,0)*Ww/Lambda
bandbending += exp(-Ww/Lambda)*Func_Lorentz(1.0,x0,fwhm,x)
bandbending *= A

return bandbending

end
//-------------------------------------------------------------------------
function Func_Band_Bending_Lorentz_int(z)

variable z
NVAR gx0, gfwhm, gVbb, gLambdaoWw, gx
variable bandbending_int
variable x0 

x0 = gx0 - gVbb*(1-z^2)
bandbending_int = exp(-z/gLambdaoWw)*Func_Lorentz(1.0,x0,gfwhm,gx)

return bandbending_int
end






