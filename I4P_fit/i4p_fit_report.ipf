// Macro :  I4PFit()
// Author : R�mi LAZZARI (lazzari@insp.jussieu.fr) based on initial work of Francesco BRUNO (bruno@tasc.infm.it) 
// Date : 2019
// Usage : Fit of XPS (see manual)


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Table of contents
// REPORT
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




#pragma rtGlobals=3		// Use modern global access method and strict wave access.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////   REPORT
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//===========================================
function REPORT_ButtonProc(ctrlName) : ButtonControl

string ctrlName

//-------------go to I4PFit folder
setdatafolder root:I4P:I4PFit	

//------------write report to windows
report_i4pfit_func()
dowindow/f fit_report

//-------------go back to current datafolder
SVAR data_folder=data_folder
setdatafolder data_folder

//------------copy the I4PFit folder in the current data folder
if(cmpstr(data_folder,"root:I4P:I4PFit")==0)
	print "==> ERROR : Cannot copy; data folder is root:I4P:I4PFit  !"
	print " "
	beep
	doalert /T="WARNING" 0, "ERROR : Cannot copy; data folder is root:I4P:I4PFit  !"
	return 0
endif
string datafolderfit = data_folder + "I4PFitReport"
killdatafolder /z $datafolderfit
duplicatedatafolder $"root:I4P:I4PFit", $datafolderfit

//------------clean the copied I4PFit folder
setdatafolder datafolderfit
killwaves /z  Nparam_peak, what, value, fix_it, low, high, epsilon, fit_value, sigma, para, satelittes, para_sigma, para_fit_value
killwaves /z para_epsilon, para_high, para_low, para_fix, para_map, limits
killvariables /z  N_lines, index, active, var_type, N_peaks, N_peaks_max, old_index
killstrings /z warning_limits1, warning_limits2, peak_description, fixfit

//-------------go back to current datafolder
setdatafolder data_folder

end

//===========================================
macro report_i4pfit()

report_i4pfit_func()

end

//===========================================
function report_i4pfit_func()

//-------------do not update
silent -1; pauseupdate

//---------Check if it is the righ folder
if(exists("data_be")!=1 || exists("data_counts")!=1 || exists("peak1")!=1 || exists("bkr1")!=1) 
	beep
	doalert /T="WARNING" 0, "ERROR \r\rNot in the right folder for plotting !"
	return 0
endif

//-------------global parameters
string textstring
wave /d N_type=N_type, data_be=data_be,data_fit=data_fit,background_tot=background_tot
NVAR nA=nA, nB=nB, xG=xG, reduced_chi2=reduced_chi2, RBragg=RBragg, RBragg_exp=RBragg_exp, Abbe=Abbe
SVAR data_comment=data_comment, data_name=data_name, data_folder=data_folder
wave /d mat_para=mat_para, cormat = cormat
wave / d peak_activate=peak_activate, peak_type = peak_type
wave /t  peak_type_name=peak_type_name
string fmtstring = "%s%d%s%s%10.3f%s%10.3f\r"
variable ii, jj, pp = 0

//-------------in case of a pure simulation : put input values in fitted values
if(reduced_chi2==0)
	mat_para[][5][] = mat_para[p][0][r]
endif

//-------------check if the window exists
dowindow /F fit_report
if(v_flag==0)
	newnotebook/n=fit_report/F=0 as "I4P : FIT REPORT : " + data_folder + data_name
endif


//-------------HEADER
textstring=""
notebook fit_report, selection={startOffile, endOfFile }, text=textstring
textstring="============================================================\r"
notebook fit_report , text=textstring
textstring="  I4P :  FIT REPORT              "+date()+"           "+time() +" \r"
notebook fit_report , text=textstring
textstring="============================================================\r"
notebook fit_report , text=textstring
sprintf textstring "%s\r", " "
notebook fit_report , text= textstring

//-------------NAME AND FOLDER
sprintf textstring "%s%s\r",  "==> Spectrum = ", data_name
notebook fit_report , text=textstring
sprintf textstring "%s%s\r\r",  "==> Folder = ", data_folder
notebook fit_report , text=textstring

//-------------RELIANCE
if(nA!=0 || nB!=numpnts(data_be)-1)
	sprintf textstring  "%s\r" "==> Fit over a reduced energy range ! "
	notebook fit_report , text=textstring
endif
sprintf textstring "%s%10.3f%s%10.3f%s\r","==> Fit over [", data_be[nA], ",", data_be[nB], "]" 
notebook fit_report , text=textstring
sprintf textstring "%s%10.3f\r","==> Reduced Chi square = ", reduced_chi2
notebook fit_report , text=textstring
sprintf textstring "%s%10.3f%s%10.3f%s\r","==> Bragg reliance factor = ", RBragg, "  [expected = ",  RBragg_exp, "]"
notebook fit_report , text=textstring
sprintf textstring "%s%10.3f\r\r","==> Abbe criterion = ", Abbe
notebook fit_report , text=textstring

//-------------NUMBER OF PEAKS
for(ii=1;ii<numpnts(N_type);ii+=1)
	if(N_type[ii]!=0)
		sprintf textstring "%4s%2d%s%s\r", "==> ", N_type[ii], " ", peak_type_name[ii]
		notebook fit_report , text=textstring
	endif
endfor
sprintf textstring "%s\r", " "
notebook fit_report , text= textstring


//-------------TRANSMISSION FUNCTION
switch(mat_para[0][0][0])
	case 1:		
	textstring="==> Transmission function = FAT/CAE (normalization to photon energy)\r\r"
	break
	case 2:		
	textstring="==> Transmission function = FRR/CRR (normalization to photon energy)\r\r"
	break
	case 3:		
	textstring="==> Transmission function = FAT/CAE (normalization to range)\r\r"
	break
	case 4:		
	textstring="==> Transmission function = FRR/CRR (normalization to range)\r\r"
	default:
	textstring="==> Transmission function = ONE\r\r"
	break	
endswitch
notebook fit_report , text=textstring


//-------------PHOTON ENERGY
sprintf textstring "%s%10.3f\r\r","==> Photon energy = ", mat_para[1][0][0]
notebook fit_report , text=textstring

//-------------EXPONENT OF TRANSMISSION FUCTION
sprintf textstring "%s%10.3f\r\r","==> Exponent of transmission function = ", mat_para[2][0][0]
notebook fit_report , text=textstring

//-------------BROADENING
switch(mat_para[3][0][0])
	case 1:
		textstring="==> Overall Gaussian Instrumental broadening = ON\r"
		notebook fit_report , text=textstring
		sprintf textstring  fmtstring+"\r", "[", np(pp,mat_para[4][6][0]), "]   ","Gaussian FWHM = ", mat_para[4][5][0], " � ", mat_para[4][6][0]
		notebook fit_report , text=textstring
	break
	case 2:
		textstring="==> Overall Gate Instrumental broadening = ON\r"
		notebook fit_report , text=textstring
		sprintf textstring fmtstring+"\r", "[", np(pp,mat_para[4][6][0]), "]   ", "Gate FWHM = ", mat_para[4][5][0], " � ", mat_para[4][6][0]	
		notebook fit_report , text=textstring
	break
	case 3:
		textstring="==> Overall FRR/CRR Gaussian Instrumental broadening = ON\r"
		notebook fit_report , text=textstring
		sprintf textstring  fmtstring+"\r", "[", np(pp,mat_para[4][6][0]), "]   ", "Gaussian FWHM = ",  mat_para[4][5][0], " � ", mat_para[4][6][0]
		notebook fit_report , text=textstring
		sprintf textstring  fmtstring+"\r", "[", np(pp,mat_para[5][6][0]), "]   ", "FRR/CRR slope    = ", mat_para[5][5][0], " � ", mat_para[5][6][0]
		notebook fit_report , text=textstring			
	break
	default:
		textstring="==> Overall  Instrumental broadening = OFF\r\r"
		notebook fit_report , text=textstring
	break
endswitch

//-------------SATELITTES
switch(mat_para[6][0][0])
	//-----------Sum of peaks
	case 1:
		textstring="==> Satelittes = ON (Sum)\r"
		notebook fit_report , text=textstring
		textstring="Source = Mg-Kalpha (Handbook XPS)\r\r"
	break 
	case 2:
		textstring="==> Satelittes = ON (Sum)\r"
		notebook fit_report , text=textstring
		textstring="Source = Al-Kalpha (Handbook XPS)\r\r"
	break 		
	case 3:
		textstring="==> Satelittes = ON (Sum)\r"
		notebook fit_report , text=textstring
		textstring="Source = He I\r\r"
	break 
	case 4:
		textstring="==> Satelittes = ON (Sum)\r"
		notebook fit_report , text=textstring
		textstring="Source = Mg-Kalpha (Klauber 1993)\r\r"
	break 
	case 5:
		textstring="==> Satelittes = ON (Sum)\r"
		notebook fit_report , text=textstring
		textstring="Source = Al-Kalpha (Klauber 1993)\r\r"	
	break
	case 6:
		textstring="==> Satelittes = ON (Sum)\r"
		notebook fit_report , text=textstring
		textstring="Source = Mg-Kalpha (Klauber 1993) / Only Kalpha2\r\r"
	break 
	case 7:
		textstring="==> Satelittes = ON (Sum)\r"
		notebook fit_report , text=textstring
		textstring="Source = Al-Kalpha (Klauber 1993) / Only Kalpha2\r\r"
	break
	//-----------Actual convolution
	case -1:
		textstring="==> Satelittes = ON (Convolution)\r"
		notebook fit_report , text=textstring
		textstring="Source = Mg-Kalpha (Handbook XPS)\r\r"
	break 
	case -2:
		textstring="==> Satelittes = ON (Convolution)\r"
		notebook fit_report , text=textstring
		textstring="Source = Al-Kalpha (Handbook XPS)\r\r"
	break 		
	case -3:
		textstring="==> Satelittes = ON (Convolution)\r"
		notebook fit_report , text=textstring
		textstring="Source = He I\r\r"
	break 
	case -4:
		textstring="==> Satelittes = ON (Convolution)\r"
		notebook fit_report , text=textstring
		textstring="Source = Mg-Kalpha (Klauber 1993)\r\r"
	break 
	case -5:
		textstring="==> Satelittes = ON (Convolution)\r"
		notebook fit_report , text=textstring
		textstring="Source = Al-Kalpha (Klauber 1993)\r\r"	
	break
	case -6:
		textstring="==> Satelittes = ON (Convolution)\r"
		notebook fit_report , text=textstring
		textstring="Source = Mg-Kalpha (Klauber 1993) / Only Kalpha2\r\r"
	break 
	case -7:
		textstring="==> Satelittes = ON (Convolution)\r"
		notebook fit_report , text=textstring
		textstring="Source = Al-Kalpha (Klauber 1993) / Only Kalpha2\r\r"
	break 	
	default:
		textstring="==> Source satelittes = OFF\r\r"
	break
endswitch	
notebook fit_report , text=textstring		


//-------------BACKGROUND
sprintf textstring "%s", "==> Intrinsic Background  [A+B*x+C*x^2+D*x^3 with x=be-<be>]\r"
notebook fit_report , text=textstring
sprintf textstring "%s%10.3f\r", "<be> = ", xG
notebook fit_report , text=textstring
sprintf textstring  fmtstring, "[", np(pp,mat_para[7][6][0]), "]   ", "A = ", mat_para[7][5][0], " � ", mat_para[7][6][0]
notebook fit_report , text=textstring
sprintf textstring fmtstring, "[", np(pp,mat_para[8][6][0]), "]   ", "B = ", mat_para[8][5][0], " � ", mat_para[8][6][0]
notebook fit_report , text=textstring
sprintf textstring fmtstring, "[", np(pp,mat_para[9][6][0]), "]   ", "C = ", mat_para[9][5][0], " � ", mat_para[9][6][0]
notebook fit_report , text=textstring
sprintf textstring fmtstring+"\r", "[", np(pp,mat_para[10][6][0]), "]   ", "D = ", mat_para[10][5][0], " � ", mat_para[10][6][0]
notebook fit_report , text=textstring


//-------------INTEGRAL BACKGROUND
//-------------Shirley : ON
switch(mat_para[11][0][0])
	case 1:
	case 6:
	case 7:
	case 8:
	case 9:
		textstring="==> Integral Shirley background = ON \r"
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[12][6][0]), "]   ", "Amplitude     =  ", mat_para[12][5][0], " � ", mat_para[12][6][0]
		notebook fit_report , text=textstring+"\r"
	break
endswitch
//-------------Tougaard : ON
switch(mat_para[11][0][0])
	case 2:
	case 6:
		textstring="==> Two-parameter integral Tougaard background = ON  [Bkgr = BT/(C+T^2)^2) with T=energy loss, T>gap]\r"
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[13][6][0]), "]   ", "B    = ", mat_para[13][5][0], " � ", mat_para[13][6][0]
		notebook fit_report , text=textstring			
		sprintf textstring fmtstring, "[", np(pp,mat_para[14][6][0]), "]   ", "C    = ", mat_para[14][5][0], " � ", mat_para[14][6][0]
		notebook fit_report , text=textstring		
		sprintf textstring fmtstring, "[", np(pp,mat_para[15][6][0]), "]   ", "gap = ", mat_para[16][5][0], " � ", mat_para[16][6][0]
		notebook fit_report , text=textstring+"\r"
	break
	case 3:
	case 7:
		textstring="==>  Normalized two-parameter integral Tougaard background = ON  [Bkgr = BT/(C+T^2)^2)  with T=energy loss, T>gap]\r"
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[14][6][0]), "]   ", "C    = ", mat_para[14][5][0], " � ", mat_para[14][6][0]
		notebook fit_report , text=textstring		
		sprintf textstring fmtstring, "[", np(pp,mat_para[16][6][0]), "]   ", "gap = ", mat_para[16][5][0], " � ", mat_para[16][6][0]
		notebook fit_report , text=textstring+"\r"
	break
	case 4:
	case 8:
		textstring="==> Three-parameter integral Tougaard background = ON  [Bkgr = BT/(C-T^2)^2+D*T^2)  with T=energy loss, T>gap]\r"
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[13][6][0]), "]   ", "B    = ", mat_para[13][5][0], " � ", mat_para[13][6][0]
		notebook fit_report , text=textstring			
		sprintf textstring fmtstring, "[", np(pp,mat_para[14][6][0]), "]   ", "C    = ", mat_para[14][5][0], " � ", mat_para[14][6][0]
		notebook fit_report , text=textstring		
		sprintf textstring fmtstring, "[", np(pp,mat_para[15][6][0]), "]   ", "D    = ", mat_para[15][5][0], " � ", mat_para[15][6][0]
		notebook fit_report , text=textstring	
		sprintf textstring fmtstring, "[", np(pp,mat_para[16][6][0]), "]   ", "gap = ", mat_para[16][5][0], " � ", mat_para[16][6][0]
		notebook fit_report , text=textstring+"\r"
	break
	case 5:
	case 9:
		textstring="==> Normalized three-parameter integral Tougaard background = ON  [Bkgr = BT/(C-T^2)^2+D*T^2)  with T=energy loss, T>gap]\r"
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[14][6][0]), "]   ", "C    = ", mat_para[14][5][0], " � ", mat_para[14][6][0]
		notebook fit_report , text=textstring		
		sprintf textstring fmtstring, "[", np(pp,mat_para[15][6][0]), "]   ", "D    = ", mat_para[15][5][0], " � ", mat_para[15][6][0]
		notebook fit_report , text=textstring	
		sprintf textstring fmtstring, "[", np(pp,mat_para[16][6][0]), "]   ", "gap = ", mat_para[16][5][0], " � ", mat_para[16][6][0]
		notebook fit_report , text=textstring+"\r"
	break						
endswitch	
//-------------Integral background : OFF
switch(mat_para[11][0][0])
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
	case 7:
	case 8:
	case 9:
	break	
	default:
		textstring="==> Integral background = OFF\r\r"
		notebook fit_report , text=textstring	
endswitch


//-------------AREAS
variable areaTOT, areaBACK, areaFIT
sprintf textstring "%s%10.3f%s%10.3f%s\r", "==> Total areas  over [", data_be[0], ",", data_be[numpnts(data_be)-1], "]" 
//textstring="==> Total areas  over [", data_be[0], ",", data_be[numpnts(data_be)-1], "]" 
notebook fit_report , text=textstring
areaTOT    = areaxy(data_be,data_fit)
areaBACK = areaxy(data_be,background_tot)
areaFIT      = areaTOT - areaBACK
sprintf textstring "%s%10.3f\r",  "Total area             = ", areaTOT
notebook fit_report , text=textstring
sprintf textstring "%s%10.3f\r", "Background area   = ", areaBACK
notebook fit_report , text=textstring
sprintf textstring "%s%10.3f\r\r", "Fit area               = ", areaFIT
notebook fit_report , text=textstring

//-------------BINDING ENERGY OFFSET / AREA MULTIPLIER
textstring="==> Offsets\r"
notebook fit_report , text=textstring	
sprintf textstring fmtstring, "[", np(pp,mat_para[17][6][0]), "]   ", "Binding energy offset = ", mat_para[17][5][0], " � ", mat_para[17][6][0]
notebook fit_report , text=textstring		
sprintf textstring fmtstring+"\r", "[", np(pp,mat_para[18][6][0]), "]   ","Area/height multiplier = ", mat_para[18][5][0], " � ", mat_para[18][6][0]
notebook fit_report , text=textstring	


//-------------PEAK INFORMATION
variable areaP, maxP, x0, fwhm, areaB
variable NN=numpnts(peak_activate)
string stringpeak, stringbkr
variable xpf = nan
variable xofsp = 	mat_para[17][0][0]
variable xofsf = 	mat_para[17][5][0]
variable areaXp =  mat_para[18][0][0]
variable areaXf =  mat_para[18][5][0]
	
// Loop over peaks
for(jj=1;jj<NN;jj+=1)

if(peak_activate[jj]==1)
		
	switch(peak_type[jj])	
				
	case 12:		
	//----------Peak type ?
	sprintf textstring "%s%2d%s%s\r", "==> PEAK ", jj, " = ", peak_type_name[peak_type[jj]]
	notebook fit_report , text=textstring
	//----------Fermi step
	sprintf textstring fmtstring, "[", np(pp,mat_para[0][6][jj]), "]   ", "Height                    =  ", mat_para[0][5][jj]*areaXf, " � ", mat_para[0][6][jj]*areaXf	
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[1][6][jj]), "]   ", "Binding energy        = ", mat_para[1][5][jj]+xofsf, " � ", mat_para[1][6][jj]			
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[2][6][jj]), "]   ", "Erf FWHM              = ", mat_para[2][5][jj], " � ", mat_para[2][6][jj]		
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[3][6][jj]), "]   ", "Atan FWHM           = ", mat_para[3][5][jj], " � ", mat_para[3][6][jj]	
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[4][6][jj]), "]   ", "Fermi width (K)       = ", mat_para[4][5][jj], " � ", mat_para[4][6][jj]	
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[5][6][jj]), "]   ", "Exp. decay lambda = ", mat_para[5][5][jj], " � ", mat_para[5][6][jj]
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[6][6][jj]), "]   ", "Lin. Poly. decay      = ", mat_para[6][5][jj], " � ", mat_para[6][6][jj]		
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[8][6][jj]), "]   ", "Sqr. Poly. decay     = ", mat_para[8][5][jj], " � ", mat_para[8][6][jj]
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[9][6][jj]), "]   ", "Cub. Poly. decay    = ", mat_para[9][5][jj], " � ", mat_para[9][6][jj]
	notebook fit_report , text=textstring+"\r"
	break
	
	case 13:		
	//----------Peak type ?
	sprintf textstring "%s%2d%s%s\r", "==> PEAK ", jj, " = ", peak_type_name[peak_type[jj]]
	notebook fit_report , text=textstring
	//----------Work function step
	sprintf textstring fmtstring, "[", np(pp,mat_para[0][6][jj]), "]   ", "Height                    =  ", mat_para[0][5][jj]*areaXf, " � ", mat_para[0][6][jj]*areaXf	
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[1][6][jj]), "]   ", "Binding energy        = ", mat_para[1][5][jj]+xofsf, " � ", mat_para[1][6][jj]			
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[2][6][jj]), "]   ", "Erf FWHM              = ", mat_para[2][5][jj], " � ", mat_para[2][6][jj]		
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[3][6][jj]), "]   ", "Atan FWHM           = ", mat_para[3][5][jj], " � ", mat_para[3][6][jj]	
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[5][6][jj]), "]   ", "Exp. decay lambda = ", mat_para[5][5][jj], " � ", mat_para[5][6][jj]
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[6][6][jj]), "]   ", "Lin. Poly. decay      = ", mat_para[6][5][jj], " � ", mat_para[6][6][jj]		
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[8][6][jj]), "]   ", "Sqr. Poly. decay     = ", mat_para[8][5][jj], " � ", mat_para[8][6][jj]
	notebook fit_report , text=textstring
	sprintf textstring fmtstring, "[", np(pp,mat_para[9][6][jj]), "]   ", "Cub. Poly. decay    = ", mat_para[9][5][jj], " � ", mat_para[9][6][jj]
	notebook fit_report , text=textstring+"\r"
	break	
	

	default: 
	
	//----------Peak general information
	stringpeak = "peak"+num2str(jj)
	peakparameters(data_be,$stringpeak,areaP,maxP,x0,fwhm)
	stringbkr = "bkr"+num2str(jj)
	areaB = areaxy(data_be,$stringbkr)
	//----------Offset : position of the first peak or binding energy offset
	if(numtype(xpf)==2) // Test for the first peak only
		if(xofsp!=0)
			xpf = xofsp
		else
			xpf = x0
		endif
		sprintf textstring "%s%10.3f\r\r", "==> Relative peak position defined from = ", xpf
		notebook fit_report , text=textstring		
	endif
	//----------Peak type ?
	sprintf textstring "%s%2d%s%s\r", "==> PEAK ", jj, " = ", peak_type_name[peak_type[jj]]
	notebook fit_report , text=textstring
	
	//----------Write the actual infos
	sprintf textstring "%s\r", "------------------------------------------------------------------------------------------------------"
	notebook fit_report , text=textstring	
	sprintf textstring "%s%10.3f%s%8.2f%s\r", "Actual peak area (abs/rel)        = ", areaP, " / ",  areaP/areaFIT*100, "%"
	notebook fit_report , text=textstring
	areaP /= 1. + mat_para[4][0][jj]
	sprintf textstring "%s%10.3f%s%8.2f%s\r", "Actual peak area/branch. ratio (abs/rel)      = ", areaP, " / ",  areaP/areaFIT*100, "%"
	notebook fit_report , text=textstring
	sprintf textstring "%s%10.3ff\r", "Actual peak extremum            = ", maxP
	sprintf textstring "%s%10.3f%s%8.2f\r",  "Actual peak position (abs/rel)   = ", x0, " / ", x0-xpf 	
	notebook fit_report , text=textstring
	sprintf textstring "%s%10.3f\r", "Actual peak FWHM                  = ", fwhm
	notebook fit_report , text=textstring	
	sprintf textstring "%s%10.3f%s%8.2f%s\r", "Actual peak background area (abs/rel)        = ", areaB, " / ",  areaB/areaBACK*100, "%"
	notebook fit_report , text=textstring		
	sprintf textstring "%s\r",  "------------------------------------------------------------------------------------------------------"
	notebook fit_report , text=textstring		
	  			
	//----------Common parameters : area, binding energy, fwhm, fwhmG, branching ratio, spin-orbit splitting
	//---Area
	sprintf textstring fmtstring, "[", np(pp,mat_para[0][6][jj]), "]   ", "Area                 = ", mat_para[0][5][jj]*areaXf, " � ", mat_para[0][6][jj]*areaXf	
	notebook fit_report , text=textstring
	//---Binding energy	
	sprintf textstring fmtstring, "[", np(pp,mat_para[1][6][jj]), "]   ", "Binding energy  = ", mat_para[1][5][jj]+xofsf, " � ", mat_para[1][6][jj]	
	notebook fit_report , text=textstring
	//---FWHM
	switch(peak_type[jj])
		case 2:
		sprintf textstring fmtstring, "[", np(pp,mat_para[2][6][jj]), "]   ", "FWHM slope  = ", mat_para[2][5][jj], " � ", mat_para[2][6][jj]	
		notebook fit_report , text=textstring
		break	
		case 10:
		sprintf textstring fmtstring, "[", np(pp,mat_para[2][6][jj]), "]   ", "FWHM high-BE  = ", mat_para[2][5][jj], " � ", mat_para[2][6][jj]
		notebook fit_report , text=textstring
		break
		default:
		sprintf textstring fmtstring, "[", np(pp,mat_para[2][6][jj]), "]   ", "FWHM       = ", mat_para[2][5][jj], " � ", mat_para[2][6][jj]
		notebook fit_report , text=textstring
		break
	endswitch
	//---Gaussian FWHM/Mixing ratio
	switch(peak_type[jj])
		case 2:
		case 4:
		sprintf textstring fmtstring, "[", np(pp,mat_para[3][6][jj]), "]   ", "Gaussian FWHM  = ", mat_para[3][5][jj], " � ", mat_para[3][6][jj]	
		notebook fit_report , text=textstring
		break
		case 3:
		case 11:
		sprintf textstring fmtstring, "[", np(pp,mat_para[3][6][jj]), "]   ", "Mixing ratio  = ", mat_para[3][5][jj], " � ", mat_para[3][6][jj]	
		notebook fit_report , text=textstring
		break
		default:
		sprintf textstring fmtstring, "[", np(pp,mat_para[3][6][jj]), "]   ", "Gaussian broadening FWHM  = ", mat_para[3][5][jj], " � ", mat_para[3][6][jj]	
		notebook fit_report , text=textstring
		break
	endswitch	
	//---Branching ratio	
	sprintf textstring fmtstring, "[", np(pp,mat_para[4][6][jj]), "]   ", "Branching ratio      = ", mat_para[4][5][jj], " � ", mat_para[4][6][jj]
	notebook fit_report , text=textstring
	//---Spin-orbit splitting	
	sprintf textstring fmtstring, "[", np(pp,mat_para[5][6][jj]), "]   ", "Spin-orbit splitting  = ", mat_para[5][5][jj], " � ", mat_para[5][6][jj]	
	notebook fit_report , text=textstring
	//---Shirley parameters	
	sprintf textstring fmtstring, "[", np(pp,mat_para[6][6][jj]), "]   ", "Shirley-S         = ", mat_para[6][5][jj], " � ", mat_para[6][6][jj]
	notebook fit_report , text=textstring			
	//---Tougaard parameters		
	sprintf textstring fmtstring, "[", np(pp,mat_para[7][6][jj]), "]   ", "Tougaard-B     = ", mat_para[7][5][jj], " � ", mat_para[7][6][jj]
	notebook fit_report , text=textstring			
	sprintf textstring fmtstring, "[", np(pp,mat_para[8][6][jj]), "]   ", "Tougaard-C     = ", mat_para[8][5][jj], " � ", mat_para[8][6][jj]
	notebook fit_report , text=textstring		
	sprintf textstring fmtstring, "[", np(pp,mat_para[9][6][jj]), "]   ", "Tougaard-D     = ", mat_para[9][5][jj], " � ", mat_para[9][6][jj]
	notebook fit_report , text=textstring	
	sprintf textstring fmtstring, "[", np(pp,mat_para[10][6][jj]), "]   ", "Tougaard-gap  = ", mat_para[10][5][jj], " � ", mat_para[10][6][jj]
	notebook fit_report , text=textstring
		
	//----------Specific parameters 
	//----------Doniach-Sunjic
	if(peak_type[jj]==6)
		sprintf textstring fmtstring, "[", np(pp,mat_para[11][6][jj]), "]   ", "Asymmetry  = ", mat_para[11][5][jj], " � ", mat_para[11][6][jj]	
		notebook fit_report , text=textstring
	endif
	//----------General Doniach-Sunjic
	if(peak_type[jj]==7)
		sprintf textstring fmtstring, "[", np(pp,mat_para[11][6][jj]), "]   ", "Gamma  = ", mat_para[11][5][jj], " � ", mat_para[11][6][jj]
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[12][6][jj]), "]   ", "Mu         = ", mat_para[12][5][jj], " � ", mat_para[12][6][jj]
		notebook fit_report , text=textstring
	endif
	//----------Mahan peak
	if(peak_type[jj]==8)
		sprintf textstring fmtstring, "[", np(pp,mat_para[11][6][jj]), "]   ", "Beta  = ", mat_para[11][5][jj], " � ", mat_para[11][6][jj]
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[12][6][jj]), "]   ", "Ksi    = ", mat_para[12][5][jj], " � ", mat_para[12][6][jj]
		notebook fit_report , text=textstring
	endif
	//----------Post-collision interaction
	if(peak_type[jj]==9)
		sprintf textstring fmtstring, "[", np(pp,mat_para[11][6][jj]), "]   ", "Asymmetry  = ", mat_para[11][5][jj], " � ", mat_para[11][6][jj]	
		notebook fit_report , text=textstring
	endif
	//----------Asymmetric Voigt
	if(peak_type[jj]==10)
		sprintf textstring fmtstring, "[", np(pp,mat_para[2][6][jj]), "]   ", "FWHM low-HE  = ", mat_para[2][5][jj], " � ", mat_para[2][6][jj]
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[11][6][jj]), "]   ", "FWHM low-BE  = ", mat_para[11][5][jj], " � ", mat_para[11][6][jj]
		notebook fit_report , text=textstring
	endif
	//----------Asymmetric pseudo-Voigt
	if(peak_type[jj]==11)
		sprintf textstring fmtstring, "[", np(pp,mat_para[11][6][jj]), "]   ", "Asymmetry       =  ", mat_para[11][5][jj], " � ", mat_para[11][6][jj]
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[12][6][jj]), "]   ", "Sigmodial shift  =  ", mat_para[12][5][jj], " � ", mat_para[12][6][jj]
		notebook fit_report , text=textstring
	endif
	//----------Fano
	if(peak_type[jj]==14)
		sprintf textstring fmtstring, "[", np(pp,mat_para[11][6][jj]), "]   ", "Asymmetry  =  ", mat_para[11][5][jj], " � ", mat_para[11][6][jj]
		notebook fit_report , text=textstring
	endif
	//----------Plasmon
	if(peak_type[jj]==15)
		sprintf textstring fmtstring, "[", np(pp,mat_para[11][6][jj]), "]   ", "Plasmon energy     =  ", mat_para[11][5][jj], " � ", mat_para[11][6][jj]
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[12][6][jj]), "]   ", "Plasmon FWHM     =  ", mat_para[12][5][jj], " � ", mat_para[12][6][jj]
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[13][6][jj]), "]   ", "Extrinsic damping a =  ", mat_para[13][5][jj], " � ", mat_para[13][6][jj]
		notebook fit_report , text=textstring		
		sprintf textstring fmtstring, "[", np(pp,mat_para[14][6][jj]), "]   ", "Extrinsic damping b =  ", mat_para[14][5][jj], " � ", mat_para[14][6][jj]
		notebook fit_report , text=textstring	
	endif
	//----------Band bending Lorentz
	if(peak_type[jj]==16)
		sprintf textstring fmtstring, "[", np(pp,mat_para[11][6][jj]), "]   ", "Band bending  =  ", mat_para[11][5][jj], " � ", mat_para[11][6][jj]
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[12][6][jj]), "]   ", "Width SCL      =  ", mat_para[12][5][jj], " � ", mat_para[12][6][jj]
		notebook fit_report , text=textstring
		sprintf textstring fmtstring, "[", np(pp,mat_para[13][6][jj]), "]   ", "Attenuation length  =  ", mat_para[13][5][jj], " � ", mat_para[13][6][jj]
		notebook fit_report , text=textstring		
	endif			
	
	textstring="\r"
	notebook fit_report , text=textstring
	
	break
	endswitch

	endif
	
endfor

//-------------CORRELATION MATRIX
textstring="==> Correlation matrix \r"
notebook fit_report , text=textstring
textstring = ""
for(ii=0;ii<dimsize(cormat,0);ii+=1)
	sprintf textstring, "%10d%10s", ii+1,"|"
	for(jj=0;jj<dimsize(cormat,1);jj+=1)
		sprintf textstring, textstring+"%10.2f", cormat[ii][jj]
	endfor
	textstring += "\r"
	notebook fit_report , text=textstring
	textstring = ""
endfor
textstring="\r"
notebook fit_report , text=textstring

//-------------COMMENT
sprintf textstring "%s",  "==> COMMENTS"
notebook fit_report , text=textstring
sprintf textstring "%s\r\r",  data_comment
notebook fit_report , text=textstring

//-------------USER NOTES
textstring="==> USER NOTES ? "
notebook fit_report , text=textstring

end


//===========================================
// Usage : Modify to appeareance of fit windows for exportation
Function PlotI4PFit2Export()

dowindow/C  fit_graph_i4pfit
ModifyGraph wbRGB=(65535,65535,65535),gbRGB=(65535,65535,65535)
ModifyGraph grid(top)=0
ModifyGraph width={Aspect,1.25}

End

//===========================================
// Usage : Get the fitted parameter number
Function np(pp,isfit)

variable &pp, isfit
variable npo = 0

if(isfit!=0)
	pp = pp + 1
	npo = pp
else
	npo = 0
endif

return npo

End