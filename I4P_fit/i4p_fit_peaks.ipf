// Macro :  I4PFit()
// Author : R�mi LAZZARI (lazzari@insp.jussieu.fr) based on initial work of Francesco BRUNO (bruno@tasc.infm.it) 
// Date : 2019
// Usage : Fit of XPS (see manual)


#pragma rtGlobals=3		// Use modern global access method and strict wave access.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Table of contents
// TOTAL FIT FUNCTION
// CONVOLUTION
// SINGLE PEAKS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////   TOTAL FIT FUNCTION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//===========================================
// TOTAL FUNCTION
function Func_full_xps(para, yw, xw) : FitFunc

	wave /d para, yw, xw
	
	//-------------do not update
	silent -1; pauseupdate
	
	//------------satelitte, background, convolution and relative values
	variable issat = para[6]
	variable isbkr = 8 // blend of Shirley and 3-parameters Tougaard for individual peaks
	variable isconv = para[3]
	variable xofs = para[17]
	variable areaX = para[18]
	
	///-------------starting positions 
	wave /d peak_type = peak_type, Nparam_peak=Nparam_peak
	wave /d N_type = N_type
	duplicate /o N_type start_type
	start_type = 0
	variable kk
	start_type[1] = Nparam_peak(0) 
	for(kk=2;kk<numpnts(start_type);kk+=1)
		start_type[kk] = start_type[kk-1] + Nparam_peak(kk-1)*N_type[kk-1]
	endfor
	
	//-----------initializations
	yw  =0
	duplicate /o yw yw_peak, yw_bkr, bkr_cut, bkr_com 
	
	//-------------------------------------------------------------------CYCLES OVER TYPES OF PEAKS
	//-----------cycle over peak types
	variable ii, jj
	for(ii=1;ii<numpnts(N_type);ii+=1)
		if (N_type[ii]>0)
			for(jj=0;jj<N_type[ii];jj+=1)
				yw_peak  = 0	
				make /d/o/n=(Nparam_peak(ii)) para_type = 0
				//-------------vector of parameters
				para_type[0,Nparam_peak(ii)-1] = para[p+start_type[ii]]
				//-------------relative values ?
				para_type[0] *= areaX
				para_type[1] += xofs
				//-------------the peak and its background
				single_func(ii, para_type, issat, isbkr, xw, yw_peak, yw_bkr)
				//------------sump up			
				yw += yw_peak
				bkr_cut += yw_bkr
				//------------next one
				start_type[ii]+=Nparam_peak(ii)
			endfor
		endif
	endfor
	//-------------------------------------------------------------------CYCLES OVER TYPES OF PEAKS
	
	
	//-------------------------------------------------------------------BACKGROUND
	//------calculate the common background
	background_total(para, xw, yw, bkr_com)
	//------add it to the sum of all peak background
	bkr_cut += bkr_com
	//------add the total background to the sum of peaks
	yw += bkr_cut
	//-------------------------------------------------------------------BACKGROUND
	
	//-----------------------------------------------------------------TRANSMISSION FUNCTION
	transmisssion_function(xw,yw,para[0],abs(para[1]),abs(para[2]))
	transmisssion_function(xw,bkr_cut,para[0],abs(para[1]),abs(para[2]))
	//-------------------------------------------------------------------TRANSMISSION FUNCTION	
					
	//------------------------------------------------------------------GLOBAL CONVOLUTION
	//--------instrumental convolution on the whole signal
	variable hnu, fwhmB, FRR_slope
	hnu			= abs(para[1])
	fwhmB 		= abs(para[4])
	FRR_slope 	= abs(para[5])
	switch(isconv)
		case 0: // None
		break
		case 1: // Gaussian
			peak_convolution(xw,yw,"gaussian",fwhmB)
			peak_convolution(xw,bkr_cut,"gaussian",fwhmB)	
		break
		case 2:  // Gate
			peak_convolution(xw,yw,"gate",fwhmB)
			peak_convolution(xw,bkr_cut,"gate",fwhmB)
		break
		case 3: // Gaussian FRR/CRR
			peak_convolution_FRR(xw,yw,fwhmB,hnu,FRR_slope)
			peak_convolution_FRR(xw,bkr_cut,fwhmB,hnu,FRR_slope)
		break
		case 4: // Triangle
			peak_convolution(xw,yw,"triangle",fwhmB)
			peak_convolution(xw,bkr_cut,"triangle",fwhmB)
		break			
	endswitch
	
	//-------------------------------------------------------------------GLOBAL CONVOLUTION
	
	//-----------------------------------------------------------------COPY THE CUT BACKGROUND TO TOTAL ONE
	wave /d background_tot = background_tot
	NVAR nA=nA, nB=nB
	background_tot = nan
	background_tot[nA,nB] = bkr_cut[p-nA]
	//-----------------------------------------------------------------COPY THE CUT BACKGROUND TO TOTAL ONE
	
	//-----------------------------------------------------------------CLEAN
	killwaves /z start_type, para_type, yw_peak, yw_bkr, bkr_cut, bkr_com
	//-----------------------------------------------------------------CLEAN
	
end

//===========================================
function SFunc_allpeaks(kindex)

	variable kindex
	
	//-------------do not update
	silent -1; pauseupdate
	
	//-------------satellite and relative values
	wave /d mat_para=mat_para
	variable issat = mat_para[6][0][0]
	variable isbkr = 8  // blend of Shirley and 3-parameters Tougaard for individual peaks
	variable xofs = mat_para[17][0][0]
	variable areaX = mat_para[18][0][0]
	
	//------------initialize
	wave /d value=value, data_be=data_be 
	duplicate/o value para_temp
	duplicate /o data_be yw, ywbkr
	wave /d peak_type=peak_type,peak_activate=peak_activate
	NVAR N_peaks_max=N_peaks_max
	NVAR nA=nA, nB=nB
	
	//-------------loop over peaks
	variable jj=1
	string stringpeak, stringbkr
	do
		stringpeak ="peak"+num2istr(jj)
		stringbkr   ="bkr"+num2istr(jj)
		wave /d wavepeak = $stringpeak
		wave /d wavebkr   = $stringbkr
		if (peak_activate[jj]==1)
			wavepeak= nan
			wavebkr  = nan
			yw = 0
			ywbkr = 0
			//----------vector of peak parameters
			para_temp[]=mat_para[p][kindex][jj]
			//----------relative values ?
			para_temp[0] *= areaX
			para_temp[1] += xofs
			//----------the peak
			single_func(peak_type[jj],para_temp,issat,isbkr,data_be,yw,ywbkr)
			if(sum(ywbkr)==0)
				ywbkr = nan
			endif
			//----------peak/bkr cut out
			wavepeak[nA,nB]  = yw[p]
			wavebkr[nA,nB]    = ywbkr[p]
		else
			wavepeak = nan
			wavebkr   = nan
		endif
		jj+=1		
	while (jj<=N_peaks_max)
	
	//-------------clean
	killwaves /z para_temp, yw, ywbkr
	
end

//===========================================
function single_func(peaktype,para,issat,isbkr,xw,yw,ywbkr)

variable peaktype, issat, isbkr
wave /d para, xw, yw, ywbkr

switch(peaktype)
case 1:
	SFunc_Lorentz(para,issat,isbkr,xw,yw,ywbkr)
break
case 2:
	SFunc_Gaussian(para,issat,isbkr,xw,yw,ywbkr)
break
case 3:
	SFunc_PseudoVoigt(para,issat,isbkr,xw,yw,ywbkr)
break	
case 4:
	SFunc_LorentzXGaussian(para,issat,isbkr,xw,yw,ywbkr)
break
case 5:
	SFunc_Oscillator(para,issat,isbkr,xw,yw,ywbkr)
break
case 6:
	SFunc_Doniach_Sunjic(para,issat,isbkr,xw,yw,ywbkr)
break
case 7:
	SFunc_Gene_Doniach_Sunjic(para,issat,isbkr,xw,yw,ywbkr)
break
case 8:
	SFunc_Mahan(para,issat,isbkr,xw,yw,ywbkr)
break	
case 9:
	SFunc_PCI(para,issat,isbkr,xw,yw,ywbkr)
break	
case 10:
	SFunc_asymVoigt(para,issat,isbkr,xw,yw,ywbkr)
break
case 11:
	SFunc_asymPsVoigt(para,issat,isbkr,xw,yw,ywbkr)
break
case 12:
	SFunc_FermiStep(para,xw,yw)
	ywbkr =  0
break									
case 13:
	SFunc_WorkFunctionStep(para,xw,yw)
	ywbkr =  0
break
case 14:
	SFunc_Fano(para,issat,isbkr,xw,yw,ywbkr)
break
case 15:
	SFunc_plasmon(para,issat,isbkr,xw,yw,ywbkr)
break	
case 16:
	SFunc_BandBendingLorentz(para,issat,isbkr,xw,yw,ywbkr)
break
endswitch

end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////   CONVOLUTION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//===========================================
// Convolution 
//===========================================
// convolves an arbitrary curve yw with a response function (Gaussian, Lorentzian, Satelittes)
// the abscissa xw does not need to be equispaced; waves are remapped accordingly for the "Convlove" operation of IGOR
// yw is modified on output
function peak_convolution(xw,yw,peaktype,fwhm,[issat])

wave /d xw,yw
string peaktype
variable fwhm, issat

//---skip convolution if the fwhm is too small
if(abs(fwhm)<5e-4)
	return 0
endif

//---redefine fwhm in case of satelittes
if(paramisdefault(issat)==0)
	wave /d satelittes = satelittes
	fwhm =  satelittes[0][issat-1][2]
endif

// minimum step for the convolution peak = number of point per fwhm	
variable stepC = fwhm/15	

//------remap the input with the righ step
variable step = min(xw[1]-xw[0],stepC)
wavestats /q xw
variable wpnts = round((V_max-V_min)/step+1)
make /d/o/n=(wpnts) xw_temp, yw_temp
xw_temp = V_min+p*step
//yw_temp=interp(xw_temp,xw,yw,ywbkr)  // linear interpolation
interpolate2 /a=0  /b=0 /e=1 /f=0 /i=3 /t=2 /x=xw_temp /y=yw_temp  xw, yw // cubic spline interpolation


//---definition of the number of points of convolution function
variable Cpnts
strswitch(peaktype)
case "gaussian":
	// +- 2*fwhm 
	Cpnts = 2*round(fwhm/step)
break
case "lorentz":
	// +- 10*fwhm 
	Cpnts = 10*round(fwhm/step)
break
case "gate":
	// +- 1*fwhm 
	Cpnts = 1*round(fwhm/step)
break
case "triangle":
	// +- 2*fwhm 
	Cpnts = 2*round(fwhm/step)
break
case "satellites":
	// distance between satellites +- 10*fwhm 
	make /d/o/n=(dimsize(satelittes,0)) sat_temp
	sat_temp = abs(satelittes[p][issat-1][0])
	wavestats /q sat_temp
	killwaves /z sat_temp
	variable dxC
	dxC = V_max + 10*satelittes[V_maxloc][issat-1][2]
	Cpnts = round(dxC/step)
break
endswitch
make /d/o/n=(2*Cpnts+1)/o xC, yC
xC = -Cpnts*step+p*step

//---definition of the convolution function
variable normyC 		
strswitch(peaktype)
case "gaussian":
	variable sigma2G=(fwhm/gGsig2fwhm)^2
	yC = exp(-(xC^2)/(2*sigma2G))/sqrt(2*pi*sigma2G)
break
case "lorentz":
	yC =  fwhm/(2*pi)/(xC^2+(fwhm/2)^2)
break
case "gate":
	yC = (xC>=-fwhm/2. && xC<=fwhm/2.) ? 1. : 0.
break
case "triangle":
	yC = (xC[p]>=-fwhm && xC[p]<=fwhm) ? 1.-abs(xC[p])/fwhm : 0.
break
case "satellites":
	variable ss
	yC = 0
	for(ss=0;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		yC += Func_Lorentz(satelittes[ss][issat-1][1],-satelittes[ss][issat-1][0],satelittes[ss][issat-1][2],xC)
	endfor
break
endswitch
//---normalize to one
normyC =  areaxy(xC,yC)
yC /= normyC

//--------convolution and edge correction
duplicate /o yw_temp yw_conv
convolve /a yC yw_conv
duplicate /o yw_temp yw1_temp
yw1_temp = 1.
convolve /a yC yw1_temp
yw_conv /= yw1_temp
//yw=interp(xw,xw_temp,yw_conv)  // linear interpolation
interpolate2 /a=0  /b=0 /e=1 /f=0 /i=3 /t=2 /x=xw /y=yw  xw_temp, yw_conv // cubic spline interpolation

//--------clean waves
killwaves /z yw1_temp xw_temp yw_temp yw_conv xC yC 

end



//===========================================
// Convolution with position dependent fwhm  
//===========================================
function peak_convolution_FRR(xw,yw,fwhm,hnu,slope)

wave /d xw,yw
variable fwhm, hnu, slope
variable ii, fwhm_ii

duplicate /o yw yyw, yyyw  
for(ii=0;ii<numpnts(xw);ii+=1)
	fwhm_ii = fwhm + slope*(hnu-xw[ii])
	fwhm_ii = abs(fwhm_ii)
	yyw = yw	
	peak_convolution(xw,yyw,"gaussian",fwhm_ii)
	yyyw[ii] = yyw[ii]						
endfor
yw = yyyw										
killwaves /z yyw, yyyw

end


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////   SINGLE PEAK
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//===========================================
// 	BACKGROUND 
function background_total(para, xw, yw, background_tot)

wave /d para, xw, yw, background_tot
NVAR xG = xG
variable zero, linbk, sqrbk, cubbk
variable backgroundtype, factShirley, B, C, D, gap

//--------initialize
duplicate/o background_tot background_pol background_int
background_tot = 0
background_int = 0 
background_tot = 0

//--------intrinsic background
zero		= para[7]
linbk	= para[8]
sqrbk	= para[9]
cubbk	= para[10]
background_pol = zero+linbk*(xw-xG)+sqrbk*(xw-xG)^2+cubbk*(xw-xG)^3

//---------eventual integral background
backgroundtype = para[11]
factShirley 	= abs(para[12])
B			= abs(para[13])
C			= para[14]
D			= abs(para[15])
gap 			= abs(para[16])
integral_background(xw,yw,background_int,backgroundtype,factShirley,B,C,D,gap)

//------add the intrinsic background to the integral one
background_tot = background_pol + background_int

//------clean the waves
killwaves /z  background_pol background_int

end

//===========================================
//	 LORENTZIANS
function SFunc_Lorentz(para, issat, isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP, xpos, fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8] 
D		= abs(para[9])
gap 		= abs(para[10])
		
//----reference peak
yw = Func_Lorentz(areaP,xpos,fwhm,xw)
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_Lorentz(areaP,xpos+spinorbit,fwhm,xw)	
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])  	 		
		yw += Func_Lorentz(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,xw)	
		if(branch!=0)	
			yw += branch*Func_Lorentz(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,xw)	
		endif						
	endfor									
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//---possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif			

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	GAUSSIAN
function SFunc_Gaussian(para, issat, isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP, xpos, slope, fwhmG, branch, spinorbit
variable S, B, C, D, gap
wave /d satelittes = satelittes
variable ss, fwhmG_sat

//----parameters
areaP	= para[0]
xpos	= para[1]
slope	= para[2]
fwhmG	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])

//----reference peak
yw = Func_Gaussian(areaP,xpos,fwhmG,slope,xw)
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_Gaussian(areaP,xpos+spinorbit,fwhmG,slope,xw)
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhmG_sat = abs(fwhmG - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])  	 
		yw += Func_Gaussian(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhmG_sat,slope,xw)
		if(branch!=0)
			yw += branch*Func_Gaussian(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhmG_sat,slope,xw)		
		endif
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	PSEUDO-VOIGT
function SFunc_PseudoVoigt(para, issat, isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP,xpos,fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
variable mix
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
mix		= max(min(abs(para[3]),1.0),0.)
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])

//----reference peak
yw = Func_PseudoVoigt(areaP,xpos,fwhm,mix,xw)
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_PseudoVoigt(areaP,xpos+spinorbit,fwhm,mix,xw)		
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])   	 	 
		yw += Func_PseudoVoigt(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,mix,xw)
		if(branch!=0)
			yw += branch*Func_PseudoVoigt(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,mix,xw)
		endif
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	LORENTZ X GAUSSIAN
function SFunc_LorentzXGaussian(para, issat,  isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP,xpos,fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG 	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])

//----reference peak
yw = Func_LorentzXGaussian(areaP,xpos,fwhm,fwhmG,xw)	
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_LorentzXGaussian(areaP,xpos+spinorbit,fwhm,fwhmG,xw)
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])   	 
		yw += Func_LorentzXGaussian(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,fwhmG,xw)
		if(branch!=0)
			yw += branch*Func_LorentzXGaussian(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,fwhmG,xw)
		endif
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	OSCILLATOR
function SFunc_Oscillator(para, issat,  isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP,xpos,fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG 	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])

//----reference peak
yw = Func_Oscillator(areaP,xpos,fwhm,xw)	
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_Oscillator(areaP,xpos+spinorbit,fwhm,xw)
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])  	 
		yw += Func_Oscillator(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,xw)
		if(branch!=0)
			yw += branch*Func_Oscillator(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,xw)	
		endif
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//---possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif	

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	DONIACH-SUNJIC
function SFunc_Doniach_Sunjic(para, issat,  isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP, xpos, fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
variable asymm 
wave /d satelittes = satelittes
variable ss, fwhm_sat


//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])
//----specific parameters
asymm	= min(abs(para[11]),0.9999)

//----reference peak
yw = Func_Doniach_Sunjic(areaP,xpos,fwhm,asymm,xw)	
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_Doniach_Sunjic(areaP,xpos+spinorbit,fwhm,asymm,xw)
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2]) 	 			
		yw += Func_Doniach_Sunjic(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,asymm,xw)
		if(branch!=0)
			yw += branch*Func_Doniach_Sunjic(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,asymm,xw)
		endif	
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//---possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif	

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	GENERALIZED DONIACH-SUNJIC
function SFunc_Gene_Doniach_Sunjic(para, issat,  isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP, xpos, fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
variable  gam, mu
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG 	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])
//----specific parameters
gam		= max(abs(para[11]),0.0001)
mu		= max(abs(para[12]),0.0001)

//----reference peak
yw = Func_Gen_Doniach_Sunjic(areaP,xpos,fwhm,gam,mu,xw)	
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_Gen_Doniach_Sunjic(areaP,xpos+spinorbit,fwhm,gam,mu,xw)	
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2]) 	 			
		yw += Func_Gen_Doniach_Sunjic(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,gam,mu,xw)
		if(branch!=0)
			yw += branch*Func_Gen_Doniach_Sunjic(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,gam,mu,xw)
		endif	
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//---possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif	

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	MAHAN 
function SFunc_Mahan(para, issat,  isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP, xpos, fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
variable bet, ksi
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG 	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])
//----specific parameters
bet		= max(abs(para[11]),0.0001)
ksi		= max(abs(para[12]),0.0001)

//----reference peak
yw += Func_Mahan(areaP,xpos,bet,ksi,xw)	
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_Mahan(areaP,xpos+spinorbit,bet,ksi,xw)	
endif
//---Lorentzian broadening
peak_convolution(xw,yw,"lorentz",fwhm)

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	duplicate /o yw ywsat
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		ywsat = Func_Mahan(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],bet,ksi,xw)
		if(branch!=0)
			ywsat += branch*Func_Mahan(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],bet,ksi,xw)
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])  	 	 
		peak_convolution(xw,ywsat,"lorentz",fwhm_sat)
		yw += ywsat
	endfor											
	killwaves /z ywsat
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//---possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif	

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	POST-COLLISION INTERACTION
function SFunc_PCI(para, issat, isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP,xpos,fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
variable asymm
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG 	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])
//----specific parameters
asymm	= abs(para[11])

//----reference peak
yw = Func_PCI(areaP,xpos,fwhm,asymm,xw)	
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_PCI(areaP,xpos+spinorbit,fwhm,asymm,xw)		
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])   	 			
		yw += Func_PCI(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,asymm,xw)
		if(branch!=0)
			yw += branch*Func_PCI(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,asymm,xw)
		endif	
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//---possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif	

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	ASYMMETRIC VOIGT
function SFunc_asymVoigt(para, issat,  isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP, xpos, fwhmG, branch, spinorbit
variable S, B, C, D, gap
variable  fwhm1, fwhm2
wave /d satelittes = satelittes
variable ss, fwhm_sat1, fwhm_sat2

		
//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm1	= abs(para[2])
fwhmG 	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])
//----specific parameters
fwhm2 = abs(para[11])

//----reference peak
yw 	= Func_asymLorentz(areaP,xpos,fwhm1,fwhm2,xw)
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_asymLorentz(areaP,xpos+spinorbit,fwhm1,fwhm2,xw)
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat1 = abs(fwhm1 - satelittes[0][issat-1][2]/2. + satelittes[ss][issat-1][2]/2.)  	 		
		fwhm_sat2 = abs(fwhm2 - satelittes[0][issat-1][2]/2. + satelittes[ss][issat-1][2]/2.)  	
		yw += Func_asymLorentz(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat1,fwhm_sat2,xw)
		if(branch!=0)
			yw += branch*Func_asymLorentz(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat1,fwhm_sat2,xw)
		endif								
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//---possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif		

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	ASYMMETRIC PSEUDO-VOIGT
function SFunc_asymPsVoigt(para, issat, isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP,xpos,fwhm, branch, spinorbit
variable S, B, C, D, gap
variable mix, asymm, sigmshift 
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
mix 		= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])
//----specific parameters
asymm	= abs(para[11])
sigmshift = para[12]

//----reference peak
yw = Func_asymPseudoVoigt(areaP,xpos,fwhm,mix,asymm,sigmshift,xw)
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_asymPseudoVoigt(areaP,xpos+spinorbit,fwhm,mix,asymm,sigmshift,xw)		
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])   	 	 
		yw += Func_asymPseudoVoigt(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,mix,asymm,sigmshift,xw)
		if(branch!=0)
			yw += branch*Func_asymPseudoVoigt(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,mix,asymm,sigmshift,xw)
		endif
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// FERMI STEP FUNCTIONS
function SFunc_FermiStep(para,  xw, yw)

wave /d para, xw, yw
variable int, fwhm_Erf,xpos, fwhm_atan,fwhm_kt, media=0

int			= abs(para[0])	// height of the Fermi edge
xpos		= para[1]
fwhm_Erf	= abs(para[2])
fwhm_atan	= abs(para[3])
fwhm_kt		= abs(para[4])

if (fwhm_Erf+fwhm_atan+fwhm_kt !=0)

if (fwhm_Erf  !=0)
	// this is equivalent to convolving a delta step with a Gaussian fwhm (fwhm@ .12- .88 height)
	yw+=int*(1-erf((xpos-xw)/(fwhm_Erf/1.6651092223153955)))/2														
	media+=1	
endif

if (fwhm_atan  !=0)
	// with this formula, it has the same slope in xpos  compared to erf with equal fwhm (fwhm@ .19 - .81 height)
	yw+=int*(0.5-atan((xpos-xw)/fwhm_atan/0.33883037580155249)/pi)															
	media+=1
endif

if (fwhm_kt  !=0)
	// cost. of Boltzmann eV.K-1; fwhm_kt It is the temperature in Kelvin
	variable kb=8.6173303e-5					
	yw+=int/(1+exp((xpos-xw)/(kb*fwhm_kt)))
	media+=1
endif

yw/=media

else		
	//in this case I have a delta of dirac, type NB: in xpos it is zero!														
	yw=int*(1-sign(xpos-xw))/2	
	
endif

//------Exponential decay
variable lambda
lambda = para[5]
if (lambda!=0)
	yw*=exp(-(xw-xpos)/lambda)
endif

//------Poly. decay
yw*=1+para[6]*(xw-xpos)+para[7]*(xw-xpos)^2+para[8]*(xw-xpos)^3

end

//===========================================
// WORK FUNCTION STEP
function SFunc_WorkFunctionStep(para,  xw, yw)

wave /d para, xw, yw
variable int, fwhm_Erf,xpos, fwhm_atan, media=0

int			= abs(para[0])	// height of the work function step
xpos		= para[1]
fwhm_Erf	= abs(para[2])
fwhm_atan	= abs(para[3])

if (fwhm_Erf+fwhm_atan !=0)

if (fwhm_Erf  !=0)
	// this is equivalent to convolving a delta step with a Gaussian fwhm (fwhm@ .12- .88 height)
	yw+=int*(1-erf((xw-xpos)/(fwhm_Erf/1.6651092223153955)))/2														
	media+=1	
endif

if (fwhm_atan  !=0)
	// with this formula, it has the same slope in xpos  compared to erf with equal fwhm (fwhm@ .19 - .81 height)
	yw+=int*(0.5-atan((xw-xpos)/fwhm_atan/0.33883037580155249)/pi)															
	media+=1
endif

yw/=media

else		
	//in this case I have a delta of dirac, type NB: in xpos it is zero!														
	yw=int*(1-sign(xw-xpos))/2	
	
endif

//------Exponential decay
variable lambda
lambda = para[4]
if (lambda!=0)
	yw*=exp(-(xpos-xw)/lambda)
endif

//------Poly. decay
yw*=1+para[5]*(xw-xpos)+para[6]*(xw-xpos)^2+para[7]*(xw-xpos)^3

end

//===========================================
// 	FANO
function SFunc_Fano(para, issat,  isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP, xpos, fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
variable asymm
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG 	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])
//---- reference peak
asymm	= max(min(para[11],0.9999),-0.9999)

//----reference peak
yw = Func_Fano(areaP,xpos,fwhm,asymm,xw)
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_Fano(areaP,xpos+spinorbit,fwhm,asymm,xw)
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])  	 	 
		yw += Func_Fano(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,asymm,xw)
		if(branch!=0)
			yw += branch*Func_Fano(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,asymm,xw)
		endif
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//-- possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif	

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end


//===========================================
//	 PLASMON
function SFunc_Plasmon(para, issat, isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP, xpos, fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
variable plasmon, fwhm_plas, damp_a, damp_b
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG 	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])
//----specific parameters
plasmon 	= abs(para[11])
fwhm_plas	= abs(para[12])
damp_a		= max(min(abs(para[13]),0.9999),0.0001)
damp_b		= max(min(abs(para[14]),0.9999),0.0001)
		
//----reference peak
yw 	= Func_Plasmon(areaP,xpos,fwhm,plasmon,fwhm_plas,damp_a,damp_b,xw)
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_Plasmon(areaP,xpos+spinorbit,fwhm,plasmon,fwhm_plas,damp_a,damp_b,xw)	
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2])  	 		
		yw += Func_Plasmon(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm_sat,plasmon,fwhm_plas,damp_a,damp_b,xw)
		if(branch!=0)
			yw += branch* Func_Plasmon(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm_sat,plasmon,fwhm_plas,damp_a,damp_b,xw)	
		endif								
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//---possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif			

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end

//===========================================
// 	BAND BENDING LORENTZ
function SFunc_BandBendingLorentz(para, issat,  isbkr, xw, yw, ywbkr)

wave /d para, xw, yw, ywbkr
variable issat, isbkr
variable areaP, xpos, fwhm, fwhmG, branch, spinorbit
variable S, B, C, D, gap
variable Vbb, Ww, Lambda
wave /d satelittes = satelittes
variable ss, fwhm_sat

//----general parameters
areaP	= para[0]
xpos	= para[1]
fwhm	= abs(para[2])
fwhmG 	= abs(para[3])
branch	= abs(para[4])
spinorbit	= abs(para[5])
S		= abs(para[6])
B		= abs(para[7])
C		= para[8]
D		= abs(para[9])
gap 		= abs(para[10])
//----specific parameters
Vbb			= para[11]
Ww			= max(abs(para[12]),0.0001)
Lambda		= max(abs(para[13]),0.0001)

//----reference peak
yw = Func_Band_Bending_Lorentz(areaP,xpos,fwhm,Vbb,Ww,Lambda,xw)	
// -----spin-orbit split peak
if(branch!=0)
	yw += branch*Func_Band_Bending_Lorentz(areaP,xpos+spinorbit,fwhm,Vbb,Ww,Lambda,xw)		
endif

//----satellites : sum
if(issat>0)
	yw *= satelittes[0][issat-1][1]
	for(ss=1;ss<dimsize(satelittes,0);ss+=1)
		if(satelittes[ss][issat-1][1]==0.) 
		 continue	
		endif
		fwhm_sat = abs(fwhm - satelittes[0][issat-1][2] + satelittes[ss][issat-1][2]) 	
		yw += Func_Band_Bending_Lorentz(areaP*satelittes[ss][issat-1][1],xpos-satelittes[ss][issat-1][0],fwhm,Vbb,Ww,Lambda,xw)
		if(branch!=0)
			yw += branch*Func_Band_Bending_Lorentz(areaP*satelittes[ss][issat-1][1],xpos+spinorbit-satelittes[ss][issat-1][0],fwhm,Vbb,Ww,Lambda,xw)	
		endif	 			
	endfor											
endif

//----satellites : convolution
if(issat<0)
	peak_convolution(xw,yw,"satellites",1.,issat=abs(issat))
endif

//---possible individual Gaussian broadening
if(fwhmG!=0)
	peak_convolution(xw,yw,"gaussian",fwhmG)
endif	

//----integral background
integral_background(xw,yw,ywbkr,isbkr,S,B,C,D,gap)

end






