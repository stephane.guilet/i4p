// Module : i4pfit_around
// Author : R�mi LAZZARI (lazzari@insp.jussieu.fr) based on initial work of Francesco BRUNO (bruno@tasc.infm.it) 
// Date : 2019
// Usage : Fit of XPS (see manual)


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Table of contents
// LOAD
// BATCH FITS
// CLEAN
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#pragma rtGlobals = 3		
#pragma ModuleName = i4pfit_around

//===========================================
static function load_in_i4pfit()

// The list of waves in the current datfolder
string saveDF = GetDataFolder(1)
string listofwaves
listofwaves = "Load from top graph;" + wavelist("*", ";", "" )

// the datafolder of variables
newdatafolder /O/S root:I4P
newdatafolder /O/S :Variables

// choice of the parameters
string tracename
prompt tracename, "Wave to be loaded", popup listofwaves
string errorbartype = strvarordefault("gerrorbartype","from interpolation")
prompt errorbartype, "Select the type of error bars", popup "from interpolation;square-root of intensity;constant"
variable np = numvarordefault("gnp",7)
prompt np, "Number of points of the moving windows"
variable deg  = numvarordefault("gdeg",2)
prompt deg, "Interpolation polynom order"
variable const_error  = numvarordefault("gconst_error",0.05)
prompt const_error "Constant error bar"
variable remap = numvarordefault("gremap",0)
prompt remap, "Remapping binding energy step"
string helpstring
helpstring   =  "1- Select wave\r"
helpstring +=  "2- Select the type of error bars\r"
helpstring +=  "3- Enter the odd number of points of moving windows\r"
helpstring +=  "4- Enter the order of the fit polynom\r"
helpstring +=  "5- Enter the constant error bar\r"
helpstring +=  "6- Enter the energy step for data remapping (0 = no remap)"
doprompt /HELP = helpstring "I4P : Load in I4PFit", tracename,errorbartype, np, deg, const_error, remap
if(V_Flag)
	setdatafolder saveDF
	print "==> Error in the input parameters !"
	return 0
endif

// save the data
string /G 	gerrorbartype = errorbartype
variable /G 	gnp			= np
variable /G 	gdeg		= deg
variable /G 	gconst_error	= const_error
variable /G 	gremap		= remap

// go back to current datafolder
setdatafolder saveDF

// tests
np = 2*floor(np/2) + 1
if(deg>=np)
	beep
	doalert /T="WARNING" 0, "ERROR \r\rThe degree of the fit polynom should not be smaller or equal to the number of points !"
	return 0	
endif
if(deg<=1)
	beep
	doalert /T="WARNING" 0, "ERROR \r\rThe degree of the fit polynom should not be smaller than one !"
	return 0	
endif

string wave2load, wavefolder, wavecomment 
If(cmpstr(tracename,"Load from top graph")==0)
	// load from graph : get the name of graph and of the wave
	string listofgrahs = winlist("*", ";","WIN:1")
	if(itemsinlist(listofgrahs)==0)
		beep
		doalert /T="WARNING" 0, "ERROR \r\rNo graph !"
		return 0	
	endif
	string topgraph = stringfromlist(0,listofgrahs,";")
	dowindow /F $topgraph 
	// check if cursors are on the curve
	if(stringmatch(csrinfo(A),"")==1 || stringmatch(csrInfo(B),"")==1)
		beep
		doalert /T="WARNING" 0, "ERROR \r\rTwo cursors must be on the graph to define the spectrum to be cropped !"
		return 0				
	endif	
	// get the waves
	wave2load = nameofwave(csrwaveref(A))
	wavecomment = note(csrwaveref(A))
	wavefolder = getwavesdatafolder(csrwaveref(A),1)
	setdatafolder wavefolder
	duplicate/O/R=[pcsr(A),pcsr(B)] csrwaveref(A)  data_counts
	if(cmpstr(csrxwave(A),"")!=0)
		duplicate/O/R=[pcsr(A),pcsr(B)]  csrxwaveref(A) data_be
	else
		duplicate/O data_counts data_be	
		data_be = min(xcsr(A),xcsr(B)) + p*deltax(data_counts)
	endif
else
	// load from current data folder
	wave2load = tracename
	wavecomment = note($tracename)
	wavefolder = getwavesdatafolder($tracename,1)		
	duplicate/O  $tracename data_counts
	duplicate/O  data_counts data_be
	data_be = dimoffset(data_counts,0) + p*deltax(data_counts)
endif

// check if I4PFit is launched
string listofwindows = winlist("*", ";","")
if(datafolderexists("root:I4P:I4Pfit")!=1 || whichlistitem("fit_graph_i4pfit",listofwindows)==-1 || whichlistitem("manager_panel_i4pfit",listofwindows)==-1 )
	start_i4pfit()
	doupdate
 endif

// warn the user
If(cmpstr(tracename,"Load from top graph")==0)
	print "==> Load from graph : ", topgraph
endif 
print "==> Loaded data : " + wave2load + " from [" + num2str(data_be[0]) + ";" + num2str(data_be[numpnts(data_be)-1]) +"] eV"

// define error bars and load the waves
func_load_in_i4pfit(errorbartype,np,deg,const_error,remap,data_be,data_counts,wave2load,wavefolder,wavecomment)

// clean 
killwaves /z data_be, data_counts

end

//===========================================
static function func_load_in_i4pfit(errorbartype,np,deg,const_error,remap,be,counts,name,folder,comment)

string errorbartype
variable np, deg, const_error, remap
wave /d be, counts
string name, folder, comment

// reset the scale and comments of data
setscale/P x, 0, 1, "",  be
setscale d 0,0,"",  be
note /K be
setscale/P x, 0, 1, "",  counts
setscale d 0,0,"",  counts
note /K counts

// wave of error bars
duplicate /O counts err
err = 0.
strswitch(errorbartype)
case "square-root of intensity":
	err = sqrt(abs(counts))		
break
case "constant":
	err = abs(const_error)
break
case "from interpolation":
	variable npt = numpnts(be)
	variable nw = floor(np/2)
	make/d/o/n=(np) win, win_be, win_res
	variable ii
	for(ii=nw;ii<npt-nw;ii+=1)
		win_be  = be[ii-nw+p]
		win 	= counts[ii-nw+p]
		CurveFit /K={be[ii]} /M=0 /N=1 /NTHR=0  /ODR=1 /Q=1 /W=2  Poly_XOffset deg+1,  win  /A=0  /R=win_res /X=win_be  
		err[ii] 	= max( abs(win_res[nw]) , sqrt(abs(counts[ii])) )
	endfor
	for(ii=0;ii<nw;ii+=1)
		err[ii] 	= err[nw]
	endfor	
	for(ii=npt-nw;ii<npt;ii+=1)
		err[ii] 	= err[npt-nw-1]
	endfor		
	killwaves /z win, win_res, win_be
	killwaves /z W_coef, M_Jacobian, W_sigma, W_fitConstants
break
endswitch

// load the data into I4Pfit 
data_load(be,counts,err,remap,name,folder,comment)

// clean
killwaves /z err

end



//===========================================
static function fit_batch()

//---parameters
string I4Pfitfolder = "root:I4P:I4Pfit"

//------ clean waves (to avoid an unknown bug ?)
killwaves /z fitted_value, peak_carac, reliance

//------ check that I4Pfit is launched
string listofwindows = winlist("*", ";","")
if(datafolderexists(" root:I4P:I4Pfit")!=1 || whichlistitem("fit_graph_i4pfit",listofwindows)==-1 || whichlistitem("manager_panel_i4pfit",listofwindows)==-1 )
	beep
	doalert /T="WARNING" 0, "ERROR \r\rI4Pfit is not launched !"
	return 0	
 endif

//------ the datafolder and variables
string saveDF = GetDataFolder(1)
newdatafolder /O/S root:I4P
newdatafolder /O/S :Variables

//------ choice of the parameters
string tracename =  strvarordefault("gtracename","*spectrum*") 
prompt tracename, "Enter core level names to be fitted (e.g. * or *Ag or *Ag* or Ag 3d;Ti 2p)"
string errorbartype = strvarordefault("gerrorbartype","from interpolation")
prompt errorbartype, "Select the type of error bars", popup "from interpolation;square-root of intensity;constant"
variable np =  numvarordefault("gnp",7)
prompt np, "Number of points of the moving windows"
variable deg = numvarordefault("gdeg",2)
prompt deg, "Interpolation polynom order"
variable const_error = numvarordefault("gconst_error",0.05)
prompt const_error "Constant error bar"
variable remap =  numvarordefault("gremap",0)
prompt remap, "Remapping binding energy step"
string startfitfrom = strvarordefault("gstartfitfrom","previous fit")
prompt startfitfrom, "Start fit from", popup "previous fit;initial guess"
string plotfit = strvarordefault("gplotfit","no")
prompt plotfit, "Plot fits", popup "no;yes"
string plotresult  = strvarordefault("gplotresult","no")
prompt plotresult, "Plot results", popup "no;yes"
string helpstring
helpstring   =  "1- Select list of waves to be fitted\r"
helpstring +=  "2- Select the type of error bars\r"
helpstring +=  "3- Enter the odd number of points of moving windows\r"
helpstring +=  "4- Enter the order of the fit polynom\r"
helpstring +=  "5- Enter the constant error bar\r"
helpstring +=  "6- Enter the energy step for data remapping (0 = no remap)"
helpstring +=  "7- Plot fits and results ?"
doprompt /HELP = helpstring "I4P : Batch fit with I4PFit", tracename,errorbartype, np, deg, const_error, remap, startfitfrom, plotfit, plotresult
if(V_Flag)
	setdatafolder saveDF
	print "==> Error in the input parameters !"
	return 0
endif

//------ save the data
string /G		gtracename = tracename
string /G 	gerrorbartype = errorbartype
variable /G 	gnp			= np
variable /G 	gdeg		= deg
variable /G 	gconst_error	= const_error
variable /G 	gremap		= remap
string /G 	gstartfitfrom	= startfitfrom
string /G 	gplotfit		= plotfit
string /G 	gplotresult	= plotresult

//------ go back to current datafolder
setdatafolder saveDF

//------- tests
np = 2*floor(np/2) + 1
if(deg>=np)
	beep
	doalert /T="WARNING" 0, "ERROR \r\rThe degree of the fit polynom should not be smaller or equal to the number of points !"
	return 0	
endif
if(deg<=1)
	beep
	doalert /T="WARNING" 0, "ERROR \r\rThe degree of the fit polynom should not be smaller than one !"
	return 0	
endif

//------ get the data folder and the list of waves to be fitted
string datafolder = getdatafolder(1)
print "==> Data folder : ", datafolder
print " "
string listofwaves = wavelist(tracename, ";", "" )
variable nwaves   = itemsinlist(listofwaves,";")
if(nwaves==0)
	beep
	doalert /T="WARNING" 0, "ERROR \r\rNo wave to be fitted with names containing : " +  tracename + " !"
	return 0	
endif

//------ get the number of fitted parameters
setdatafolder I4Pfitfolder
wave /d para_fit_value	= para_fit_value
wave /d para_sigma	= para_sigma
wave /d peak_activate	= peak_activate
NVAR N_peaks_max 	= N_peaks_max
NVAR reduced_chi2   = reduced_chi2
NVAR RBragg		= RBragg
NVAR RBragg_exp	= RBragg_exp
variable npeak 		  = sum(peak_activate)
variable npara_fit_value = numpnts(para_fit_value)
variable ii, kk
kk = 0
for(ii=0;ii<npara_fit_value;ii+=1)
	if(para_fit_value[ii]!=0)
 		kk+=1
 	endif
 endfor

//------ Killwindows in case of
string stringwindowsname, command
for (ii=0; ii<nwaves; ii+=1)
	stringwindowsname = "DATA_FIT_" + num2str(ii)
	if(whichlistitem(stringwindowsname,listofwindows)!=-1)
		command = "killwindow " + stringwindowsname
		execute command
	endif
endfor
if(whichlistitem("peak_carac_area",listofwindows)!=-1)
	killwindow  peak_carac_area
endif
if(whichlistitem("peak_carac_position",listofwindows)!=-1)
	killwindow  peak_carac_position
endif
if(whichlistitem("peak_carac_fwhm",listofwindows)!=-1)
	killwindow  peak_carac_fwhm
endif
	
//------ make the waves to store the fitted data
setdatafolder datafolder
make /d/o/n=(nwaves,kk,2)  fitted_value  = 0	// fitted values : [#waves; #param; 0=value,1=sigma]
note fitted_value, " "
note fitted_value, "# Row      : spectrum number"
note fitted_value, "# Column : parameter number "
note fitted_value, "# Chunck : [0]=value, [1]=error bar"
make /d/o/n=(nwaves,4,npeak) 	peak_carac = 0	// peak characteristics : [#waves; 0=areaP,1=maxP,2=x0,3=fwhm ;#peak]
note peak_carac, " "
note peak_carac, "# Row      : spectrum number"
note peak_carac, "# Column : peak characteristics ( [0]=areaP, [1]=maxP, [2]=x0, [3]=fwhm) "
note peak_carac, "# Chunck : peak number "
make /d/o/n=(nwaves,3) 		reliance 	    = 0	// reliance : [#waves; 0=chi2,1=RBragg,2=RBragg_exp]
note reliance, " "
note reliance, "# Row      : spectrum number"
note reliance, "# Column : peak characteristics ( [0]=chi2, [1]=RBragg, [2]=RBragg_exp) "

//------ loop over waves to be fitted
string datafolder2fit, stringpeak, stringwave2fit, stringwave2fitcomment
variable areaP,maxP,x0,fwhm
variable jj
for (ii=0; ii<nwaves; ii+=1)
	//------ get the waves : counts and be
	setdatafolder datafolder
	stringwave2fit = stringfromlist(ii, listofwaves, ";")
	stringwave2fitcomment = note($stringwave2fit)
	print "==> Fit wave : ", stringwave2fit
	duplicate /o $stringwave2fit counts
	duplicate /o counts be	
	be = dimoffset(counts,0) + p*deltax(counts)
	//------ load in I4Pfit
	func_load_in_i4pfit(errorbartype,np,deg,const_error,remap,be,counts,stringwave2fit,datafolder,stringwave2fitcomment)
	//------ clean waves
	killwaves /z counts, be
	//------ launch the fit
	FIT_ButtonProc("button_fit")
	//------store the values of the fitted parameters
	setdatafolder I4Pfitfolder
	kk = 0
	for(jj=0;jj<npara_fit_value;jj+=1)
		if(para_fit_value[jj]!=0)
			fitted_value[ii][kk][0] = para_fit_value[jj]
			fitted_value[ii][kk][1] = para_sigma[jj]
			kk+=1
		endif
	endfor
	//------ strore the peak characteristics
	kk = 0
	for(jj=1;jj<=N_peaks_max;jj+=1)
		 if(peak_activate[jj]==1)
		 	stringpeak = "peak"+num2str(jj)
		 	wave /d wavepeak	= $stringpeak 
		 	wave /d data_be 	= data_be
			peakparameters(data_be,wavepeak,areaP,maxP,x0,fwhm)
			peak_carac[ii][0][kk] = areaP
			peak_carac[ii][1][kk] = maxP
			peak_carac[ii][2][kk] = x0
			peak_carac[ii][3][kk] = fwhm	
			kk +=1
		 endif
	endfor
	//------ strore reliance factors
	reliance[ii][0] = reduced_chi2
	reliance[ii][1] = RBragg
	reliance[ii][2] = RBragg_exp
	setdatafolder datafolder 
	//------ update the windows
	doupdate
	//------ copy the I4Pfit folder into the data folder
	// datafolder2fit = datafolder + "I4PFitReport_" + "'" +stringwave2fit  + "'"
	datafolder2fit = datafolder +  "'" +  "I4PFitReport_" + stringwave2fit  + "'"
	killdatafolder /z $datafolder2fit
	duplicatedatafolder $I4Pfitfolder, $datafolder2fit
	//------ update fitted values for the next one
	If(cmpstr(startfitfrom,"previous fit")==0)
		UPDATEVALUES_ButtonProc("button_updatevalues")
	endif
endfor

//------ plot all the fits
If(cmpstr(plotfit,"yes")==0)
	//------ Plot
	//variable xmin, xmax
	//getaxis /W=fit_graph_i4pfit /Q bottom
	//xmin  = V_min
	//xmax = V_max
	//variable ymin, ymax
	//getaxis /W=fit_graph_i4pfit /Q left
	//ymin  = V_min
	//ymax = V_max	
	for (ii=0; ii<nwaves; ii+=1)
		stringwave2fit = stringfromlist(ii, listofwaves, ";")
		//datafolder2fit = datafolder + "I4PFitReport_" + "'" + stringwave2fit + "'"
		datafolder2fit = datafolder +  "'" +  "I4PFitReport_" + stringwave2fit  + "'"
		setdatafolder datafolder2fit
		command =  "plot_i4pfit()"
		execute command
		stringwindowsname =  "DATA_FIT_" + num2str(ii)
		command = "dowindow /C " +  stringwindowsname
		execute command
		command = "dowindow /T " + stringwindowsname +  "\"DATA FIT : " + datafolder + stringwave2fit + "\""
		execute command
		//setaxis  /W=$stringwindowsname bottom, xmin, xmax	
		//setaxis  /W=$stringwindowsname top, xmin, xmax
		//setaxis  /W=$stringwindowsname left, ymin, ymax	
		setdatafolder datafolder
	endfor
endif

//------ plot all the results
If(cmpstr(plotresult,"yes")==0)
	setdatafolder datafolder
	//--- peak areas	
	plot_results_fit_batch(0,"peak_carac_area","PEAK AREA","Spectrum scale ?","Peak area")
	//--- peak positions
	plot_results_fit_batch(2,"peak_carac_position","PEAK POSITION","Spectrum scale ?","Peak position")
	//--- peak fwhm
	plot_results_fit_batch(3,"peak_carac_fwhm","PEAK FWHM","Spectrum scale ?","Peak FWHM")	
endif

//------ go back to the inital data folder
setdatafolder datafolder

end

//===========================================
static function plot_results_fit_batch(jj,winame,wintitle,xtitle,ytitle)

variable jj
string winame, wintitle, xtitle, ytitle
variable kk
wave /d peak_carac = peak_carac
string command

//---- create the scale in case of
string listofwaves = wavelist("*", ";", "" )
if(FindListItem("scale_data",listofwaves)==-1)
	make /d/o/n=(dimsize(peak_carac,0)) scale_data
	scale_data = p
endif
wave /d scale_data = scale_data


//-----  display the data
display peak_carac[][jj][0] vs scale_data
for(kk=1;kk<dimsize(peak_carac,2);kk+=1)
	 appendtograph peak_carac[][jj][kk] vs scale_data
endfor
command = "dowindow /C " +  winame
execute command
command = "dowindow /T " +  winame + " \"" + wintitle + "\""
execute command

//----- plot appeareance
setcolor()
ModifyGraph standoff=0
ModifyGraph mirror=2
ModifyGraph tick=2,minor=1
ModifyGraph fSize=16,font="Arial"
Label bottom "\\Z20 "+xtitle 
Label left "\\Z20 "+ ytitle 
ModifyGraph mode=4,marker=19
Legend/C/N=text0/F=0/A=MC
Legend/C/N=text0/J/B=1

end


//===========================================
static function clean_i4pfit()

string listofwindows = winlist("*", ";","")
if(whichlistitem("fit_graph_i4pfit",listofwindows)!=-1)
	killwindow fit_graph_i4pfit
endif
if(whichlistitem("manager_panel_i4pfit",listofwindows)!=-1)
	killwindow manager_panel_i4pfit
endif
if(whichlistitem("fit_report",listofwindows)!=-1)
	killwindow fit_report
endif
killdatafolder /z  root:I4P:I4Pfit:

end

