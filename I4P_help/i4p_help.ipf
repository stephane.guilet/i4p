// Module : i4p_help
// Author :  R�mi LAZZARI (lazzari@insp.jussieu.fr)
// Date : 2019
// Usage : help for I4P macro


#pragma rtGlobals=3		
#pragma ModuleName = i4phelp



//===========================================
//--usage : launch the pdf manual
Static Function help_i4p()

string command
command =  "\"C:\\Users\\remil\\Documents\\WaveMetrics\\Igor Pro 6 User Files\\User Procedures\\I4P\\i4p_help\\start_manual.bat\""

executescripttext /B/Z command


End

//===========================================
//--usage : launch a notebook with a list of available commands
Static Function help_command_i4p()

//-------------do not update
silent -1; pauseupdate

//-------------check if the window exists
dowindow /F command_list
if (v_flag==0)
	newnotebook/n=command_list/F=0 as "I4P : COMMAND LIST"
endif

//-------------header
string textstring
textstring=""
notebook command_list, selection={startOffile, endOfFile },  fSize=13, text=textstring
textstring="============================================================\r"
notebook command_list, text=textstring
textstring="  LIST OF AVAILABLE COMMANDS IN I4P \r"
notebook command_list, text=textstring
textstring="============================================================\r"
notebook command_list, text=textstring
textstring="\r\r"
notebook command_list, text=textstring

//-------------list of commands
textstring="============================================================\r"
notebook command_list, text=textstring
textstring="==> Calculate the background substracted area of a core level (unit = cps.eV)\r\r"
notebook command_list, text=textstring
textstring="Area_BGS(backgroundtype,start,stop,spectrum_name,keepbackground,[S,B,C,D,Gap])\r\r"
notebook command_list, text=textstring
textstring="string backgroundtype : type of background  (Shirley; Tougaard; Tougaard_fixed; ShirleyTougaard_user; Linear; None)\r"
notebook command_list, text=textstring
textstring="variable start : start binding energy (eV) \r"
notebook command_list, text=textstring
textstring="variable stop : stop binding energy (eV)\r"
notebook command_list, text=textstring
textstring="string spectrum_name : name of the wave \r"
notebook command_list, text=textstring 
textstring="variable keepbackground : 1 = keep intermediate waves; 0 = do not\r"
notebook command_list, text=textstring 
textstring="variable S : Shirley background factor (eV-1); optional (in case of ShirleyTougaard_user) \r"
notebook command_list, text=textstring
textstring="variable B, C, D : Tougaard background parameters (eV^2); optional  (in case of ShirleyTougaard_user) \r"
notebook command_list, text=textstring
textstring="variable Gap : Gap for Tougaard background (eV); optional  (in case of ShirleyTougaard_user) \r"
notebook command_list, text=textstring
textstring="example : print Area_BGS(\"Shirley\",525,540,\"O1s\",0)\r"
notebook command_list, text=textstring
textstring="example : print Area_BGS(\"ShirleyTougaard_user\",525,540,\"O1s\",0,S=0.01,B=3006,C=1643,D=0,Gap=0)\r\r"
notebook command_list, text=textstring

textstring="============================================================\r"
notebook command_list, text=textstring
textstring="==> Correct from analyzer transmission function\r\r"
notebook command_list, text=textstring
textstring="CorrectFromAnalyzerTF(spectrum_name,[exponent,generateTF])\r\r"
notebook command_list, text=textstring
textstring="string spectrum_name: name of the wave\r"
notebook command_list, text=textstring
textstring="variable exponent : exponent of the transmission function in FAT/CAE  (optional, otherwise=1.0) \r"
notebook command_list, text=textstring
textstring="variable generateTF : 1 = generate transmission function wave; 0 = do not (optional, otherwise=0)\r"
notebook command_list, text=textstring
textstring="example : CorrectFromAnalyzerTF(\"O1s\",exponent=1.0,generateTF=0) \r\r"
notebook command_list, text=textstring

textstring="============================================================\r"
notebook command_list, text=textstring
textstring="==> Calculate the photo-ionization cross section  (unit =barn)\r\r"
notebook command_list, text=textstring
textstring="CoreLevel2PICS(TagElement,Atomic_level,Xray_source,[PrintInfo,Angle])\r\r"
notebook command_list, text=textstring
textstring="string TagElement: element symbol\r"
notebook command_list, text=textstring
textstring="string Atomic_level: considered core-level\r"
notebook command_list, text=textstring
textstring="string Xray_source: X-ray source (Al, Mg)\r"
notebook command_list, text=textstring
textstring="variable PrintInfo : 1= print the values from database; 0= don't (optional; otherwise no printing)\r"
notebook command_list, text=textstring
textstring="variable Angle : angle (deg) between X-ray source and detector (optional; otherwise Angle = 54.73 deg)\r"
notebook command_list, text=textstring
textstring="example : print CoreLevel2PICS(\"C\",\"1s\",\"Al\",PrintInfo=0,Angle=75)\r\r"
notebook command_list, text=textstring

textstring="============================================================\r"
notebook command_list, text=textstring
textstring="==> Find the closest photoemission or Auger lines to a given binding energy\r\r"
notebook command_list, text=textstring
textstring="BE2Peaks([Xray_source,BE,dBE])\r\r"
notebook command_list, text=textstring
textstring="string Xray_source: X-ray source (Al, Mg); optional, otherwise that of the trace markerd by cursor A on top graph\r"
notebook command_list, text=textstring
textstring="variable BE : searched binding energy; optional, otherwise cursor A position on top graph\r"
notebook command_list, text=textstring
textstring="variable dBE : energy windows of search; optional, otherwise 1 eV\r"
notebook command_list, text=textstring
textstring="example : BE2Peaks(Xray_source=\"Al\",BE=285,dBE=1.5) \r\r"
notebook command_list, text=textstring

textstring="============================================================\r"
notebook command_list, text=textstring
textstring="==> Calculate the inelastic mean free path from universal formula and material database  (unit =�)\r\r"
notebook command_list, text=textstring
textstring="print Material2IMFP(typeIMFP,material,E,[Xray_source])\r\r"
notebook command_list, text=textstring
textstring="variable typeIMFP : 0=TPP2M non-relativistic; 1= TPP2M relativistic; 2= Gries\r"
notebook command_list, text=textstring
textstring="string material : material name\r"
notebook command_list, text=textstring
textstring="variable E : kinetic/binding energy (eV) \r"
notebook command_list, text=textstring
textstring="variable Xray_source : used X-ray source (Al,Mg); optional. If present, E is the binding energy. Otherwise, kinetic energy.\r"
notebook command_list, text=textstring
textstring="example : print Material2IMFP(0,\"Ag\",368,Xray_source=\"Al\")\r\r"
notebook command_list, text=textstring

textstring="============================================================\r"
notebook command_list, text=textstring
textstring="==> Calculate the transport mean free path from IMFP universal formula and Jablonski's databases (unit =�)\r\r"
notebook command_list, text=textstring
textstring="print Material2TRMFP(typeIMFP,typeTRMFP,material,E,[Xray_source])\r\r"
notebook command_list, text=textstring
textstring="variable typeIMFP : 0=TPP2M non-relativistic; 1= TPP2M relativistic; 2= Gries\r"
notebook command_list, text=textstring
textstring="variable typeTRMFP : 1=database 1; 2=database 2; 3=database 3\r"
notebook command_list, text=textstring
textstring="string material : material name\r"
notebook command_list, text=textstring
textstring="variable E : kinetic/binding energy (eV) \r"
notebook command_list, text=textstring
textstring="variable Xray_source : used X-ray source (Al,Mg); optional. If present, E is the binding energy. Otherwise, kinetic energy.\r"
notebook command_list, text=textstring
textstring="example : print Material2TRMFP(0,1,\"Ag\",368,Xray_source=\"Al\")\r\r"
notebook command_list, text=textstring

textstring="============================================================\r"
notebook command_list, text=textstring
textstring="==> Calculate the effective attenuation lengths, mean escape depth (unit =�) or emission depth distribution function (normalized)\r\r"
notebook command_list, text=textstring
textstring="MED             : print Get_MED(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta)\r"
notebook command_list, text=textstring
textstring="EAL(Bulk)     : print Get_EALb(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta)\r"
notebook command_list, text=textstring
textstring="EAL(Film)     : print Get_EALf(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta,Thick)\r"
notebook command_list, text=textstring
textstring="EAL(Marker) : print Get_EALm(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta,Thick)\r"
notebook command_list, text=textstring
textstring="EDDF  : print Get_EDDF(typeChandra,IMFP,Omega,Alpha,Psi,Theta,aBeta,Depth)\r\r"
notebook command_list, text=textstring
textstring="variable  typeChandra : choice of Chandrasekhar functions: 0: integral, 1=approximate 1, 2=approximate 2, 3=approximate 3 \r"
notebook command_list, text=textstring
textstring="variable IMFP : inelastic mean free path (�)\r"
notebook command_list, text=textstring
textstring="variable Omega : albedo\r"
notebook command_list, text=textstring
textstring="variable Alpha : sample normal/analyzer angle (deg)\r"
notebook command_list, text=textstring
textstring="variable Psi : analyzer/X-ray angle (deg)\r"
notebook command_list, text=textstring
textstring="variable Theta : sample normal/X-ray angle (deg)\r"
notebook command_list, text=textstring
textstring="variable aBeta : asymmetry factor of core-level\r"
notebook command_list, text=textstring
textstring="variable Thick/Depth : thickness/depth (�)\r"
notebook command_list, text=textstring
textstring="example : print Get_EALf(2,20,0.2,0,54.7,54.7,2,10)\r\r"
notebook command_list, text=textstring

textstring="============================================================\r"
notebook command_list, text=textstring
textstring="==> Calculate the surface excitation parameter\r\r"
notebook command_list, text=textstring
textstring="print Material2SEP(material,E,Alpha,[Xray_source])\r\r"
notebook command_list, text=textstring
textstring="string material : material name\r"
notebook command_list, text=textstring
textstring="variable E : kinetic/binding energy (eV) \r"
notebook command_list, text=textstring
textstring="variable Alpha : angle between sample normal and analyzer (deg) \r"
notebook command_list, text=textstring
textstring="variable Xray_source : used X-ray source (Al,Mg); optional. If present, E is the binding energy. Otherwise, kinetic energy.\r"
notebook command_list, text=textstring
textstring="example : print Material2SEP(\"Ag\",368,0,Xray_source=\"Al\")\r\r"
notebook command_list, text=textstring


End

//==========================================================
//--usage : launch a notebook with a list of available commands for plotting
Static Function help_plot_command_i4p()

//-------------do not update
silent -1; pauseupdate

//-------------check if the window exists
dowindow /F plot_command_list
if (v_flag==0)
	newnotebook/n=plot_command_list/F=0 as "I4P : PLOT COMMAND LIST"
endif

//-------------header
string textstring
textstring=""
notebook plot_command_list, selection={startOffile, endOfFile }, fSize=13,  text=textstring
textstring="============================================================\r"
notebook plot_command_list, text=textstring
textstring="  LIST OF AVAILABLE PLOT COMMANDS IN I4P \r"
notebook plot_command_list, text=textstring
textstring="============================================================\r"
notebook plot_command_list, text=textstring
textstring="\r\r"
notebook plot_command_list, text=textstring

//-------------list of commands
textstring="============================================================\r"
notebook plot_command_list, text=textstring
textstring="==> Replot data or rewrite report from a I4PFit folder\r\r"
notebook plot_command_list, text=textstring
textstring="plot_i4pfit()\r"
notebook plot_command_list,   text=textstring
textstring="report_i4pfit()\r\r"
notebook plot_command_list, text=textstring

textstring="============================================================\r"
notebook plot_command_list, text=textstring
textstring="==> Calculate the parameters (area, position and FWHM) of background subtracted peaks in the top graph \r\r"
notebook plot_command_list, text=textstring
textstring="Graph2Peaks(backgroundtype,start,stop,keepbackground)\r\r"
notebook plot_command_list, text=textstring
textstring="string backgroundtype : type of background  (Shirley; Tougaard; Tougaard_fixed; Linear; None)\r"
notebook plot_command_list, text=textstring
textstring="variable start : start binding energy (eV) \r"
notebook plot_command_list, text=textstring
textstring="variable stop : stop binding energy (eV)\r"
notebook plot_command_list, text=textstring
textstring="variable keepbackground : 1 = keep intermediate waves; 0 = do not\r"
notebook plot_command_list, text=textstring 
textstring="example : Graph2Peaks(\"Shirley\",525,540,0)\r"
notebook plot_command_list, text=textstring
textstring="               results are stored into waves in the current data folder; spectra order corresponds to plot\r\r"
notebook plot_command_list, text=textstring

textstring="============================================================\r"
notebook plot_command_list, text=textstring
textstring="==> Plot the inelastic electron scattering cross section (unit =eV^-1)\r\r"
notebook plot_command_list, text=textstring
textstring="Plot_IESCS([Type, B, C, D, G, Estep])\r\r"
notebook plot_command_list, text=textstring
textstring="variable Type : 1 = Tougaard; 2 = Distorted-Shirley (optional, otherwise=1)\r"
notebook plot_command_list, text=textstring
textstring="variable B : B-parameter (eV^2 or eV) (optional, otherwise=3006)\r"
notebook plot_command_list, text=textstring
textstring="variable C : C-parameter (eV^2) (optional, otherwise=1643)\r"
notebook plot_command_list, text=textstring
textstring="variable D : D-parameter (eV^2 or eV^-1) (optional, otherwise=0)\r"
notebook plot_command_list, text=textstring
textstring="variable G : gap of the material (eV) (optional, otherwise=0)\r"
notebook plot_command_list, text=textstring
textstring="variable Estep : step in energy loss  (eV) (optional, otherwise=0.5\r"
notebook plot_command_list, text=textstring
textstring="example : Plot_IESCS(Type=1,B=3000,C=1643,D=0,G=0,Estep=0.5)\r\r"
notebook plot_command_list, text=textstring


End


//===========================================
//--usage : about I4P
Static Function about_i4p()

//-------------do not update
silent -1; pauseupdate


//-------------check if the window exists
dowindow /F about_windows
if (v_flag==0)
	newnotebook/n=about_windows/F=0 as "About I4P"
endif


//-------------header
string textstring
textstring=""
notebook about_windows, selection={startOffile, endOfFile }, fSize=18, text=textstring
textstring="============================================================\r"
notebook about_windows, text=textstring
textstring="               Igor Pro Paris Photoemission Package \r"
notebook about_windows, text=textstring
textstring="============================================================\r\r"
notebook about_windows, text=textstring

textstring="Author :  R�mi Lazzari \r"
notebook about_windows, text=textstring
textstring="Email :  remi.lazzari@insp.jussieu.fr\r"
notebook about_windows, text=textstring
textstring="Institution :  Institut des NanoSciences de Paris, CNRS, France\r\r"
notebook about_windows, text=textstring
textstring="License\r"
textstring="The I4P package can be obtained freely upon request to the author (remi.lazzari@insp.jussieu.fr).\r"
notebook about_windows, text=textstring
textstring="It is given as such without any warranty about accuracy, reliability, completeness and fitness for purpose.\r"
notebook about_windows, text=textstring
textstring="The author can not be held responsible of inaccurate results or misuse of the present package.\r"
notebook about_windows, text=textstring
textstring="An acknowledgement about its use in any kind of publication would be appreciated.\r"
notebook about_windows, text=textstring

End

//===========================================
//--usage :clean all the I4P folders and windows
Static Function clean_i4p()

//-------------clean the command list and about windows
string listofwindows = winlist("*", ";","")
if(whichlistitem("about_windows",listofwindows)!=-1)
	killwindow about_windows
endif
if(whichlistitem("command_list",listofwindows)!=-1)
	killwindow command_list
endif
if(whichlistitem("plot_command_list",listofwindows)!=-1)
	killwindow plot_command_list
endif
//-------------clean the database windows and folders
i4pdatabase#Clean_I4PDatabase()
//-------------clean the i4pfit windows and folders
i4pfit_around#clean_i4pfit()
//-------------clean the all the folders
killdatafolder /z  root:I4P:Variables
killdatafolder /z  root:I4P

End